
@extends('layouts.master')

@section('head')

@endsection


@section('head-styles')

    <style>
        .ui-favorite-active svg{
            fill: #c72a27
        }
    </style>

@endsection


@section('head-scripts')

@endsection


@section('header')

@endsection {{-- #header--}}

@section('content')

    <div class="ui-layout-center">
        <div class="ui-layout-center__content">

            <table class="ui-panelgrid ui-table-layout--fixed ui-panelgrid--layout ui-width--full">
                <tbody>
                <tr>
                    <td class="ui-panelgrid__cell--ad">
                        <div class="ui-panel ui-panel--favorite">

                            @if(!empty($user_info))
                            <div class="ui-panel__header">

                                <label title="Add to My Favorites" class="ui-favorite ui-favorite ui-float--left ui-display--block ui-height--25 ui-width--25 ui-border-radius--full @if($retailer['is_liked'] != 0) ui-favorite-active @endif">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                        <path d="M414.9 24C361.8 24 312 65.7 288 89.3 264 65.7 214.2 24 161.1 24 70.3 24 16 76.9 16 165.5c0 72.6 66.8 133.3 69.2 135.4l187 180.8c8.8 8.5 22.8 8.5 31.6 0l186.7-180.2c2.7-2.7 69.5-63.5 69.5-136C560 76.9 505.7 24 414.9 24z"/>
                                    </svg>
                                </label>

                                <span class="ui-float--left ui-font-size--14 ui-font--light ui-padding-left--13">Add to My Favorites</span>
                                <p class="clear" ></p>
                            </div>
                            @endif

                            <div class="ui-panel__content ui-text-align--center">
                                <img class="ui-width--full" src="{{ $retailer['image'] }}" />

                                <p class="clear"></p>
                                <span class="ui-commandlink--gray ui-font-size--13">was {{ $retailer['old_cashback'] }}</span>

                                <p class="clear"></p>
                                <span class="ui-color--red ui-font-size--22 ui-font--bold">{{ $retailer['cashback'] }} Cash Back</span>

                                <p class="clear"></p>
                                <button type="submit" class="ui-border--none ui-button--red ui-button ui-font-size--17 ui-color--white ui-width--230 ui-margin-top--8 js-shop-now-login">Shop Now</button>
                            </div>

                        </div>

                        {{--<div class="ui-panel ui-panel--favorite">--}}
                            {{--<div class="ui-panel__header ui-text-align--center">--}}
                                {{--<div class="ui massive star rating" data-max-rating="5"></div>--}}
                                {{--<p class="clear"></p>--}}
                                {{--<span class="ui-commandlink--gray ui-font-size--17 ui-font--light rating-feedback">Rate us</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="ui-panel ui-panel--favorite">
                            <div class="ui-panel__content ui-text-align--center">
                                <span class=" ui-font-size--85 ui-color--gray">{{ count($retailer['coupons']) }}</span>
                                <p class="clear"></p>
                                <span class="ui-font-size--14 ui-commandlink--gray ui-font--light">COUPONS AVAILABLE</span>
                                <p class="clear ui-height--5"></p>
                                <span class="ui-font-size--17 ui-color--gray">Share these coupons</span>
                                <p class="clear ui-height--5"></p>

                                <div class="ui-separator--top-gray ui-width--190 ui-margin--center ui-padding-top--8">

                                    <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>&t={{ $retailer['seo_title'] }}" target="_blank" rel="nofollow" class="ui-text-align--center ui-width--33 ui-height--33 ui-commandlink--social-media-dark ui-border-radius--full">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 264 512"><path d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5 70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229"/></svg>
                                    </a>

                                    {{--<a href="#" class="ui-text-align--center ui-width--33 ui-height--33 ui-commandlink--social-media-dark ui-border-radius--full">--}}
                                        {{--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M386.061 228.496c1.834 9.692 3.143 19.384 3.143 31.956C389.204 370.205 315.599 448 204.8 448c-106.084 0-192-85.915-192-192s85.916-192 192-192c51.864 0 95.083 18.859 128.611 50.292l-52.126 50.03c-14.145-13.621-39.028-29.599-76.485-29.599-65.484 0-118.92 54.221-118.92 121.277 0 67.056 53.436 121.277 118.92 121.277 75.961 0 104.513-54.745 108.965-82.773H204.8v-66.009h181.261zm185.406 6.437V179.2h-56.001v55.733h-55.733v56.001h55.733v55.733h56.001v-55.733H627.2v-56.001h-55.733z"/></svg>--}}
                                    {{--</a>--}}

                                    <a href="https://twitter.com/intent/tweet?text={{ $retailer['seo_title'] }}&url=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>&via={{ $retailer['seo_title'] }}" target="_blank" rel="nofollow" class="ui-text-align--center ui-width--33 ui-height--33 ui-commandlink--social-media-dark ui-border-radius--full">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"/></svg>
                                    </a>

                                    {{--<a href="#" class="ui-text-align--center ui-width--33 ui-height--33 ui-commandlink--social-media-dark ui-border-radius--full">--}}
                                        {{--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"/></svg>--}}
                                    {{--</a>--}}
                                    {{--<a href="#" class="ui-text-align--center ui-width--33 ui-height--33 ui-commandlink--social-media-dark ui-border-radius--full">--}}
                                        {{--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M204 6.5C101.4 6.5 0 74.9 0 185.6 0 256 39.6 296 63.6 296c9.9 0 15.6-27.6 15.6-35.4 0-9.3-23.7-29.1-23.7-67.8 0-80.4 61.2-137.4 140.4-137.4 68.1 0 118.5 38.7 118.5 109.8 0 53.1-21.3 152.7-90.3 152.7-24.9 0-46.2-18-46.2-43.8 0-37.8 26.4-74.4 26.4-113.4 0-66.2-93.9-54.2-93.9 25.8 0 16.8 2.1 35.4 9.6 50.7-13.8 59.4-42 147.9-42 209.1 0 18.9 2.7 37.5 4.5 56.4 3.4 3.8 1.7 3.4 6.9 1.5 50.4-69 48.6-82.5 71.4-172.8 12.3 23.4 44.1 36 69.3 36 106.2 0 153.9-103.5 153.9-196.8C384 71.3 298.2 6.5 204 6.5z"/></svg>--}}
                                    {{--</a>--}}
                                </div>

                            </div>
                        </div>
                        <div class="ui-panel ui-panel--favorite">
                            <div class="ui-panel__content ui-text-align--center">
                                <a href="#reviews" class="ui-font-size--18
                                                          ui-color--gray
                                                          ui-font-size--22
                                                          ui-display--block
                                                          ui-margin-top--10">
                                    <span class="ui-color--green fa fa-comments-o ui-font-size--30"></span>
                                    {{ count($retailer['reviews']) }} reviews
                                </a>
                            </div>
                        </div>

                        {{--<div class="ui-panel ui-panel--favorite">--}}
                            {{--<div class="ui-panel__content ui-text-align--center">--}}
                                {{--<a href="#reviews" class="ui-font-size--18--}}
                                                          {{--ui-color--gray--}}
                                                          {{--ui-font-size--22--}}
                                                          {{--ui-display--block--}}
                                                          {{--ui-margin-top--10">--}}
                                    {{--<span class="ui-color--green fa fa-exclamation-triangle ui-font-size--30"></span>--}}
                                    {{--Report a store--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="ui-panel ui-panel--favorite">--}}
                            {{--<div class="ui-panel__content ui-text-align--center">--}}
                                {{--<a href="#reviews" class="ui-font-size--18--}}
                                                          {{--ui-color--gray--}}
                                                          {{--ui-font-size--22--}}
                                                          {{--ui-display--block--}}
                                                          {{--ui-margin-top--10">--}}
                                    {{--<span class="ui-color--green fa fa-paper-plane ui-font-size--30"></span>--}}
                                    {{--Submit a coupon--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="ui-panel ui-panel--favorite">

                            <div class="ui-panel__header">
                                <span class="ui-font-size--22 ui-font--medium ui-color--green">Cash Back Terms</span>
                            </div>

                            <div class="ui-panel__content">
                                <span class="ui-color--gray ui-font-size--13">{{ $retailer['conditions'] }}</span>
                            </div>
                        </div>


                        <div class="ui-panel ui-panel--favorite">
                            <div class="ui-panel__header">
                                <span class="ui-font-size--22 ui-font--medium ui-color--green">Free Shipping</span>
                            </div>
                            <div class="ui-panel__content">
                                <span class="ui-font-size--13">{{ $retailer['title'] }} offers free shipping on orders of $50 or more and free returns on all orders.</span>
                                <p class="clear ui-height--5"></p>
                                <a href="#" class="ui-font-size--14 ui-color--red ui-font--bold">Shop {{ $retailer['title'] }} with {{ $retailer['cashback'] }} Cash Back</a>
                            </div>
                        </div>

                        {{--<div class="ui-panel ui-panel--favorite ui-margin--center" >--}}
                            {{--<div class="ui-panel__header ui-margin-top--10 ui-margin-bottom--7">--}}
                                {{--<span class="ui-font-size--20 ui-color--gray ui-font--medium">Featured Stores</span>--}}
                            {{--</div>--}}
                            {{--<div class="ui-panel__content ui-separator--top-gray">--}}
                                {{--<a class="ui-commandlink ui-line-height--30 ui-margin-top--8 ui-display--block ui-font-size--17 ui-color--blue" href="#">Target</a>--}}
                                {{--<a class="ui-commandlink ui-line-height--30 ui-display--block ui-font-size--17 ui-color--blue" href="#">America Eagle</a>--}}
                                {{--<a class="ui-commandlink ui-line-height--30 ui-display--block ui-font-size--17 ui-color--blue" href="#">Nordstrom</a>--}}
                                {{--<a class="ui-commandlink ui-line-height--30 ui-display--block ui-font-size--17 ui-color--blue" href="#">Macy's</a>--}}
                                {{--<a class="ui-commandlink ui-line-height--30 ui-display--block ui-font-size--17 ui-color--blue" href="#">Kohl's</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </td>

                    <td class="ui-panelgrid__cell--content">
                        <label class="ui-font-size--35 ui-color--dark-gray ui-margin-top--12 ui-display--block ui-text--wrap">
                            <div class="breadcrumbs">
                                <ul class="breadcrumbs__crumbs">
                                    <li class="breadcrumbs__crumb"><a href="/" class="breadcrumbs__crumb-link">Home</a></li>
                                    <li class="breadcrumbs__crumb"><a href="{!! $retailer['category_link'] !!}" class="breadcrumbs__crumb-link">{{ $retailer['category_name'] }}</a></li>
                                    <li class="breadcrumbs__crumb"><a class="breadcrumbs__crumb-link breadcrumbs__crumb-link--current">{{ $retailer['title'] }}</a></li>
                                </ul>
                            </div>

                            {{ $retailer['title'] }} Coupon Code Discounts & Coupons

                        </label>

                        <div class="ui divided items">

                            @foreach($retailer['coupons'] as $coupon)
                            <!-- One coupon start -->
                            <div class="coupon coupon--stores" data-coupon-object="{!! $retailer['website'] !!}" id="coupon-id-{{ $coupon['coupon_id'] }}" data-modal-id="coupon-id-{{ $coupon['coupon_id'] }}">

                                <input type="hidden" name="coupon_code" value="{{ $coupon['code'] }}">
                                <input type="hidden" name="coupon_type" value="{{ $coupon['coupon_type'] }}">

                                <div class="coupon__logo-wrapper">
                                    <img src="{{ $retailer['image'] }}" alt="{{ $retailer['title'] }}" style="width: 150px; height: 40px;">
                                </div>

                                <ul class="coupon__content">
                                    <li>
                                        @if($coupon['coupon_type'] == 'coupon')
                                            <span class="coupon__type coupon__type--promocode">Promo Code</span>
                                        @elseif ($coupon['coupon_type'] == 'printable')
                                            <span class="coupon__type coupon__type--printable">Printable</span>
                                        @else
                                            <span class="coupon__type coupon__type--sale">Sale</span>
                                        @endif

                                        <span class="coupon__times-used">{{ $coupon['visits_today'] }} used today</span>
                                    </li>
                                    <li class="coupon__cdiscount">
                                        <span class="ui-font-size--20 coupon__ctitle">{{ $coupon['title'] }}</span><br>

                                        {{--<span class="coupon__ctitle-discount ui-color--red ui-font--bold">+ Up to 45.0% Cash Back</span>--}}
                                    </li>
                                    <li class="coupon__description ui-font-size--14">
                                        <p class="coupon__description-text expandable-text expandable-text--collapsed">{!! $coupon['description'] !!}</p>
                                        <span class="more dashboard__c-read-more">Read More +</span>
                                    </li>
                                    <li class="coupon__code-expires">
                                        <span class="ui-color--gray ui-font-size--14"><span class="fa fa-clock-o ui-margin-right--10"></span>Expires {{ $coupon['end_date'] }}</span>
                                    </li>
                                </ul>

                                <a href=" @if(empty($user_info)){!! $retailer['website'] !!}@else{!! $coupon['link'] !!}@endif" class="ui-button ui-button--green coupon__shop-btn ui-border-radius--4">

                                    @if($coupon['coupon_type'] == 'discount')
                                        Get Sale
                                    @else
                                        Get Coupon
                                    @endif
                                    <span class="fa fa-angle-right"></span>
                                </a>

                            </div>
                            @endforeach

                            <!-- One coupon end -->

                        </div>

                        <!-- Description block  -->
                        <div class="ui-padding--15 ui-border" id="description">
                            <span class="ui-font-size--20 ui-font--medium ui-display--block ui-margin-bottom--15">About {{ $retailer['title'] }} Coupons, Deals and Cash Back</span>
                            {!! $retailer['description'] !!}
                        </div>

                        <!-- Reviews block -->
                        <div class="stores-reviews"  id="reviews">

                            <div class="clr stores-reviews__heading">
                                <span class="ui-font-size--22 ui-float--left"><span class="ui-color--green fa fa-comments-o"></span> Here you can leave some feedback</span>
                                <span id="leaveFeedback" class="ui-float--right stores-reviews__leave"><span class="fa fa-pencil-square-o"></span> Leave a feedback</span>
                            </div>

                            @if(empty($user_info))
                                <p class="ui-font--bold ui-padding--10 ui-display--none" id="unregFeedback">Please, <a href="{{ $router->generate('login') }}" class="modal__link" id="loginMessage">log in</a> to leave a feedback</p>
                            @else
                                <form class="ui-font--bold ui-padding--10 ui-display--none" id="regFeedback">
                                    <input type="hidden" name="action" value="add_review">
                                    <input type="hidden" name="retailer_id" value="{{ $retailer['retailer_id'] }}">
                                    <select name="rating" id="feedbackRate" class="ui-font--bold">
                                        <option value="5" selected>&#9733;&#9733;&#9733;&#9733;&#9733; - Very good</option>
                                        <option value="4">&#9733;&#9733;&#9733;&#9733; - Good</option>
                                        <option value="3">&#9733;&#9733;&#9733; - Average</option>
                                        <option value="2">&#9733;&#9733; - Bad</option>
                                        <option value="1">&#9733; - Very bad</option>
                                    </select>

                                    <br><br>
                                    <label for="feedbackTitle">Feedback title here</label>
                                    <br>
                                    <input type="text" name="review_title" id="feedbackTitle" class="ui-font--bold" required>
                                    <br><br>
                                    <label for="feedbackText">Feedback text here</label>
                                    <br>
                                    <textarea name="review" id="feedbackText" cols="30" rows="10" class="ui-font--bold" required></textarea>
                                    <br>
                                    <br>
                                    <button class="ui-button ui-button--green stores-reviews__submit-btn ui-border-radius--4 ui-color--white" type="submit">Submit feedback</button>
                                </form>

                                <div class="review-form-message"></div>
                            @endif

                            @foreach($retailer['reviews'] as $review)
                            <!-- One review begins -->
                            <div class="stores-reviews__review">
                                <div class="clr">
                                    <span class="stores-reviews__username ui-float--left"><span class="fa fa-user-circle"></span> {{ $review['user_name'] }}</span>
                                    <span class="stores-reviews__date ui-float--right">{{ $review['added'] }}</span>
                                </div>
                                <br>
                                <div class="stores-reviews__rate-title">
                                   <span class="stores-reviews__rate stores-reviews__rate--{{ $review['rating'] }}">
                                       <span class="fa fa-star stores-reviews__star stores-reviews__star--1"></span>
                                       <span class="fa fa-star stores-reviews__star stores-reviews__star--2"></span>
                                       <span class="fa fa-star stores-reviews__star stores-reviews__star--3"></span>
                                       <span class="fa fa-star stores-reviews__star stores-reviews__star--4"></span>
                                       <span class="fa fa-star stores-reviews__star stores-reviews__star--5"></span>
                                   </span>
                                   <span class="ui-font--bold ui-margin-left--10">{{ $review['review_title'] }}</span>
                                </div>
                                <p class="stores-reviews__description">{{ $review['review'] }}</p>
                            </div>
                            <!-- End of one review  -->
                            @endforeach


                            <!-- Reviews pagination -->
                            {{--<div class="stores-reviews__pagination">--}}
                                {{--<a class="stores-reviews__pag-link stores-reviews__pag-link--disabled stores-reviews__pag-prev"><span class="fa fa-angle-left"></span> Previous</a>--}}
                                {{--<a href="#" class="stores-reviews__pag-link stores-reviews__pag-link--current">1</a>--}}
                                {{--<a href="#" class="stores-reviews__pag-link">2</a>--}}
                                {{--<a href="#" class="stores-reviews__pag-link">3</a>--}}
                                {{--<a href="#" class="stores-reviews__pag-link stores-reviews__pag-next">Next <span class="fa fa-angle-right"></span></a>--}}
                            {{--</div>--}}
                            <!-- End of the reviews -->
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--end of panelgrid (layout)-->
            <div class="ui-panel">
                <div class="ui-panel__content">
                    <div class="ui-width--full-175">

                    </div>
                </div>
            </div>

            <table class="ui-panelgrid ui-table-layout--fixed ui-panelgrid--layout ui-width--full">
                <tbody>
                <tr>
                    <td class="ui-panelgrid__cell--content">
                        <!-- You might also like block -->
                        <div class="ui-panel">
                            <div class="ui-panel__header ui-margin-top--10 ui-margin-bottom--7">
                                <span class="ui-font-size--15 ui-color--gray ui-line-height--27 ui-font--medium">You might also like</span>
                            </div>

                            <div class="ui-panel__content ui-margin-top--25">

                                @foreach($latest as $item)
                                <div class="ui-width--quad ui-float--left ui-text-align--center">
                                    <span class="ui-font-size--15 ui-color--gray ui-font--bold">{{ $item['title'] }}</span>
                                    <p class="clear ui-height--5"></p>
                                    <a href="{{ $item['retailer_link'] }}">
                                        <img src="{{ $item['image'] }}" class="ui-max-width--full ui-border--none ui-height--45 ui-display--block ui-margin--center"  style="width: 280px; height: 60px;"/>
                                    </a>
                                    <span class="ui-font-size--13 ui-color--orange">{{ $item['cashback'] }}</span>
                                    <span class="ui-font-size--11 ui-color--gray"> CASHBACK</span>
                                </div>
                                @endforeach


                                <p class="clear"></p>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection {{-- #content --}}


@section('footer')

    @if(empty($user_info))

        <!-- Coupon's modal  -->
        <div class="modal coupon__modal">
            <div class="modal__overlay"></div>
            <div class="coupon__modal-window">

                <div class="coupon__modal-body ui-text-align--center">
                    <div class="coupon__modal-code">
                        <span class="ui-font--bold coupon__modal-code-text" id="coupon"></span>
                        <button class="coupon__modal-copy-btn" data-clipboard-target="#coupon" data-clipboard-action="copy">Copy Code</button>
                    </div>
                </div>

                <div class="coupon__modal-login ui-text-align--center">
                    <div class="modal__caption">
                        <p class="modal__cap-error"><span class="fa fa-info-circle modal__capicon"></span> You are not a member of the service</p>
                        <p>To get a cashback please login below</p>
                        <p>Not a member? <a href="{{ $router->generate('login') }}" class="modal__link">Register Now</a></p>
                    </div>
                    <div class="clr ui-margin-bottom--15">

                        <form id="login-form-page" class="grid-2-col__1-of-2 modal__login-form">
                            <input type="hidden" name="action" value="login">
                            <label for="email" class="ui-font--bold">EMAIL:</label><br>
                            <input class="modal__input-t" type="email" name="username" id="email"><br>
                            <label for="password" class="ui-font--bold">PASSWORD:</label><br>
                            <input class="modal__input-t" type="password" name="password" id="password"><br>

                            <div class="ui-margin--center" id="login-form-page-err"></div>

                            <a href="{{ $router->generate('forgot') }}" class="modal__link">I forgot my password</a> <br>
                            <button type="submit" class="modal__btn ui-button ui-button--green">Login</button> <br>
                        </form>

                        <div class="grid-2-col__1-of-2 modal-coupon-login__socials ui-padding-right--20">
                            <a href="{{ $router->generate('fblogin') }}" class="modal-coupon-login__social modal-coupon-login__social--fb">
                                <span class="modal-coupon-login__socicon fa fa-facebook"></span>
                                Login with Facebook
                            </a> <br>
                            <a href="#" class="modal-coupon-login__social modal-coupon-login__social--gp">
                                <span class="modal-coupon-login__socicon fa fa-google-plus"></span>
                                Login with Google Plus
                            </a>
                        </div>

                    </div>

                    <a href="" class="modal__link ui-font-size--22 ui-margin-bottom--15 ui-display--block modal__link-shop" target="_blank">No thanks, continue without cahsback <span class="fa fa-angle-right ui-font--bold"></span></a>

                    <div class="coupon__modal-footer">
                        <a href="#" class="ui-color--sm-grey ui-font-size--12">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </div>

    @else

        <!-- Coupon's modal  -->
        <div class="modal coupon__modal">
            <div class="modal__overlay"></div>
            <div class="coupon__modal-window">

                <div class="coupon__modal-body ui-text-align--center">

                </div>

                <div class="coupon__modal-footer">
                    <a href="#" class="ui-color--sm-grey ui-font-size--12">Terms &amp; Conditions</a>
                </div>

            </div>
        </div>
    @endif

@endsection


@section('footer-scripts')

    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/classList.min.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/clipboard.min.js"></script>

    <script>
        $("#leaveFeedback").on("click", function(){
            $("#unregFeedback, #regFeedback").slideToggle(200);
        });

        function openInNewTab(url, coupon_id) {
            window.open(url, '_self');
            window.open('#'+coupon_id,'_blank');
        }

        var hash = location.hash;

        if(hash){
            getModalContent($(hash));

            $(document).ready(function () {
                $('.modal').fadeToggle(500);
                $('.modal__overlay').show();
                $('.modal').css({ "top": $(window).scrollTop() + "px"});
            });
        }

        @if(empty($user_info))

            $(".coupon__shop-btn, .js-shop-now-login").on('click', function(e){
                e.preventDefault();

                var coupon_id = $(this).parent().data('modal-id');
                var current_url = '@if(empty($user_info)){!! $retailer['website'] !!}@else{!! $retailer['url'] !!}@endif';

                openInNewTab(current_url, coupon_id);
            });

            function getModalContent(el) {

                var current_url = '@if(empty($user_info)){!! $retailer['website'] !!}@else{!! $retailer['url'] !!}@endif';
                var coupon_code = el.find("input[name='coupon_code']").val();

                if(typeof(coupon_code) !== 'undefined' && coupon_code !== '') {

                    var html = '';

                    html = '<span class="ui-font--bold coupon__modal-code-text" id="coupon">'+ coupon_code +'</span>';
                    html += '<button class="coupon__modal-copy-btn" data-clipboard-target="#coupon" data-clipboard-action="copy">Copy Code</button>';

                    $(".coupon__modal-code").html(html);
                    $(".coupon__modal-code").show();

                } else {
                    $(".coupon__modal-code").hide();
                }

                $('.modal__link-shop').attr('href', current_url);

            }

        @else

            var current_url = '@if(empty($user_info)){!! $retailer['website'] !!}@else{!! $retailer['url'] !!}@endif';

            $(".js-shop-now-login").on('click', function(e){
                e.preventDefault();
                window.open(current_url, '_blank');
            });


            $(".coupon__shop-btn").on('click', function(e){
                e.preventDefault();

                var coupon_id = $(this).parent().data('modal-id');
                var current_url = '@if(empty($user_info)){!! $retailer['website'] !!}@else{!! $retailer['url'] !!}@endif';

                openInNewTab(current_url, coupon_id);
            });

            function getModalContent(el) {
                var coupon_code = el.find("input[name='coupon_code']").val();
                var title = '{{ $retailer['title'] }}';
                var coupon_type = el.find("input[name='coupon_type']").val();

                $('.coupon__modal-show-this').html('Show this code in-store at '+ title);

                if(typeof(coupon_code) !== 'undefined' && coupon_code !== '' && coupon_type === 'coupon') {

                    var html = '';

                    html = '<div class="coupon__modal-code">';
                    html += '<span class="ui-font--bold coupon__modal-code-text" id="coupon">'+ coupon_code +'</span>';
                    html += '<button class="coupon__modal-copy-btn" data-clipboard-target="#coupon" data-clipboard-action="copy">Copy Code</button>';
                    html += '</div>';
                    html += '<a href="'+ current_url +'" target="_blank" class="modal__link ui-font-size--19">Continue to '+ title +' ';
                    html += '<span class="fa fa-angle-right ui-font--bold"></span>';
                    html += '</a>';

                    $('.coupon__modal-body').html(html);

                } else if(typeof(coupon_code) !== 'undefined' && coupon_code !== '' && coupon_type === 'printable') {

                    console.log(coupon_code);

                    var html = '';

                    html = '<p class="ui-color--gray ui-margin-top--10 ui-margin-bottom--10 coupon__modal-show-this">Show this code in-store at '+ title +'</p>';
                    // html += '<p class="ui-font-size--14 ui-color--gray ui-margin-bottom--10">We have also sent this to e@mail.com</p>';
                    html += '<div class="ui-display--inblock">';
                    // html += '<div class="coupon__modal-code coupon__modal-code--qr">';
                    // html += '<img src="images/qr-sample.jpg" alt="Code to show in a shop">';
                    // html += '<p class="ui-font--bold coupon__modal-code-text ui-margin-top--10 ui-font-size--14">ABABABABA</p>';
                    // html += '</div>';
                    html += '<div class="coupon__modal-code coupon__modal-code--qr">';
                    html += '<p class="ui-font--bold coupon__modal-code-text">'+ coupon_code +'</p>';
                    html += '</div>';
                    html += '<button class="coupon__modal-print-btn ui-button ui-button--green ui-color--white ui-border-radius--4 ui-font--bold ui-font-size--17 ui-width--full">Print</button>';
                    html += '</div>';

                    $('.coupon__modal-body').html(html);

                } else {
                    var html = '';

                    html = '<p class="ui-color--green ui-font-size--30 ui-margin-bottom--30">No Code Required</p>';
                    html += '<a href="'+ current_url +'" target="_blank" class="modal__link ui-font-size--19">Continue to '+ title +' ';
                    html += '<span class="fa fa-angle-right ui-font--bold"></span>';
                    html += '</a>';

                    $('.coupon__modal-body').html(html);
                }
            }

            $(".coupon__modal-body").on("click", ".coupon__modal-print-btn", function(){
                var isIE = /*@cc_on!@*/false || !!document.documentMode;
                var ieHelp = "<p class='printhide'> Please, press CTRL + P to print the page </p>";
                var w = window.open();
                var printOne = ($(this).parent()).html();
                var printingStyles = '<style> body { text-align: center }  .coupon__modal-print-btn {display: none} @media print {.printhide {display: none}} </style>';
                var printingTitle = $(this).parent().parent().find(".coupon__modal-show-this").text();

                if (isIE) {
                    w.document.write('<html><head><title>'+ printingTitle +'</title>' + printingStyles + '</head><body><h1>'+ printingTitle +'</h1><hr />' + printOne + '<hr />' + ieHelp ) + '</body></html>';
                } else {
                    w.document.write('<html><head><title>'+ printingTitle +'</title>' + printingStyles + '</head><body><h1>'+ printingTitle +'</h1><hr />' + printOne + '<hr />') + '</body></html>';
                    w.window.print();
                    w.close();
                    return false;
                }
            });

        @endif

    </script>

    <script>
        $("#login-form-page").on('submit', function(e){
            e.preventDefault();

            var form_data = $(this).serialize();

            $.ajax({
                type: "POST",
                url: "{{ $router->generate('login') }}",
                data: form_data,
                dataType: 'json',
                success: function (msg) {
                    if(typeof msg.success !== 'undefined'){
                        location.reload();
                    } else {
                        $("#login-form-page-err").text(msg.errs).css('color','red');
                    }
                }
            });
        });
    </script>

    <script>
        $("#regFeedback").on('submit', function(e){
            e.preventDefault();

            var form_data = $(this).serialize();

            $.ajax({
                type: "POST",
                url: "/kouponia/{{ $retailer['title'] }}/{{ $retailer['retailer_id'] }}",
                data: form_data,
                dataType: 'json',
                success: function (msg) {
                    if(typeof msg.success !== 'undefined'){
                        $(".review-form-message").text(msg.success).css('color','green');
                        $("#regFeedback").slideToggle(200);
                    } else {
                        $(".review-form-message").text(msg.error).css('color','red');
                    }
                }
            });
        });
    </script>

    <script>

        $(".ui-favorite").on("click", function () {


            if($(this).hasClass('ui-favorite-active')){

                $.ajax({
                    type: "GET",
                    url: "{{ $router->generate('my-favorites') }}",
                    data: {'id':$(this).find("input[name=id]").val(), 'act':'del'},
                    dataType: 'json',
                    success: function (msg) {
                        console.log(msg);
                    }
                });

                $(this).removeClass('ui-favorite-active');
            } else {

                $.ajax({
                    type: "GET",
                    url: "{{ $router->generate('my-favorites') }}",
                    data: {'id':$(this).find("input[name=id]").val(), 'act':'add'},
                    dataType: 'json',
                    success: function (msg) {
                        console.log(msg);
                    }
                });

                $(this).addClass('ui-favorite-active');
            }

        });

    </script>

    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/expand-text.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/modal.js"></script>

@endsection