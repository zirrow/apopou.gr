
@extends('layouts.master')

@section('head')

@endsection


@section('head-styles')

@endsection


@section('head-scripts')

@endsection


@section('header')

@endsection {{-- #header--}}

@section('content')

    <h1 align="center"><?php echo CBE1_404_TITLE; ?></h1>

    <p align="center"><img src="<?php echo SITE_URL; ?>images/404.png" /></p>
    <p align="center"><?php echo CBE1_404_TEXT; ?></p><br/>
    <p align="center"><a class="ui-font-size--22 ui-margin-bottom--15 ui-display--block" href="<?php echo SITE_URL; ?>"><?php echo CBE1_404_GOBACK; ?></a></p>

@endsection {{-- #content --}}


@section('footer')

@endsection


@section('footer-scripts')


@endsection