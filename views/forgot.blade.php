<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="stylesheet" type="text/css" href="{{ SITE_URL }}grabdid-front-felix/build/css/style.css" />

        <script src="{{ SITE_URL }}grabdid-front-felix/build/js/jquery-3.3.1.min.js"></script>

        <title>{{ $PAGE_TITLE }}</title>
    </head>

    <body>

        <form id="recoveryPasswordForm" class="forgot">
            <input type="hidden" name="action" value="forgot">
            <h1>{{ $PAGE_TITLE }}</h1>
            <p class="ui-font-size--18 ui-margin-top--10 ui-margin-bottom--10">Please enter your email address below and we will send you an email that contains your new password.</p>
            <label for="recEmail" class="forgot__label">{{$header['CBE1_FORGOT_EMAIL']}}</label>
            <br>
            <input type="email" name="email" id="recEmail" class="forgot__input-t" required>
            <br>
            <label for="captcha" class="forgot__label">{{$header['CBE1_SIGNUP_SCODE']}}</label>
            <br>
            <input type="text" name="captcha" id="captcha" class="forgot__input-t" required value="" size="8">
            <br>
            <img src="<?php echo SITE_URL; ?>captcha.php?rand=<?php echo rand(); ?>&bg=grey" id="captchaimg" alt="Captcha image here">
            <a href="javascript: refreshCaptcha();" class="forgot__refresh-btn"><span class="fa fa-refresh"></span></a>
            <br>
            <button type="submit" class="ui-button ui-button--green forgot__submit-btn" >Send Password</button>
            <br>
            <br>
            <a href="{{ $router->generate('login') }}" class="modal__link">Back to login page</a>
        </form>

        <div class="ui-font-size--18 ui-margin-top--10 ui-margin-bottom--10 ui-text-align--center" id="alert"></div>

        <script language="javascript" type="text/javascript">
            function refreshCaptcha()
            {
                var img = document.images['captchaimg'];
                img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000+"&bg=grey";
            }
        </script>

        <script>
            $('#recoveryPasswordForm').on('submit', function (e) {
                e.preventDefault();

                var form_data = $("#recoveryPasswordForm").serialize();

                $.ajax({
                    type: "POST",
                    url: "{{ $router->generate('forgot') }}",
                    data: form_data,
                    dataType: 'json',
                    success: function (msg) {

                        console.log('test');

                        console.log(msg);

                        if(typeof msg.success !== 'undefined'){
                            $("#recoveryPasswordForm").hide();

                            $("#alert").html(msg.success).css('color', 'green');

                            setTimeout(function(){
                                document.location.href = '{{ $router->generate('login') }}';
                            },2000);
                        } else {
                            $("#alert").html(msg.error).css('color', 'red');
                        }

                    }
                });


            });
        </script>

    </body>
</html>