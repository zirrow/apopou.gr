@if($breadcrumbs)
    <div class="breadcrumbs">
        <ul class="breadcrumbs__crumbs">
            @foreach($breadcrumbs as $breadcrumb)
                @if(!empty($breadcrumb['link']))
                    <li class="breadcrumbs__crumb"><a href="{{ $breadcrumb['link'] }}" class="breadcrumbs__crumb-link">{{ $breadcrumb['name'] }}</a></li>
                @else
                    <li class="breadcrumbs__crumb"><a class="breadcrumbs__crumb-link breadcrumbs__crumb-link--current">{{ $breadcrumb['name'] }}</a></li>
                @endif
            @endforeach
        </ul>
    </div>
@endif