<div class="member__menu">
    <ul class="member__menu-list">

        @foreach($account_menu['menu_array'] as $menu_link => $menu_name)

            @if(strpos($menu_link, $account_menu['current_link']) !== false)
                <li class="member__menu-item member__menu-item--active"><div class="member__menu-link">{{$menu_name}} <span class="fa  fa-angle-right  ui-float--right dashboard__m"></span></div></li>
            @else
                <li class="member__menu-item"><a href="{{ $menu_link }}" class="member__menu-link">{{$menu_name}} <span class="fa  fa-angle-right  ui-float--right dashboard__m"></span></a></li>
            @endif
        @endforeach

    </ul>
</div>