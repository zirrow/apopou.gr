<ul class="stores-sidebar__categories-list">
    <li class="ui-text--uppercase ui-padding-15-0">Categories</li>
    <li class="ui-padding-15-0"><a href="/online-katasthmata">All stores</a></li>

    @foreach($coupons_menu['menu_array'] as $menu_item)

        @if($coupons_menu['HIDE_SUB_CATEGORIES'] != 1)

            @if($menu_item['parent_id'] == 0 && $menu_item['child_count'] == 0)

                @if($menu_item['category_id'] == $coupons_menu['active'])
                    <li class="ui-padding-10-0 stores-sidebar__subcategory--expanded stores-sidebar__subcategory--checked my-menu-active">
                @else
                    <li class="ui-padding-10-0">
                @endif
                    <a href="{{ $menu_item['link'] }}">
                        {{ $menu_item['name'] }}
                    </a>
                </li>

            @elseif($menu_item['child_count'] != 0)

                @if($menu_item['category_id'] == $coupons_menu['active'] || $menu_item['category_id'] == $coupons_menu['active_parent'])
                    <li class="ui-padding-10-0 stores-sidebar__subcategory--expanded stores-sidebar__subcategory--checked my-menu-active">
                @else
                    <li class="ui-padding-10-0">
                @endif

                        <a href="{{ $menu_item['link'] }}">{{ $menu_item['name'] }}</a>
                        <span class="fa fa-angle-right ui-float--right ui-cursor--pointer stores-sidebar__cat-open"></span>

                    @if($menu_item['category_id'] == $coupons_menu['active'] || $menu_item['category_id'] == $coupons_menu['active_parent'])
                        <ul class="stores-sidebar__subcategory stores-sidebar__subcategory--expanded stores-sidebar__subcategory--checked">
                    @else
                        <ul class="stores-sidebar__subcategory">
                    @endif

                            @foreach($coupons_menu['menu_array'] as $sub_menu_item)
                                @if($sub_menu_item['parent_id'] == $menu_item['category_id'])

                                    @if($sub_menu_item['category_id'] == $coupons_menu['active'])
                                        <li class="ui-padding-15-0 my-menu-active">
                                    @else
                                        <li class="ui-padding-15-0">
                                    @endif


                                        <a href="{{ $sub_menu_item['link'] }}">
                                            {{ $sub_menu_item['name'] }}
                                        </a>
                                    </li>

                                @endif
                            @endforeach
                        </ul>
                    </li>
            @endif

        @else

            @if($menu_item['parent_id'] == 0)
                <li class="ui-padding-10-0">
                    <a href="{{ $menu_item['link'] }}">
                        {{ $menu_item['name'] }}
                    </a>
                </li>
            @endif
        @endif
    @endforeach

</ul>