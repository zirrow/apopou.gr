<!DOCTYPE html>
<html lang="en">
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{ $PAGE_TITLE }} | {{ $head['SITE_TITLE'] }}</title>

        <link rel="stylesheet" href="{{ SITE_URL }}grabdid-front-felix/build/js/owl.theme.default.min.css" />
        <link rel="stylesheet" href="{{ SITE_URL }}grabdid-front-felix/build/js/owl.carousel.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="{{ SITE_URL }}grabdid-front-felix/build/css/style.css" />
        <link rel="stylesheet" type="text/css" href="{{ SITE_URL }}grabdid-front-felix/build/js/search.min.css" />
        <link rel="stylesheet" type="text/css" href="{{ SITE_URL }}grabdid-front-felix/build/js/transition.min.css" />
        <link rel="stylesheet" type="text/css" href="{{ SITE_URL }}grabdid-front-felix/build/js/dropdown.min.css" />

        <link href="https://fonts.googleapis.com/css?family=GFS+Didot" rel="stylesheet">
        <style>
            body{font-family: 'GFS Didot', serif; }
            .my-menu-active > a{font-style: italic; font-weight:bold; text-decoration:underlineж}
        </style>
        @yield('head')
        @yield('head-styles')
        @yield('head-scripts')
{{-- Part: anything else in head --}}
    </head>
    <body>

    <div class="ui-layout-west sticky-header">
        <div class="ui-layout-west__content">

            <table class="ui-panelgrid ui-width--full" >
                <tbody>
                <tr>

                    <!-- logo column -->

                    <td class="ui-width--215 header-logo ui-position--relative">

                        <a href="/">
                            <img src="{{ SITE_URL }}grabdid-front-felix/build/images/logo-t.png" alt="{{ $head['SITE_TITLE'] }}" title="{{ $head['SITE_TITLE'] }}">
                        </a>

                        {{ $languages['current_lang'] }}

                        @if (true == $multilanguage)
                            <!-- flag column -->
                            <div class="flag-drop">

                                <div class="flag-drop__selected">
                                    @foreach ($languages['languages'] as $language)
                                        @if ($language['language'] == $languages['current_lang'])
                                            <span class="flag-drop__lang">{{ $language['language_code'] }}</span> <span class="flag-drop__arrow"></span>
                                        @endif
                                    @endforeach
                                </div>

                                <ul class="flag-drop__list">
                                    @foreach ($languages['languages'] as $language)
                                        <li class="flag-drop__item flag-drop__item--{{ $language['language_code'] }}">
                                            <a href="/index.php?lang={{ $language['language'] }}">{{ $language['language_code'] }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <!-- Mobile button for showing login/signup buttons -->

                        <span class="fa
                                         fa-sign-in
                                         ui-font-size--25
                                         ui-float--right
                                         ui-margin-top--10
                                         ui-color--orange
                                         ui-cursor--pointer
                                         header__login" id="signButton"></span>
                    </td>

                    <!-- search column -->

                    <form action="/search.php" method="get">
                        <td class="ui-search-column">
                            <div class="ui search ui-inputfield ui-inputfield--search">
                                <input type="hidden" name="action" value="search">
                                <input class="prompt" type="text" name="searchtext" @if(isset($search_key)) value='{{ $search_key }}' @endif placeholder="{{ $header['CBE_SEARCH_MSG'] }}" />
                                <button class="ui-button ui-position--relative ui-border--none ui-button--green ui-button--search">
                                    <span class="ui-icon ui-icon--search"></span>
                                </button>
                                <div class="results"></div>
                            </div>
                        </td>
                    </form>

                    <!-- sign in column -->
                    @if ($user_info)
                    <td class="ui-width--230">
                        <div class="user-block">
                            <a href="#" class="ui-color--green ui-float--right">{{ $user_info['user_name'] }} {{ $user_info['user_balance'] }} &nbsp;<span class="fa fa-angle-down"></span></a>
                            <ul class="user-block__menu">
                                <li class="user-block__item-wrapper"><a href="{{ $router->generate('my-account') }}" class="user-block__menu-link">My Account</a></li>
                                <li class="user-block__item-wrapper"><a href="{{ $router->generate('my-favorites') }}" class="user-block__menu-link">Favorite stores</a></li>
                                <li class="user-block__item-wrapper"><a href="{{ $router->generate('my-clicks') }}" class="user-block__menu-link">Shopping Trips</a></li>
                                <li class="user-block__item-wrapper"><a href="{{ $router->generate('my-balance') }}" class="user-block__menu-link">Cash Back Balance: <span class="user-block__cbalance">{{ $user_info['user_balance'] }}</span></a></li>
                                <li class="user-block__item-wrapper user-block__item-wrapper--socials">
                                    <a href="#" class="user-block__connect-link user-block__connect-link--fb"><span class="fa fa-facebook"></span> Connect</a>
                                    <a href="#" class="user-block__connect-link user-block__connect-link--gp"><span class="fa fa-google"></span> Connect</a>
                                </li>
                                <li class="user-block__item-wrapper"><a href="/logout.php" class="user-block__menu-link">Sign Out</a></li>
                                <li class="user-block__item-wrapper"><a href="/mysupport.php" class="user-block__menu-link">Help</a></li>
                            </ul>
                        </div>
                    </td>

                    @else

                    <td class="ui-width--230 ui-text-align--center header__log-reg" id="loginBlock">
                        <button id="login-btn" class="ui-button ui-border-radius--5 ui-font-size--11 ui-border--none ui-button--orange ui-button--left ui-button--header" >{{ $header['CBE_LOGOUT'] }}</button>
                        <button id="register-btn" class="ui-button ui-border-radius--5 ui-font-size--11 ui-border--none ui-button--orange ui-button--right ui-button--header" >{{ $header['CBE_SIGNUP'] }}</button>
                    </td>

                    @endif

                </tr>
                </tbody>
            </table>
            <!-- end of panelgrid -->
        </div>
    </div>

    <div class="ui-layout-west">

        <!-- footer inside of west layout -->

        <div class="ui-layout-west__footer menu">
            <div class="ui-footer__content">
                <table class="ui-panelgrid ui-width--full">
                    <tbody>
                    <tr>
                        <td>
                            <a href="/">{{ $header['CBE_MENU_HOME'] }}</a>
                        </td>
                        <td>
                            <a href="{{ $router->generate('retailers_by_category') }}">{{ $header['CBE_MENU_STORES'] }}</a>
                        </td>
                        <td>
                            <a href="{{ $router->generate('coupons') }}">{{ $header['CBE_MENU_COUPONS'] }}</a>
                        </td>
                         <!-- {{-- 
                        <td>
                            <a href="/">{{ $header['CBE_MENU_FEATURED'] }}</a>
                        </td>
                     <td>
                            <a href="/">{{ $header['CBE_MENU_FAVORITES'] }}</a>
                        </td>
                        <td>
                            <a href="/">{{ $header['CBE_MENU_HELP'] }}</a>
                        </td>
                        --}} -->


                        {{--<td>--}}
                            {{--<div class="ui-dropdown">--}}
                                {{--<a>All stores</a>--}}
                                {{--<div class="ui-dropdown__content">--}}
                                    {{--<a href="/melinamay">Melinamay</a>--}}
                                    {{--<a href="/ebay">Ebay</a>--}}
                                    {{--<a href="/aliexpress">AliExpress</a>--}}
                                    {{--<a href="/aegean">Aegean</a>--}}
                                    {{--<a href="/shop-gr">E-shop GR</a>--}}
                                    {{--<a href="/deliveras-gr">Deliveras GR</a>--}}
                                    {{--<a href="/zackret">Zackret Sports</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</td>--}}
                    </tr>
                    </tbody>
                </table>
                <button class="ui-button ui-background--none ui-button--navigation ui-float--right ui-border--none" >
                    <span class="ui-icon ui-icon--bars"></span>
                </button>
                <p class="clear"></p>

                <!-- end of panelgrid -->

            </div>
        </div>

        <!-- end of layout-west__footer -->

    </div>

        @yield('header')
        @yield('content')
        @yield('footer')

    <!-- put footer  content here -->

    <div class="ui-footer">
        @if (!$user_info)
        <div class="ui-footer__header ui-text-align--center ui-footer__header--green">
            <span class="ui-font-size--38 ui-color--white">Discover even more deals with Cash Back</span>
            <p class="clear ui-height--8"></p>
            <span class="ui-font-size--20 ui-color--white ui-font--light">Start earning Cash Back at over 1000+ of the most popular stores and speciality boutiques.</span>
            <p class="clear"></p>

            <form action="{{ $router->generate('login') }}">
                <button id="join-up" class="ui-button ui-border--none ui-button--white ui-button--footer-header ui-color--green ui-border-radius--5 ui-font-size--17 ui-font--medium">Join Now</button>
            </form>
        </div>
        @endif

        <div class="ui-footer__content">
            <div class="ui-text-align--center ui-footer__logo-wrapper">
                <img src="{{ SITE_URL }}grabdid-front-felix/src/images/logo-dark.png" />
                <p class="clear"></p>
                <span class="ui-color--white ui-font-size--17 ui-font--medium">Money back and discount coupons</span>
            </div>



            <table class="ui-panelgrid--col-4 ui-panelgrid ui-panelgrid--sitemap ui-width--full">
                <tbody>
                <tr>
                    <td>
                        <span class="ui-font-size--18 ui-color--white ui-font--medium" >Quick links</span>
                        <p class="clear ui-height--20"></p>

                        <a href="<?php echo SITE_URL; ?>aboutus" class="ui-commandlink ui-commandlink--gray">{{ $footer['CBE1_FMENU_ABOUT'] }}</a>
                        <a href="<?php echo SITE_URL; ?>news" class="ui-commandlink ui-commandlink--gray">{{ $footer['CBE1_FMENU_NEWS'] }}</a>
                        <a href="<?php echo SITE_URL; ?>terms" class="ui-commandlink ui-commandlink--gray">{{ $footer['CBE1_FMENU_TERMS'] }}</a>
                        <a href="<?php echo SITE_URL; ?>privacy" class="ui-commandlink ui-commandlink--gray">{{ $footer['CBE1_FMENU_PRIVACY'] }}</a>
                        <a href="<?php echo SITE_URL; ?>contact" class="ui-commandlink ui-commandlink--gray">{{ $footer['CBE1_FMENU_CONTACT'] }}</a>
                        <a href="<?php echo SITE_URL; ?>rss" class="ui-commandlink ui-commandlink--gray">{{ $footer['CBE1_FMENU_RSS'] }}</a>
                    </td>
                    <td>
                        <span class="ui-font-size--18 ui-color--white ui-font--medium" >Categories</span>
                        <p class="clear ui-height--20"></p>
                        <a href="#"  class="ui-commandlink ui-commandlink--gray">Hair and Spa</a>
                        <a href="#"  class="ui-commandlink ui-commandlink--gray">Restaurants</a>
                        <a href="#"  class="ui-commandlink ui-commandlink--gray">Grocery Stores</a>
                        <a href="#"  class="ui-commandlink ui-commandlink--gray">Fashion and Accessories</a>
                        <a href="#"  class="ui-commandlink ui-commandlink--gray">Air tickets</a>
                    </td>

                    <td>
                        <span class="ui-font-size--18 ui-color--white ui-font--medium" >Popular Stores</span>
                        <p class="clear ui-height--20"></p>
                        <a href="#" class="ui-commandlink ui-commandlink--gray">Amazon</a>
                        <a href="#"  class="ui-commandlink ui-commandlink--gray">Nike Store</a>
                        <a href="#"  class="ui-commandlink ui-commandlink--gray">Super market</a>
                        <a href="#"  class="ui-commandlink ui-commandlink--gray">Myntra.com</a>
                        <a href="#"  class="ui-commandlink ui-commandlink--gray">Airbnb</a>
                    </td>

                    <td>
                        <span class="ui-font-size--18 ui-color--white ui-font--medium" >Recent articles</span>
                        <table class="ui-panelgrid ui-panelgrid--footer-article ui-width--full">
                            <tbody>
                            <tr>
                                <td>
                                    <img src="{{ SITE_URL }}grabdid-front-felix/src/images/article-img-1.png" />
                                </td>
                                <td>
                                    <span class="ui-font-size--18 ui-color--white ui-font--medium" >Chinese site</span>
                                    <p class="clear"></p>
                                    <div class="ui-clamp">
                                        <a href="#" class="ui-commandlink--gray ui-wrap ui-font-size--14"  >In izocard we work with the best and most reliable Chinese 123123123</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="{{ SITE_URL }}grabdid-front-felix/src/images/article-img-2.png" />
                                </td>
                                <td>
                                    <span class="ui-font-size--18 ui-color--white ui-font--medium" >Recent articles</span>
                                    <p class="clear"></p>
                                    <div class="ui-clamp">
                                        <a href="#" class="ui-commandlink--gray ui-wrap ui-font-size--14"  >In izocard we work with the best and most reliable Chinese 123123123</a>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <!--end of panelgrid sitemap-->

            <div class="ui-footer--social-media ui-text-align--center">

                @if(isset($footer['FACEBOOK_PAGE']) && !empty($footer['FACEBOOK_PAGE']))
                    <div>
                        <a href="{{ $footer['FACEBOOK_PAGE'] }}" class="ui-text-align--center ui-commandlink--social-media ui-border-radius--full">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 264 512"><path d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5 70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229"/></svg>
                        </a>
                    </div>
                @endif

                {{--<div>--}}
                    {{--<a href="#" class="ui-text-align--center ui-commandlink--social-media ui-border-radius--full">--}}
                        {{--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M386.061 228.496c1.834 9.692 3.143 19.384 3.143 31.956C389.204 370.205 315.599 448 204.8 448c-106.084 0-192-85.915-192-192s85.916-192 192-192c51.864 0 95.083 18.859 128.611 50.292l-52.126 50.03c-14.145-13.621-39.028-29.599-76.485-29.599-65.484 0-118.92 54.221-118.92 121.277 0 67.056 53.436 121.277 118.92 121.277 75.961 0 104.513-54.745 108.965-82.773H204.8v-66.009h181.261zm185.406 6.437V179.2h-56.001v55.733h-55.733v56.001h55.733v55.733h56.001v-55.733H627.2v-56.001h-55.733z"/></svg>--}}
                    {{--</a>--}}
                {{--</div>--}}

                @if(isset($footer['TWITTER_PAGE']) && !empty($footer['TWITTER_PAGE']))
                    <div>
                        <a href="{{ $footer['TWITTER_PAGE'] }}" class="ui-text-align--center ui-commandlink--social-media ui-border-radius--full">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"/></svg>
                        </a>
                    </div>
                @endif

                {{--<div>--}}
                    {{--<a href="#" class="ui-text-align--center ui-commandlink--social-media ui-border-radius--full">--}}
                        {{--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"/></svg>--}}
                    {{--</a>--}}
                {{--</div>--}}

                {{--<div>--}}
                    {{--<a href="#" class="ui-text-align--center ui-commandlink--social-media ui-border-radius--full">--}}
                        {{--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M204 6.5C101.4 6.5 0 74.9 0 185.6 0 256 39.6 296 63.6 296c9.9 0 15.6-27.6 15.6-35.4 0-9.3-23.7-29.1-23.7-67.8 0-80.4 61.2-137.4 140.4-137.4 68.1 0 118.5 38.7 118.5 109.8 0 53.1-21.3 152.7-90.3 152.7-24.9 0-46.2-18-46.2-43.8 0-37.8 26.4-74.4 26.4-113.4 0-66.2-93.9-54.2-93.9 25.8 0 16.8 2.1 35.4 9.6 50.7-13.8 59.4-42 147.9-42 209.1 0 18.9 2.7 37.5 4.5 56.4 3.4 3.8 1.7 3.4 6.9 1.5 50.4-69 48.6-82.5 71.4-172.8 12.3 23.4 44.1 36 69.3 36 106.2 0 153.9-103.5 153.9-196.8C384 71.3 298.2 6.5 204 6.5z"/></svg>--}}
                    {{--</a>--}}
                {{--</div>--}}
            </div>
            <div class="ui-footer--copyright ui-commandlink--gray ui-text-align--center">
                © 2018 {{ SITE_TITLE }}. {{ CBE1_FMENU_RIGHTS }}.
            </div>
        </div>
    </div>

    <!--end of page footer-->

    <!--modal sidebar navigation (used on mobile devices)-->

    <div class="ui-modal ui-sidebar--navigation">
        <div class="ui-sidebar__content">
            <button id="closeSidebarBtn" class="ui-button ui-border--none ui-float--right ui-background--none ui-sidebar__close">
                <span class="ui-icon ui-icon--close"></span>
            </button>
            <p class="clear"></p>
            <div class="ui-accordion">
                <div class="ui-accordion__header">
                    <span>All Stores</span>
                </div>
                <div class="ui-accordion__content">
                    <a href="/melinamay">Melinamay</a>
                    <a href="/ebay">Ebay</a>
                    <a href="/aliexpress">AliExpress</a>
                    <a href="/aegean">Aegean</a>
                    <a href="/shop-gr">E-shop GR</a>
                    <a href="/deliveras-gr">Deliveras GR</a>
                    <a href="/zackret">Zackret Sports</a>
                </div>
            </div>
            <a href="/coupons">Coupons</a>
            <a href="/categories">Categories</a>
            <a href="/hot-deal">Hot Deals</a>
            <div class="ui-accordion">
                <div class="ui-accordion__header">
                    <span>All Stores</span>
                </div>
                <div class="ui-accordion__content">
                    <a href="/melinamay">Melinamay</a>
                    <a href="/ebay">Ebay</a>
                    <a href="/aliexpress">AliExpress</a>
                    <a href="/aegean">Aegean</a>
                    <a href="/shop-gr">E-shop GR</a>
                    <a href="/deliveras-gr">Deliveras GR</a>
                    <a href="/zackret">Zackret Sports</a>
                </div>
            </div>
            <a href="{{ $router->generate('howitworks') }}">How it works</a>
            <a href="{{ $router->generate('help') }}">Help</a>
        </div>
    </div>

    <!--modal login-->

    <div class="ui-modal ui-dialog ui-dialog--login">
        <table class="ui-width--full ui-height--full ">
            <tbody>
            <tr>
                <td class="ui-vertical-align--middle">
                    <div class="ui-dialog__content">
                        <div class="ui-dialog__header ui-font--medium ui-font-size--18 ui-text-align--center">
                            <span>{{ $header['CBE1_BOX_LOGIN'] }}</span>
                            <button id="closeLoginBtn" class="ui-button ui-border--none ui-float--right ui-background--none ui-sidebar__close">
                                <span class="ui-icon ui-icon--close"></span>
                            </button>
                        </div>
                        <form id="loginForm">
                            <p class="clear"></p>
                            <div class="ui-text-align--center ui-margin-top--10 ui-margin-bottom--7 ui-font-size--17 ui-color--gray">
                                Have a Facebook Account?
                            </div>
                            <a href="{{ $router->generate('fblogin') }}" class="ui-commandlink  ui-commandlink--facebook-login"></a>
                            <div class="ui-text-align--center ui-margin-top--5 ui-font-size--12 ui-color--gray">
                                We'll never post anything without your permission.
                            </div>
                            <div class="ui-text-align--center ui-font-size--14 ui-margin-top--10">
                                — OR —
                            </div>
                            <div class="ui-width--300 ui-margin--center">
                                <input type="hidden" name="action" value="login">
                                <input name="username" placeholder="{{ $header['CBE1_LOGIN_EMAIL'] }}" type="email" pattern="[^@\s]+@[^@\s]+\.[^@\s]+" required class="ui-inputfield ui-width--284 ui-inputfield--dialog ui-margin-top--10" />
                                <input name="password" placeholder="{{ $header['CBE1_LOGIN_PASSWORD'] }}" minlength="6" required type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$"  class="ui-inputfield ui-width--284 ui-inputfield--dialog ui-margin-top--10 ui-margin-bottom--11" />
                            </div>
                            <div class="ui-width--300 ui-margin--center" id="login-form-err"></div>
                            {{--<div class="g-recaptcha ui-margin--center ui-width--300" data-sitekey="6Lf7rEUUAAAAADFrTtVT_fZvsX7HEJfxJN8b-UMW"></div>--}}
                            <div class="ui-width--300 ui-margin--center ui-text-align--center">
                                <button class="ui-width--full ui-button ui-border--none ui-border-radius--none ui-margin-top--10 ui-margin-bottom--11">{{ $header['CBE1_LOGIN_BUTTON'] }}</button>
                                <a href="{{ $router->generate('forgot') }}" class="ui-commandlink ui-commandlink--gray" >{{ $header['CBE1_LOGIN_FORGOT'] }}</a>
                            </div>
                        </form>
                        <p class="clear ui-height--20"></p>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!--end of login dialog-->

    <div class="ui-modal ui-dialog ui-dialog--register">
        <table class="ui-width--full ui-height--full ">
            <tbody>
            <tr>
                <td class="ui-vertical-align--middle">
                    <div class="ui-dialog__content">
                        <div class="ui-dialog__header ui-font--medium ui-font-size--18 ui-text-align--center">
                            <span>Register</span>
                            <button id="closeRegisterBtn" class="ui-button ui-border--none ui-float--right ui-background--none ui-sidebar__close">
                                <span class="ui-icon ui-icon--close"></span>
                            </button>
                        </div>
                        <form id="registerForm">
                            <p class="clear"></p>
                            <div class="ui-text-align--center ui-margin-top--10 ui-margin-bottom--7 ui-font-size--17 ui-color--gray">
                                Have a Facebook Account?
                            </div>
                            <a href="{{ $router->generate('fblogin') }}" class="ui-commandlink  ui-commandlink--facebook-login"></a>
                            <div class="ui-text-align--center ui-margin-top--5 ui-font-size--12 ui-color--gray">
                                We'll never post anything without your permission.
                            </div>
                            <div class="ui-text-align--center ui-font-size--14 ui-margin-top--10">
                                — OR —
                            </div>
                            <div class="ui-width--300 tag example ui-margin--center">
                                <input type="hidden" name="action" value="signup">
                                <select name="country" aria-required="true" required class="ui search selection ui-margin-top--10 dropdown" id="search-select">

                                    <option value="">State</option>
                                    @foreach($countries as $country)
                                    <option value="{{ $country['country_id'] }}">{{ $country['name'] }}</option>
                                    @endforeach

                                </select>
                                <input name="email" placeholder="{{ $header['CBE1_LOGIN_EMAIL'] }}" type="email" pattern="[^@\s]+@[^@\s]+\.[^@\s]+" required class="ui-inputfield ui-width--284 ui-inputfield--dialog ui-margin-top--10" />
                                <input name="password" placeholder="{{ $header['CBE1_LOGIN_PASSWORD'] }}" minlength="6" required type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$"  class="ui-inputfield ui-width--284 ui-inputfield--dialog ui-margin-top--10 ui-margin-bottom--11" />

                                <div class="login__check-agree">
                                    <input type="checkbox" name="tos" id="news" class="login__news-checkbox" required>
                                    <label for="news" class="login__news-label ui-font-size--13">I agree with <a href="#" class="ui-color--green">Terms</a> &amp; <a href="#" class="ui-color--green">Conditions and Privacy Policy.</a></label>
                                </div>
                            </div>
                            <div class="ui-width--300 ui-margin--center" id="register-form-err"></div>
                            {{--<div class="g-recaptcha ui-margin--center ui-width--300" data-sitekey="6Lf7rEUUAAAAADFrTtVT_fZvsX7HEJfxJN8b-UMW"></div>--}}
                            <div class="ui-width--300 ui-margin--center ui-text-align--center">
                                <button class="ui-width--full ui-button ui-border--none ui-border-radius--none ui-margin-top--10 ui-margin-bottom--11">Register</button>
                                <a href="{{ $router->generate('forgot') }}" class="ui-commandlink ui-commandlink--gray" >{{ $header['CBE1_LOGIN_FORGOT'] }}</a>
                            </div>
                        </form>
                        <p class="clear ui-height--20"></p>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!--top button-->
    <div id="backToTheTop"> <span class="topIcon" /> </div>

    <!-- help button -->
    <a href="{{ $router->generate('help') }}" class="helpLink ui-button ui-button--green"><span class="fa fa-question-circle"></span> {{ $header['CBE_MENU_HELP'] }}</a>

    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/jquery-3.3.1.min.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/ftellipsis.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/easing.min.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/transition.min.js" ></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/dropdown.min.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/search.min.js" ></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/validate.js"></script>

    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/flag.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/stickyfill.min.js"></script>

    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/owl.carousel.min.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/carousel.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/main.js"></script>

    <script>
        $("#loginForm").validate();
        $("#registerForm").validate();

        var content = {!! $search_array !!};

        $('.ui.search')
            .search({
                source: content
            })
        ;
        $('.tag.example .ui.dropdown')
            .dropdown({
                allowAdditions: true,
                minSelections: 1
            })
        ;
    </script>

    <script>
        $("#loginForm").on('submit', function(e){
            e.preventDefault();

            var form_data = $(this).serialize();

            $.ajax({
                type: "POST",
                url: "{{ $router->generate('login') }}",
                data: form_data,
                dataType: 'json',
                success: function (msg) {
                    if(typeof msg.success !== 'undefined'){
                        location.reload();
                        // $(location).attr('href', msg.success);
                    } else {
                        $("#login-form-err").text(msg.errs).css('color','red');
                    }
                }
            });
        });
    </script>

    <script>
        $("#registerForm").on('submit', function(e){
            e.preventDefault();

            var form_data = $(this).serialize();

            $.ajax({
                type: "POST",
                url: "{{ $router->generate('login') }}",
                data: form_data,
                dataType: 'json',
                success: function (msg) {
                    if(typeof msg.success !== 'undefined'){
                        location.reload();
                        // $(location).attr('href', msg.success);
                    } else {
                        $("#register-form-err").text(msg.errs).css('color','red');
                    }
                }
            });
        });
    </script>

        @yield('footer-scripts')
    </body>
</html>
