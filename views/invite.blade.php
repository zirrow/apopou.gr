
@extends('layouts.master')

@section('head')

@endsection


@section('head-styles')

@endsection


@section('head-scripts')

@endsection


@section('header')

@endsection {{-- #header--}}

@section('content')
    <div class="ui-layout-center__content">
        <div class="invite">

            {!! $content['text'] !!}

            <!-- Social sharing block -->
            <div class="invite__share">
                <div class="invite__m ui-text-align--center ui-margin-bottom--10">
                    <img src="grabdid-front-felix/build/images/raf_mw.png" class="ui-width--100 ui-text-align--center" alt="">
                </div>
                <h1 class="invite__heading invite__heading--socials-d ui-font-size--20 ui-text-align--center">Share Your Invite Link</h1>
                <h2 class="invite__heading invite__heading--socials-m ui-font-size--20 ui-text-align--center">Every Referral. No Limit.</h2>
                <span class="invite__heading--socials-m ui-text-align--center ui-color--sm-grey">When your friend joins and spends at least $25, you'll get $25.</span>

                <div class="grid-3-col invite__socials">
                    <!-- Field with ref link *Mobile* -->
                    <div class="grid-3-col__1-of-3 grid-3-col__1-of-3--inv-socials invite__m">
                        <div class="invite__copylink">
                            <input type="text"
                                   value="ebates.com/r/UPLOAD17?eeid=28187"
                                   readonly
                                   class="invite__input-text invite__input-text--reflink invite__reflink copy__reflink"
                                   id="referalLink"
                                   data-clipboard-target="#referalLink"
                                   data-clipboard-action="copy">

                            <input type="button"
                                   value="Copy"
                                   class="invite__greentext-btn ui-padding-top--10 copy__reflink"
                                   data-clipboard-target="#referalLink"
                                   data-clipboard-action="copy">
                        </div>
                    </div>

                    <!-- Share via email link -->
                    {{--<a href="mailto link" class="grid-3-col__1-of-3 grid-3-col__1-of-3--inv-socials invite__placeholder invite__placeholder--email invite__m">--}}
                        {{--<span class="fa fa-envelope  invite__soc-icon"></span> Share via Email--}}
                    {{--</a>--}}
                    <!-- Share via SMS link -->
                    {{--<a href="sms:? Nya. Sms text here" class="grid-3-col__1-of-3 grid-3-col__1-of-3--inv-socials invite__placeholder invite__placeholder--sms invite__m">--}}
                        {{--<span class="fa fa-comment invite__soc-icon"></span> Share via Text--}}
                    {{--</a>--}}

                    <!-- Share via FB -->
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ $referral_link_fb }}&t={{ $head['SITE_TITLE'] }}" class="grid-3-col__1-of-3 grid-3-col__1-of-3--inv-socials invite__placeholder invite__placeholder--fb" target="_blank">
                        <span class="fa fa-facebook invite__soc-icon"></span> Share on Facebook
                    </a>
                    <!-- Share via Twitter block -->
                    <a href="https://twitter.com/intent/tweet?text={{ $head['SITE_TITLE'] }}&url={{ $referral_link }}" class="grid-3-col__1-of-3 grid-3-col__1-of-3--inv-socials invite__placeholder invite__placeholder--tw" target="_blank">
                        <span class="fa fa-twitter invite__soc-icon"></span>Share on Twitter
                    </a>
                    <!-- Share via Google Plus block  -->
                    <a href="https://plus.google.com/share?url={{ $referral_link }}" class="grid-3-col__1-of-3 grid-3-col__1-of-3--inv-socials invite__placeholder invite__placeholder--gp">
                        <span class="fa fa-google-plus invite__soc-icon"></span>Share on Google Plus
                    </a>

                    <!-- Field with ref link *Desktop* -->
                    <div class="grid-3-col__3-of-3 grid-3-col__3-of-3--inv-socials invite__d ui-margin-top--10">
                        <div class="invite__copylink">
                            <input type="text"
                                   value="{{ $referral_link_main }}"
                                   readonly
                                   class="invite__input-text invite__input-text--reflink invite__reflink copy__reflink"
                                   id="referalLinkD"
                                   data-clipboard-target="#referalLink"
                                   data-clipboard-action="copy">

                            <input type="button"
                                   value="Copy Link"
                                   class="invite__greentext-btn ui-padding-top--10 copy__reflink"
                                   data-clipboard-target="#referalLinkD"
                                   data-clipboard-action="copy">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Email sending form -->
            <div class="invite__form ui-text-align--center">
                <h2 class="ui-font-size--20 invite__heading">Invite Your Friends by Email</h2>
                <div class="invite__heading" id="mail-error"></div>
                <form id='invite_form' class="invite__email-wrapper ui-text-align--left">
                    <input type="hidden" name="action" value="friend">
                    <input type="text"
                           name="friend_email"
                           id="inviteEmail"
                           class="invite__input-text invite__input-text--email"
                           placeholder="Enter Email Adress">

                    <input type="button"
                           disabled
                           value="Send Email"
                           class="invite__email-btn ui-font-size--16"
                           id="sendEmail">
                </form>
                <!-- Email preview button -->
                <div class="ui-text-align--right ui-padding-15-0 ui-position--relative">
                    <button class="invite__greentext-btn ui-padding-top--10 modal-email-show">Preview Email</button>
                </div>
            </div>

            <!-- *Mobile block* How it works -->
            <div class="invite__how invite__m ui-text-align--center">
                <h2 class="invite__heading ui-font-size--24">How It Works</h2>
                <div class="invite__how-num">1</div>
                <p>Share your invite link with friends.</p>
                <div class="invite__how-num">2</div>
                <p>Remind them to sign up and shop on Ebates.</p>
                <div class="invite__how-num">3</div>
                <p>You both get paid when they spend $25.</p>
            </div>

            <!-- Information table for referrals -->
            <div class="invite__referrals">
                <h1 class="invite__heading  ui-font-size--20 ui-text-align--center">Your Referral Activity </h1>
                <!-- Referral statistics -->
                <div class="invite__ref-stats">
                    <div class="invite__ref-stat">
                        <p class="invite__ref-val invite__ref-val--clicks">{{ $referral_clicks }}</p>
                        <p class="ui-font-size--15">Referral link clicks</p>
                    </div>
                    <div class="invite__ref-stat">
                        <p class="invite__ref-val invite__ref-val--referrals">{{ $referrals_total }}</p>
                        <p class="ui-font-size--15">Referrals</p>
                    </div>
                    <div class="invite__ref-stat">
                        <p class="invite__ref-val invite__ref-val--pending"> {{ $referrals_pending_bonuses }}</p>
                        <p class="ui-font-size--15">Referrals pending earnings</p>
                    </div>
                    <div class="invite__ref-stat">
                        <p class="invite__ref-val invite__ref-val--earnings"> {{ $referrals_paid_bonuses }}</p>
                        <p class="ui-font-size--15">Referrals paid earnings</p>
                    </div>
                </div>
                <!-- Referral table tabs -->
                <div class="invite__tabs ui-text-align--center clr" id="tabs">
                    <div class="grid-3-col__1-of-3 invite__tab invite__tab--active" id="invited">Invited ({{ count($referrals_invites) }})</div>
                    <div class="grid-3-col__1-of-3 invite__tab" id="signedup">Signed Up ({{ count($referrals_invited) }})</div>
                    <div class="grid-3-col__1-of-3 invite__tab" id="qualified">Qualified({{ count($referrals_qualified) }})</div>
                </div>
                <!-- Referral table content starts -->
                <div class="invite__tabcontent-wrapper">

                    <!-- Table for invited referrals -->
                    <div class="invite__tabcontent invite__tabcontent--invited invite__tabcontent--active ui-border ui-border-t-none">
                        <table class="invite__tabcontent-table" data-pag-prefix="inv">
                            <tbody>

                            @if(count($referrals_invites) > 0)
                                @foreach($referrals_invites as $referral_invite)
                                    <tr class="invite__row-inv">
                                        <td class="ui-width--75"><span class="ui-font-size--18">{{ $referral_invite['recipients'] }}</span> <br>
                                            <span class="ui-font-size--14 ui-color--gray">{{ $referral_invite['sent_date'] }}</span></td>
                                        <td class="ui-width--quad ui-font-size--14 ui-color--gray">Invited</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="ui-width--full ui-text-align--center ui-font-size--14">
                                        You don't have any invited referrals at the moment.
                                    </td>
                                </tr>
                            @endif

                            </tbody>
                        </table>

                        @if(count($referrals_invites) > 2)
                            <div class="pagination">
                                <button class="fa fa-angle-left pagination__button pagination__prev" id="invitedPrev"></button>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <button class="fa fa-angle-right pagination__button pagination__next" id="invitedNext"></button>
                            </div>
                        @endif

                    </div>

                    <div class="invite__tabcontent invite__tabcontent--signedup ui-border ui-border-t-none">
                        <!-- Table for signed-up referrals -->
                        <table class="invite__tabcontent-table" data-pag-prefix="su">
                            <tbody>

                            @if(count($referrals_invited) > 0)
                                @foreach($referrals_invited as $referral_invited)
                                    <tr class="invite__row-inv">
                                        <td class="ui-width--75">
                                            <span class="ui-font-size--18">{{ $referral_invited['username'] }}</span> <br>
                                            <span class="ui-font-size--14 ui-color--gray">{{ $referral_invited['signup_date'] }}</span>
                                        </td>
                                        <td class="ui-width--quad ui-font-size--14 ui-color--gray">{{ $referral_invited['status'] }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="ui-width--full ui-text-align--center ui-font-size--14">
                                        You don't have any Signed Up referrals at the moment.
                                    </td>
                                </tr>
                            @endif

                            </tbody>
                        </table>

                        @if(count($referrals_invited) > 2)
                            <div class="pagination">
                                <button class="fa fa-angle-left pagination__button pagination__prev" id="signedupPrev"></button>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <button class="fa fa-angle-right pagination__button pagination__next" id="signedupNext"></button>
                            </div>
                        @endif
                    </div>

                    <div class="invite__tabcontent invite__tabcontent--qualified ui-border ui-border-t-none">
                        <!-- Table for qualified referrals -->
                        <table class="invite__tabcontent-table">
                            <tbody>

                            @if(count($referrals_qualified) > 0)
                                @foreach($referrals_qualified as $referral_qualified)
                                    <tr class="invite__row-inv">
                                        <td class="ui-width--75"><span class="ui-font-size--18">{{ $referral_qualified['username'] }}</span> <br>
                                            <span class="ui-font-size--14 ui-color--gray">{{ $referral_qualified['signup_date'] }}</span></td>
                                        <td class="ui-width--quad ui-font-size--14 ui-color--gray">{{ $referral_qualified['status'] }}</td>
                                    </tr>
                                @endforeach
                            @else
                                 <tr>
                                    <td class="ui-width--full ui-text-align--center ui-font-size--14">
                                        You don't have any qualified referrals at the moment. If someone you invited already signed up, please tell them to make an order through Ebates.
                                    </td>
                                </tr>
                            @endif

                            </tbody>
                        </table>

                        @if(count($referrals_qualified) > 2)
                            <div class="pagination">
                                <button class="fa fa-angle-left pagination__button pagination__prev" id="qualifiedPrev"></button>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <button class="fa fa-angle-right pagination__button pagination__next" id="qualifiedNext"></button>
                            </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>

        <div class="grid-3-col invite__conditions">
            <div class="grid-3-col__2-of-3 invite__terms">
                <p class="ui-margin-bottom--15">How our referral program works</p>
                <p class="ui-font-size--11 ui-color--gray">Ebates will pay you for every new member you refer to our site who then shops and makes a purchase at our partner merchants sites in accordance with the Referral Program Terms set forth below. While we love them, too, pets and children do not qualify, nor do referrals to friends or housemates who do not do their own shopping via their own account on Ebates.com.</p>

                <p class="ui-margin-top--15 ui-margin-bottom--15">Referral Program Terms </p>
                <p class="ui-font-size--11 ui-color--gray">Subject to these Referral Program Terms, Ebates will pay you a $25 Referral Bonus through your Ebates account for every Qualified Referral (defined below) you refer during the period ending on June 30, 2018 ("Referral Period"). A "Qualified Referral" must (i) be a new member of Ebates, (ii) be referred by you via your custom referral link, the links via the invitation forms on this page, or other methods supplied by Ebates that allow for proper tracking of referrals, (iii) sign up at Ebates.com during the Referral Period, and (iv) make qualifying purchases totaling $25 or more that earn Cash Back within one year of signing up. The following is a non-exhaustive list of activities that are not permitted and that will disqualify you from earning Referral Bonuses: (i) self-referral, (ii) posting your referral link on any page that is not owned and controlled by you, including, but not limited to, any Ebates merchant's Facebook or forum page, (iii) bidding on any keywords containing Ebates, including, but not limited to, Ebates, Ebates.com, Ebates.ca, (iv) placement of the Ebates logo or mention of Ebates in any ad text, extensions or banner ads, (v) paid advertising for the purpose of generating traffic directly to your referral link, (vi) posing as a representative of Ebates in an official capacity, and (vii) any bulk email distribution, submission or distribution to strangers, or any other promotion that would constitute or appear to constitute unsolicited commercial email or "spam." Ebates reserves the right to review all referrals and to deny, withhold, or cancel any bonuses for any referrals that Ebates deems, in its sole discretion, as fraudulent, abusive, unethical, suspicious or otherwise inconsistent with these Referral Program Terms, the Ebates Cash Back Shopping Terms & Conditions, or any other applicable law or regulation. Ebates' decisions are final. Ebates reserves the right to suspend or terminate the Referral Program or to change these Referral Program Terms at any time and for any reason in its sole discretion.</p>
            </div>
            <div class="grid-3-col__1-of-3 invite__ref-but invite__d">
                <p>Ebates Referral Button</p>
                <br>
                <p class="ui-font-size--14">Are you a blogger or power seller? Copy this source code to display our Referral Button on your site.</p>
                <br>
                <img src="grabdid-front-felix/build/images/ebates-referral-button.png" alt="Referral button image">
                <br>
                <!-- Field with ref link *Desktop* -->
                <div class="grid-3-col__3-of-3 grid-3-col__3-of-3--inv-socials invite__d ui-margin-top--10">
                    <div class="invite__copylink">
                        <input type="text"
                               value="{{ $referral_link_main }}"
                               readonly
                               class="invite__input-text invite__input-text--reflink invite__reflink copy__reflink"
                               id="referalLinkD"
                               data-clipboard-target="#refButLink"
                               data-clipboard-action="copy">

                        <input type="button"
                               value="Copy Link"
                               class="invite__greentext-btn ui-padding-top--10 copy__reflink"
                               data-clipboard-target="#refButLink"
                               data-clipboard-action="copy">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="overlay"></div>
    <div class="modal-email-prev">
        <span class="fa fa-times ui-font-size--16 ui-float--right ui-color--gray ui-padding--10 ui-cursor--pointer modal-email-hide"></span>
    </div>

@endsection {{-- #content --}}


@section('footer')

@endsection


@section('footer-scripts')

    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/clipboard.min.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/classList.min.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/expand-text.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/invite-pagination.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/signed-up-pagination.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/qualified-pagination.js"></script>

    <script>
        new ClipboardJS('.copy__reflink');

        $(document).ready(function(){
            $(".copy__reflink").on("click", function(){
                $(".invite__greentext-btn").val("Copied!");
                $('.invite__input-text--reflink').addClass("invite__input-text--active");

                setTimeout(function(){
                    $(".invite__greentext-btn").val("Copy Link");
                    $(".invite__input-text--active").removeClass("invite__input-text--active");
                },2000);
            });

            $(".invite__tab").on("click", function(){
                var tabId = $(this).attr("id").toString();
                var tabToDisplay = ".invite__tabcontent--" + tabId;

                $(".invite__tab").each(function(){
                    $(this).removeClass("invite__tab--active");
                });

                $(".invite__tabcontent--active").removeClass("invite__tabcontent--active");
                $(tabToDisplay).addClass("invite__tabcontent--active");
                $(this).addClass("invite__tab--active");
            });

            $(".modal-email-show").on("click", function(){
                $(".overlay").addClass("overlay--shown");
                $(".modal-email-prev").addClass("modal-email-prev--shown");
            });

            $(".modal-email-hide").on("click", function(){
                $(".overlay").removeClass("overlay--shown");
                $(".modal-email-prev").removeClass("modal-email-prev--shown");
            });

            $(".overlay").on("click", function(){
                $(".overlay").removeClass("overlay--shown");
                $(".modal-email-prev").removeClass("modal-email-prev--shown");
            });

            $("#inviteEmail").on("keyup",function(){
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (regex.test($(this).val())){
                    $("#sendEmail").attr("disabled",false);
                } else {
                    $("#sendEmail").attr("disabled",true);
                }
            });

            $("#sendEmail").on("click", function(){
                if ($(this).attr("disabled") === true) {
                    true;
                } else {

                    var form_data = $("#invite_form").serialize();

                    $.ajax({
                        type: "POST",
                        url: "{{ $router->generate('invite') }}",
                        data: form_data,
                        dataType: 'json',
                        success: function (msg) {

                            if(typeof msg.success !== 'undefined'){
                                $("#sendEmail").val("Email Sent");
                                $("#sendEmail").attr("disabled", true);
                            } else {
                                $("#mail-error").html(msg.error).css('color', 'red');
                            }

                        }
                    });
                }
            })

        });
    </script>

@endsection