
@extends('layouts.master')

@section('head')

@endsection


@section('head-styles')

@endsection


@section('head-scripts')

@endsection


@section('header')

@endsection {{-- #header--}}

@section('content')

    <div class="ui-layout-center">
        <div class="ui-layout-center__content">

            {!! $content['text'] !!}

            <div class="ui-margin-top--12 ui-margin-bottom--11">
                <span class="ui-font-size--20 ui-font--light">Featured Stores</span>
            </div>

            <!--        Here is the recommended shops block -->

            <!-- ROW 1 -->

            <div class="grid-6-col stores-recommended owl-carousel owl-theme" id="owlCarousel">
                @foreach ($featured_stores_1[0] as $featured_store)
                    <div class="grid-6-col__1-of-6">
                        <a href="{!! $featured_store['retailer_link'] !!}" class="stores-recommended__rec">
                            <img src="{{ $featured_store['image'] }}" alt="{{ $featured_store['title'] }}" class="stores-recommended__logo" style="height: {{ $retailer_img_height }}px; width: {{ $retailer_img_width }}px">
                            @if (!empty($featured_store['old_cashback']))
                                <p class="stores-recommended__old-cashback">was {{ $featured_store['old_cashback'] }}</p>
                            @endif
                            <p class="stores-recommended__cashback">{{ $featured_store['cashback'] }} Cash Back</p>
                            <p class="stores-recommended__see-all">See all {{ $featured_store['title'] }}</p>
                        </a>
                    </div>
                @endforeach
            </div>

            <!-- ROW 1 END  -->

            <!-- ROW 2 -->

            <div class="grid-6-col stores-recommended owl-carousel owl-theme" id="owlCarousel">
                @foreach ($featured_stores_1[1] as $featured_store)
                    <div class="grid-6-col__1-of-6">
                        <a href="{!! $featured_store['retailer_link'] !!}" class="stores-recommended__rec">
                            <img src="{{ $featured_store['image'] }}" alt="{{ $featured_store['title'] }}" class="stores-recommended__logo" style="height: {{ $retailer_img_height }}px; width: {{ $retailer_img_width }}px">
                            @if (!empty($featured_store['old_cashback']))
                                <p class="stores-recommended__old-cashback">was {{ $featured_store['old_cashback'] }}</p>
                            @endif
                            <p class="stores-recommended__cashback">{{ $featured_store['cashback'] }} Cash Back</p>
                            <p class="stores-recommended__see-all">See all {{ $featured_store['title'] }}</p>
                        </a>
                    </div>
                @endforeach
            </div>

            <!-- ROW 2 END  -->

            <div class="ui-separator ui-font-size--29 clr">
                <a href="{{ $router->generate('retailers_by_category') }}" class="ui-commandlink">View all</a>
            </div>


            <div class="ui-separator ui-font-size--29 ui-separator--margin ui-text-align--center">
                How
                <span class="ui-state--highlight">izocard</span>
                works
            </div>

            <!-- end of separator -->

            <table class="ui-panelgrid ui-panelgrid--col-3 ui-panelgrid--basic">
                <tbody>
                <tr>
                    <td>
                        <div class="ui-panelgrid__image ui-panelgrid__image--compass" >

                        </div>
                        <div class="ui-panelgrid__subheader ui-font-size--24 ui-text-align--center">
                            Browse a store
                        </div>
                        <div class="ui-panelgrid__output ui-font-size--16 ui-text-align--center" >
                            Find the store you are interested in and just go to the store by clicking on link Go to Store
                        </div>
                    </td>
                    <td>
                        <div class="ui-panelgrid__image ui-panelgrid__image--cart">

                        </div>
                        <div class="ui-panelgrid__subheader ui-font-size--24 ui-text-align--center">
                            Shop normally
                        </div>
                        <div class="ui-panelgrid__output ui-font-size--16 ui-text-align--center">
                            Now you can Make your online purchases as usual everytime
                        </div>
                    </td>
                    <td>
                        <div class="ui-panelgrid__image ui-panelgrid__image--refund">

                        </div>
                        <div class="ui-panelgrid__subheader ui-font-size--24 ui-text-align--center">
                            Refund
                        </div>
                        <div class="ui-panelgrid__output ui-font-size--16 ui-text-align--center">
                            The store will pay us a commission because we sent him a customer. In return we will refund your part of the commission we earn
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>

            <!--end of panelgrid-->



            <div class="ui-separator ui-font-size--29 ui-separator--margin">
                Latest
                <span class="ui-state--highlight">Coupons</span>
            </div>
            <table class="ui-panelgrid ui-width--full ui-panelgrid--col-4 ui-panelgrid--borders ui-panelgrid--basic">
                <tbody>
                <tr>
                    @foreach ($latest_coupons[0] as $latest_coupon)
                        <td class="ui-panelgrid__cell">
                            <div>
                                <div class="ui-panelgrid__background ui-panelgrid__background--logo">
                                    <img src="{{ $latest_coupon['image'] }}" alt="{{ $latest_coupon['title'] }}">
                                </div>
                                <div class="ui-panelgrid__subheader ui-font-size--22">
                                    {{ $latest_coupon['title'] }}
                                </div>
                                <div class="ui-panelgrid__output ui-font-size--16 ui-padding-left--13" >
                                    {{ $latest_coupon['description'] }}
                                </div>
                                <div class="ui-panelgrid__footer" >
                                    <form action="{!! $latest_coupon['retailer_link'] !!}/{{ $latest_coupon['coupon_id'] }}">
                                        <button class="ui-button ui-border-radius--4 ui-font-size--12 ui-button--green-light">View Coupon</button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    @endforeach
                </tr>
                </tbody>
            </table>

            <table class="ui-panelgrid ui-width--full ui-panelgrid--col-4 ui-panelgrid--borders ui-panelgrid--basic ui-margin-top--25">
                <tbody>
                <tr>
                    @foreach ($latest_coupons[1] as $latest_coupon)
                        <td class="ui-panelgrid__cell">
                            <div>
                                <div class="ui-panelgrid__background ui-panelgrid__background--logo">
                                    <img src="{{ $latest_coupon['image'] }}" alt="{{ $latest_coupon['title'] }}">
                                </div>
                                <div class="ui-panelgrid__subheader ui-font-size--22">
                                    {{ $latest_coupon['title'] }}
                                </div>
                                <div class="ui-panelgrid__output ui-font-size--16 ui-padding-left--13" >
                                    {{ $latest_coupon['description'] }}
                                </div>
                                <div class="ui-panelgrid__footer" >
                                    <form action="{!! $latest_coupon['retailer_link'] !!}/{{ $latest_coupon['coupon_id'] }}">
                                        <button class="ui-button ui-border-radius--4 ui-font-size--12 ui-button--green-light">View Coupon</button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    @endforeach
                </tr>
                </tbody>
            </table>

            <div class="ui-separator ui-font-size--29 ui-separator--margin clr">
                <a href="{{ $router->generate('coupons') }}" class="ui-commandlink">View all</a>
            </div>

            <!--end of panelgrid (discount coupons)-->

            <!-- Here is the recommended shops block -->

            <div class="ui-margin-top--12 ui-margin-bottom--11">
                <span class="ui-font-size--20 ui-font--light">Featured Stores</span>
            </div>

            <!-- ROW 1 -->

            <div class="grid-6-col stores-recommended owl-carousel owl-theme" id="owlCarousel">
                @foreach ($featured_stores_2[0] as $featured_store)
                    <div class="grid-6-col__1-of-6">
                        <a href="{!! $featured_store['retailer_link'] !!}" class="stores-recommended__rec">
                            <img src="{{ $featured_store['image'] }}" alt="{{ $featured_store['title'] }}" class="stores-recommended__logo" style="height: {{ $retailer_img_height }}px; width: {{ $retailer_img_width }}px">
                            @if (!empty($featured_store['old_cashback']))
                                <p class="stores-recommended__old-cashback">was {{ $featured_store['old_cashback'] }}</p>
                            @endif
                            <p class="stores-recommended__cashback">{{ $featured_store['cashback'] }} Cash Back</p>
                            <p class="stores-recommended__see-all">See all {{ $featured_store['title'] }}</p>
                        </a>
                    </div>
                @endforeach
            </div>

            <!-- ROW 1 END  -->

            <!-- ROW 2 -->

            <div class="grid-6-col stores-recommended owl-carousel owl-theme" id="owlCarousel">
                @foreach ($featured_stores_2[1] as $featured_store)
                    <div class="grid-6-col__1-of-6">
                        <a href="{!! $featured_store['retailer_link'] !!}" class="stores-recommended__rec">
                            <img src="{{ $featured_store['image'] }}" alt="{{ $featured_store['title'] }}" class="stores-recommended__logo" style="height: {{ $retailer_img_height }}px; width: {{ $retailer_img_width }}px">
                            @if (!empty($featured_store['old_cashback']))
                                <p class="stores-recommended__old-cashback">was {{ $featured_store['old_cashback'] }}</p>
                            @endif
                            <p class="stores-recommended__cashback">{{ $featured_store['cashback'] }} Cash Back</p>
                            <p class="stores-recommended__see-all">See all {{ $featured_store['title'] }}</p>
                        </a>
                    </div>
                @endforeach
            </div>

            <!-- ROW 2 END  -->
            <div class="ui-separator ui-font-size--29 clr">
                <a href="{{ $router->generate('retailers_by_category') }}" class="ui-commandlink">View all</a>
            </div>


            <div class="ui-separator ui-font-size--29 ui-separator--margin ui-text-align--center">
                What our
                <span class="ui-state--highlight">members</span>
                say
            </div>
            <table class="ui-panelgrid ui-width--full ui-panelgrid--col-3  ui-panelgrid--testemonials ui-panelgrid--borders ui-panelgrid--basic">
                <tbody>
                <tr>
                    <td>
                        <div class="ui-border-radius--5">
                            <div class="ui-testemonials-grid">
                                <div class=" ui-first ui-float--left">
                                    <div class="ui-testemonials-grid__subheader ui-font-size--19">
                                        Maria
                                    </div>
                                    <div class="ui-testemonials-grid__output ui-font-size--13 ui-padding-left--23" >
                                        Unbelievable with one extra click you can save a lot of money
                                    </div>
                                </div>
                                <div class="ui-last ui-float--left">
                                    <img class="ui-border-radius--full" src="grabdid-front-felix/src/images/testemonials-icon-1.png" />
                                </div>
                                <p class="clear" />
                                <div class="ui-testemonials-grid__footer">
                                    <span class="ui-color--gray ui-font-size--13">member since 2012</span>
                                    <span class="ui-float--right ui-font--medium">Saved <span class="ui-state--highlight">1690 €</span></span>
                                    <p class="clear" />
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="ui-border-radius--5">
                            <div class="ui-testemonials-grid">
                                <div class=" ui-first ui-float--left">
                                    <div class="ui-testemonials-grid__subheader ui-font-size--19">
                                        Maria
                                    </div>
                                    <div class="ui-testemonials-grid__output ui-font-size--13 ui-padding-left--23" >
                                        Although I do not shop very often online I always come through...
                                    </div>
                                </div>
                                <div class="ui-last ui-float--left">
                                    <img class="ui-border-radius--full" src="grabdid-front-felix/src/images/testemonials-icon-1.png" />
                                </div>
                                <p class="clear" />
                                <div class="ui-testemonials-grid__footer">
                                    <span class="ui-color--gray ui-font-size--13">member since 2015</span>
                                    <span class="ui-float--right ui-font--medium">Saved <span class="ui-state--highlight">1690 €</span></span>
                                    <p class="clear" />
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="ui-border-radius--5">
                            <div class="ui-testemonials-grid">
                                <div class=" ui-first ui-float--left">
                                    <div class="ui-testemonials-grid__subheader ui-font-size--19">
                                        Maria
                                    </div>
                                    <div class="ui-testemonials-grid__output ui-font-size--13 ui-padding-left--23" >
                                        Unbelievable with one extra click you can save a lot of money
                                    </div>
                                </div>
                                <div class="ui-last ui-float--left">
                                    <img class="ui-border-radius--full" src="grabdid-front-felix/src/images/testemonials-icon-1.png" />
                                </div>
                                <p class="clear" />
                                <div class="ui-testemonials-grid__footer">
                                    <span class="ui-color--gray ui-font-size--13">member since 2012</span>
                                    <span class="ui-float--right ui-font--medium">Saved <span class="ui-state--highlight">1690 €</span></span>
                                    <p class="clear" />
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--end of panelgrid (testemonials)-->
        </div>
    </div>

@endsection {{-- #content --}}


@section('footer')

@endsection


@section('footer-scripts')

@endsection