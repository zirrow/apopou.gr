
@extends('layouts.master')

@section('head')

@endsection


@section('head-styles')

@endsection


@section('head-scripts')

@endsection


@section('header')

@endsection {{-- #header--}}

@section('content')

    <div class="ui-layout-center">
        <div class="ui-layout-center__content">
            <div class="ui-panel ui-panel--all-stores">
                <div class="ui-panel__header">

                </div>
                <div class="ui-margin-m1em-b ui-padding-left--1em ui-padding-bottom--10">

                    @include('layouts.breadcrumbs')

                </div>
                <div class="ui-panel__content ui-position--relative ui-overflow--hidden ui-height--85 u-box-sizing--cb">
                    <span class="ui-font--medium ui-font-size--24">{{ $content['name'] }}</span>
                    <p class="clear ui-height--8"></p>
                    <span class="ui-font-size--13">{!! $content['description'] !!}</span>
                    <label class="ui-read-more ui-background--white ui-font-size--13 ui-color--blue  ui-cursor--pointer">Read More +</label>
                    <label class="ui-read-less ui-background--white ui-font-size--13 ui-color--blue ui-text-align--right ui-display--none ui-cursor--pointer">Read Less -</label>
                </div>
            </div>
            <div class="ui-margin-top--12 ui-margin-bottom--11">
                <span class="ui-font-size--20 ui-font--light">Today’s Recommended Stores</span>
            </div>

            <!--        Here is the recommended shops block -->
            <div class="grid-6-col stores-recommended owl-carousel owl-theme">

                @foreach($recommended_coupons as $recommended_coupon)
                    <div class="grid-6-col__1-of-6">
                        <a href="{{ $recommended_coupon['retailer_link'] }}" class="stores-recommended__rec">
                            <img src="{{ $recommended_coupon['image'] }}" alt="store_logo" class="stores-recommended__logo" style="width: 100px; height: 27px;">
                            @if(!empty($recommended_coupon['retailer_old_cashback']))
                                <p class="stores-recommended__old-cashback">was {{ $recommended_coupon['retailer_old_cashback'] }}</p>
                            @endif

                            <p class="stores-recommended__cashback">{{ $recommended_coupon['retailer_cashback'] }} Cash Back</p>
                            <p class="stores-recommended__see-all">See all {{ $recommended_coupon['retailer_title'] }} Coupons</p>
                        </a>
                    </div>
                @endforeach

            </div>

            <div class="grid-3-5-col">
                <div class="grid-3-5-col__1-of-3-5 grid-3-5-col__1-of-3-5 stores-sidebar">
                    <div class="stores-sidebar__categories">
                        <p class="ui-font-size--24 ui-text-align--center">Refine stores</p>

                        @include('layouts.verticalmenu')

                    </div>

                    <div class="stores-sidebar__promo">
                        <img src="{{ SITE_URL }}grabdid-front-felix/build/images/riteaid_asbanner_5142018.jpg" alt="Promo Banner" class="stores-sidebar__promo-banner">
                        <img src="{{ SITE_URL }}grabdid-front-felix/build/images/icon-150x40-3.gif" alt="Promo Logo" class="stores-sidebar__promo-logo">
                        <div class="stores-sidebar__logo-wrapper"></div>
                        <p><a href="#">Shop with 1.0% Cash Back</a></p>
                        <p><a href="#">See All Coupons, Deals &amp; Cash Back</a></p>
                    </div>

                    <div class="stores-sidebar__promo-sticky promo-sticky">
                        <div class="promo-sticky__heading"><a href="{{ $router->generate('retailers_by_category') }}">Double Cash Back Services <span class="ui-float--right">See All</span></a></div>
                        <div class="grid-3-col promo-sticky__links">
                            @foreach($double_cash_module as $item)
                                <div class="grid-3-col__1-of-3 promo-sticky__link-wrapper">
                                    <a href="{{ $item['retailer_link']  }}" class="promo-sticky__link">
                                        <img src="{{ $item['image'] }}" alt="{{ $item['title'] }}" class="promo-sticky__logo" style="width: 100px; height: 27px;">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
                <!-- End of sidebar -->
                <div class="grid-3-5-col__2x1-of-3-5 allstores" id="allstores">
                    <!-- Header, category name -->
                    <div class="allstores__heading">
                        <p class="allstores__heading-paragraph ui-font-size--20">Stores</p>
                        <!-- Sorting selects here  -->
                        <!-- Sorting by shipping -->
                        <div class="allstores__sorting sorting-shipping">
                            <ul class="allstores__sorting-wrapper">
                                <li class="sorting-shipping__value sorting-shipping__value--ww">Shipping</li>
                                <span class="allstores__arrowdown"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                <ul class="allstores__inner-list" id="shipping-filter">
                                    <li class="allstores__inner-item-wrapper allstores__inner-item-wrapper--checked"><span class="allstores__inner-item">Worldwide</span></li>
                                    <li class="allstores__inner-item-wrapper"><span class="allstores__inner-item ui-color--greece">Greece</span></li>
                                    <li class="allstores__inner-item-wrapper"><span class="allstores__inner-item ui-color--cyprus">Cyprus</span></li>
                                </ul>
                            </ul>
                        </div>

                        <!-- Sorting by alphabet, popularity and cash-back -->
                        <div class="allstores__sorting sorting-general">
                            <ul class="allstores__sorting-wrapper">
                                <li>Sort by</li>
                                <span class="allstores__arrowdown"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                <ul class="allstores__inner-list">
                                    <li class="allstores__inner-item-wrapper @if($current_sort == 'added') allstores__inner-item-wrapper--checked @endif"><a class="allstores__inner-item" href="?column=added">The newest</a></li>
                                    <li class="allstores__inner-item-wrapper @if($current_sort == 'visits') allstores__inner-item-wrapper--checked @endif"><a class="allstores__inner-item" href="?column=visits">Popularity</a></li>
                                    <li class="allstores__inner-item-wrapper @if($current_sort == 'cashback') allstores__inner-item-wrapper--checked @endif"><a class="allstores__inner-item" href="?column=cashback">Cashback</a></li>
                                </ul>
                            </ul>
                        </div>

                    </div>
                    <!-- End of allstores header -->
                    <!-- Literal sorting -->
                    <div class="allstores__alpha alpha alpha--ww" id="alpha">
                        <span class="alpha__filter alpha__ALL ui-margin-right--25 alpha__filter--checked" data-l="all">ALL</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__A ui-margin-right--10" data-l="A">A</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__B ui-margin-right--10" data-l="B">B</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__C ui-margin-right--10" data-l="C">C</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__D ui-margin-right--10" data-l="D">D</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__E ui-margin-right--10" data-l="E">E</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__F ui-margin-right--10" data-l="F">F</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__G ui-margin-right--10" data-l="G">G</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__H ui-margin-right--10" data-l="H">H</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__I ui-margin-right--10" data-l="I">I</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__J ui-margin-right--10" data-l="J">J</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__K ui-margin-right--10" data-l="K">K</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__L ui-margin-right--10" data-l="L">L</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__M ui-margin-right--10" data-l="M">M</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__N ui-margin-right--10" data-l="N">N</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__O ui-margin-right--10" data-l="O">O</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__P ui-margin-right--10" data-l="P">P</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__Q ui-margin-right--10" data-l="Q">Q</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__R ui-margin-right--10" data-l="R">R</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__S ui-margin-right--10" data-l="S">S</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__T ui-margin-right--10" data-l="T">T</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__U ui-margin-right--10" data-l="U">U</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__V ui-margin-right--10" data-l="V">V</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__W ui-margin-right--10" data-l="W">W</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__X ui-margin-right--10" data-l="X">X</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__Y ui-margin-right--10" data-l="Y">Y</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__Z ui-margin-right--10" data-l="Z">Z</span>
                        <span class="alpha__break"><br></span>
                        <span class="alpha__filter alpha__0 ui-float--right" data-l="num">0-9</span>
                    </div>

                    <!-- List of stores starts here -->
                    <div id="stores" class="stores">
                        <ul id="stores-list" class="stores__items">

                            @foreach($retailers as $retailer)

                            <!-- One store item begin -->
                            <li class="stores__item stores__item--paginated liter_{{ $retailer['title'][0] }}" data-store-id="{{ $retailer['retailer_id'] }}">
                                @if(!empty($user_info))
                                    <span class="stores__fav-but @if($retailer['favorite'] != 0) stores__fav-but--checked @endif tt__fav">
                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                    </span>
                                @endif

                                <div class="grid-4-col stores__item-block">
                                    <a href="{{ $retailer['retailer_link'] }}" class="grid-3-col grid-4-col__3-of-4 stores__link">
                                        <p class="grid-3-col__1-of-3 stores__item-name">{{ $retailer['title'] }}</p>
                                        <p class="grid-3-col__1-of-3 stores__coupons">{{ $retailer['count_coupons'] }} coupon</p>
                                        <p class="grid-3-col__1-of-3 stores__cashback">{{ $retailer['cashback'] }} Cash Back</p>
                                    </a>

                                    <a href="
                                    @if(!empty($user_info))
                                        {{ $retailer['url'] }}
                                    @else
                                        {{ $retailer['website'] }}
                                    @endif
                                            " class="grid-4-col__1-of-4 ui-text-align--right stores__shop-btn js-shop-now-login" data-shop_id="shop-id-{{$retailer['retailer_id']}}" id="shop-id-{{$retailer['retailer_id']}}" target="_blank">Shop Now</a>
                                </div>
                            </li>
                            <!-- One store item end -->

                            @endforeach

                        </ul>
                    </div>
                    <!-- End of list of stores -->
                    <!-- Pagination block  -->

                    <div class="pagination ui-margin-top--15">
                        <button class="fa fa-angle-left pagination__button pagination__prev"></button>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button class="fa fa-angle-right pagination__button pagination__next"></button>
                    </div>
                </div>


                <!-- End of Pagination  -->
            </div>
        </div>
    </div>

@endsection {{-- #content --}}


@section('footer')

    @if(empty($user_info))

        <!-- Coupon's modal  -->
        <div class="modal coupon__modal">
            <div class="modal__overlay"></div>
            <div class="coupon__modal-window">

                <div class="coupon__modal-login ui-text-align--center">
                    <div class="modal__caption">
                        <p class="modal__cap-error"><span class="fa fa-info-circle modal__capicon"></span> You are not a member of the service</p>
                        <p>To get a cashback please login below</p>
                        <p>Not a member? <a href="{{ $router->generate('login') }}" class="modal__link">Register Now</a></p>
                    </div>
                    <div class="clr ui-margin-bottom--15">

                        <form id="login-form-page" class="grid-2-col__1-of-2 modal__login-form">
                            <input type="hidden" name="action" value="login">
                            <label for="email" class="ui-font--bold">EMAIL:</label><br>
                            <input class="modal__input-t" type="email" name="username" id="email"><br>
                            <label for="password" class="ui-font--bold">PASSWORD:</label><br>
                            <input class="modal__input-t" type="password" name="password" id="password"><br>

                            <div class="ui-margin--center" id="login-form-page-err"></div>

                            <a href="{{ $router->generate('forgot') }}" class="modal__link">I forgot my password</a> <br>
                            <button type="submit" class="modal__btn ui-button ui-button--green">Login</button> <br>
                        </form>

                        <div class="grid-2-col__1-of-2 modal-coupon-login__socials ui-padding-right--20">
                            <a href="{{ $router->generate('fblogin') }}" class="modal-coupon-login__social modal-coupon-login__social--fb">
                                <span class="modal-coupon-login__socicon fa fa-facebook"></span>
                                Login with Facebook
                            </a> <br>
                            <a href="#" class="modal-coupon-login__social modal-coupon-login__social--gp">
                                <span class="modal-coupon-login__socicon fa fa-google-plus"></span>
                                Login with Google Plus
                            </a>
                        </div>

                    </div>

                    <a href="" class="modal__link ui-font-size--22 ui-margin-bottom--15 ui-display--block modal__link-shop" target="_blank">No thanks, continue without cahsback <span class="fa fa-angle-right ui-font--bold"></span></a>

                    <div class="coupon__modal-footer">
                        <a href="#" class="ui-color--sm-grey ui-font-size--12">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection


@section('footer-scripts')

    <script>
        @if(empty($user_info))

            function openInNewTab(url, shop_id) {
                window.open(url, '_self');
                window.open('#'+shop_id,'_blank');
            }

            var hash = location.hash;

            if(hash){
                getModalContent($(hash));

                $(document).ready(function () {
                    $('.modal').fadeToggle(500);
                    $('.modal__overlay').show();
                    $('.modal').css({ "top": $(window).scrollTop() + "px"});
                });
            }

            $(".js-shop-now-login").on('click', function(e){
                e.preventDefault();

                var current_url = $(this).attr("href");
                var shop_id = $(this).data("shop_id");

                $('.modal__link-shop').attr('href', current_url);

                openInNewTab(current_url, shop_id);
            });

            function getModalContent(el) {

                var current_url = el.attr("href");

                $('.modal__link-shop').attr('href', current_url);

            }

            $("#login-form-page").on('submit', function(e){
                e.preventDefault();

                var form_data = $(this).serialize();

                $.ajax({
                    type: "POST",
                    url: "{{ $router->generate('login') }}",
                    data: form_data,
                    dataType: 'json',
                    success: function (msg) {
                        if(typeof msg.success !== 'undefined'){
                            location.reload();
                        } else {
                            $("#login-form-page-err").text(msg.errs).css('color','red');
                        }
                    }
                });
            });
        @endif
    </script>

    <script>
        $(document).ready(function(){

            $('.tag.example .ui.dropdown')
                .dropdown({
                    allowAdditions: true,
                    minSelections: 1
                })
            ;

            $(".stores-sidebar__subcategory--current").parentsUntil(".stores-sidebar__categories-list",
                [".stores-sidebar__subcategory"])
                .addClass("stores-sidebar__subcategory--expanded")
            ;

            $(".stores-sidebar__subcategory--current").parentsUntil(".stores-sidebar__categories-list",
                [".stores-sidebar__subcategory"])
                .addClass("stores-sidebar__subcategory--checked")
            ;

            $(".stores-sidebar__subcategory--checked").children(".stores-sidebar__cat-open").toggleClass("stores-sidebar__cat-open--checked");

            $(".stores-sidebar__cat-open").on("click", function(){
                $(this).parent().children(".stores-sidebar__subcategory").slideToggle(200);
                //   $(this).parent().toggleClass("stores-sidebar__subcategory--checked");
                $(this).toggleClass("stores-sidebar__cat-open--checked");
            })

            document.addEventListener("click", function(e){
                if (e.target.parentNode.parentNode.id === 'shipping-filter'){
                    e.preventDefault();
                    var val = document.querySelector(".sorting-shipping__value");
                    var alpha = document.querySelector(".alpha");

                    if (e.target.textContent === 'Greece') {
                        val.classList.add('sorting-shipping__value--greece');
                        val.classList.remove('sorting-shipping__value--cyprus');
                        val.classList.remove('sorting-shipping__value--ww');
                        alpha.classList.add('alpha--greece');
                        alpha.classList.remove('alpha--cyprus');
                        alpha.classList.remove('alpha--ww');
                    } else if (e.target.textContent === 'Cyprus') {
                        val.classList.remove('sorting-shipping__value--greece');
                        val.classList.add('sorting-shipping__value--cyprus');
                        val.classList.remove('sorting-shipping__value--ww');
                        alpha.classList.remove('alpha--greece');
                        alpha.classList.add('alpha--cyprus');
                        alpha.classList.remove('alpha--ww');
                    } else {
                        val.classList.remove('sorting-shipping__value--greece');
                        val.classList.remove('sorting-shipping__value--cyprus');
                        val.classList.add('sorting-shipping__value--ww');
                        alpha.classList.remove('alpha--greece');
                        alpha.classList.remove('alpha--cyprus');
                        alpha.classList.add('alpha--ww');
                    }
                }

            });

        });
    </script>

    <script>
        var elsOnPage = '{{ $retailers_per_page }}';
    </script>

    <script>

        $(".stores__fav-but").on("click", function () {

            if($(this).hasClass('stores__fav-but--checked')){

                $.ajax({
                    type: "GET",
                    url: "{{ $router->generate('my-favorites') }}",
                    data: {'id':$(this).parent().data('store-id'), 'act':'del'},
                    dataType: 'json',
                    success: function (msg) {
                        console.log(msg);
                    }
                });

                $(this).removeClass('stores__fav-but--checked');
            } else {

                $.ajax({
                    type: "GET",
                    url: "{{ $router->generate('my-favorites') }}",
                    data: {'id':$(this).parent().data('store-id'), 'act':'add'},
                    dataType: 'json',
                    success: function (msg) {
                        console.log(msg);
                    }
                });

                $(this).addClass('stores__fav-but--checked');
            }

        });

    </script>

    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/sticky_all_stores.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/all-items-sort.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/modal.js"></script>

@endsection