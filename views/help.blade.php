
@extends('layouts.master')

@section('head')

@endsection


@section('head-styles')

@endsection


@section('head-scripts')

@endsection


@section('header')

@endsection {{-- #header--}}

@section('content')

    <div class="ui-layout-center__content">
        <div class="clr">
            <div class="sidebar sidebar--help clr">
                <div class="help__sidebar">
                    <h2>Contact us</h2>
                    <span id="ooopen" class="modal__link"><span class="fa fa-envelope-o"></span>  Open form!</span>
                </div>

                <div class="help__sidebar">
                    <h2>Additional block</h2>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestiae nobis deserunt nihil modi reprehenderit veritatis officia fugiat corporis cum? Cupiditate, aspernatur sint. Quaerat natus reprehenderit nesciunt magnam quidem sint aliquam.</p>

                </div>
            </div>

            <div class="s-content help">
                <h1 class="ui-font-size--35">Help</h1>
                <p>Here you can find answers to a popular questions</p>
                <h2 class="header-block ui-margin-top--15">Frequently Asking questions by topics</h2>
                <div class="help__topics">

                    {!! $content['description'] !!}

                </div>
            </div>
        </div>
    </div>

@endsection {{-- #content --}}


@section('footer')

    <!-- Contact form popup -->
    <div class="overlay"></div>
    <div class="modal modal-help-email">
        <p class="ui-font-size--40">Contact</p>
        <p>Fill all fields</p>

        <form class="ui-margin-top--15 request-form">

            <input type="hidden" name="action" value="send_request">
            <!-- Enter the name -->

            <label for="name" class="modal__label">Name:</label> <br>
            <input class="modal__input-t modal__input-t--help" type="text" id="helpName" name="name" required>
            <br>

            <!-- Enter e-mail -->

            <label for="email" class="modal__label">Email:</label> <br>
            <input class="modal__input-t modal__input-t--help" type="email" id="helpEmail" name="email" required>
            <br>

            <!-- Tema -->
            {{--<label for="theme" class="modal__label">Theme</label><br>--}}
            {{----}}
            {{--<select name="theme" id="theme" class="modal__input-select">--}}
                {{--<option value="theme1">Choose a theme...</option>--}}
                {{--<option value="theme2">Theme 1</option>--}}
                {{--<option value="theme3">Theme 2</option>--}}
                {{--<option value="theme4">Theme 3</option>--}}
            {{--</select>--}}
            {{--<br>--}}

            <label for="question" class="modal__label">Your Question</label> <br>
            <textarea name="question" id="quiestion" cols="50" rows="10" class="modal__textarea" required></textarea>
            <br>
            <button type="submit" class="ui-button ui-button--green modal__btn">Send</button>
        </form>
        <button class="modal__close-btn fa fa-close"></button>
    </div>

@endsection


@section('footer-scripts')

    <script>
        $(document).ready(function(){
            $(".help__topic").on("click", function(){
                $(this).next(".help__subtopics").slideToggle(350);
                $(this).children(".help__exp").toggleClass("fa-plus");
                $(this).children(".help__exp").toggleClass("fa-minus");
                $(this).children(".help__exp").toggleClass("help__exp--open");
            });

            $(".help__subtopic-question").on("click", function(){
                $(this).next(".help__subtopic-definition").slideToggle(200);
                $(this).toggleClass("help__subtopic-question--expanded");
                $(this).children(".help__sub-exp").toggleClass("help__sub-exp--open");
            });

            $("#ooopen").on("click", function(){
                $(".overlay").toggle();
                $(".modal-help-email").toggle();
            });

            $(".overlay").on("click", function(){
                $(".overlay").hide();
                $(".modal").hide();
            })

            $(".modal__close-btn").on("click", function(){
                $(".overlay").toggle();
                $(".modal").toggle();
            })
        });
    </script>

    <script>

        $(".request-form").on('submit', function(e){
            e.preventDefault();

            var form_data = $(this).serialize();

            $.ajax({
                type: "POST",
                url: "{{ $router->generate('help') }}",
                data: form_data,
                dataType: 'json',
                success: function (msg) {
                    if(typeof msg.success !== 'undefined'){
                        $('.modal-help-email').html('Message sent successfully!');
                    }
                }
            });
        });

    </script>

@endsection