
@extends('layouts.master')

@section('head')

@endsection


@section('head-styles')

@endsection


@section('head-scripts')

@endsection


@section('header')

@endsection {{-- #header--}}

@section('content')

    <!-- put main content here -->
    <div class="ui-layout-center__content">
        <div class="grid-3-5-col">
            <!-- Memeber left sidebar column -->
            <div class="grid-3-5-col__1-of-3-5 member">
                <div class="member__welcome">
                    <p class="member__name ui-margin--n">Welcome,</p>
                    <!-- Username down here -->
                    <p class="member__name ui-font-size--30 ui-margin--n u-word-wrap--brw .ui-margin-5-0">{{ $user_info['user_name'] }}</p>
                    <!-- Registration date down here -->
                    <p class="ui-font-size--14 ui-color--sm-grey ui-margin--n">Member since {{ $user_info['created'] }}</p>
                </div>
                <div class="member__lt-cb">
                    <p class="ui-font-size--14 ui-margin--n">Lifetime Cash Back</p>
                    <p class="ui-margin--n">{{ $lifetime_cash_back }}</p>
                    <a href="{{ $router->generate('invite') }}" class="ui-button ui-button--green ui-button--ref-btn ui-margin-top--10">Refer &amp; Earn $25+</a>
                </div>

                <!-- Sidebar navigation start -->
                @include('layouts.accountmenu')
                <!-- Sidebar nav end -->

            </div>
            <!-- End of the member block -->

            <!-- Dashboard is main content block -->
            <div class="grid-3-5-col__2x1-of-3-5 dashboard">

            </div>
        </div>
    </div>

@endsection {{-- #content --}}


@section('footer')

@endsection


@section('footer-scripts')
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/expand-text.js"></script>

    <script>
        $(document).ready(function() {

            $(".dashboard__notification").click(function(event){
                //Settings if design changes

                $targ = ($(this).next());
                $targetWidth = "100%";
                $tagetHeight = "100%";
                $targetFontSize = "14px";
                $targetPadding = "20px";
                if (!$targ.hasClass("dashboard__notification--expanded")){
                    ($(this).next()).slideToggle();
                    $targ.toggleClass("dashboard__notification--expanded").animate({
                        width: "0",
                        height: "0",
                        fontSize: "0",
                    },300).fadeOut(50);
                } else {
                    ($(this).next()).slideToggle(10);
                    $targ.toggleClass("dashboard__notification--expanded").fadeIn(100).animate({
                        width: $targetWidth,
                        height: $tagetHeight,
                        fontSize: $targetFontSize,
                    },300);
                }
            });

            $(".clicktocopy").click(function(e){
                $targ = ($(this).next());
                $targ.css("width", "120%");
                $targ.html("<span class='fa fa-check' aria-hidden='true'></span> Copied to clipboard");
            });

            $(".clicktocopy").mouseleave(function(e){
                $targ = ($(this).next());
                $targ.css("width", "100%");
                $targ.text('Click to copy');
            });
        });

        new ClipboardJS('.clicktocopy');
    </script>

    <script>

        $(".dashboard__fav-add").on("click", function () {


            if($(this).hasClass('dashboard__fav-add--added')){

                $.ajax({
                    type: "GET",
                    url: "{{ $router->generate('my-favorites') }}",
                    data: {'id':$(this).find("input[name=id]").val(), 'act':'del'},
                    dataType: 'json',
                    success: function (msg) {
                        console.log(msg);
                    }
                });

                $(this).removeClass('dashboard__fav-add--added');
            } else {

                $.ajax({
                    type: "GET",
                    url: "{{ $router->generate('my-favorites') }}",
                    data: {'id':$(this).find("input[name=id]").val(), 'act':'add'},
                    dataType: 'json',
                    success: function (msg) {
                        console.log(msg);
                    }
                });

                $(this).addClass('dashboard__fav-add--added');
            }

        });

    </script>

    <script>

        $("#addFavorites").on("keyup",function () {

            var query = $(this).val();
            var html = '';

            $.ajax({
                type: "POST",
                url: "/autocomplete.php",
                data: {'query':query},
                cache: false,
                success: function(data)
                {
                    if (query == '' || query.length <= 1) {
                        $('#little-search-block').addClass('ui-display--none');
                    } else {

                        $('#little-search-block').removeClass('ui-display--none');

                        $.each(JSON.parse(data), function(index, value){
                            html +='<li class="dashboard__fav-found-item ui-cursor--pointer clr ui-border-radius--4 ui-padding-0-10" data-search_retailer_id='+ value.retailer_id +'>';
                            html +='<span class="ui-font-size--14 ui-float--left">'+ value.title +'</span>';
                            html +='<span class="ui-color--red ui-font--bold ui-font-size--16 ui-float--right">'+ value.cashback +'</span>';
                            html +='</li>';
                        });

                        $('.dashboard__fav-found-items').html(html);
                    }
                }
            });
        });

        $(".dashboard__fav-found-items").on('click', '.dashboard__fav-found-item', function () {

            var retailer_id = $(this).data('search_retailer_id');
            var html = '';

            $.ajax({
                type: "GET",
                url: "{{ $router->generate('my-favorites') }}",
                data: {'id':retailer_id, 'act':'add'},
                dataType: 'json',
                success: function (msg) {
                    console.log(msg);
                }
            });

            html +='<li class="dashboard__fav-found-item ui-cursor--pointer clr ui-border-radius--4 ui-padding-0-10">';
            html +='<span class="ui-font-size--14 ui-float--left">Favorites added successfully</span>';
            html +='</li>';

            $('.dashboard__fav-found-items').html(html);

            setTimeout(function () {
                $('#little-search-block').addClass('ui-display--none');
            }, 1000);
        });

    </script>



@endsection