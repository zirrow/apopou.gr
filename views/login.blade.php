
@extends('layouts.master')

@section('head')

@endsection


@section('head-styles')

@endsection


@section('head-scripts')

@endsection


@section('header')

@endsection {{-- #header--}}

@section('content')

    <!-- put main content here -->
    <div class="ui-layout-center__content">
        <h1 class="ui-font-size--24 ui-text-align--center ui-margin-30-0 login__d">Join Biggest Cash Back Shopping Site </h1>

        <div class="login">
            <!-- *Mobile only* switching tabs for signup/login forms -->
            <div class="login__tabs login__m">
                <div class="login__tab" id="signup">Sign Up</div>
                <div class="login__tab login__tab--active" id="login">Log In</div>
            </div>
        </div>

        <div class="login__forms">
            <!-- Signup form -->
            <div class="login__form-wrapper login__form-wrapper--signup ui-float--left">
                <h2 class="login__form-heading login__form-heading--signup login__d">Sign Up for Free Now!</h2>
                <form id="singup-form-page" class="login__form">
                    <div class="login__form-fields">
                        <p class="ui-font-size--15 ui-margin-bottom--15 ui-padding-top--15 login__d">Have a Facebook Account?</p>
                        <a href="{{ $router->generate('fblogin') }}" class="login__fb">
                            <p class="login__m ui-text-align--center ui-color--white login__fb-m ui-font-size--13 ui-padding--10">
                                <span class="fa fa-facebook ui-float--left ui-font-size--18"></span>
                                Log in with Facebook
                            </p>
                        </a>
                        <p class="ui-font-size--11 ui-margin-top--10 ui-margin-bottom--10 ui-color--light-grey login__d">We'll never post anything without your permission.</p>
                        <div class="login__or-box">
                            <div class="login__or-text">OR</div>
                        </div>
                        <input type="hidden" name="action" value="signup">
                        <input type="email"
                               pattern="[^@\s]+@[^@\s]+\.[^@\s]+"
                               required
                               name="email"
                               id="signUpMail"
                               placeholder="{{ $header['CBE1_LOGIN_EMAIL'] }}"
                               class="login__input-t">
                        <br>
                        <input type="password"
                               name="password"
                               id="signupPassword"
                               placeholder="{{ $header['CBE1_LOGIN_PASSWORD'] }}"
                               required
                               class="login__input-t ui-margin-bottom--15">
                        <br>
                        * <select name="country" aria-required="true" id="country" required class="ui-margin-bottom--15 selection search">

                            <option value="">State</option>
                            @foreach($countries as $country)
                                <option value="{{ $country['country_id'] }}">{{ $country['name'] }}</option>
                            @endforeach
                        </select>
                        {{--<div class="g-recaptcha ui-width--300 ui-margin--center" data-sitekey="6Lf7rEUUAAAAADFrTtVT_fZvsX7HEJfxJN8b-UMW"></div>--}}
                        <div class="login__check-agree">
                            <input type="checkbox" name="sub_news" id="news" class="login__news-checkbox">
                            <label for="news" class="login__news-label ui-font-size--13">Please email me newsletters, coupons, and special offers from Ebates. </label>
                        </div>
                        <div class="login__check-agree">
                            <input type="checkbox" name="tos" id="news" class="login__news-checkbox" value="1" required>
                            <label for="news" class="login__news-label ui-font-size--13">I agree with <a href="#" class="ui-color--green">Terms</a> &amp; <a href="#" class="ui-color--green">Conditions and Privacy Policy.</a></label>
                        </div>
                        <p class="ui-font-size--11 ui-color--light-grey"><span class="ui-color--red">*</span> Required Fields</p>

                        <div class="ui-margin--center" id="singup-form-page-err"></div>

                        <button type="submit" class="login__submit-btn"><span class="traftext traftext--px6 traftext--white">Sign Up Today</span> </button>
                        <p class="ui-font-size--11 ui-color--light-grey">You may withdraw this consent at any time. </p>
                    </div>
                </form>
                <p class="login__agreement">As a member, you agree to our <a href="#" class="ui-color--green">Terms</a> &amp; <a href="#" class="ui-color--green">Conditions and Privacy Policy.</a></p>
            </div>

            <!-- Login form -->
            <div class="login__form-wrapper login__form-wrapper--login login__form-wrapper--active ui-float--right">
                <h2 class="login__form-heading login__form-heading--login login__d">Member login</h2>
                <form id="login-form-page" class="login__form">
                    <div class="login__form-fields">
                        <p class="ui-font-size--15 ui-margin-bottom--15 ui-padding-top--15 login__d">Have a Facebook Account?</p>
                        <a href="{{ $router->generate('fblogin') }}" class="login__fb">
                            <p class="login__m ui-text-align--center ui-color--white login__fb-m ui-font-size--13 ui-padding--10">
                                <span class="fa fa-facebook ui-float--left ui-font-size--18"></span>
                                Log in with Facebook
                            </p>
                        </a>
                        <p class="ui-font-size--11 ui-margin-top--10 ui-margin-bottom--10 ui-color--light-grey login__d">We'll never post anything without your permission.</p>
                        <div class="login__or-box">
                            <div class="login__or-text">OR</div>
                        </div>
                        <input type="hidden" name="action" value="login">
                        <input type="email"
                               pattern="[^@\s]+@[^@\s]+\.[^@\s]+"
                               required
                               name="username"
                               id="signUpMail"
                               placeholder="{{ $header['CBE1_LOGIN_EMAIL'] }}"
                               class="login__input-t"/>
                        <br>
                        <input name="password"
                               placeholder="{{ $header['CBE1_LOGIN_PASSWORD'] }}"
                               minlength="6"
                               required
                               type="password"
                               class="login__input-t ui-margin-bottom--15"
                               id="signupPassword"/>
                        {{--<div class="g-recaptcha ui-width--300 ui-margin--center" data-sitekey="6Lf7rEUUAAAAADFrTtVT_fZvsX7HEJfxJN8b-UMW"></div>--}}
                        <p class="ui-font-size--11 ui-color--light-grey ui-text-align--left ui-display--block clr ui-margin-top--15">
                            <span class="ui-color--red">*</span>
                            Required Fields
                            <a href="{{ $router->generate('forgot') }}" class="ui-font-size--13 ui-float--right ui-color--green">{{ $header['CBE1_LOGIN_FORGOT'] }}</a>
                        </p>
                        <div class="ui-margin--center" id="login-form-page-err"></div>
                        <button type="submit" class="login__submit-btn"><span class="traftext traftext--px6 traftext--white">{{ $header['CBE1_LOGIN_BUTTON'] }}</span> </button>
                    </div>
                </form>
                <p class="login__agreement">As a member, you agree to our <a href="#" class="ui-color--green">Terms</a> &amp; <a href="#" class="ui-color--green">Conditions and Privacy Policy.</a></p>
            </div>
        </div>
    </div>

@endsection {{-- #content --}}


@section('footer')

@endsection


@section('footer-scripts')

    <script>
        $(document).ready(function() {
            setTimeout(function(){
                $('#signup').trigger('click');
            }, 10);
        });
    </script>

    <script>
        $("#login-form-page").on('submit', function(e){
            e.preventDefault();

            var form_data = $(this).serialize();

            $.ajax({
                type: "POST",
                url: "{{ $router->generate('login') }}",
                data: form_data,
                dataType: 'json',
                success: function (msg) {
                    if(typeof msg.success !== 'undefined'){
                        $(location).attr('href', msg.success);
                    } else {
                        $("#login-form-page-err").text(msg.errs).css('color','red');
                    }
                }
            });
        });
    </script>

    <script>
        $("#singup-form-page").on('submit', function(e){
            e.preventDefault();

            var form_data = $(this).serialize();

            $.ajax({
                type: "POST",
                url: "{{ $router->generate('login') }}",
                data: form_data,
                dataType: 'json',
                success: function (msg) {
                    if(typeof msg.success !== 'undefined'){
                        $(location).attr('href', msg.success);
                    } else {
                        $("#singup-form-page-err").text(msg.errs).css('color','red');
                    }
                }
            });
        });
    </script>


    <script>
        $(document).ready(function(){
            $(".login__tab").on("click", function(){
                $(".login__tab--active").removeClass("login__tab--active");
                $(this).addClass("login__tab--active");
                var formSelector = ".login__form-wrapper--" + ($(this).attr("id").toString());
                $(".login__form-wrapper--active").removeClass("login__form-wrapper--active");
                $(formSelector).addClass("login__form-wrapper--active");
            });

            $("#country")
                .dropdown({
                    allowAdditions: true,
                    minSelections: 1
                })
        });
    </script>
@endsection