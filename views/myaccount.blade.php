
@extends('layouts.master')

@section('head')

@endsection


@section('head-styles')

@endsection


@section('head-scripts')

@endsection


@section('header')

@endsection {{-- #header--}}

@section('content')

    <!-- put main content here -->
    <div class="ui-layout-center__content">
        <div class="grid-3-5-col">
            <!-- Memeber left sidebar column -->
            <div class="grid-3-5-col__1-of-3-5 member">
                <div class="member__welcome">
                    <p class="member__name ui-margin--n">Welcome,</p>
                    <!-- Username down here -->
                    <p class="member__name ui-font-size--30 ui-margin--n u-word-wrap--brw .ui-margin-5-0">{{ $user_info['user_name'] }}</p>
                    <!-- Registration date down here -->
                    <p class="ui-font-size--14 ui-color--sm-grey ui-margin--n">Member since {{ $user_info['created'] }}</p>
                </div>
                <div class="member__lt-cb">
                    <p class="ui-font-size--14 ui-margin--n">Lifetime Cash Back</p>
                    <p class="ui-margin--n">{{ $lifetime_cash_back }}</p>
                    <a href="{{ $router->generate('invite') }}" class="ui-button ui-button--green ui-button--ref-btn ui-margin-top--10">Refer &amp; Earn $25+</a>
                </div>

                <!-- Sidebar navigation start -->
                @include('layouts.accountmenu')
                <!-- Sidebar nav end -->
            </div>
            <!-- End of the member block -->

            <!-- Dashboard is main content block -->
            <div class="grid-3-5-col__2x1-of-3-5 dashboard">
                <!-- Cashback balance block -->
                <div class="grid-3-col ui-border ui-margin-bottom--10">
                    <div class="block-heading ui-border-b">My Cash Back Account</div>
                    <div class="grid-3-col__1-of-3 dashboard__balance-wrapper">
                        <div class="dashboard__balance ui-border-r ui-padding-25-0">
                            <p class="ui-font-size--16 ui-font--bold ui-padding-bottom--15">Cash Back Balance</p>
                            <a href="#" class="ui-font-size--40 ui-color--green ui-font--bold ui-padding-bottom--25">{{ $user_info['user_balance'] }}</a>
                        </div>
                    </div>
                    <!-- This block is for "where is my cashback" but can be used for anything, takes 2/3 of entire block -->
                    <div class="grid-3-col__2-of-3 dashboard__wh-cb-wrapper">
                        <div class="dashboard__wh-cb ui-padding-25-0 ui-padding-left--60">
                            <p class="ui-font-size--18 ui-font--bold ui-padding-bottom--10">Where's my Cash Back?</p>
                            <p class="ui-font-size--14">Cash Back will be added when a store notifies us of your order. For more details, visit the store page and look under Cash Back Facts. </p>
                            <a href="{{ $router->generate('retailers_by_category') }}" class="ui-font-size--14 ui-color--green ui-margin-top--10">See All Stores</a>
                        </div>
                    </div>
                </div>
                <!-- Expandable notification block for bonuses -->
                <div class="ui-margin-bottom--10">
                    <h3 class="heading-tertiary ui-padding-15-20 dashboard__notification dashboard__notification--bonus"> <span class="fa fa-bell"></span> You have a <span class="ui-font--bold">$10 Walmart Gift Card</span> waiting for you.</h3>
                    <div class="dashboard__notification-desc ui-padding--20 ui-font-size--14 ui-border">
                        <p>Get your $10 Walmart Gift Card when you make qualifying purchases totaling $25 or more by 8/20/18.</p>
                    </div>
                </div>
                <!-- Expandable notification block for double cashback notifications -->
                <div class="ui-margin-bottom--10 dashboard__d">
                    <h3 class="heading-tertiary ui-padding-15-20 dashboard__notification dashboard__notification--double-cb">
                        <span class="fa fa-bell"></span>
                        <span>2</span> Favorite Stores is Double Cash Back Today
                    </h3>
                    <div class="dashboard__notification-desc ui-padding-top--8 ui-font-size--14 ui-border">

                        @foreach($favorite_stores_double as $favorite_store)
                            <div class="clr ui-border-b">
                                <a href="{!! $favorite_store['retailer_link'] !!}" class="ui-display--block clr ui-padding-10-0 ui-float--left">
                                    <img src="{{ $favorite_store['image'] }}" alt="Store Logo" class="ui-border-r ui-padding-10-20 ui-float--left" style="width: 100px; height: 27px;">
                                    <span class="ui-float--left ui-margin-left--40">
                                    <span class="ui-font-size--18 ui-color--red ui-font--bold">Up to {{ $favorite_store['cashback'] }} Cash Back </span>
                                    <span class="ui-font-size--14 ui-color--sm-grey">was {{ $favorite_store['old_cashback'] }}</span> <br>
                                    <span class="ui-font-size--12 ui-color--green">See all {{ $favorite_store['title'] }} Coupons</span>
                                </span>
                                </a>
                                <a href="{{ $favorite_store['url'] }}" class="ui-float--right ui-padding-top--20 ui-font-size--16 ui-color--red ui-padding-right--30 traftext">Shop Now</a>
                            </div>
                        @endforeach

                        <div class="ui-text-align--center ui-padding-15-0">
                            <a href="{{ $router->generate('my-favorites') }}" class="ui-color--green traftext traftext--green">See All Favorite Stores</a>
                        </div>
                    </div>
                </div>

                <!-- Favorites block -->
                <div class="dashboard__fav dashboard__d">
                    <div class="grid-2-col block-heading ui-border">
                        <div class="grid-2-col__1-of-2">
                            <span>Favorite Stores</span>
                        </div>
                        <div class="grid-2-col__1-of-2">
                            <!-- Search for favs block -->
                            <span class="input-t-b ui-position--relative ui-display--block">
                            <input type="text" name="addFavorites" id="addFavorites" placeholder="Add all your favorite stores" class="input-text ui-width--full">
                                <!-- Found items dropdown, remove ui-display-none to look at example -->

                                <div class="dashboard__fav-found ui-line-height--24 clr ui-display--none" id="little-search-block">
                                    <ul class="dashboard__fav-found-items ui-width--full ui-border ui-border-t-none ui-border--green ui-padding-right--10 ui-padding-top--8">

                                    </ul>
                                </div>

                            {{--<button type="submit" class="ui-button ui-button--green ui-button--inputbtn">Add</button>--}}
                        </span>
                        </div>
                    </div>

                    <!-- Block for already added fav items -->
                    <div class="grid-2-col ui-border ui-border-t-none">
                        <ul class="grid-2-col">

                            <!-- One found item -->
                            @foreach($favorite_stores as $favorite_store)
                            <li class="grid-2-col__1-of-2 dashboard__fav-item ui-border-b">
                                <div class="dashboard__fav-item-wrapper">
                                    <span class="fa fa-heart dashboard__fav-add dashboard__fav-add--added">
                                        <input type="hidden" name="id" value="{{ $favorite_store['retailer_id'] }}">
                                    </span>
                                    <a href="{!! $favorite_store['retailer_link'] !!}" class="dashboard__fav-link">
                                        <img src="{{ $favorite_store['image'] }}" alt="{{ $favorite_store['title'] }}" class="dashboard__fav-logo" style="width: 100px; height: 27px;">
                                        <span class="ui-float--right">
                                            <span class="ui-margin-right--10 ui-font-size--13 ui-color--gray">was {{ $favorite_store['old_cashback'] }}</span>
                                            <span class="ui-color--red ui-font--bold">Up to {{ $favorite_store['cashback'] }}</span>
                                        </span>
                                    </a>
                                </div>
                            </li>
                            @endforeach

                        </ul>
                    </div>

                    <div class="dashboard__fav-hint ui-padding--20 ui-font-size--14 ui-border ui-border-t-none">
                        <p>Add your favorites stores for easy shopping and personalized deals. When you see a <span class="fa fa-heart ui-color--gray"></span>, click it and the store will be added to your favorites here. Get started by searching above for the stores you already love.</p>
                    </div>
                    <div class="dashboard__fav-recommended ui-margin-bottom--10">
                        <div class="ui-border ui-border-t-none"><h4 class="ui-padding-15-20 ui-color--gray ui-background--gray">Recommended Favorite Stores</h4></div>
                        <ul class="grid-2-col dashboard__fav-items ui-border ui-border-t-none">

                            @foreach($recommended as $recommend)
                                <li class="grid-2-col__1-of-2 dashboard__fav-item ui-border-b">
                                    <div class="dashboard__fav-item-wrapper">
                                        <span class="fa fa-heart dashboard__fav-add @if($recommend['favorite'] != 0) dashboard__fav-add--added @endif">
                                            <input type="hidden" name="id" value="{{ $recommend['retailer_id'] }}">
                                        </span>
                                        <a href="{!! $recommend['retailer_link'] !!}" class="dashboard__fav-link">
                                            <img src="{{ $recommend['image'] }}" alt="{{ $recommend['title'] }}" class="dashboard__fav-logo" style="width: 100px; height: 27px;">
                                            <span class="ui-float--right">
                                            <span class="ui-margin-right--10 ui-font-size--13 ui-color--gray">was {{ $recommend['old_cashback'] }}</span>
                                            <span class="ui-color--red ui-font--bold">Up to {{ $recommend['cashback'] }}</span>
                                        </span>
                                        </a>
                                    </div>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>

                <div class="dashboard__trips ui-margin-bottom--10 dashboard__d">
                    <div class="block-heading ui-border"><h3 class="ui-color--green">Shopping trips</h3></div>
                    <div class="dashboard__trips-table ui-border-l ui-border-r">
                        <table>
                            <tbody>
                            <tr class="ui-background--gray">
                                <th class="dashboard__th-date">Date / Time</th>
                                <th class="dashboard__th-store">Store</th>
                                <th class="dashboard__th-cb">Cash Back</th>
                                <th class="dashboard__th-st">Shopping Trip</th>
                            </tr>

                            @foreach($transactions as $transaction)
                                <tr>
                                    <td>{{ $transaction['process_date'] }}</td>
                                    <td>{{ $transaction['retailer'] }}</td>
                                    <td>{{ $transaction['amount'] }}</td>
                                    <td>{{ $transaction['reference_id'] }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="dashboard-recommends ui-border dashboard__d">
                    <div class="block-heading ui-border-b ui-color--green"><h3>Recommended Deals</h3></div>

                    @foreach ($recommended_deals as $recommended_deal)
                    <div class="dashboard__coupon clr ui-border-b">
                        <div class="dashboard__clogo-wrapper">
                            <a href="{!! $recommended_deal['retailer_link'] !!}/{{ $recommended_deal['coupon_id'] }}">
                                <img src="{{ $recommended_deal['image'] }}" alt="eBay logo" style="width: 150px; height: 40px;">
                            </a>
                        </div>
                        <ul class="dashboard__ccontent">
                            <li class="dashboard__cdiscount">
                                <span class="ui-font-size--20 dashboard__ctitle">{{ $recommended_deal['title'] }}</span><br>
                                <span class="dashboard__ctitle-discount ui-color--red ui-font--bold">+ Up to {{ $recommended_deal['retailer_cashback'] }} Cash Back</span>
                            </li>

                            @if (!empty($recommended_deal['description']))
                                <li class="dashboard__cdescription ui-font-size--14">
                                    <p class="dashboard__cdescription-text expandable-text expandable-text--collapsed">{{ $recommended_deal['description'] }}</p>
                                    <span class="more dashboard__c-read-more">Read More +</span>
                                </li>
                            @endif

                            <li class="dashboard__code-expires">
                                @if (!empty($recommended_deal['code']))
                                    <span class="ui-margin-right--25 ui-font-size--14 ui-position--relative">
                                        Code:
                                        <span class="ui-font--bold ui-color--green ui-cursor--pointer clicktocopy ui-font-size--16" data-clipboard-action="copy" data-clipboard-target="#coupon{{ $recommended_deal['coupon_id'] }}" id="coupon{{ $recommended_deal['coupon_id'] }}">
                                            {{ $recommended_deal['code'] }}
                                        </span>
                                        <div class="clicktocopy__hint clicktocopy__hint--green">Click to copy</div>
                                    </span>
                                @endif

                                <span class="ui-color--gray ui-font-size--14">
                                    <span class="fa fa-clock-o ui-margin-right--10"></span>Expires {{ $recommended_deal['end_date'] }}
                                </span>
                            </li>
                        </ul>
                        <a href="{{ $recommended_deal['link'] }}" class="ui-button ui-button--red dashboard__shop-btn ui-border-radius--4">Shop Now</a>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>

@endsection {{-- #content --}}


@section('footer')

@endsection


@section('footer-scripts')
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/expand-text.js"></script>
    <script src="{{ SITE_URL }}grabdid-front-felix/build/js/clipboard.min.js"></script>

    <script>
        $(document).ready(function() {

            $(".dashboard__notification").click(function(event){
                //Settings if design changes

                $targ = ($(this).next());
                $targetWidth = "100%";
                $tagetHeight = "100%";
                $targetFontSize = "14px";
                $targetPadding = "20px";
                if (!$targ.hasClass("dashboard__notification--expanded")){
                    ($(this).next()).slideToggle();
                    $targ.toggleClass("dashboard__notification--expanded").animate({
                        width: "0",
                        height: "0",
                        fontSize: "0",
                    },300).fadeOut(50);
                } else {
                    ($(this).next()).slideToggle(10);
                    $targ.toggleClass("dashboard__notification--expanded").fadeIn(100).animate({
                        width: $targetWidth,
                        height: $tagetHeight,
                        fontSize: $targetFontSize,
                    },300);
                }
            });

            $(".clicktocopy").click(function(e){
                $targ = ($(this).next());
                $targ.css("width", "120%");
                $targ.html("<span class='fa fa-check' aria-hidden='true'></span> Copied to clipboard");
            });

            $(".clicktocopy").mouseleave(function(e){
                $targ = ($(this).next());
                $targ.css("width", "100%");
                $targ.text('Click to copy');
            });
        });

        new ClipboardJS('.clicktocopy');
    </script>

    <script>

        $(".dashboard__fav-add").on("click", function () {


            if($(this).hasClass('dashboard__fav-add--added')){

                $.ajax({
                    type: "GET",
                    url: "{{ $router->generate('my-favorites') }}",
                    data: {'id':$(this).find("input[name=id]").val(), 'act':'del'},
                    dataType: 'json',
                    success: function (msg) {
                        console.log(msg);
                    }
                });

                $(this).removeClass('dashboard__fav-add--added');
            } else {

                $.ajax({
                    type: "GET",
                    url: "{{ $router->generate('my-favorites') }}",
                    data: {'id':$(this).find("input[name=id]").val(), 'act':'add'},
                    dataType: 'json',
                    success: function (msg) {
                        console.log(msg);
                    }
                });

                $(this).addClass('dashboard__fav-add--added');
            }

        });

    </script>

    <script>

        $("#addFavorites").on("keyup",function () {

            var query = $(this).val();
            var html = '';

            $.ajax({
                type: "POST",
                url: "/autocomplete.php",
                data: {'query':query},
                cache: false,
                success: function(data)
                {
                    if (query == '' || query.length <= 1) {
                        $('#little-search-block').addClass('ui-display--none');
                    } else {

                        $('#little-search-block').removeClass('ui-display--none');

                        $.each(JSON.parse(data), function(index, value){
                            html +='<li class="dashboard__fav-found-item ui-cursor--pointer clr ui-border-radius--4 ui-padding-0-10" data-search_retailer_id='+ value.retailer_id +'>';
                            html +='<span class="ui-font-size--14 ui-float--left">'+ value.title +'</span>';
                            html +='<span class="ui-color--red ui-font--bold ui-font-size--16 ui-float--right">'+ value.cashback +'</span>';
                            html +='</li>';
                        });

                        $('.dashboard__fav-found-items').html(html);
                    }
                }
            });
        });

        $(".dashboard__fav-found-items").on('click', '.dashboard__fav-found-item', function () {

            var retailer_id = $(this).data('search_retailer_id');
            var html = '';

            $.ajax({
                type: "GET",
                url: "/my-favorites",
                data: {'id':retailer_id, 'act':'add'},
                dataType: 'json',
                success: function (msg) {
                    console.log(msg);
                }
            });

            html +='<li class="dashboard__fav-found-item ui-cursor--pointer clr ui-border-radius--4 ui-padding-0-10">';
            html +='<span class="ui-font-size--14 ui-float--left">Favorites added successfully</span>';
            html +='</li>';

            $('.dashboard__fav-found-items').html(html);

            setTimeout(function () {
                $('#little-search-block').addClass('ui-display--none');
            }, 1000);
        });

    </script>



@endsection