
@extends('layouts.master')

@section('head')

@endsection


@section('head-styles')

@endsection


@section('head-scripts')

@endsection


@section('header')

@endsection

@section('content')
    <!-- put main content here -->
    <div class="ui-layout-center">
        <div class="ui-layout-center__content balance clr">


            <div class="grid-3-5-col__1-of-3-5 member">
                <div class="member__welcome">
                    <p class="member__name ui-margin--n">Welcome,</p>
                    <!-- Username down here -->
                    <p class="member__name ui-font-size--30 ui-margin--n u-word-wrap--brw .ui-margin-5-0">{{ $user_info['user_name'] }}</p>
                    <!-- Registration date down here -->
                    <p class="ui-font-size--14 ui-color--sm-grey ui-margin--n">Member since {{ $user_info['created'] }}</p>
                </div>
                <div class="member__lt-cb">
                    <p class="ui-font-size--14 ui-margin--n">Lifetime Cash Back</p>
                    <p class="ui-margin--n">{{ $lifetime_cashback }}</p>
                    <a href="{{ $router->generate('invite') }}" class="ui-button ui-button--green ui-button--ref-btn ui-margin-top--10">Refer &amp; Earn $25+</a>
                </div>

                <!-- Sidebar navigation start -->
                @include('layouts.accountmenu')
                <!-- Sidebar nav end -->
            </div>
            <!-- End of the member block -->

            <div class="grid-3-5-col__2x1-of-3-5 grid-2-col balance__wrapper">
                <h1 class="ui-margin-top--10">{{ $header['CBE1_BALANCE_TITLE'] }}</h1>
                <div class="grid-2-col__1-of-2 balance__table-wrapper">
                    <table class="balance__table">
                        <tr class="balance__table-row balance__table-row--available">
                            <td class="balance__cname">{{ $header['CBE1_WITHDRAW_BALANCE'] }}</td>
                            <td class="balance__cvalue">{{ $user_balance }}</td>
                        </tr>
                        <tr class="balance__table-row balance__table-row--pending">
                            <td class="balance__cname">{{ $header['CBE1_BALANCE_PCASHBACK'] }}</td>
                            <td class="balance__cvalue">{{ $pending_balance }}</td>
                        </tr>
                        <tr class="balance__table-row balance__table-row--declined">
                            <td class="balance__cname">{{ $header['CBE1_BALANCE_DCASHBACK'] }}</td>
                            <td class="balance__cvalue">{{ $declined_balance }}</td>
                        </tr>
                        <tr class="balance__table-row balance__table-row--requested">
                            <td class="balance__cname">{{ $header['CBE1_BALANCE_CREQUESTED'] }}</td>
                            <td class="balance__cvalue">{{ $cash_out_reqest }}</td>
                        </tr>
                        <tr class="balance__table-row balance__table-row--proceeded">
                            <td class="balance__cname">{{ $header['CBE1_BALANCE_CPROCESSED'] }}</td>
                            <td class="balance__cvalue">{{ $cash_out_processed }}</td>
                        </tr>
                        <tr class="balance__table-row balance__table-row--lifetime">
                            <td class="balance__cname">Lifetime Cashback{{ $header['CBE1_BALANCE_LCASHBACK'] }}</td>
                            <td class="balance__cvalue">{{ $lifetime_cashback }}</td>
                        </tr>
                    </table>
                </div>

                <div class="grid-2-col__1-of-2 balance__withdraw">
                    <p>Available balance:</p>
                    <span class="ui-color--green ui-font-size--30">{{ $user_balance }}</span><br>

                    <a href="{{ $router->generate('withdraw') }}" class="balance__withdraw-btn ui-button ui-button--green">
                        <span class="fa fa-money"></span> Withdraw
                    </a>
                </div>

                <div class="history grid-2-col__2-of-2">
                    <h1 class="ui-margin-top--10">History</h1>
                    <table class="history__table tablesorter"
                           id="myTable">
                        <thead>
                        <tr class="footable-header">
                            <th class="history__theading">Date</th>
                            <th class="history__theading" data-breakpoints="xs sm">Reference id</th>
                            <th class="history__theading" data-breakpoints="xs sm">Payment type</th>
                            <th class="history__theading" data-breakpoints="xs sm">Store</th>
                            <th class="history__theading">Amount</th>
                            <th class="history__theading">Status</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($user_transactions as $user_transaction)
                            <tr>
                                <td class="history__tcell">{{ $user_transaction['created'] }}</td>
                                <td class="history__tcell">{{ $user_transaction['reference_id'] }}</td>
                                <td class="history__tcell">{{ $user_transaction['payment_type'] }}</td>
                                <td class="history__tcell">{{ $user_transaction['retailer'] }}</td>
                                <td class="history__tcell">{{ $user_transaction['amount'] }}</td>
                                <td class="history__tcell">{{ $user_transaction['status'] }}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection {{-- #content --}}


@section('footer')

@endsection


@section('footer-scripts')


@endsection