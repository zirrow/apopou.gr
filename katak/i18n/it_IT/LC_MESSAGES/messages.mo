��    %      D  5   l      @     A     P     W     e     u     �     �  "   �  	   �  
   �  
   �     �     �          "     0     A  
   J     U     c  
   p     {  u   �               &     /  
   =     H     V     e  6   |     �     �     �  6   �  �  !          3     B  #   \     �     �  ^   �  ]   �  
   J	     U	  !   a	  "   �	  8   �	     �	     �	  L   
     Z
     h
  2   �
  !   �
     �
  B   �
  $  8     ]  L   t     �     �     �            L   6  s   �  %   �       0   9  T   j     "                           	          
                                                            $                                                      #                !   %    CLOSED TICKETS Cancel Captcha Text: Delete Messages Email Address Email Address: Enter text shown on the image Enter the text shown on the image. Full Name Full Name: Help Topic Help Topic: Help topic required Internal Error Message Body: Message required Message: NEW TICKET Name required New Message: New Ticket Please enter a valid email. Please fill in the form below to open a new ticket and provide as much detail as possible, so we can best assist you. Subject Subject required Subject: Submit Ticket Telephone: Ticket Status Ticket Status: Title/subject required To update a previously submitted ticket, please go to  Unknown action %s Unknown command Valid email required Valid phone # required: only numbers and ()+-/ allowed Project-Id-Version: Katak-support ver 1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-03-27 14:42+0100
PO-Revision-Date: 2017-11-12 22:18+0200
Language-Team: Katak-support Team <info@katak-support.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.4
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
Last-Translator: 
Language: el_EL
X-Poedit-SearchPath-0: /var/www/html/katak
 ΚΛΕΙΣΤΑ TICKETS Ακύρωση Γράμματα Captcha: Διαγραφή Μηνυμάτων Email Email: Εισάγεται τα γράμματα που αναγράφονται στην εικόνα Εισάγετε τα γράμματα που φένονται στην φωτογραφία. Όνομα Όνομα: Κατηγορία Θέματος Κατηγορία Θέματος: Επιλέξτε ‘Κατηγορία Θέματος’ Εσωτερικό Σφάλμα Μήνυμα: Το ‘μήνυμα’ απαιτείται να μην είναι κενό Μήνυμα: ΚΑΙΝΟΥΡΓΙΟ TICKET ‘Όνομα’ υποχρεωτικό πεδίο Καινούργιο Μήνυμα Καινούργιο ticket Παρακαλούμε εισάγετε ένα έγκυρο email. Συμπληρώστε την πιο κάτω φόρμα για να ανοίξετε ένα καινούργιο ticket. Δώστε όσες περισσότερες πληροφορίες μπορείτε ώστε να μπορέσουμε να σας βοηθήσουμε καλύτερα. Τίτλος/ Θέμα Ο ‘Τίτλος/ Θέμα’ είναι υποχρεωτικό πεδίο Τίτλος/ Θέμα: Υποβολή Ticket Τηλέφωνο: Κατάσταση Ticket Κατάσταση Ticket: Ο ‘Τίτλος/ Θέμα’ είναι υποχρεωτικό πεδίο Για να αλλάξετε ένα παλιό ticket, παρακαλώ πηγαίνετε στην επιλογή  Μη έγκυρη ενέργεια %s Άγνωστη εντολή Έγκυρο  ‘email’  απαιτείται Μόνο αριθμοί επιτρέπονται π.χ 6977934212 ή (+357) 99699013 