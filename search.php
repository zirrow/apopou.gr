<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	session_start();
	require_once("inc/config.inc.php");
    require_once("inc/blade_config.inc.php");
    require_once("inc/var_config.inc.php");

    ////////////////// filter  //////////////////////
    if (isset($_GET['column']) && $_GET['column'] != "") {

        switch ($_GET['column']) {
            case "added": $column = "added"; break;
            case "visits": $column = "visits"; break;
            case "cashback": $column = "cashback"; break;
            default: $column = "added"; break;
        }
    } else {
        $column = "added";
    }

    $stext = '';
    if (isset($_GET['action']) && $_GET['action'] == "search") {

        $stext = mysqli_real_escape_string($conn, getGetParameter('searchtext'));
        $stext = substr(trim($stext), 0, 100);

    }

    $cat_id = 0;
    $content = GetContent('search');

    ///////////////  Page config  ///////////////
    $PAGE_TITLE	= CBE1_SEARCH_TITLE." ".$stext;
    $PAGE_DESCRIPTION	= $content['meta_description'];
    $PAGE_KEYWORDS		= $content['meta_keywords'];


    $breadcrumbs[] = [
        'name' => 'Home',
        'link' => '/'
    ];

    $breadcrumbs[] = [
        'name' => CBE1_SEARCH_TITLE.' '.$stext,
        'link' => ''
    ];

    $data = [
        'head'=>$head,
        'header'=>$header,
        'footer'=>$footer,
	    'router'=>$router,
        'PAGE_TITLE'=>$PAGE_TITLE,
        'PAGE_DESCRIPTION'=>$PAGE_DESCRIPTION,
        'PAGE_KEYWORDS'=>$PAGE_KEYWORDS,
        'countries'=>GetCountries(),
        'languages'=>GetLanguagesArray(),
        'current_lang'=>isset($_COOKIE['site_lang']) ? $_COOKIE['site_lang'] : SITE_LANGUAGE,
        'multilanguage'=>MULTILINGUAL,
        'search_array'=>GetRetailersForSearch(),
        'content' => $content,
        'user_info'=>GetUserInfo(),
        'breadcrumbs' => $breadcrumbs,

        'recommended_coupons'=>GetLatestCoupons(6),
        'coupons_menu'=>ShowCategoriesVerticaMenu(),
        'double_cash_module'=>GetLatestRetailers(9),
        'retailers'=>GetLatestRetailers(0, $column, $cat_id, $stext),
        'retailers_per_page'=>RESULTS_PER_PAGE,
        'current_sort'=>$column,
        'search_key'=>$stext
    ];

    echo $blade->make('retailers', $data);

