<?php
require '../../vendor/autoload.php';


/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	session_start();
	require_once("inc/config.inc.php");
	require_once("inc/blade_config.inc.php");
	require_once("inc/var_config.inc.php");

	// save referral id //////////////////////////////////////////////
	if (isset($_GET['ref']) && is_numeric($_GET['ref']))
	{
		$ref_id = (int)$_GET['ref'];
		setReferral($ref_id);

		// count ref link clicks
		if (!isLoggedIn())
		{
			smart_mysql_query("UPDATE cashbackengine_users SET ref_clicks=ref_clicks+1 WHERE user_id='$ref_id' LIMIT 1");
		}

		header("Location: index.php");
		exit();
	}

	$content = GetContent('home');

	///////////////  Page config  ///////////////
	$PAGE_TITLE			= SITE_HOME_TITLE;
	$PAGE_DESCRIPTION	= $content['meta_description'];
	$PAGE_KEYWORDS		= $content['meta_keywords'];

    
  $data = [
	  'head'=>$head,
	  'header'=>$header,
	  'footer'=>$footer,
	  'router'=>$router,
	  'PAGE_TITLE'=>$PAGE_TITLE,
	  'PAGE_DESCRIPTION'=>$PAGE_DESCRIPTION,
	  'PAGE_KEYWORDS'=>$PAGE_KEYWORDS,
	  'content' => $content,
	  //ADDED by Denis
	  'countries'=>GetCountries(),
	  'languages'=>GetLanguagesArray(),
	  'current_lang'=>isset($_COOKIE['site_lang']) ? $_COOKIE['site_lang'] : SITE_LANGUAGE,
	  'multilanguage'=>MULTILINGUAL,
	  'search_array'=>GetRetailersForSearch(),
	  'user_info'=>GetUserInfo(),
	  'featured_stores_1'=>getProductsForModulFeatured(),
	  'featured_stores_2'=>getProductsForModulLatest(),
	  'latest_coupons'=>getCouponsForModulLatest(),
	  'retailer_img_width'=>GetSetting('image_width'),
	  'retailer_img_height'=>GetSetting('image_height'),
  ];

  
	echo $blade->make('index', $data);


	function getProductsForModulFeatured(){
		$featured_stores = GetLatestRetailers();

		shuffle($featured_stores);
		$featured_stores = array_chunk($featured_stores, 8);

		$result[0] = $featured_stores[0];
		$result[1] = $featured_stores[1];

		return $result;
	}

	function getProductsForModulLatest(){
		$latest_stores = GetLatestRetailers();
		$latest_stores = array_chunk($latest_stores, 8);

		$result[0] = $latest_stores[0];
		$result[1] = $latest_stores[1];

		return $result;
	}

	function getCouponsForModulLatest(){
		$latest_coupons = GetLatestCoupons();

		$delimiter = 4;
		if(count($latest_coupons) < 8){
			$delimiter = ceil(count($latest_coupons) / 2);
		}

		$latest_coupons = array_chunk($latest_coupons, $delimiter);

		$result[0] = $latest_coupons[0];
		$result[1] = $latest_coupons[1];

		return $result;
	}

