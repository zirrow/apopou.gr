<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	if (!defined("CBengine_PAGE")) exit();

	$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) or die ('Could not connect to MySQL server');
	// N.R. addition to support greek characters in the frontend instead of ????????
	if( function_exists('mysql_set_charset') ){
	    mysqli_set_charset($conn, 'utf8');
	} else {
	    mysqli_query($conn, "SET NAMES 'utf8'");
	}
	// end

?>