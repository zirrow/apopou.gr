<?php// var_dump($this->router);exit(); ?>


<?php $__env->startSection('head'); ?>

    <?php echo $__env->make('inc.head', ['head' => $head], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $__env->stopSection(); ?>


<?php $__env->startSection('head-styles'); ?>

 

<?php $__env->stopSection(); ?>

<?php $__env->startSection('head-scripts'); ?>
<meta property="og:image" content="http://www.shopsite.com/blog/wp-content/uploads/2010/12/Coupon.jpg" />
<?php $__env->stopSection(); ?>



<?php $__env->startSection('header'); ?>

    <?php echo $__env->make('inc.header', ['header'=>$header], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $__env->stopSection(); ?> 

<?php $__env->startSection('content'); ?>
<!-- put main content here -->
<div class="ui-layout-center">
    <div class="ui-layout-center__content">

         <table class="ui-panelgrid ui-table-layout--fixed ui-panelgrid--layout ui-width--full">
              <tbody>
                <tr>
                    <td class="ui-panelgrid__cell--ad">
                        <div class="ui-panel ui-panel--favorite">
                            <div class="ui-panel__header">
                            <label title="Add to My Favorites" class="ui-favorite ui-float--left ui-display--block ui-height--25 ui-width--25 ui-border-radius--full"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path    d="M414.9 24C361.8 24 312 65.7 288 89.3 264 65.7 214.2 24 161.1 24 70.3 24 16 76.9 16 165.5c0 72.6 66.8 133.3 69.2 135.4l187 180.8c8.8 8.5 22.8 8.5 31.6 0l186.7-180.2c2.7-2.7 69.5-63.5 69.5-136C560 76.9 505.7 24 414.9 24z"/></svg></label>
                            <span class="ui-float--left ui-font-size--14 ui-font--light ui-padding-left--13">Add to My Favorites</span>
                            <p class="clear" ></p>
                            </div>
                            <div class="ui-panel__content ui-text-align--center">
                                <img class="ui-width--full" src="images/old_navy_280x60.gif" />
                                <p class="clear"></p>
                                <span class="ui-commandlink--gray ui-font-size--13">was 2.0%</span>
                                <p class="clear"></p>
                                <span class="ui-color--red ui-font-size--22 ui-font--bold">6.0% Cash Back</span>
                                <p class="clear"></p>
                                <button class="ui-border--none ui-button--red ui-button ui-font-size--17 ui-color--white ui-width--230 ui-margin-top--8 js-shop-now-login">Shop Now</button>
                            </div>

                        </div>
                        <div class="ui-panel ui-panel--favorite">
                            <div class="ui-panel__header ui-text-align--center">
                                <div class="ui massive star rating" data-max-rating="5"></div>
                                <p class="clear"></p>
                                <span class="ui-commandlink--gray ui-font-size--17 ui-font--light rating-feedback">Rate us</span>
                            </div>
                        </div>
                        <div class="ui-panel ui-panel--favorite">
                            <div class="ui-panel__content ui-text-align--center">
                                <span class=" ui-font-size--85 ui-color--gray">99</span>
                                <p class="clear"></p>
                                <span class="ui-font-size--14 ui-commandlink--gray ui-font--light">COUPONS AVAILABLE</span>
                                <p class="clear ui-height--5"></p>
                                <span class="ui-font-size--17 ui-color--gray">Share these coupons</span>
                                <p class="clear ui-height--5"></p>
                                <div class="ui-separator--top-gray ui-width--190 ui-margin--center ui-padding-top--8">
                                    <a href="#" class="ui-text-align--center ui-width--33 ui-height--33 ui-commandlink--social-media-dark ui-border-radius--full">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 264 512"><path d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5 70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229"/></svg>
                                    </a>
                                    <a href="#" class="ui-text-align--center ui-width--33 ui-height--33 ui-commandlink--social-media-dark ui-border-radius--full">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M386.061 228.496c1.834 9.692 3.143 19.384 3.143 31.956C389.204 370.205 315.599 448 204.8 448c-106.084 0-192-85.915-192-192s85.916-192 192-192c51.864 0 95.083 18.859 128.611 50.292l-52.126 50.03c-14.145-13.621-39.028-29.599-76.485-29.599-65.484 0-118.92 54.221-118.92 121.277 0 67.056 53.436 121.277 118.92 121.277 75.961 0 104.513-54.745 108.965-82.773H204.8v-66.009h181.261zm185.406 6.437V179.2h-56.001v55.733h-55.733v56.001h55.733v55.733h56.001v-55.733H627.2v-56.001h-55.733z"/></svg>
                                    </a>
                                    <a href="#" class="ui-text-align--center ui-width--33 ui-height--33 ui-commandlink--social-media-dark ui-border-radius--full">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"/></svg>
                                    </a>
                                    <a href="#" class="ui-text-align--center ui-width--33 ui-height--33 ui-commandlink--social-media-dark ui-border-radius--full">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"/></svg>
                                    </a>
                                    <a href="#" class="ui-text-align--center ui-width--33 ui-height--33 ui-commandlink--social-media-dark ui-border-radius--full">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M204 6.5C101.4 6.5 0 74.9 0 185.6 0 256 39.6 296 63.6 296c9.9 0 15.6-27.6 15.6-35.4 0-9.3-23.7-29.1-23.7-67.8 0-80.4 61.2-137.4 140.4-137.4 68.1 0 118.5 38.7 118.5 109.8 0 53.1-21.3 152.7-90.3 152.7-24.9 0-46.2-18-46.2-43.8 0-37.8 26.4-74.4 26.4-113.4 0-66.2-93.9-54.2-93.9 25.8 0 16.8 2.1 35.4 9.6 50.7-13.8 59.4-42 147.9-42 209.1 0 18.9 2.7 37.5 4.5 56.4 3.4 3.8 1.7 3.4 6.9 1.5 50.4-69 48.6-82.5 71.4-172.8 12.3 23.4 44.1 36 69.3 36 106.2 0 153.9-103.5 153.9-196.8C384 71.3 298.2 6.5 204 6.5z"/></svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="ui-panel ui-panel--favorite">
                            <div class="ui-panel__content ui-text-align--center">
                                <a href="#reviews" class="ui-font-size--18
                                                          ui-color--gray 
                                                          ui-font-size--22 
                                                          ui-display--block 
                                                          ui-margin-top--10">
                                    <span class="ui-color--green fa fa-comments-o ui-font-size--30"></span> 
                                    5 reviews
                                </a>
                            </div>
                        </div>
                        <div class="ui-panel ui-panel--favorite">
                            <div class="ui-panel__content ui-text-align--center">
                                <a href="#reviews" class="ui-font-size--18
                                                          ui-color--gray 
                                                          ui-font-size--22 
                                                          ui-display--block 
                                                          ui-margin-top--10">
                                    <span class="ui-color--green fa fa-exclamation-triangle ui-font-size--30"></span> 
                                    Report a store
                                </a>
                            </div>
                        </div>
                        <div class="ui-panel ui-panel--favorite">
                            <div class="ui-panel__content ui-text-align--center">
                                <a href="#reviews" class="ui-font-size--18
                                                          ui-color--gray 
                                                          ui-font-size--22 
                                                          ui-display--block 
                                                          ui-margin-top--10">
                                    <span class="ui-color--green fa fa-paper-plane ui-font-size--30"></span> 
                                    Submit a coupon
                                </a>
                            </div>
                        </div>
                        <div class="ui-panel ui-panel--favorite">
                            <div class="ui-panel__header">
                                <span class="ui-font-size--22 ui-font--medium ui-color--green">Cash Back Terms</span>
                            </div>
                            <div class="ui-panel__content">
                                <span class="ui-color--gray ui-font-size--13">Cash Back will be automatically added to your Izocard account today.</span>
                                <p class="clear ui-height--20"></p>
                                <span class="ui-color--gray ui-font-size--13"><b>Exclusions: </b>Cash Back is not available on Gap Outlet/Factory, Banana Republic Factory items, gift cards including e-gift cards.</span>

                            </div>

                        </div>
                        <div class="ui-panel ui-panel--favorite">
                            <div class="ui-panel__header">
                                <span class="ui-font-size--20 ui-font--medium">About Old Navy Coupons, Deals and Cash Back</span>
                            </div>
                            <div class="ui-panel__content ui-position--relative ui-overflow--hidden ui-height--100">
                                    <span class="ui-font-size--13">Find the latest and hottest fashion trends, as well as your favorite simple and stylish basics, all at incredible prices at Old Navy. This store has something for everyone in your family, from Dad to baby to your picky and fashion-conscious teenager. Save on stylish women's clothes like cardigan sweaters, sexy sun dresses and skinny ankle pants. Get everything from business casual looks for the office to fun casual or party wear for the weekends. The men’s section is full of practical fashion, from quality men's jeans to classic button-downs and hooded sweatshirts. There’s also a wide variety of adorable and durable kids’ clothes, and fun styles for your hip teenager. Save on everyone’s clothes with Old Navy coupons, promo codes and fantastic year-round sales. Outfit your entire crew for even less when you take advantage of the Cash Back perks found at Ebates, as well as free shipping offers on apparel for the entire family. Babies and kids go through clothes so fast with their constant growth spurts and hours of playing outside.</span>
                                    <p class="clear ui-height--5"></p>
                                    <span class="ui-font-size--13">When you shop through Ebates, you’ll receive a percentage of your purchase price back, so you end up saving even more money. Don’t feel bad anymore about your little ones tearing through clothes when you shop Old Navy for discount kids’ and baby clothes, and save even more with Old Navy promo and coupon codes. Keep your teens and juniors feeling cool and trendy when you shop here for their back-to-school and summer break clothes. You can enjoy everyday apparel sales that help you stock everyone’s closets without breaking your budget or your bank account. Get this season’s hottest styles and fashion looks and still save money on these and your other favorite styles with clothing coupons and OldNavy.com coupon codes found on Ebates. Old Navy has an outstanding clearance and sale clothing section where you can discover the best clearance clothing deals and make a serious fashion statement while saving. You’ll find affordable clothing in the clearance section, including discount dresses, leggings, sleepwear, sweaters and fashion-forward jeans for everybody. Slip into something comfortable with stylish shoes for men, women and kids at prices you’ll love. With affordable accessories always on sale at Old Navy, you can fill your wardrobe with the hottest hats, scarves, sunglasses and jewelry — and always be ready to add a unique, personalized touch to every outfit. From earrings to necklaces and trendy bracelets, accessories can really take an ordinary outfit from simple to stunning, and you’ll always get the best prices on fun fashion </span>
                                    <p class="clear ui-height--5"></p>

                                    <span class="ui-font-size--13">At Old Navy, there are endless ways to spruce up your style and do it affordably. On top of already low prices, you’ll score big savings when you use your Old Navy Credit Card for in-store or online purchases. Combine these card perks with Ebates Cash Back and unbeatable clothing coupon and promo codes to make every dollar count. Find the latest and greatest deals on discount school uniforms, sleepwear, activewear and swimwear while saving even more on everything with Cash Back at Ebates. Special clearance pricing paired with money-saving rewards makes Old Navy the perfect destination for every family looking for modern clothes for everyone at a price that everyone can afford and that everyone will love.</span>
                                    <a href="#description" class="ui-read-more-link ui-background--white ui-font-size--13 ui-color--blue  ui-cursor--pointer">Read More +</a>
                                    <label class="ui-read-less ui-background--white ui-font-size--13 ui-color--blue ui-text-align--right ui-display--none ui-cursor--pointer">Read Less -</label>
                            </div>
                        </div>
                        <div class="ui-panel ui-panel--favorite">
                            <div class="ui-panel__header">
                                <span class="ui-font-size--22 ui-font--medium ui-color--green">Free Shipping</span>
                            </div>
                            <div class="ui-panel__content">
                                <span class="ui-font-size--13">Old Navy offers free shipping on orders of $50 or more and free returns on all orders.</span>
                                <p class="clear ui-height--5"></p>
                                <a href="#" class="ui-font-size--14 ui-color--red ui-font--bold">Shop Old Navy with 6.0% Cash Back</a>
                            </div>
                        </div>

                        <div class="ui-panel ui-panel--favorite ui-margin--center" >
                                <div class="ui-panel__header ui-margin-top--10 ui-margin-bottom--7">
                                    <span class="ui-font-size--20 ui-color--gray ui-font--medium">Featured Stores</span>
                                </div>
                                <div class="ui-panel__content ui-separator--top-gray">
                                    <a class="ui-commandlink ui-line-height--30 ui-margin-top--8 ui-display--block ui-font-size--17 ui-color--blue" href="#">Target</a>
                                    <a class="ui-commandlink ui-line-height--30 ui-display--block ui-font-size--17 ui-color--blue" href="#">America Eagle</a>
                                    <a class="ui-commandlink ui-line-height--30 ui-display--block ui-font-size--17 ui-color--blue" href="#">Nordstrom</a>
                                    <a class="ui-commandlink ui-line-height--30 ui-display--block ui-font-size--17 ui-color--blue" href="#">Macy's</a>
                                    <a class="ui-commandlink ui-line-height--30 ui-display--block ui-font-size--17 ui-color--blue" href="#">Kohl's</a>
                                </div>
                            </div>
                    </td>
                    <td class="ui-panelgrid__cell--content">
                        <label class="ui-font-size--35 ui-color--dark-gray ui-margin-top--12 ui-display--block ui-text--wrap">
                                <div class="breadcrumbs">
                                    <ul class="breadcrumbs__crumbs">
                                        <li class="breadcrumbs__crumb"><a href="#" class="breadcrumbs__crumb-link">Bread</a></li>
                                        <li class="breadcrumbs__crumb"><a href="#" class="breadcrumbs__crumb-link">Crumb</a></li>
                                        <li class="breadcrumbs__crumb"><a href="#" class="breadcrumbs__crumb-link">Crumb 2</a></li>
                                        <li class="breadcrumbs__crumb"><a class="breadcrumbs__crumb-link breadcrumbs__crumb-link--current">You are here</a></li>
                                    </ul>
                                </div>
                            ebay.com Coupon Code Discounts & Coupons</label>

                        <div class="ui divided items">
                            <!-- One coupon start -->

                                <div class="coupon coupon--stores" data-coupon-object="https://ebay.com">
                                    <div class="coupon__logo-wrapper">
                                        <a href="#"> <img src="images/icon-150x40-ebay.gif" alt="eBay logo"></a>
                                        <p class="ui-text-align--center ui-color--red">45% <span>OFF</span></p>
                                    </div>
                                    <ul class="coupon__content">
                                        <li>
                                            <span class="coupon__type coupon__type--promocode">Promo Code</span> <span class="coupon__times-used">858 used today</span>
                                        </li>
                                        <li class="coupon__cdiscount">
                                            <span class="ui-font-size--20 coupon__ctitle">15% off select purchases of $1000000 or more.</span><br>
                                            <span class="coupon__ctitle-discount ui-color--red ui-font--bold">+ Up to 45.0% Cash Back</span>
                                        </li>
                                        <li class="coupon__description ui-font-size--14">
                                            <p class="coupon__description-text expandable-text expandable-text--collapsed">Coupon can be used up to two times per eBay account.  Limit one use per transaction (or cart) while supplies last. Valid only for purchases from ebay.com, cafr.ebay.ca and ebay.ca. Coupon is subject to U.S laws, void where prohibited, not redeemable for cash, has no face value, and cannot be combined with any other Coupon, or when paying with PayPal Credit Easy Payments or Gift Cards. eBay may cancel, amend, or revoke the Coupon at any time. The discount will be applied to eligible item(s) only and will be capped at a value of $50.</p>
                                            <span class="more dashboard__c-read-more">Read More +</span>
                                        </li>
                                        <li class="coupon__code-expires">
                                        <span class="ui-color--gray ui-font-size--14"><span class="fa fa-clock-o ui-margin-right--10"></span>Expires 5/25/2018</span>
                                        </li>
                                    </ul>
                                    <a href="file:///C:/Work/grabdid-work/build/stores.html" class="ui-button ui-button--green coupon__shop-btn ui-border-radius--4">Get Coupon <span class="fa fa-angle-right"></span></a>
                                    <!-- Coupon's modal  -->
                                    <div class="modal coupon__modal" id="1">
                                            <div class="modal__overlay"></div>
                                            <div class="coupon__modal-window">
                                                <div class="coupon__modal-header">
                                                    <img src="images/icon-150x40-ebay.gif" alt="eBay logo" class="coupon__modal-logo">
                                                    <span>Lorem ipsum dolor afom ifomc pidorasy</span>
                                                </div>
                                                <div class="coupon__modal-body ui-text-align--center">
                                                    <div class="coupon__modal-code">
                                                        <span class="ui-font--bold coupon__modal-code-text" id="coupon">ABABABABA</span>
                                                        <button class="coupon__modal-copy-btn" data-clipboard-target="#coupon" data-clipboard-action="copy">Copy Code</button>
                                                    </div>
                                                </div>
                                                <div class="coupon__modal-login ui-text-align--center">
                                                    <div class="modal__caption">
                                                            <p class="modal__cap-error"><span class="fa fa-info-circle modal__capicon"></span> You are not a member of the service</p>
                                                            <p>To get a cashback please login below</p>
                                                            <p>Not a member? <a href="#" class="modal__link">Register Now</a></p>
                                                        </div>
                                                    <div class="clr ui-margin-bottom--15">
                                                        <form action="" method="POST" class="grid-2-col__1-of-2 modal__login-form">
                                                            <label for="email" class="ui-font--bold">EMAIL:</label><br>
                                                            <input class="modal__input-t" type="email" name="email" id="email"><br>
                                                            <label for="password" class="ui-font--bold">PASSWORD:</label><br>
                                                            <input class="modal__input-t" type="password" name="password" id="password"><br>
                                                            <a href="#" class="modal__link">I forgot my password</a> <br>
                                                            <button type="submit" class="modal__btn ui-button ui-button--green">Login</button> <br>
                                                            <input type="checkbox" name="remember" id="remember"> <label for="remember">Remember Me</label>
                                                        </form>
                                                        <div class="grid-2-col__1-of-2 modal-coupon-login__socials ui-padding-right--20">
                                                            <a href="#" class="modal-coupon-login__social modal-coupon-login__social--fb">
                                                                <span class="modal-coupon-login__socicon fa fa-facebook"></span> 
                                                                Login with Facebook
                                                            </a> <br>
                                                            <a href="#" class="modal-coupon-login__social modal-coupon-login__social--gp">
                                                                <span class="modal-coupon-login__socicon fa fa-google-plus"></span>
                                                                    Login with Google Plus
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <a href="https://ebay.com" class="modal__link ui-font-size--22 ui-margin-bottom--15 ui-display--block" target="_blank">No thanks, continue without cahsback <span class="fa fa-angle-right ui-font--bold"></span></a>
                                                    <div class="coupon__modal-footer">
                                                        <a href="#" class="ui-color--sm-grey ui-font-size--12">Terms &amp; Conditions</a>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>

                            <!-- One coupon end -->


                            <!-- One coupon start -->

                                <div class="coupon coupon--stores" data-coupon-object="https://ebay.com">
                                    <div class="coupon__logo-wrapper">
                                        <a href="#"> <img src="images/icon-150x40-ebay.gif" alt="eBay logo"></a>
                                        <!-- <a href="#" class="ui-display--block ui-text-align--center ui-font-size--13">See All Coupons</a> -->
                                    </div>
                                    <ul class="coupon__content">
                                        <li>
                                            <span class="coupon__type coupon__type--sale">Sale</span> <span class="coupon__times-used">858 used today</span>
                                        </li>
                                        <li class="coupon__cdiscount">
                                            <span class="ui-font-size--20 coupon__ctitle">15% off select purchases of $1000000 or more.</span><br>
                                            <span class="coupon__ctitle-discount ui-color--red ui-font--bold">+ Up to 45.0% Cash Back</span>
                                        </li>
                                        <li class="coupon__description ui-font-size--14">
                                            <p class="coupon__description-text expandable-text expandable-text--collapsed">Coupon can be used up to two times per eBay account.  Limit one use per transaction (or cart) while supplies last. Valid only for purchases from ebay.com, cafr.ebay.ca and ebay.ca. Coupon is subject to U.S laws, void where prohibited, not redeemable for cash, has no face value, and cannot be combined with any other Coupon, or when paying with PayPal Credit Easy Payments or Gift Cards. eBay may cancel, amend, or revoke the Coupon at any time. The discount will be applied to eligible item(s) only and will be capped at a value of $50.</p>
                                            <span class="more dashboard__c-read-more">Read More +</span>
                                        </li>
                                        <li class="coupon__code-expires">
                                            <span class="ui-color--gray ui-font-size--14"><span class="fa fa-clock-o ui-margin-right--10"></span>Expires 5/25/2018</span>
                                        </li>
                                    </ul>
                                    <a href="file:///C:/Work/grabdid-work/build/stores.html" class="ui-button ui-button--green coupon__shop-btn ui-border-radius--4">Get Sale <span class="fa fa-angle-right"></span></a>
                                    <!-- Coupon's modal  -->
                                    <div class="modal coupon__modal" id="2">
                                            <div class="modal__overlay"></div>
                                            <div class="coupon__modal-window">
                                                <div class="coupon__modal-header">
                                                    <img src="images/icon-150x40-ebay.gif" alt="eBay logo" class="coupon__modal-logo">
                                                    <span>Lorem ipsum dolor afom ifomc pidorasy</span>
                                                </div>
                                                <div class="coupon__modal-body ui-text-align--center">
                                                    <p class="ui-color--green ui-font-size--30">No Code Required</p>
                                                </div>
                                                <div class="coupon__modal-login ui-text-align--center">
                                                    <div class="modal__caption">
                                                            <p class="modal__cap-error"><span class="fa fa-info-circle modal__capicon"></span> You are not a member of the service</p>
                                                            <p>To get a cashback please login below</p>
                                                            <p>Not a member? <a href="#" class="modal__link">Register Now</a></p>
                                                        </div>
                                                    <div class="clr ui-margin-bottom--15">
                                                        <form action="" method="POST" class="grid-2-col__1-of-2 modal__login-form">
                                                            <label for="email" class="ui-font--bold">EMAIL:</label><br>
                                                            <input class="modal__input-t" type="email" name="email" id="email"><br>
                                                            <label for="password" class="ui-font--bold">PASSWORD:</label><br>
                                                            <input class="modal__input-t" type="password" name="password" id="password"><br>
                                                            <a href="#" class="modal__link">I forgot my password</a> <br>
                                                            <button type="submit" class="modal__btn ui-button ui-button--green">Login</button> <br>
                                                            <input type="checkbox" name="remember" id="remember"> <label for="remember">Remember Me</label>
                                                        </form>
                                                        <div class="grid-2-col__1-of-2 modal-coupon-login__socials ui-padding-right--20">
                                                            <a href="#" class="modal-coupon-login__social modal-coupon-login__social--fb">
                                                                <span class="modal-coupon-login__socicon fa fa-facebook"></span> 
                                                                Login with Facebook
                                                            </a> <br>
                                                            <a href="#" class="modal-coupon-login__social modal-coupon-login__social--gp">
                                                                <span class="modal-coupon-login__socicon fa fa-google-plus"></span>
                                                                    Login with Google Plus
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <a href="https://ebay.com" class="modal__link ui-font-size--22 ui-margin-bottom--15 ui-display--block" target="_blank">No thanks, continue without cahsback <span class="fa fa-angle-right ui-font--bold"></span></a>
                                                    <div class="coupon__modal-footer">
                                                        <a href="#" class="ui-color--sm-grey ui-font-size--12">Terms &amp; Conditions</a>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>

                            <!-- One coupon end -->
                            

                            <!-- One coupon start -->

                                <div class="coupon coupon--stores" data-coupon-object="https://ebay.com">
                                    <div class="coupon__logo-wrapper">
                                        <a href="#"> <img src="images/icon-150x40-ebay.gif" alt="eBay logo"></a>
                                        <!-- <a href="#" class="ui-display--block ui-text-align--center ui-font-size--13">See All Coupons</a> -->
                                    </div>
                                    <ul class="coupon__content">
                                        <li>
                                            <span class="coupon__type coupon__type--printable">Printable</span> <span class="coupon__times-used">858 used today</span>
                                        </li>
                                        <li class="coupon__cdiscount">
                                            <span class="ui-font-size--20 coupon__ctitle">15% off select purchases of $1000000 or more.</span><br>
                                            <span class="coupon__ctitle-discount ui-color--red ui-font--bold">+ Up to 45.0% Cash Back</span>
                                        </li>
                                        <li class="coupon__description ui-font-size--14">
                                            <p class="coupon__description-text expandable-text expandable-text--collapsed">Coupon can be used up to two times per eBay account.  Limit one use per transaction (or cart) while supplies last. Valid only for purchases from ebay.com, cafr.ebay.ca and ebay.ca. Coupon is subject to U.S laws, void where prohibited, not redeemable for cash, has no face value, and cannot be combined with any other Coupon, or when paying with PayPal Credit Easy Payments or Gift Cards. eBay may cancel, amend, or revoke the Coupon at any time. The discount will be applied to eligible item(s) only and will be capped at a value of $50.</p>
                                            <span class="more dashboard__c-read-more">Read More +</span>
                                        </li>
                                        <li class="coupon__code-expires">
                                            <span class="ui-color--gray ui-font-size--14"><span class="fa fa-clock-o ui-margin-right--10"></span>Expires 5/25/2018</span>
                                        </li>
                                    </ul>
                                    <a href="file:///C:/Work/grabdid-work/build/stores.html" class="ui-button ui-button--green coupon__shop-btn ui-border-radius--4">Get Coupon <span class="fa fa-angle-right"></span></a>

                                    <!-- Coupon's modal  -->

                                    <div class="modal coupon__modal" id="3">
                                            <div class="modal__overlay"></div>
                                            <div class="coupon__modal-window">
                                                <div class="coupon__modal-header">
                                                    <img src="images/icon-150x40-ebay.gif" alt="eBay logo" class="coupon__modal-logo">
                                                    <span>Lorem ipsum dolor afom ifomc pidorasy</span>
                                                </div>
                                                <div class="coupon__modal-body ui-text-align--center">
                                                </div>
                                                <div class="coupon__modal-login ui-text-align--center">
                                                    <div class="modal__caption">
                                                            <p class="modal__cap-error"><span class="fa fa-info-circle modal__capicon"></span> You are not a member of the service</p>
                                                            <p>To get a printable coupon and cashback please login below</p>
                                                            <p>Not a member? <a href="#" class="modal__link">Register Now</a></p>
                                                        </div>
                                                    <div class="clr ui-margin-bottom--15">
                                                        <form action="" method="POST" class="grid-2-col__1-of-2 modal__login-form">
                                                            <label for="email" class="ui-font--bold">EMAIL:</label><br>
                                                            <input class="modal__input-t" type="email" name="email" id="email"><br>
                                                            <label for="password" class="ui-font--bold">PASSWORD:</label><br>
                                                            <input class="modal__input-t" type="password" name="password" id="password"><br>
                                                            <a href="#" class="modal__link">I forgot my password</a> <br>
                                                            <button type="submit" class="modal__btn ui-button ui-button--green">Login</button> <br>
                                                            <input type="checkbox" name="remember" id="remember"> <label for="remember">Remember Me</label>
                                                        </form>
                                                        <div class="grid-2-col__1-of-2 modal-coupon-login__socials ui-padding-right--20">
                                                                    <a href="#" class="modal-coupon-login__social modal-coupon-login__social--fb">
                                                                        <span class="modal-coupon-login__socicon fa fa-facebook"></span> 
                                                                        Login with Facebook
                                                                    </a> <br>
                                                                    <a href="#" class="modal-coupon-login__social modal-coupon-login__social--gp">
                                                                        <span class="modal-coupon-login__socicon fa fa-google-plus"></span>
                                                                            Login with Google Plus
                                                                    </a>
                                                        </div>
                                                    </div>
                                                    <a href="https://ebay.com" class="modal__link ui-font-size--22 ui-margin-bottom--15 ui-display--block" target="_blank">No thanks, continue without cahsback <span class="fa fa-angle-right ui-font--bold"></span></a>
                                                    <div class="coupon__modal-footer">
                                                        <a href="#" class="ui-color--sm-grey ui-font-size--12">Terms &amp; Conditions</a>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>

                            <!-- One coupon end -->
                        </div>
                        <!-- Description block  -->
                        <div class="ui-padding--15 ui-border" id="description">
                            <span class="ui-font-size--20 ui-font--medium ui-display--block ui-margin-bottom--15">About Old Navy Coupons, Deals and Cash Back</span>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores, voluptatem eius! Corporis est ut eius dolore. Eos animi ullam quas atque similique. Voluptas, possimus earum voluptatum natus maiores optio amet.</p>
                        </div>
                        <!-- Reviews block -->
         <div class="stores-reviews"  id="reviews">
                <div class="clr stores-reviews__heading">
                   <span class="ui-font-size--22 ui-float--left"><span class="ui-color--green fa fa-comments-o"></span> Here you can leave some feedback</span>
                   <span id="leaveFeedback" class="ui-float--right stores-reviews__leave"><span class="fa fa-pencil-square-o"></span> Leave a feedback</span>
               </div>
               <p class="ui-font--bold ui-padding--10 ui-display--none" id="unregFeedback">Please, <a href="#" class="modal__link" id="loginMessage">log in</a> to leave a feedback</p>
               <!-- One review begins -->
               <div class="stores-reviews__review">
                   <div class="clr">
                           <span class="stores-reviews__username ui-float--left"><span class="fa fa-user-circle"></span> Parapapa</span>
                           <span class="stores-reviews__date ui-float--right">05/02/2018</span>
                   </div>
                   <br>
                   <div class="stores-reviews__rate-title">
                       <span class="stores-reviews__rate stores-reviews__rate--5">
                           <span class="fa fa-star stores-reviews__star stores-reviews__star--1"></span>
                           <span class="fa fa-star stores-reviews__star stores-reviews__star--2"></span>
                           <span class="fa fa-star stores-reviews__star stores-reviews__star--3"></span>
                           <span class="fa fa-star stores-reviews__star stores-reviews__star--4"></span>
                           <span class="fa fa-star stores-reviews__star stores-reviews__star--5"></span>
                       </span>
                       <span class="ui-font--bold ui-margin-left--10">Review title</span>
                   </div>
                   <p class="stores-reviews__description">Very good!</p>
               </div>
               <!-- End of one review  -->
               <!-- One review begins -->
               <div class="stores-reviews__review">
                       <div class="clr">
                               <span class="stores-reviews__username ui-float--left"><span class="fa fa-user-circle"></span> Parapapa</span>
                               <span class="stores-reviews__date ui-float--right">05/02/2018</span>
                       </div>
                       <br>
                       <div class="stores-reviews__rate-title">
                           <span class="stores-reviews__rate stores-reviews__rate--4">
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--1"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--2"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--3"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--4"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--5"></span>
                           </span>
                           <span class="ui-font--bold ui-margin-left--10">Review title</span>
                       </div>
                       <p class="stores-reviews__description">Good!</p>
                   </div>
                   <!-- End of one review  -->
                   <!-- One review begins -->
               <div class="stores-reviews__review">
                       <div class="clr">
                               <span class="stores-reviews__username ui-float--left"><span class="fa fa-user-circle"></span> Parapapa</span>
                               <span class="stores-reviews__date ui-float--right">05/02/2018</span>
                       </div>
                       <br>
                       <div class="stores-reviews__rate-title">
                           <span class="stores-reviews__rate stores-reviews__rate--3">
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--1"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--2"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--3"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--4"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--5"></span>
                           </span>
                           <span class="ui-font--bold ui-margin-left--10">Review title</span>
                       </div>
                       <p class="stores-reviews__description">Average</p>
                   </div>
                   <!-- End of one review  -->
                   <!-- One review begins -->
               <div class="stores-reviews__review">
                       <div class="clr">
                               <span class="stores-reviews__username ui-float--left"><span class="fa fa-user-circle"></span> Parapapa</span>
                               <span class="stores-reviews__date ui-float--right">05/02/2018</span>
                       </div>
                       <br>
                       <div class="stores-reviews__rate-title">
                           <span class="stores-reviews__rate stores-reviews__rate--2">
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--1"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--2"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--3"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--4"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--5"></span>
                           </span>
                           <span class="ui-font--bold ui-margin-left--10">Review title</span>
                       </div>
                       <p class="stores-reviews__description">Bad!</p>
                   </div>
                   <!-- End of one review  -->
                   <!-- One review begins -->
               <div class="stores-reviews__review">
                       <div class="clr">
                               <span class="stores-reviews__username ui-float--left"><span class="fa fa-user-circle"></span> Parapapa</span>
                               <span class="stores-reviews__date ui-float--right">05/02/2018</span>
                       </div>
                       <br>
                       <div class="stores-reviews__rate-title">
                           <span class="stores-reviews__rate stores-reviews__rate--1">
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--1"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--2"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--3"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--4"></span>
                               <span class="fa fa-star stores-reviews__star stores-reviews__star--5"></span>
                           </span>
                           <span class="ui-font--bold ui-margin-left--10">Review title</span>
                       </div>
                       <p class="stores-reviews__description">Very bad!</p>
                   </div>
                   <!-- End of one review  -->
                   <!-- Reviews pagination -->
                   <div class="stores-reviews__pagination">
                       <a class="stores-reviews__pag-link stores-reviews__pag-link--disabled stores-reviews__pag-prev"><span class="fa fa-angle-left"></span> Previous</a>
                       <a href="#" class="stores-reviews__pag-link stores-reviews__pag-link--current">1</a>
                       <a href="#" class="stores-reviews__pag-link">2</a>
                       <a href="#" class="stores-reviews__pag-link">3</a>
                       <a href="#" class="stores-reviews__pag-link stores-reviews__pag-next">Next <span class="fa fa-angle-right"></span></a>
                   </div>
                   <!-- End of the reviews -->
           </div>
                    </td>
                </tr>
              </tbody>
         </table>
        <!--end of panelgrid (layout)-->
        <div class="ui-panel">
            <div class="ui-panel__content">
                <div class="ui-width--full-175">

                </div>
            </div>
        </div>
        <table class="ui-panelgrid ui-table-layout--fixed ui-panelgrid--layout ui-width--full">
            <tbody>
                <tr>
                    <td class="ui-panelgrid__cell--content">
                        <!-- You might also like block -->
                        <div class="ui-panel">
                            <div class="ui-panel__header ui-margin-top--10 ui-margin-bottom--7">
                                <span class="ui-font-size--15 ui-color--gray ui-line-height--27 ui-font--medium">You might also like</span>
                            </div>
                            <div class="ui-panel__content ui-margin-top--25">
                                <div class="ui-width--quad ui-float--left ui-text-align--center">
                                    <span class="ui-font-size--15 ui-color--gray ui-font--bold">Vitacost</span>
                                    <p class="clear ui-height--5"></p>
                                    <a href="vitacost"><img src="images/vitacost_280x60.gif" class="ui-max-width--full ui-border--none ui-height--45 ui-display--block ui-margin--center"  /></a>
                                    <span class="ui-font-size--13 ui-color--orange">3%</span><span class="ui-font-size--11 ui-color--gray"> CASHBACK</span>
                                </div>
                                <div class="ui-width--quad ui-float--left ui-text-align--center">
                                    <span class="ui-font-size--15 ui-color--gray ui-font--bold">Expedia</span>
                                    <p class="clear ui-height--5"></p>
                                    <a href="vitacost"> <img src="images/expedia_280x80.gif" class="ui-max-width--full ui-border--none ui-height--45 ui-display--block ui-margin--center"  /></a>
                                    <span class="ui-font-size--13 ui-color--orange">5%</span><span class="ui-font-size--11 ui-color--gray"> CASHBACK</span>
                                </div>
                                <div class="ui-width--quad ui-float--left ui-text-align--center">
                                    <span class="ui-font-size--15 ui-color--gray ui-font--bold">Getsz</span>
                                    <p class="clear ui-height--5"></p>
                                    <a href="vitacost"><img src="images/getzs_280x80.gif" class="ui-max-width--full ui-border--none ui-height--45 ui-display--block ui-margin--center"  /></a>
                                    <span class="ui-font-size--13 ui-color--orange">5.5%</span><span class="ui-font-size--11 ui-color--gray"> CASHBACK</span>
                                </div>
                                <div class="ui-width--quad ui-float--left ui-text-align--center">
                                    <span class="ui-font-size--15 ui-color--gray ui-font--bold">Sixt</span>
                                    <p class="clear ui-height--5"></p>
                                    <a href="vitacost"><img src="images/sixt_280x60.gif" class="ui-max-width--full ui-border--none ui-height--45 ui-display--block ui-margin--center"  /></a>
                                    <span class="ui-font-size--13 ui-color--orange">3.5%</span><span class="ui-font-size--11 ui-color--gray"> CASHBACK</span>
                                </div>
                                <p class="clear"></p>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<!-- end of layout-center -->


<!--modal sidebar navigation (used on mobile devices)-->
<div   class="ui-modal ui-sidebar--navigation">

    <div class="ui-sidebar__content">
        <button id="closeSidebarBtn" class="ui-button ui-border--none ui-float--right ui-background--none ui-sidebar__close">
            <span class="ui-icon ui-icon--close"></span>
        </button>
        <p class="clear"></p>
        <div class="ui-accordion">
            <div class="ui-accordion__header">
                <span>All Stores</span>
            </div>
            <div class="ui-accordion__content">
                <a href="/melinamay">Melinamay</a>
                <a href="/ebay">Ebay</a>
                <a href="/aliexpress">AliExpress</a>
                <a href="/aegean">Aegean</a>
                <a href="/shop-gr">E-shop GR</a>
                <a href="/deliveras-gr">Deliveras GR</a>
                <a href="/zackret">Zackret Sports</a>
            </div>
        </div>
        <a href="/coupons">Coupons</a>
        <a href="/categories">Categories</a>
        <a href="/hot-deal">Hot Deals</a>
        <div class="ui-accordion">
            <div class="ui-accordion__header">
                <span>All Stores</span>
            </div>
            <div class="ui-accordion__content">
                <a href="/melinamay">Melinamay</a>
                <a href="/ebay">Ebay</a>
                <a href="/aliexpress">AliExpress</a>
                <a href="/aegean">Aegean</a>
                <a href="/shop-gr">E-shop GR</a>
                <a href="/deliveras-gr">Deliveras GR</a>
                <a href="/zackret">Zackret Sports</a>
            </div>
        </div>
        <a href="/how-it-works">How it works</a>
        <a href="/help">Help</a>
    </div>

</div>

<!--modal login-->
<div class="ui-modal ui-dialog ui-dialog--login">
    <table class="ui-width--full ui-height--full ">
        <tbody>
            <tr>
                <td class="ui-vertical-align--middle">
                    <div class="ui-dialog__content">
                        <div class="ui-dialog__header ui-font--medium ui-font-size--18 ui-text-align--center">
                            <span>Login</span>
                            <button id="closeLoginBtn" class="ui-button ui-border--none ui-float--right ui-background--none ui-sidebar__close">
                                <span class="ui-icon ui-icon--close"></span>
                            </button>
                        </div>
<form id="loginForm">
                        <p class="clear"></p>
                        <div class="ui-text-align--center ui-margin-top--10 ui-margin-bottom--7 ui-font-size--17 ui-color--gray">
                            Have a Facebook Account?
                        </div>
                        <a href="#" class="ui-commandlink  ui-commandlink--facebook-login"></a>
                        <div class="ui-text-align--center ui-margin-top--5 ui-font-size--12 ui-color--gray">
                            We'll never post anything without your permission.
                        </div>
                        <div class="ui-text-align--center ui-font-size--14 ui-margin-top--10">
                            — OR —
                        </div>
                        <div class="ui-width--300 ui-margin--center">
                            <input name="email" placeholder="*Email" type="email" pattern="[^@\s]+@[^@\s]+\.[^@\s]+" required class="ui-inputfield ui-width--284 ui-inputfield--dialog ui-margin-top--10" />
                            <input name="password" placeholder="*Password" minlength="6" required type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$"  class="ui-inputfield ui-width--284 ui-inputfield--dialog ui-margin-top--10 ui-margin-bottom--11" />
                        </div>
                        <div class="g-recaptcha ui-margin--center ui-width--300" data-sitekey="6Lf7rEUUAAAAADFrTtVT_fZvsX7HEJfxJN8b-UMW"></div>
                        <div class="ui-width--300 ui-margin--center ui-text-align--center">
                            <button class="ui-width--full ui-button ui-border--none ui-border-radius--none ui-margin-top--10 ui-margin-bottom--11">LOGIN</button>
                            <a href="#" class="ui-commandlink ui-commandlink--gray" >Forgot Password?</a>
                        </div>
</form>
                        <p class="clear ui-height--20"></p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</div>
<!--end of login dialog-->
<div class="ui-modal ui-dialog ui-dialog--register">
    <table class="ui-width--full ui-height--full ">
        <tbody>
        <tr>
            <td class="ui-vertical-align--middle">
                <div class="ui-dialog__content">
                    <div class="ui-dialog__header ui-font--medium ui-font-size--18 ui-text-align--center">
                        <span>Register</span>
                        <button id="closeRegisterBtn" class="ui-button ui-border--none ui-float--right ui-background--none ui-sidebar__close">
                            <span class="ui-icon ui-icon--close"></span>
                        </button>
                    </div>
                    <form id="registerForm">
                        <p class="clear"></p>
                        <div class="ui-text-align--center ui-margin-top--10 ui-margin-bottom--7 ui-font-size--17 ui-color--gray">
                            Have a Facebook Account?
                        </div>
                        <a href="#" class="ui-commandlink  ui-commandlink--facebook-login"></a>
                        <div class="ui-text-align--center ui-margin-top--5 ui-font-size--12 ui-color--gray">
                            We'll never post anything without your permission.
                        </div>
                        <div class="ui-text-align--center ui-font-size--14 ui-margin-top--10">
                            — OR —
                        </div>
                        <div class="ui-width--300 tag example ui-margin--center">

                            <select name="selectC" aria-required="true"  required class="ui search selection ui-margin-top--10 dropdown" id="search-select">
                                <option value="">State</option>
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="CA">California</option>
                                <!-- Saving your scroll sanity !-->
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR">Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                            <input name="registerEmail" placeholder="*Email" type="email" pattern="[^@\s]+@[^@\s]+\.[^@\s]+" required class="ui-inputfield ui-width--284 ui-inputfield--dialog ui-margin-top--10" />
                            <input name="registerPassword" placeholder="*Password" minlength="6" required type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$"  class="ui-inputfield ui-width--284 ui-inputfield--dialog ui-margin-top--10 ui-margin-bottom--11" />
                        </div>
                        <div class="g-recaptcha ui-margin--center ui-width--300" data-sitekey="6Lf7rEUUAAAAADFrTtVT_fZvsX7HEJfxJN8b-UMW"></div>
                        <div class="ui-width--300 ui-margin--center ui-text-align--center">
                            <button class="ui-width--full ui-button ui-border--none ui-border-radius--none ui-margin-top--10 ui-margin-bottom--11">Register</button>
                            <a href="#" class="ui-commandlink ui-commandlink--gray" >Forgot Password?</a>
                        </div>
                    </form>
                    <p class="clear ui-height--20"></p>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>

<!--top button-->
<div id="backToTheTop"> <span class="topIcon" /> </div>

<!-- help button -->
<a href="#" class="helpLink ui-button ui-button--green"><span class="fa fa-question-circle"></span> Help</a>
<!-- Login popup -->
<div class="overlay"></div>
<div class="modal modal-coupon-login clr">
        <p class="ui-font-size--40">Login or register</p>
    <div class="modal__caption">
        <p class="modal__cap-error"><span class="fa fa-info-circle modal__capicon"></span> You are not a member of the service</p>
        <p>To get a cashback please register below</p>
        <p>Not a member? <a href="#" class="modal__link">Register Now</a></p>
    </div>
    <form action="" method="POST" class="grid-2-col__1-of-2 modal__login-form">
        <label for="email" class="ui-font--bold">EMAIL:</label><br>
        <input class="modal__input-t" type="email" name="email" id="email"><br>
        <label for="password" class="ui-font--bold">PASSWORD:</label><br>
        <input class="modal__input-t" type="password" name="password" id="password"><br>
        <a href="#" class="modal__link">I forgot my password</a> <br>
        <button type="submit" class="modal__btn ui-button ui-button--green">Login</button> <br>
        <input type="checkbox" name="remember" id="remember"> <label for="remember">Remember Me</label>
    </form>
    <div class="grid-2-col__1-of-2 modal-coupon-login__socials ui-padding-right--20 ui-text-align--center ui-margin-top--25 ui-margin-bottom--15">
        <div class="modal-coupon-login__or">or</div>
        <div class="modal-coupon-login__socials clr">
                <a href="#" class="modal-coupon-login__social modal-coupon-login__social--fb">
                    <span class="modal-coupon-login__socicon fa fa-facebook"></span> 
                    Login with Facebook
                </a> <br>
                <a href="#" class="modal-coupon-login__social modal-coupon-login__social--gp">
                    <span class="modal-coupon-login__socicon fa fa-google-plus"></span>
                        Login with Google Plus
                </a>
        </div>
    </div>
    <a href="#" class="modal__link modal-coupon-login__without-cb">No thanks, continue without cahsback</a>
    <!-- <button class="modal__close-btn fa fa-close"></button> -->
</div>

<!-- scripts -->


<script src="js/expand-text.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="js/main.js"></script>
<script src="js/modal.js"></script>
<script>
    $("#loginForm").validate();
    $("#registerForm").validate();
    var content = [
        { title: 'Andorra', description: '10% discount' },
        { title: 'United Arab Emirates', description: '10% discount' },
        { title: 'Afghanistan', description: '10% discount' },
        { title: 'Antigua', description: '10% discount' },
        { title: 'Anguilla', description: '10% discount' },
        { title: 'Albania', description: '10% discount' },
        { title: 'Armenia', description: '10% discount' },
        { title: 'Netherlands Antilles', description: '10% discount' },
        { title: 'Angola', description: '10% discount' },
        { title: 'Argentina', description: '10% discount' },
        { title: 'American Samoa', description: '10% discount' },
        { title: 'Austria', description: '10% discount' },
        { title: 'Australia', description: '10% discount' },
        { title: 'Aruba', description: '10% discount' },
        { title: 'Aland Islands', description: '10% discount' },
        { title: 'Azerbaijan', description: '10% discount' },
        { title: 'Bosnia', description: '10% discount' },
        { title: 'Barbados', description: '10% discount' },
        { title: 'Bangladesh', description: '10% discount' },
        { title: 'Belgium', description: '10% discount' },
        { title: 'Burkina Faso', description: '10% discount' },
        { title: 'Bulgaria', description: '10% discount' },
        { title: 'Bahrain', description: '10% discount' },
        { title: 'Burundi', description: '10% discount' }
        // etc
    ];
    $('.ui.search')
        .search({
            source: content
        })
    ;
    $('.tag.example .ui.dropdown')
        .dropdown({
            allowAdditions: true,
            minSelections: 1
        })
    ;
    $('.ui.rating')
        .rating()
    ;

    $("#leaveFeedback").on("click", function(){
        $("#unregFeedback").slideToggle(200);
    });

    function openInNewTab(url) {
        var win = window.open(url, '_blank');
        win.focus();
    }

    $(".coupon__shop-btn").click(function(e){
        e.preventDefault();
        $current_url = $(this).attr("href");
        $coupon_modal_id = $(this).parent().find(".coupon__modal").attr("id");
        console.log($coupon_modal_id);
        console.log($current_url);
        $url_to_open = $current_url + "#" + $coupon_modal_id;
        $coupon_url = $(this).parent().attr("data-coupon-object");
        window.location.href = $coupon_url;
        openInNewTab($url_to_open);
    });

    $hash = window.location.hash;
    //console.log($hash);
    setTimeout(function(){
        $($hash).fadeToggle(500);
    }, 200);

</script>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-share-button" data-href="https://www.apopou.gr/kouponia/antenagold/66" data-layout="button" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.apopou.gr%2Fkouponia%2Fantenagold%2F66&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>



<!----------------------------------------------------------------------------!>


<?php
	$query = "SELECT *, DATE_FORMAT(added, '".DATE_FORMAT."') AS date_added FROM cashbackengine_retailers WHERE retailer_id='$retailer_id' AND (end_date='0000-00-00 00:00:00' OR end_date > NOW()) AND status='active' LIMIT 1";
	$result = smart_mysql_query($query);
	$total = mysqli_num_rows($result);

	if ($total > 0)
	{
		$row = mysqli_fetch_array($result);
		
		$retailer_id	= $row['retailer_id'];
		$cashback		= DisplayCashback($row['cashback']);
		$retailer_url	= GetRetailerLink($row['retailer_id'], $row['title']);
		if (isLoggedIn()) $retailer_url .= "&ref=".(int)$_SESSION['userid'];

		// save referral //
		if (!isLoggedIn() && isset($_GET['ref']) && is_numeric($_GET['ref']))
		{
			$ref_id = (int)$_GET['ref'];
			setReferral($ref_id);
		}

		if ($row['seo_title'] != "")
		{
			$ptitle	= $row['seo_title'];
		}
		else
		{
			if ($cashback != "") 
				$ptitle	= $row['title'].". ".CBE1_STORE_EARN." ".$cashback." ".CBE1_CASHBACK2;
			else
				$ptitle	= $row['title'];
		}		

		//// ADD REVIEW ///////////////////////////////////////////////////////////////////////
		if (isset($_POST['action']) && $_POST['action'] == "add_review" && isLoggedIn())
		{
			$userid			= (int)$_SESSION['userid'];
			$retailer_id	= (int)getPostParameter('retailer_id');
			$rating			= (int)getPostParameter('rating');
			$review_title	= mysqli_real_escape_string($conn, getPostParameter('review_title'));
			$review			= mysqli_real_escape_string($conn, nl2br(trim(getPostParameter('review'))));
			$review			= ucfirst(strtolower($review));

			unset($errs);
			$errs = array();

			if (!($userid && $retailer_id && $rating && $review_title && $review))
			{
				$errs[] = CBE1_REVIEW_ERR;
			}
			else
			{
				$number_lines = count(explode("<br />", $review));
				
				if (strlen($review) > MAX_REVIEW_LENGTH)
					$errs[] = str_replace("%length%",MAX_REVIEW_LENGTH,CBE1_REVIEW_ERR2);
				else if ($number_lines > 5)
					$errs[] = CBE1_REVIEW_ERR3;
				else if (stristr($review, 'http'))
					$errs[] = CBE1_REVIEW_ERR4;
			}

			if (count($errs) == 0)
			{
				$review = substr($review, 0, MAX_REVIEW_LENGTH);
				
				if (ONE_REVIEW == 1)
					$check_review = mysqli_num_rows(smart_mysql_query("SELECT * FROM cashbackengine_reviews WHERE retailer_id='$retailer_id' AND user_id='$userid'"));
				else
					$check_review = 0;

				if ($check_review == 0)
				{
					(REVIEWS_APPROVE == 1) ? $status = "pending" : $status = "active";
					$review_query = "INSERT INTO cashbackengine_reviews SET retailer_id='$retailer_id', rating='$rating', user_id='$userid', review_title='$review_title', review='$review', status='$status', added=NOW()";
					$review_result = smart_mysql_query($review_query);
					$review_added = 1;

					// send email notification //
					if (NEW_REVIEW_ALERT == 1) 
					{
						SendEmail(SITE_ALERTS_MAIL, CBE1_EMAIL_ALERT2, CBE1_EMAIL_ALERT2_MSG);
					}
					/////////////////////////////
				}
				else
				{
					$errormsg = CBE1_REVIEW_ERR5;
				}

				unset($_POST['review']);
			}
			else
			{
				$errormsg = "";
				foreach ($errs as $errorname)
					$errormsg .= "&#155; ".$errorname."<br/>";
			}
		}
		//////////////////////////////////////////////////////////////////////////////////////////
	}
	else
	{
		$ptitle = CBE1_STORE_NOT_FOUND;
	}


	///////////////  Page config  ///////////////
	$PAGE_TITLE			= $ptitle;
	$PAGE_DESCRIPTION	= $row['meta_description'];
	$PAGE_KEYWORDS		= $row['meta_keywords'];

	require_once ("inc/header.inc.php");

?>	

	<?php

		if ($total > 0) {

	?>
			<h1><?php echo $ptitle; ?></h1>

			<div class="breadcrumbs"><a href="<?php echo SITE_URL; ?>" class="home_link"><?php echo CBE1_BREADCRUMBS_HOME; ?></a> &#155; <a href="<?php echo SITE_URL; ?>retailers.php"><?php echo CBE1_BREADCRUMBS_STORES; ?></a> &#155; <?php echo $row['title']; ?></div>

			<table align="center" width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr class="odd">
					<td width="<?php echo IMAGE_WIDTH; ?>" align="center" valign="top">
						<br/>
						<a href="<?php echo SITE_URL; ?>go2store.php?id=<?php echo $row['retailer_id']; ?>" target="_blank">
						<?php if ($row['featured'] == 1) { ?><span class="featured" alt="<?php echo CBE1_FEATURED_STORE; ?>" title="<?php echo CBE1_FEATURED_STORE; ?>"></span><?php } ?>
						<div class="imagebox"><img src="<?php if (!stristr($row['image'], 'http')) echo SITE_URL."img/"; echo $row['image']; ?>" width="<?php echo IMAGE_WIDTH; ?>" height="<?php echo IMAGE_HEIGHT; ?>" alt="<?php echo $row['title']; ?>" title="<?php echo $row['title']; ?>" border="0" /></div></a>
						<a class="coupons" href="#coupons"><?php echo GetStoreCouponsTotal($row['retailer_id']); ?> <?php echo CBE1_STORE_COUPONS1; ?></a><br/><br/>
						<a class="scroll2reviews" data-location="#reviews" href="#reviews" style="color: #707070; font-weight: bold;"><?php echo GetStoreReviewsTotal($row['retailer_id']); ?></a><br/>
						<?php echo GetStoreRating($row['retailer_id'], $show_start = 1); ?>
					</td>
					<td align="left" valign="top">
						<div class="info_box">
							<a class="stitle" href="<?php echo SITE_URL; ?>go2store.php?id=<?php echo $row['retailer_id']; ?>" target="_blank"><?php echo $row['title']; ?></a>
							<div class="retailer_description"><?php echo TruncateText(stripslashes($row['description']), STORES_DESCRIPTION_LIMIT, $more_link = 1); ?>&nbsp;</div>
							<?php echo GetStoreCountries($row['retailer_id']); ?>
							<?php if ($row['tags'] != "") { ?><p><span class="tags"><?php echo $row['tags']; ?></span></p><?php } ?>
							<?php if ($row['conditions'] != "") { ?>
								<p><b><?php echo CBE1_CONDITIONS; ?></b><br/>
								<span class="conditions_desc"><?php echo $row['conditions']; ?></span>
								</p>
							<?php } ?>
						</div>
						<?php if ($cashback != "") { ?>
						<div class="cashback_box">
							<?php if ($row['old_cashback'] != "") { ?><span class="old_cashback"><?php echo DisplayCashback($row['old_cashback']); ?></span><?php } ?>
							<span class="bcashback"><?php echo $cashback; ?></span> <?php echo CBE1_CASHBACK; ?>
						</div>
						<?php } ?>					
						<div style="clear: both"></div>
						<div class="info_links" style="width: 100%; padding-top: 15px;">
							<a class="favorites" href="<?php echo SITE_URL; ?>myfavorites.php?act=add&id=<?php echo $row['retailer_id']; ?>"><?php echo CBE1_ADD_FAVORITES; ?></a>
							<a class="report" href="<?php echo SITE_URL; ?>report_retailer.php?id=<?php echo $row['retailer_id']; ?>"><?php echo CBE1_REPORT; ?></a>
							<?php if (SUBMIT_COUPONS == 1) { ?>
								<a class="submit_coupon" href="<?php echo SITE_URL; ?>submit_coupon.php?id=<?php echo $row['retailer_id']; ?>"><?php echo CBE1_STORE_COUPONS2; ?></a>
							<?php } ?>
							<br/><br/>
							<p align="center">
							<a class="go2store_large" href="<?php echo SITE_URL; ?>go2store.php?id=<?php echo $row['retailer_id']; ?>" target="_blank"><?php echo CBE1_GO_TO_STORE2; ?></a>
							</p>
						</div>
					</td>
				</tr>
			</table>

			<table bgcolor="#F9F9F9" align="center" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<?php if (SHOW_RETAILER_STATS == 1) { ?>
				<td nowrap="nowrap" style="border-right: 1px solid #F3F3F3; border-bottom: 1px solid #F3F3F3;">
					<div class="retailer_statistics">
						<center><b><?php echo CBE1_STORE_STATS; ?></b></center>
						<label><?php echo CBE1_STORE_COUPONS; ?>:</label> <?php echo GetStoreCouponsTotal($row['retailer_id']); ?><br/>
						<label><?php echo CBE1_STORE_REVIEWS; ?>:</label> <?php echo GetStoreReviewsTotal($row['retailer_id'], $all = 0, $word = 0); ?><br/>
						<label><?php echo CBE1_STORE_FAVORITES; ?>:</label> <?php echo GetFavoritesTotal($row['retailer_id']); ?><br/>
						<label><?php echo CBE1_STORE_DATE; ?>:</label> <?php echo $row['date_added']; ?><br/>
					 </div>
				</td>
				<?php } ?>
				<?php if (SHOW_CASHBACK_CALCULATOR == 1 && strstr($row['cashback'], '%')) { ?>
				<td nowrap="nowrap" align="center" style="border-right: 1px solid #F3F3F3; border-bottom: 1px solid #F3F3F3;">
					<center><b><?php echo CBE1_STORE_CCALCULATOR; ?></b></center>
					<table align="center" width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td width="50%" align="center"><?php echo CBE1_STORE_SPEND; ?></td>
						<td width="50%" align="center"><?php echo CBE1_CASHBACK; ?></td>
					</tr>
					<tr>
						<td align="center"><span class="calc_spend"><?php echo DisplayMoney("100", 0, 1); ?></span></td>
						<td align="center"><span class="calc_cashback"><?php echo DisplayMoney(CalculatePercentage(100, $cashback),0,1); ?></span></td>
					</tr>
					<tr>
						<td align="center"><span class="calc_spend"><?php echo DisplayMoney("500", 0, 1); ?></span></td>
						<td align="center"><span class="calc_cashback"><?php echo DisplayMoney(CalculatePercentage(500, $cashback),0,1); ?></span></td>
					</tr>
					<tr>
						<td align="center"><span class="calc_spend"><?php echo DisplayMoney("1000", 0, 1); ?></span></td>
						<td align="center"><span class="calc_cashback"><?php echo DisplayMoney(CalculatePercentage(1000, $cashback),0,1); ?></span></td>
					</tr>
					</table>
				</td>
				<?php } ?>
				<td nowrap="nowrap" align="center" style="border-bottom: 1px solid #F3F3F3;">
					<div class="share_box" <?php if (SHARE_ICONS_STYLE != 1) { ?>style="width: 99%;"<?php } ?>>
						<!-- AddThis Share Buttons -->
						<div class="addthis_toolbox" addthis:url="<?php echo $retailer_url; ?>" addthis:title="<?php echo $ptitle; ?>">
						<div class="addthis_toolbox addthis_default_style">
							<?php if (SHARE_ICONS_STYLE == 1) { ?>
								<a class="addthis_button_facebook_like" fb:like:layout="box_count"></a> 
								<a class="addthis_button_tweet" tw:count="vertical"></a> 
								<a class="addthis_button_google_plusone" g:plusone:size="tall"></a>
							<?php }else{ ?>
								<a class="addthis_button_facebook_like"></a> 
								<a class="addthis_button_tweet"></a> 
								<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
							<?php } ?>
							<a title="<?php echo CBE1_STORE_MORE; ?>" href="http://addthis.com/bookmark.php?v=250" class="addthis_button_expanded at300m" style="color: #999;">&nbsp;</a>
						</div>
						</div>
					</div>
					<br/><span style="color: #333"><?php echo CBE1_STORE_SHARE; ?>:</span>
					<input type="text" class="share_textbox" size="53" READONLY onfocus="this.select();" onclick="this.focus();this.select();" value="<?php echo $retailer_url; ?>" />
				</td>
				</tr>
			</table>


		<?php
				// start store coupons //
				$ee = 0;
				$query_coupons = "SELECT *, DATE_FORMAT(end_date, '".DATE_FORMAT."') AS coupon_end_date, UNIX_TIMESTAMP(end_date) - UNIX_TIMESTAMP() AS time_left FROM cashbackengine_coupons WHERE retailer_id='$retailer_id' AND (start_date<=NOW() AND (end_date='0000-00-00 00:00:00' OR end_date > NOW())) AND status='active' ORDER BY sort_order, added DESC";
				$result_coupons = smart_mysql_query($query_coupons);
				$total_coupons = mysqli_num_rows($result_coupons);

				if ($total_coupons > 0)
				{
		?>
			<a name="coupons"></a>
			<h3 class="store_coupons"><?php echo $row['title']; ?> <?php echo CBE1_STORE_COUPONS; ?></h3>
			<ul class="coupons-list">
				<?php while ($row_coupons = mysqli_fetch_array($result_coupons)) { $ee++; ?>
				<li class="coupon">
					<span class="scissors"></span>
					<a class="coupon_title" href="<?php echo SITE_URL; ?>go2store.php?id=<?php echo $row['retailer_id']; ?>&c=<?php echo $row_coupons['coupon_id']; ?>" target="_blank"><?php echo $row_coupons['title']; ?></a>
					<?php if ($row_coupons['exclusive'] == 1) { ?> <sup><span class="exclusive_s"><?php echo CBE1_COUPONS_EXCLUSIVE; ?></span></sup><?php } ?>
					<?php echo ($row_coupons['visits'] > 0) ? "<span class='coupon_times_used'><sup>".$row_coupons['visits']." ".CBE1_COUPONS_TUSED."</sup></span>" : ""; ?>
					<?php if ($row_coupons['description'] != "") { ?><div class="coupon_description"><?php echo TruncateText($row_coupons['description'], COUPONS_DESCRIPTION_LIMIT, $more_link = 1); ?>&nbsp;</div><?php } ?>
					<a class="go2store" href="<?php echo SITE_URL; ?>go2store.php?id=<?php echo $row['retailer_id']; ?>&c=<?php echo $row_coupons['coupon_id']; ?>" target="_blank"><?php echo ($row_coupons['code'] != "") ? CBE1_COUPONS_LINK : CBE1_COUPONS_LINK2; ?></a>
					<?php if ($row_coupons['code'] != "") { ?>
						<span class="coupon_code"><?php echo (HIDE_COUPONS == 0 || isLoggedIn()) ? $row_coupons['code'] : CBE1_COUPONS_CODE_HIDDEN; ?></span>
						<span class="coupon_note"><?php echo CBE1_COUPONS_MSG; ?></span><br/><br/>
					<?php } ?>
					<?php if ($row_coupons['end_date'] != "0000-00-00 00:00:00") { ?>
						<span class="expires"><?php echo CBE1_COUPONS_EXPIRES; ?>: <?php echo $row_coupons['coupon_end_date']; ?></span> &nbsp; 
						<span class="time_left"><?php echo CBE1_COUPONS_TIMELEFT; ?>: <?php echo GetTimeLeft($row_coupons['time_left']); ?></span>
					<?php } ?>
				</li>
				<?php } ?>
			</ul>
			<div style="clear: both"></div>
		<?php } // end store coupons // ?>

		<?php
				// start expired coupons //
				$ee = 0;
				$query_exp_coupons = "SELECT *, DATE_FORMAT(end_date, '".DATE_FORMAT."') AS coupon_end_date FROM cashbackengine_coupons WHERE retailer_id='$retailer_id' AND end_date != '0000-00-00 00:00:00' AND end_date < NOW() AND status!='inactive' ORDER BY sort_order, added DESC";
				$result_exp_coupons = smart_mysql_query($query_exp_coupons);
				$total_exp_coupons = mysqli_num_rows($result_exp_coupons);

				if ($total_exp_coupons > 0)
				{
		?>
			<h3 class="store_exp_coupons"><?php echo CBE1_STORE_ECOUPONS; ?></h3>
			<ul class="coupons-list">
				<?php while ($row_exp_coupons = mysqli_fetch_array($result_exp_coupons)) { $ee++; ?>
				<li class="coupon" style="opacity: 0.5; filter: alpha(opacity=50);">
					<span class="scissors"></span>
					<b><?php echo $row_exp_coupons['title']; ?></b>
					<?php echo ($row_exp_coupons['visits'] > 0) ? "<span class='coupon_times_used'><sup>".$row_exp_coupons['visits']." ".CBE1_COUPONS_TUSED."</sup></span>" : ""; ?>
					<?php if ($row_exp_coupons['description'] != "") { ?><div class="coupon_description"><?php echo TruncateText($row_exp_coupons['description'], COUPONS_DESCRIPTION_LIMIT, $more_link = 1); ?>&nbsp;</div><?php } ?>
					<?php if ($row_exp_coupons['code'] != "") { ?><span class="coupon_code" style="background: #EEE; border-color: #EEE"><?php echo $row_exp_coupons['code']; ?></span><?php } ?>
					<span class="expires"><?php echo CBE1_COUPONS_ENDED; ?>: <?php echo $row_exp_coupons['coupon_end_date']; ?></span>
				</li>
				<?php } ?>
			</ul>
			<div style="clear: both"></div>
		<?php } // end expired coupons // ?>

		<?php
				// store reviews //
				$results_per_page = REVIEWS_PER_PAGE;

				if (isset($_GET['cpage']) && is_numeric($_GET['cpage']) && $_GET['cpage'] > 0) { $page = (int)$_GET['cpage']; } else { $page = 1; }
				$from = ($page-1)*$results_per_page;

				$reviews_query = "SELECT r.*, DATE_FORMAT(r.added, '".DATE_FORMAT."') AS review_date, u.user_id, u.username, u.fname, u.lname FROM cashbackengine_reviews r LEFT JOIN cashbackengine_users u ON r.user_id=u.user_id WHERE r.retailer_id='$retailer_id' AND r.status='active' ORDER BY r.added DESC LIMIT $from, $results_per_page";
				$reviews_result = smart_mysql_query($reviews_query);
				$reviews_total = mysqli_num_rows(smart_mysql_query("SELECT * FROM cashbackengine_reviews WHERE retailer_id='$retailer_id' AND status='active'"));
		?>

		<div id="add_review_link"><a id="add-review" href="javascript:void(0);"><?php echo CBE1_REVIEW_TITLE; ?></a></div>
		<div id="reviews"></div>
		<h3 class="store_reviews"><?php echo $row['title']; ?> <?php echo CBE1_STORE_REVIEWS; ?> <?php echo ($reviews_total > 0) ? "($reviews_total)" : ""; ?></h3>

		<script type="text/javascript">
		$("#add-review").click(function () {
			$("#review-form").toggle("slow");
		});
		$('.scroll2reviews').click(function() {
			var location = jQuery(this).data('location');
			if (jQuery(location).length > 0) { jQuery('html, body').animate({scrollTop:jQuery(location).offset().top},1000); }
		});
		</script>

		<div id="review-form" class="review-form" style="<?php if (!(isset($_POST['action']) && $_POST['action'] == "add_review")) { ?>display: none;<?php } ?>">
			<?php if (isset($errormsg) && $errormsg != "") { ?>
				<div style="width: 91%;" class="error_msg"><?php echo $errormsg; ?></div>
			<?php } ?>
			<?php if (REVIEWS_APPROVE == 1 && $review_added == 1) { ?>
				<div style="width: 91%;" class="success_msg"><?php echo CBE1_REVIEW_SENT; ?></div>
			<?php }else{ ?>
				<?php if (isLoggedIn()) { ?>
					<form method="post" action="#reviews">
						<select name="rating">
							<option value=""><?php echo CBE1_REVIEW_RATING_SELECT; ?></option>
							<option value="5" <?php if ($rating == 5) echo "selected"; ?>>&#9733;&#9733;&#9733;&#9733;&#9733; - <?php echo CBE1_REVIEW_RATING1; ?></option>
							<option value="4" <?php if ($rating == 4) echo "selected"; ?>>&#9733;&#9733;&#9733;&#9733; - <?php echo CBE1_REVIEW_RATING2; ?></option>
							<option value="3" <?php if ($rating == 3) echo "selected"; ?>>&#9733;&#9733;&#9733; - <?php echo CBE1_REVIEW_RATING3; ?></option>
							<option value="2" <?php if ($rating == 2) echo "selected"; ?>>&#9733;&#9733; - <?php echo CBE1_REVIEW_RATING4; ?></option>
							<option value="1" <?php if ($rating == 1) echo "selected"; ?>>&#9733; - <?php echo CBE1_REVIEW_RATING5; ?></option>					
						</select><br/>
						<?php echo CBE1_REVIEW_RTITLE; ?><br/>
						<input type="text" name="review_title" id="review_title" value="<?php echo getPostParameter('review_title'); ?>" size="47" class="textbox" /><br/>
						<?php echo CBE1_REVIEW_REVIEW; ?><br/>
						<textarea id="review" name="review" cols="45" rows="5" class="textbox2" style="width: 299px;"><?php echo getPostParameter('review'); ?></textarea><br/>
						<input type="hidden" id="retailer_id" name="retailer_id" value="<?php echo $retailer_id; ?>" />
						<input type="hidden" name="action" value="add_review" />
						<input type="submit" class="submit" value="<?php echo CBE1_REVIEW_BUTTON; ?>" />
					</form>
				<?php }else{ ?>
					<?php echo CBE1_REVIEW_MSG; ?>
				<?php } ?>
			<?php } ?>
		</div>

		<div style="clear: both"></div>
		<?php if ($reviews_total > 0) { ?>

			<?php while ($reviews_row = mysqli_fetch_array($reviews_result)) { ?>
            <div id="review">
                <span class="review-author"><?php echo $reviews_row['fname']." ".substr($reviews_row['lname'], 0, 1)."."; ?></span>
				<span class="review-date"><?php echo $reviews_row['review_date']; ?></span><br/><br/>
				<img src="<?php echo SITE_URL; ?>images/icons/rating-<?php echo $reviews_row['rating']; ?>.gif" />&nbsp;
				<span class="review-title"><?php echo $reviews_row['review_title']; ?></span><br/>
				<div class="review-text"><?php echo $reviews_row['review']; ?></div>
                <div style="clear: both"></div>
            </div>
			<?php } ?>
		
			<?php echo ShowPagination("reviews",REVIEWS_PER_PAGE,"?id=$retailer_id&","WHERE retailer_id='$retailer_id' AND status='active'"); ?>
		
		<?php }else{ ?>
				<?php echo CBE1_REVIEW_NO; ?>
		<?php } ?>


		<?php
			// start related retailers //
			$query_like = "SELECT * FROM cashbackengine_retailers WHERE retailer_id<>'$retailer_id' AND (end_date='0000-00-00 00:00:00' OR end_date > NOW()) AND status='active' ORDER BY RAND() LIMIT 5";
			$result_like = smart_mysql_query($query_like);
			$total_like = mysqli_num_rows($result_like);

			if ($total_like > 0)
			{
		?>
			<div style="clear: both"></div><br/>
			<h3><?php echo CBE1_STORE_LIKE; ?></h3>
			<table align="center" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<?php while ($row_like = mysqli_fetch_array($result_like)) { ?>
					<td class="like" width="<?php echo IMAGE_WIDTH; ?>" align="center" valign="middle">
						<?php echo $row_like['title']; ?><br/>
						<a href="<?php echo GetRetailerLink($row_like['retailer_id'], $row_like['title']); ?>"><img src="<?php if (!stristr($row_like['image'], 'http')) echo SITE_URL."img/"; echo $row_like['image']; ?>" width="<?php echo IMAGE_WIDTH/2; ?>" height="<?php echo IMAGE_HEIGHT/2; ?>" alt="<?php echo $row_like['title']; ?>" title="<?php echo $row_like['title']; ?>" border="0" style="margin:5px;" class="imgs" /></a><br/>
						<?php if ($row_like['cashback'] != "") { ?><span class="cashback"><?php echo DisplayCashback($row_like['cashback']); ?></span> <?php echo CBE1_CASHBACK; ?><?php } ?>
					</td>
				<?php } ?>
			</tr>
			</table>
		<?php } // end related retailers // ?>

	<?php }else{ ?>
		<h1><?php echo $ptitle; ?></h1>
		<p align="center"><?php echo CBE1_STORE_NOT_FOUND2; ?></p>
		<p align="center"><a class="goback" href="#" onclick="history.go(-1);return false;"><?php echo CBE1_GO_BACK; ?></a></p>
	<?php } ?>


<!----------------------------------------------------------------------------!>



<?php $__env->stopSection(); ?> 


<?php $__env->startSection('footer'); ?>

    <?php echo $__env->make('inc.footer', ['fc' => $fc], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>