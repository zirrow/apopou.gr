<?php $__env->startSection('head'); ?>

        <?php echo $__env->make('inc.head', ['meta' => $meta, 'mc' => $mc], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('head-styles'); ?>
        
    <link rel="stylesheet" type="text/css" href="<?php echo e($mc['SITE_URL']); ?>css/grabdid_com/style.css" />

    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head-scripts'); ?>
    <script src="<?php echo e($mc['SITE_URL']); ?>js/grabdid_com/jquery-3.3.1.min.js"></script>
    <script src="<?php echo e($mc['SITE_URL']); ?>js/grabdid_com/ftellipsis.js"></script>
    <script src="<?php echo e($mc['SITE_URL']); ?>js/grabdid_com/easing.min.js"></script>
    <script src="<?php echo e($mc['SITE_URL']); ?>js/grabdid_com/transition.min.js" ></script>
    <script src="<?php echo e($mc['SITE_URL']); ?>js/grabdid_com/dropdown.min.js"></script>
    <script src="<?php echo e($mc['SITE_URL']); ?>js/grabdid_com/search.min.js" ></script>
    <script src="<?php echo e($mc['SITE_URL']); ?>js/grabdid_com/validate.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo e($mc['SITE_URL']); ?>js/grabdid_com/search.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e($mc['SITE_URL']); ?>js/grabdid_com/transition.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e($mc['SITE_URL']); ?>js/grabdid_com/dropdown.min.css" />
<?php $__env->stopSection(); ?>



<?php $__env->startSection('header'); ?>

  <?php echo $__env->make('inc.header', ['meta' => $meta, 'mc' => $mc], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  
<?php $__env->stopSection(); ?> 

<?php $__env->startSection('content'); ?>




<script src='https://www.google.com/recaptcha/api.js'></script>


        <table width="100%" style="border-bottom: 2px solid #F7F7F7;" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
			<td align="left" valign="middle"><h1 style="margin-bottom:0;border:none;"><?php echo CBE1_SIGNUP_TITLE; ?></h1></td>
			<td align="right" valign="bottom"><?php echo CBE1_SIGNUP_MEMBER; ?> <a href="<?php echo SITE_URL; ?>login.php"><?php echo CBE1_LOGIN_TITLE; ?></a></td>
        </tr>
        </table>

		<?php if (FACEBOOK_CONNECT == 1 && FACEBOOK_APPID != "" && FACEBOOK_SECRET != "") { ?>
			<p align="center"><a href="javascript: void(0);" onclick="facebook_login();" class="connect-f"><img src="<?php echo SITE_URL; ?>images/facebook_connect.png" /></a></p>
			<div style="border-bottom: 1px solid #ECF0F1; width: 400px; margin: 0 auto;">
				<div style="font-weight: bold; background: #FFF; color: #CECECE; margin: 0 auto; top: 5px; text-align: center; width: 50px; position: relative;">or</div>
			</div><br/>
		<?php } ?>

		<?php if (isset($allerrors) || isset($_GET['msg'])) { ?>
			<div class="error_msg">
				<?php if (isset($_GET['msg']) && $_GET['msg'] == "exists") { ?>
					<?php echo CBE1_SIGNUP_ERR10; ?> <a href="<?php echo SITE_URL; ?>forgot.php"><?php echo CBE1_LOGIN_FORGOT; ?></a></font><br/>
				<?php }elseif (isset($allerrors)) { ?>
					<?php echo $allerrors; ?>
				<?php }	?>
			</div>
		<?php } ?>

        <form action="" method="post">
        <table width="100%" align="center" cellpadding="3" cellspacing="0" border="0">
          <tr>
            <td colspan="2" align="right" valign="top"><span class="req">* <?php echo CBE1_LABEL_REQUIRED; ?></span></td>
          </tr>
       <!--   <tr>
            <td width="210" align="right" valign="middle"><span class="req">* </span><?php echo CBE1_LABEL_FNAME; ?>:</td>
            <td align="left" valign="middle"><input type="text" id="fname" class="textbox" name="fname" value="<?php echo getPostParameter('fname'); ?>" size="27" /></td>
          </tr>
        
          <tr>
            <td align="right" valign="middle"><span class="req">* </span><?php echo CBE1_LABEL_LNAME; ?>:</td>
            <td align="left" valign="middle"><input type="text" id="lname" class="textbox" name="lname" value="<?php echo getPostParameter('lname'); ?>" size="27" /></td>
          </tr>
        -->
          <tr>
            <td align="right" valign="middle"><span class="req">* </span><?php echo CBE1_LABEL_EMAIL2; ?>:</td>
            <td align="left" valign="middle"><input type="text" id="email" class="textbox" name="email" value="<?php echo getPostParameter('email'); ?>" size="27" /></td>
          </tr>
          <tr>
            <td align="right" valign="middle"><span class="req">* </span><?php echo CBE1_LABEL_PWD; ?>:</td>
            <td nowrap="nowrap" align="left" valign="middle"><input type="password" id="password" class="textbox" name="password" value="" size="27" /> <span class="note"><?php echo CBE1_SIGNUP_PTEXT; ?></span></td>
          </tr>
          <!--
          <tr>
            <td align="right" valign="middle"><span class="req">* </span><?php echo CBE1_LABEL_CPWD; ?>:</td>
            <td nowrap="nowrap" align="left" valign="middle"><input type="password" id="password2" class="textbox" name="password2" value="" size="27" /></td>
          </tr>
          -->
          <tr>
            <td align="right" valign="middle"><span class="req">* </span><?php echo CBE1_LABEL_COUNTRY; ?>:</td>
            <td align="left" valign="middle">
				<select name="country" class="textbox2" id="country" style="width: 180px;">
				<option value=""><?php echo CBE1_LABEL_COUNTRY_SELECT; ?></option>
				<?php

					$sql_country = "SELECT * FROM cashbackengine_countries WHERE signup='1' AND status='active' ORDER BY sort_order, name";
					$rs_country = smart_mysql_query($sql_country);
					$total_country = mysqli_num_rows($rs_country);

					if ($total_country > 0)
					{
						while ($row_country = mysqli_fetch_array($rs_country))
						{
							if ($country == $row_country['country_id'])
								echo "<option value='".$row_country['country_id']."' selected>".$row_country['name']."</option>\n";
							else
								echo "<option value='".$row_country['country_id']."'>".$row_country['name']."</option>\n";
						}
					}

				?>
				</select>
			</td>
          </tr>
      <!--
          <tr>
            <td align="right" valign="middle"><?php echo CBE1_LABEL_PHONE; ?>:</td>
            <td align="left" valign="middle"><input type="text" id="phone" class="textbox" name="phone" value="<?php echo getPostParameter('phone'); ?>" size="27" /></td>
          </tr>
          -->
		  <?php if (SIGNUP_CAPTCHA == 1) { ?>
          <tr>
            <td align="right" valign="middle"><span class="req">* </span><?php echo CBE1_SIGNUP_SCODE; ?>:</td>
            <td align="left" valign="middle">
				<input type="text" id="captcha" class="textbox" name="captcha" value="" size="8" />
				<img src="<?php echo SITE_URL; ?>captcha.php?rand=<?php echo rand(); ?>" id="captchaimg" align="absmiddle" /> <small><a href="javascript: refreshCaptcha();" title="<?php echo CBE1_SIGNUP_RIMG; ?>"><img src="<?php echo SITE_URL; ?>images/icon_refresh.png" align="absmiddle" alt="<?php echo CBE1_SIGNUP_RIMG; ?>" /></a></small>
			</td>
          </tr>
			<script language="javascript" type="text/javascript">
				function refreshCaptcha()
				{
					var img = document.images['captchaimg'];
					img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
				}
			</script>
		  <?php } ?>
		  <?php if (is_array($reg_sources) && count($reg_sources) > 0) { ?>
       <!--
          <tr>
            <td align="right" valign="middle">&nbsp;</td>
            <td align="left" valign="middle">
				<select name="reg_source" class="textbox2" id="reg_source">
					<option value=""><?php echo CBE1_SIGNUP_REG_SOURCE; ?></option>
					<?php foreach ($reg_sources as $v) { ?>
						<option value="<?php echo trim($v); ?>" <?php if ($reg_source == $v) echo "selected"; ?>><?php echo trim($v); ?></option>
					<?php } ?>
				</select>
			</td>
          </tr>
        -->
		  <?php } ?>
      <!--    <tr>
            <td align="right" valign="middle">&nbsp;</td>
            <td align="left" valign="middle"><input type="checkbox" name="newsletter" class="checkboxx" value="1" <?php // echo (!$_POST['action'] || @$newsletter == 1) ? "checked" : "" ?>/> <?php // echo CBE1_SIGNUP_NEWSLETTER; ?></td>
          </tr>
          -->
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="middle"><input type="checkbox" name="tos" class="checkboxx" value="1" <?php echo (!$_POST['action'] || @$tos == 1) ? "checked" : "" ?>/> <?php echo CBE1_SIGNUP_AGREE; ?> <a href="<?php echo SITE_URL; ?>terms.php" target="_blank"><?php echo CBE1_SIGNUP_TERMS; ?></a></td>
          </tr>
      <!--  </tr>  -->
        <tr>
        <div class="g-recaptcha" data-sitekey="6LdAjTkUAAAAANUYtZ4o43KfrioV2F5RnbRyhBxQ"></div>
        </tr>
          <tr>
            <td align="right" valign="middle">&nbsp;</td>
			<td align="left" valign="middle">
				<?php if (isset($_COOKIE['referer_id']) && is_numeric($_COOKIE['referer_id'])) { ?>
					<input type="hidden" name="referer_id" id="referer_id" value="<?php echo (int)$_COOKIE['referer_id']; ?>" />
				<?php } ?>
				<input type="hidden" name="action" id="action" value="signup" />
				<input type="submit" class="submit signup" name="Signup" id="Signup" value="<?php echo CBE1_SIGNUP_BUTTON; ?>" />
		  </td>
          </tr>
        </table>
        </form>
        

        
        
                        <?php $__env->stopSection(); ?> 


<?php $__env->startSection('footer'); ?>

  <?php echo $__env->make('inc.footer', ['fc' => $fc], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
           
<?php $__env->stopSection(); ?>
        
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>