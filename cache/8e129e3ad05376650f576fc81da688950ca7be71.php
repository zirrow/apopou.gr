<?php $__env->startSection('header'); ?>


<!-- put header content here -->

<div class="ui-layout-west sticky-header">
        <div class="ui-layout-west__content">

                <table class="ui-panelgrid ui-width--full" >
                    <tbody>
                    <tr>

                        <!-- logo column -->
                        
                        <td class="ui-width--215 header-logo ui-position--relative">
                            <img src="<?php echo e(SITE_URL); ?>images/apopou/<?=$GLOBALS['top_level_domain']?>/theme/ApopougrLogo3.png" width="180" >
                        
                        <!-- flag column -->
                            <div class="flag-drop">
                                <div class="flag-drop__selected"><span class="flag-drop__lang">GR</span> <span class="flag-drop__arrow"></span></div>
                                <ul class="flag-drop__list">
                                    <li class="flag-drop__item flag-drop__item--cy">CY</li>
                                    <li class="flag-drop__item flag-drop__item--gr">GR</li>
                                </ul>
                            </div>

                        <!-- Mobile button for showing login/signup buttons -->
                        
                            <span class="fa
                                         fa-sign-in
                                         ui-font-size--25
                                         ui-float--right
                                         ui-margin-top--10
                                         ui-color--orange
                                         ui-cursor--pointer
                                         header__login" id="signButton"></span>
                        </td>
                        
                        <!-- search column -->
                        
                        <td class="ui-search-column">
                            <div class="ui search ui-inputfield ui-inputfield--search">
                                <input class="prompt" type="text" placeholder="Search"/>
                                <button class="ui-button ui-position--relative ui-border--none ui-button--green ui-button--search">
                                    <span class="ui-icon ui-icon--search"></span>
                                </button>
                                <div class="results"></div>
                            </div>
                        </td>
                        
                        <!-- sign in column -->
                        
                        <td class="ui-width--230 ui-text-align--center header__log-reg" id="loginBlock">
                            <button id="login-btn" class="ui-button ui-border-radius--5 ui-font-size--15 ui-border--none ui-button--orange ui-button--left ui-button--header" >Login</button>
                            <button id="register-btn" class="ui-button ui-border-radius--5 ui-font-size--15 ui-border--none ui-button--orange ui-button--right ui-button--header" >Register</button>
                        </td>
        
                    </tr>
                    </tbody>
                </table>
                
                <!-- end of panelgrid -->
            
            </div>
</div>
<div class="ui-layout-west">
    
    <!-- footer inside of west layout -->
    
    <div class="ui-layout-west__footer menu">
        <div class="ui-footer__content">
            <table class="ui-panelgrid ui-width--full">
                <tbody>
                <tr>
                    <td>
                        <div class="ui-dropdown">
                        <a>All stores</a>
                            <div class="ui-dropdown__content">
                                <a href="/melinamay">Melinamay</a>
                                <a href="/ebay">Ebay</a>
                                <a href="/aliexpress">AliExpress</a>
                                <a href="/aegean">Aegean</a>
                                <a href="/shop-gr">E-shop GR</a>
                                <a href="/deliveras-gr">Deliveras GR</a>
                                <a href="/zackret">Zackret Sports</a>
                            </div>
                        </div>

                    </td>
                    <td>
                        <a href="/coupons">Coupons</a>
                    </td>
                    <td>
                        <a href="/categories">Categories</a>
                    </td>
                    <td>
                        <a href="/hot-deal">Hot Deals</a>
                    </td>
                    <td>
                        <div class="ui-dropdown">
                            <a>All stores</a>
                            <div class="ui-dropdown__content">
                                <a href="/melinamay">Melinamay</a>
                                <a href="/ebay">Ebay</a>
                                <a href="/aliexpress">AliExpress</a>
                                <a href="/aegean">Aegean</a>
                                <a href="/shop-gr">E-shop GR</a>
                                <a href="/deliveras-gr">Deliveras GR</a>
                                <a href="/zackret">Zackret Sports</a>
                            </div>
                        </div>
                    </td>
                    <td>
                        <a href="/how-it-works">How it works</a>
                    </td>
                    <td>
                        <a href="/help">Help</a>
                    </td>
                </tr>
                </tbody>
            </table>
            <button class="ui-button ui-background--none ui-button--navigation ui-float--right ui-border--none" >
                <span class="ui-icon ui-icon--bars"></span>
            </button>
            <p class="clear"></p>
    
            <!-- end of panelgrid -->
    
        </div>
    </div>
    
    <!-- end of layout-west__footer -->

</div>

<!-- end of layout-west -->



<!--modal sidebar navigation (used on mobile devices)-->

<div   class="ui-modal ui-sidebar--navigation">

    <div class="ui-sidebar__content">
        <button id="closeSidebarBtn" class="ui-button ui-border--none ui-float--right ui-background--none ui-sidebar__close">
            <span class="ui-icon ui-icon--close"></span>
        </button>
        <p class="clear"></p>
        <div class="ui-accordion">
            <div class="ui-accordion__header">
                <span>All Stores</span>
            </div>
            <div class="ui-accordion__content">
                <a href="/melinamay">Melinamay</a>
                <a href="/ebay">Ebay</a>
                <a href="/aliexpress">AliExpress</a>
                <a href="/aegean">Aegean</a>
                <a href="/shop-gr">E-shop GR</a>
                <a href="/deliveras-gr">Deliveras GR</a>
                <a href="/zackret">Zackret Sports</a>
            </div>
        </div>
        <a href="/coupons">Coupons</a>
        <a href="/categories">Categories</a>
        <a href="/hot-deal">Hot Deals</a>
        <div class="ui-accordion">
            <div class="ui-accordion__header">
                <span>All Stores</span>
            </div>
            <div class="ui-accordion__content">
                <a href="/melinamay">Melinamay</a>
                <a href="/ebay">Ebay</a>
                <a href="/aliexpress">AliExpress</a>
                <a href="/aegean">Aegean</a>
                <a href="/shop-gr">E-shop GR</a>
                <a href="/deliveras-gr">Deliveras GR</a>
                <a href="/zackret">Zackret Sports</a>
            </div>
        </div>
        <a href="/how-it-works">How it works</a>
        <a href="/help">Help</a>
    </div>

</div>

<!--modal login-->

<div class="ui-modal ui-dialog ui-dialog--login">
    <table class="ui-width--full ui-height--full ">
        <tbody>
            <tr>
                <td class="ui-vertical-align--middle">
                    <div class="ui-dialog__content">
                        <div class="ui-dialog__header ui-font--medium ui-font-size--18 ui-text-align--center">
                            <span>Login</span>
                            <button id="closeLoginBtn" class="ui-button ui-border--none ui-float--right ui-background--none ui-sidebar__close">
                                <span class="ui-icon ui-icon--close"></span>
                            </button>
                        </div>
<form id="loginForm">
                        <p class="clear"></p>
                        <div class="ui-text-align--center ui-margin-top--10 ui-margin-bottom--7 ui-font-size--17 ui-color--gray">
                            Have a Facebook Account?
                        </div>
                        <a href="#" class="ui-commandlink  ui-commandlink--facebook-login"></a>
                        <div class="ui-text-align--center ui-margin-top--5 ui-font-size--12 ui-color--gray">
                            We'll never post anything without your permission.
                        </div>
                        <div class="ui-text-align--center ui-font-size--14 ui-margin-top--10">
                            — OR —
                        </div>
                        <div class="ui-width--300 ui-margin--center">
                            <input name="email" placeholder="*Email" type="email" pattern="[^@\s]+@[^@\s]+\.[^@\s]+" required class="ui-inputfield ui-width--284 ui-inputfield--dialog ui-margin-top--10" />
                            <input name="password" placeholder="*Password" minlength="6" required type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$"  class="ui-inputfield ui-width--284 ui-inputfield--dialog ui-margin-top--10 ui-margin-bottom--11" />
                        </div>
                        <div class="g-recaptcha ui-margin--center ui-width--300" data-sitekey="6Lf7rEUUAAAAADFrTtVT_fZvsX7HEJfxJN8b-UMW"></div>
                        <div class="ui-width--300 ui-margin--center ui-text-align--center">
                            <button class="ui-width--full ui-button ui-border--none ui-border-radius--none ui-margin-top--10 ui-margin-bottom--11">LOGIN</button>
                            <a href="#" class="ui-commandlink ui-commandlink--gray" >Forgot Password?</a>
                        </div>
</form>
                        <p class="clear ui-height--20"></p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</div>

<!--end of login dialog-->

<div class="ui-modal ui-dialog ui-dialog--register">
    <table class="ui-width--full ui-height--full ">
        <tbody>
        <tr>
            <td class="ui-vertical-align--middle">
                <div class="ui-dialog__content">
                    <div class="ui-dialog__header ui-font--medium ui-font-size--18 ui-text-align--center">
                        <span>Register</span>
                        <button id="closeRegisterBtn" class="ui-button ui-border--none ui-float--right ui-background--none ui-sidebar__close">
                            <span class="ui-icon ui-icon--close"></span>
                        </button>
                    </div>
                    <form id="registerForm">
                        <p class="clear"></p>
                        <div class="ui-text-align--center ui-margin-top--10 ui-margin-bottom--7 ui-font-size--17 ui-color--gray">
                            Have a Facebook Account?
                        </div>
                        <a href="#" class="ui-commandlink  ui-commandlink--facebook-login"></a>
                        <div class="ui-text-align--center ui-margin-top--5 ui-font-size--12 ui-color--gray">
                            We'll never post anything without your permission.
                        </div>
                        <div class="ui-text-align--center ui-font-size--14 ui-margin-top--10">
                            — OR —
                        </div>
                        <div class="ui-width--300 tag example ui-margin--center">

                            <select name="selectC" aria-required="true"  required class="ui search selection ui-margin-top--10 dropdown" id="search-select">
                                <option value="">State</option>
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="CA">California</option>
                                <!-- Saving your scroll sanity !-->
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR">Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                            <input name="registerEmail" placeholder="*Email" type="email" pattern="[^@\s]+@[^@\s]+\.[^@\s]+" required class="ui-inputfield ui-width--284 ui-inputfield--dialog ui-margin-top--10" />
                            <input name="registerPassword" placeholder="*Password" minlength="6" required type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$"  class="ui-inputfield ui-width--284 ui-inputfield--dialog ui-margin-top--10 ui-margin-bottom--11" />
                        </div>
                        <div class="g-recaptcha ui-margin--center ui-width--300" data-sitekey="6Lf7rEUUAAAAADFrTtVT_fZvsX7HEJfxJN8b-UMW"></div>
                        <div class="ui-width--300 ui-margin--center ui-text-align--center">
                            <button class="ui-width--full ui-button ui-border--none ui-border-radius--none ui-margin-top--10 ui-margin-bottom--11">Register</button>
                            <a href="#" class="ui-commandlink ui-commandlink--gray" >Forgot Password?</a>
                        </div>
                    </form>
                    <p class="clear ui-height--20"></p>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

</div>






<script src="grabdid-front-felix/build/js/flag.js"></script>
<script src="grabdid-front-felix/build/js/stickyfill.min.js"></script>


<?php $__env->stopSection(); ?>

                