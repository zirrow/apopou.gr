<?php// var_dump($this->router);exit(); ?>


<?php $__env->startSection('head'); ?>

    <?php echo $__env->make('inc.head', ['head' => $head], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $__env->stopSection(); ?>


<?php $__env->startSection('head-styles'); ?>

    <link rel="stylesheet" href="grabdid-front-felix/build/js/owl.theme.default.min.css" />
    <link rel="stylesheet" href="grabdid-front-felix/build/js/owl.carousel.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="grabdid-front-felix/build/css/style.css" />
    <link rel="stylesheet" type="text/css" href="grabdid-front-felix/build/js/search.min.css" />
    <link rel="stylesheet" type="text/css" href="grabdid-front-felix/build/js/transition.min.css" />
    <link rel="stylesheet" type="text/css" href="grabdid-front-felix/build/js/dropdown.min.css" />
    <script src="grabdid-front-felix/build/js/jquery-3.3.1.min.js"></script>
    <script src="grabdid-front-felix/build/js/ftellipsis.js"></script>
    <script src="grabdid-front-felix/build/js/easing.min.js"></script>
    <script src="grabdid-front-felix/build/js/transition.min.js" ></script>
    <script src="grabdid-front-felix/build/js/dropdown.min.js"></script>
    <script src="grabdid-front-felix/build/js/search.min.js" ></script>
    <script src="grabdid-front-felix/build/js/validate.js"></script> 

<?php $__env->stopSection(); ?>

<?php $__env->startSection('head-scripts'); ?>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('header'); ?>

    <?php echo $__env->make('inc.header', ['header'=>$header], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $__env->stopSection(); ?> 

<?php $__env->startSection('content'); ?>

 

<!-- put main content here -->

<div class="ui-layout-center">
    <div class="ui-layout-center__content">

        <!-- panel component and also a banner panel -->

        <div class="ui-panel ui-panel__banner">
            <div class="ui-panel__content">
                <div class="ui-width--half ui-float--right ui-text-align--center">
              <span class="ui-text--orange ui-padding-left--30 ui-font-size--39 ui-font--light" >
                 Ψωνήστε διαδικτυακά φθηνότερα
              </span>
                    <p class="clear" ></p>
                    <span class="ui-font-size--20 ui-padding-left--30 ui-margin-top--10 ui-text ui-text--dark" >
                        Η No.1 Σελίδα επιστροφής χρημάτων σε Ελλάδα και Κύπρο
                    </span>
                    <p class="clear" ></p>
                    <div class="ui-padding-left--30">
                        <table class="ui-panelgrid ui-margin--center ui-panelgrid--banner-price">
                            <tbody>
                            <tr>
                                <td class="ui-panelgrid__cell ui-first ui-position--relative">
                                    <span class="ui-font-size--36 ui-font--light" >1000+</span>
                                    <p class="clear"></p>
                                    <label class="ui-separator--vertical "></label>
                                    <a href="/stores" class="ui-font-size--18">Stores</a>
                                </td>

                                <!-- end of first panelgrid__cell -->

                                <td class="ui-panelgrid__cell ui-last">
                                    <span class="ui-font-size--36 ui-font--light" >50,000+</span>
                                    <p class="clear"></p>
                                    <a href="/stores" class="ui-font-size--18">Μέλη</a>
                                </td>

                                <!-- end of last panelgrid__cell -->

                            </tr>
                            </tbody>
                        </table>

                        <!-- end of panelgrid -->
                        <a href="<?php echo e(SITE_URL); ?>login.php">
                        <button class="ui-button ui-button--banner ui-border-radius--5 ui-font-size--20">Δωρεάν Εγγραφή - Εγγραφείτε Τώρα</button>
                    </div>
                </div>
                <p class="clear"></p>
            </div>
        </div>

        <!-- end of panel (banner) -->

  <!--        Here is the featured shops block -->
 
        <div class="ui-margin-top--12 ui-margin-bottom--11">
            <span class="ui-font-size--20 ui-font--light"><?=CBE1_HOME_FEATURED_STORES ?></span>
        </div>

        
        
     	<div class="grid-6-col stores-recommended owl-carousel owl-theme" id="owlCarousel">
          	
		<?php $i=0; 
		while( ($row_featured = mysqli_fetch_array($result_featured)) ) {
	?>
	
		 <?php if($i>6){ ?>
       </div>
      
       <!-- ROW 2 -->

       <div class="grid-6-col stores-recommended owl-carousel owl-theme" id="owlCarousel">
        
      <?php  $i-=6;} //#end if ?>
       
           	<div class="grid-6-col__1-of-6">
            <a href="#" class="stores-recommended__rec">
               <img src="images/macys_100x27.gif" alt="store_logo" class="stores-recommended__logo">
               <p class="stores-recommended__old-cashback">was <?= $i ?></p>
               <p class="stores-recommended__cashback">6.0% Cash Back</p>
               <p class="stores-recommended__see-all">See all Macy's Coupons</p>
           </a>
         </div>

     
       
      
       <?php $i++;
			
			} // #end while ?>
			
			</div>

 
    <div class="ui-separator ui-font-size--29 clr">
        <a href="online-katasthmata" class="ui-commandlink">Δες όλα τα καταστήματα</a>
    </div>


        <div class="ui-separator ui-font-size--29 ui-separator--margin ui-text-align--center">
            How
            <span class="ui-state--highlight">Apopou</span>
            works
        </div>

        <!-- end of separator -->

        <table class="ui-panelgrid ui-panelgrid--col-3 ui-panelgrid--basic">
            <tbody>
            <tr>
                <td>
                    <div class="ui-panelgrid__image ui-panelgrid__image--compass" >

                    </div>
                    <div class="ui-panelgrid__subheader ui-font-size--24 ui-text-align--center">
                        Browse a store
                    </div>
                    <div class="ui-panelgrid__output ui-font-size--16 ui-text-align--center" >
                        Find the store you are interested in and just go to the store by clicking on link Go to Store
                    </div>
                </td>
                <td>
                    <div class="ui-panelgrid__image ui-panelgrid__image--cart">

                    </div>
                    <div class="ui-panelgrid__subheader ui-font-size--24 ui-text-align--center">
                        Shop normally
                    </div>
                    <div class="ui-panelgrid__output ui-font-size--16 ui-text-align--center">
                        Now you can Make your online purchases as usual everytime
                    </div>
                </td>
                <td>
                    <div class="ui-panelgrid__image ui-panelgrid__image--refund">

                    </div>
                    <div class="ui-panelgrid__subheader ui-font-size--24 ui-text-align--center">
                        Refund
                    </div>
                    <div class="ui-panelgrid__output ui-font-size--16 ui-text-align--center">
                        The store will pay us a commission because we sent him a customer. In return we will refund your part of the commission we earn
                    </div>
                </td>
            </tr>
            </tbody>
        </table>

        <!--end of panelgrid-->

        
        <!--panelgrid (discount coupons)-->
        <div class="ui-separator ui-font-size--29 ui-separator--margin">
            Τελευταία    <!--    -->
            <span class="ui-state--highlight">Κουπόνια</span>
        </div>
        
        <table class="ui-panelgrid ui-width--full ui-panelgrid--col-4 ui-panelgrid--borders ui-panelgrid--basic">
            <tbody>
            <tr>
                <?php $z=1; 
		while( ($row_coupons = mysqli_fetch_array($top_result)) ) { ?>
<?php if($z % 5) {  ?>
                <td class="ui-panelgrid__cell">
                    <div>
                       <?php if($row_coupons['coupon_type']=='coupon'): ?> <div class="ui-panelgrid__background ui-panelgrid__background--purple">
                        <?php elseif($row_coupons['coupon_type']=='discount'): ?>
                        <div class="ui-panelgrid__background ui-panelgrid__background--blue">
                        <?php else: ?> <!-- printable -->
                        <div class="ui-panelgrid__background ui-panelgrid__background--pink">
                        <?php endif; ?>
                            <div class="ui-text-align--center">
                                    <span class="ui-font-size--25 ui-font--light ui-color--white ui-float--left">
                                        <?php echo e($row_coupons['coupon_title']); ?>

                                        
                                    </span>
                                    <p class="clear">
                                    <span class="ui-font-size--85 ui-line-height--full ui-font--medium ui-color--white"> 
                                   <!-- <img src="https://image.flaticon.com/icons/png/512/287/287585.png" width="85" /> -->
                                    </span>
                                    <span class="ui-font-size--36 ui-font--medium ui-color--white"><?php echo e($row_coupons['coupon_type']); ?></span>
                                                                                            </p></div>
                        </div>
                        <div class="ui-panelgrid__subheader ui-font-size--22">
                            <a href="https://www.apopou.gr/koyponia/bula/283"><?php echo e($row_coupons['title']); ?></a>
                        </div>
                        <div class="ui-panelgrid__output ui-font-size--16 ui-padding-left--13">
                            <?= ($row_coupons['cashback']>0 ?  DisplayCashback($row_coupons['cashback']): 'Έως '.abs(DisplayCashback($row_coupons['cashback'])).'%'); ?> Επιστροφή Χρημάτων
                        </div>
                        <div class="ui-panelgrid__footer">
                            <button class="ui-button ui-border-radius--4 ui-font-size--12 ui-button--orange-light">Get <?php echo e($row_coupons['coupon_type']); ?> </button>
                        </div>
                    </div>
                </td>
                <?php } else { ?>
                     
                </tr>  </tbody>
        </table>  <table class="ui-panelgrid ui-width--full ui-panelgrid--col-4 ui-panelgrid--borders ui-panelgrid--basic ui-margin-top--25"> <tr>
                <?php } // #end if-else ?>
<?php $z++; } //#end while ?>

            </tr>
            </tbody>
        </table>
    <!--    
        <table class="ui-panelgrid ui-width--full ui-panelgrid--col-4 ui-panelgrid--borders ui-panelgrid--basic ui-margin-top--25">
            <tbody>
            <tr>
                <td class="ui-panelgrid__cell">
                    <div>
                        <div class="ui-panelgrid__background ui-panelgrid__background--logo ui-panelgrid__background--market">

                        </div>
                        <div class="ui-panelgrid__subheader ui-font-size--22">
                            World Market
                        </div>
                        <div class="ui-panelgrid__output ui-font-size--16 ui-padding-left--13" >
                            Up to 20% discount on selected products
                        </div>
                        <div class="ui-panelgrid__footer" >
                            <button class="ui-button ui-border-radius--4 ui-font-size--12 ui-button--green-light">View Coupon</button>
                        </div>
                    </div>
                </td>
                <td class="ui-panelgrid__cell">
                    <div>
                        <div class="ui-panelgrid__background ui-panelgrid__background--logo ui-panelgrid__background--bonton">

                        </div>
                        <div class="ui-panelgrid__subheader ui-font-size--22">
                            Bonton
                        </div>
                        <div class="ui-panelgrid__output ui-font-size--16 ui-padding-left--13" >
                            Up to 20% discount on selected products
                        </div>
                        <div class="ui-panelgrid__footer" >
                            <button class="ui-button ui-border-radius--4 ui-font-size--12 ui-button--green-light">View Coupon</button>
                        </div>
                    </div>
                </td>
                <td class="ui-panelgrid__cell">
                    <div>
                        <div class="ui-panelgrid__background ui-panelgrid__background--logo ui-panelgrid__background--robot">

                        </div>
                        <div class="ui-panelgrid__subheader ui-font-size--22">
                            iRobot
                        </div>
                        <div class="ui-panelgrid__output ui-font-size--16 ui-padding-left--13" >
                            Up to 40% discount on selected products
                        </div>
                        <div class="ui-panelgrid__footer" >
                            <button class="ui-button ui-border-radius--4 ui-font-size--12 ui-button--green-light">View Coupon</button>
                        </div>
                    </div>
                </td>
                <td class="ui-panelgrid__cell">
                    <div>
                        <div class="ui-panelgrid__background ui-panelgrid__background--logo ui-panelgrid__background--nike">

                        </div>
                        <div class="ui-panelgrid__subheader ui-font-size--22">
                            Nike Store
                        </div>
                        <div class="ui-panelgrid__output ui-font-size--16 ui-padding-left--13" >
                            Up to 30% discount on selected products
                        </div>
                        <div class="ui-panelgrid__footer" >
                            <button class="ui-button ui-border-radius--4 ui-font-size--12 ui-button--green-light">View Coupon</button>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="ui-separator ui-font-size--29 ui-separator--margin clr">
            <a href="#" class="ui-commandlink">Περισσότερα Κουπόνια</a>
        </div>
-->
        <!--end of panelgrid (discount coupons)-->



   <!--        Here is the featured shops block -->
 
        <div class="ui-margin-top--12 ui-margin-bottom--11">
            <span class="ui-font-size--20 ui-font--light"><?=CBE1_HOME_NEW_STORES ?></span>
        </div>

        
        
     	<div class="grid-6-col stores-recommended owl-carousel owl-theme" id="owlCarousel">
          	
		<?php $j=1; 
		while( ($row_featured = mysqli_fetch_array($n_result)) && ($j<=12) ) { ?>
	<?php	if($j<=6){ ?>
<?php	// ROW 1   ?>
	     <div class="grid-6-col__1-of-6">
	         <?php// var_dump('we are here 1');// exit(); ?>
	         <?php// var_dump($router); exit(); ?>
             <a href="<?php echo e(GetRetailerLink($row_featured['id'],$row_featured['title'])); ?>" class="stores-recommended__rec">
                 
                <img src="<?php if (!stristr($row_featured['image'], 'http')) echo SITE_URL."images/apopou/gr/stores/logos/"; echo $row_featured['image']; ?>" width="<?php echo IMAGE_WIDTH; ?>" height="<?php echo IMAGE_HEIGHT; ?>" alt="<?php echo $row_featured['title']; ?>" title="<?php echo $row_featured['title']; ?>" border="0" />
                <?php if(!empty($row_featured['old_cashback'])): ?>
                <p class="stores-recommended__old-cashback">Ήταν <?php echo e($row_featured['old_cashback']); ?>%</p>
                <?php endif; ?>
                <p class="stores-recommended__cashback"><?php echo e($row_featured['cashback']); ?> Cash Back</p>
                <p class="stores-recommended__see-all">Δες τα όλα από <?php echo e($row_featured['title']); ?> </p>
            </a>
          </div>
  <?php	//end of ROW 1   ?> 
  <?php	// ROW 2  ?>
        
          <?php } else if($j%7==0) { ?>
                       </div> 
          <div class="grid-6-col stores-recommended owl-carousel owl-theme" id="owlCarousel">
          <?php } else { ?>
            <div class="grid-6-col__1-of-6">
             <a href="#" class="stores-recommended__rec">
                <img src="images/apopou/gr/stores/logos/<?php echo e($row_featured['title']); ?>.gif" alt="store_logo" class="stores-recommended__logo" width="100" height="27">
                <?php if(!empty($row_featured['old_cashback'])): ?>
                <p class="stores-recommended__old-cashback">Ήταν <?php echo e($row_featured['old_cashback']); ?></p>
                <?php endif; ?>
                <p class="stores-recommended__cashback"><?php echo e($row_featured['cashback']); ?>% Cash Back</p>
                <p class="stores-recommended__see-all">Δες τα όλα από <?php echo e($row_featured['title']); ?> </p>
            </a>
            </div>
			<?php } // #end if-else ?>	 
			<?php 
			//var_dump($result_featured);
			$j++;
			} // #end while ?>
			
			</div>

 <?php	//end of ROW 2   ?>
 
 
    <div class="ui-separator ui-font-size--29 clr">
        <a href="online-katasthmata" class="ui-commandlink">Δες όλα τα καταστήματα</a>
    </div>

 <!--        End of featured shops block -->

      

        <div class="ui-separator ui-font-size--29 ui-separator--margin ui-text-align--center">
            What our
            <span class="ui-state--highlight">members</span>
            say
        </div>
        <table class="ui-panelgrid ui-width--full ui-panelgrid--col-3  ui-panelgrid--testemonials ui-panelgrid--borders ui-panelgrid--basic">
            <tbody>
            <tr>
                <td>
                    <div class="ui-border-radius--5">
                        <div class="ui-testemonials-grid">
                            <div class=" ui-first ui-float--left">
                                <div class="ui-testemonials-grid__subheader ui-font-size--19">
                                    Ελπινίκη
                                </div>
                                <div class="ui-testemonials-grid__output ui-font-size--13 ui-padding-left--23" >
                                    Unbelievable with one extra click you can save a lot of money
                                </div>
                            </div>
                            <div class="ui-last ui-float--left">
                                <img class="ui-border-radius--full" src="images/testemonials-icon-1.png" />
                            </div>
                            <p class="clear" />
                            <div class="ui-testemonials-grid__footer">
                                <span class="ui-color--gray ui-font-size--13">member since 2012</span>
                                <span class="ui-float--right ui-font--medium">Saved <span class="ui-state--highlight">1690 €</span></span>
                                <p class="clear" />
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="ui-border-radius--5">
                        <div class="ui-testemonials-grid">
                            <div class=" ui-first ui-float--left">
                                <div class="ui-testemonials-grid__subheader ui-font-size--19">
                                    Γιώργος
                                </div>
                                <div class="ui-testemonials-grid__output ui-font-size--13 ui-padding-left--23" >
                                    Although I do not shop very often online I always come through...
                                </div>
                            </div>
                            <div class="ui-last ui-float--left">
                                <img class="ui-border-radius--full" src="images/testemonials-icon-1.png" />
                            </div>
                            <p class="clear" />
                            <div class="ui-testemonials-grid__footer">
                                <span class="ui-color--gray ui-font-size--13">member since 2015</span>
                                <span class="ui-float--right ui-font--medium">Saved <span class="ui-state--highlight">1690 €</span></span>
                                <p class="clear" />
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="ui-border-radius--5">
                        <div class="ui-testemonials-grid">
                            <div class=" ui-first ui-float--left">
                                <div class="ui-testemonials-grid__subheader ui-font-size--19">
                                    Κλεάνθης
                                </div>
                                <div class="ui-testemonials-grid__output ui-font-size--13 ui-padding-left--23" >
                                    Unbelievable with one extra click you can save a lot of money
                                </div>
                            </div>
                            <div class="ui-last ui-float--left">
                                <img class="ui-border-radius--full" src="images/testemonials-icon-1.png" />
                            </div>
                            <p class="clear" />
                            <div class="ui-testemonials-grid__footer">
                                <span class="ui-color--gray ui-font-size--13">member since 2012</span>
                                <span class="ui-float--right ui-font--medium">Saved <span class="ui-state--highlight">1690 €</span></span>
                                <p class="clear" />
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>

        <!--end of panelgrid (testemonials)-->

    </div>
</div>
<!-- end of layout-center -->





<!--top button-->
<div id="backToTheTop"> <span class="topIcon" /> </div>

<!-- help button -->
<a href="#" class="helpLink ui-button ui-button--green"><span class="fa fa-question-circle"></span> Help</a>

<script src="grabdid-front-felix/build/js/owl.carousel.min.js"></script>
<script src="grabdid-front-felix/build/js/carousel.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="grabdid-front-felix/build/js/main.js"></script>
<script>
    $("#loginForm").validate();
    $("#registerForm").validate();
    var content = [
        { title: 'Andorra', description: '10% discount' },
        { title: 'United Arab Emirates', description: '10% discount' },
        { title: 'Afghanistan', description: '10% discount' },
        { title: 'Antigua', description: '10% discount' },
        { title: 'Anguilla', description: '10% discount' },
        { title: 'Albania', description: '10% discount' },
        { title: 'Armenia', description: '10% discount' },
        { title: 'Netherlands Antilles', description: '10% discount' },
        { title: 'Angola', description: '10% discount' },
        { title: 'Argentina', description: '10% discount' },
        { title: 'American Samoa', description: '10% discount' },
        { title: 'Austria', description: '10% discount' },
        { title: 'Australia', description: '10% discount' },
        { title: 'Aruba', description: '10% discount' },
        { title: 'Aland Islands', description: '10% discount' },
        { title: 'Azerbaijan', description: '10% discount' },
        { title: 'Bosnia', description: '10% discount' },
        { title: 'Barbados', description: '10% discount' },
        { title: 'Bangladesh', description: '10% discount' },
        { title: 'Belgium', description: '10% discount' },
        { title: 'Burkina Faso', description: '10% discount' },
        { title: 'Bulgaria', description: '10% discount' },
        { title: 'Bahrain', description: '10% discount' },
        { title: 'Burundi', description: '10% discount' }
        // etc
    ];
    $('.ui.search')
        .search({
            source: content
        })
    ;
    $('.tag.example .ui.dropdown')
        .dropdown({
            allowAdditions: true,
            minSelections: 1
        })
    ;
</script>


 
 
 

<?php $__env->stopSection(); ?> 


<?php $__env->startSection('footer'); ?>

    <?php echo $__env->make('inc.footer', ['fc' => $fc], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>