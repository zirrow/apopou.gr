<!DOCTYPE html>
<html>
    <head>
        <?php echo $__env->yieldContent('head'); ?>
        <?php echo $__env->yieldContent('head-styles'); ?>
        <?php echo $__env->yieldContent('head-scripts'); ?>

    </head>
    <body>
        <?php echo $__env->yieldContent('header'); ?>
        <?php echo $__env->yieldContent('content'); ?>
        <?php echo $__env->yieldContent('footer'); ?>
        <?php echo $__env->yieldContent('footer-scripts'); ?>
    </body>
</html>
