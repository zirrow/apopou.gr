
<title><?= PAGE_TITLE ?> | <?= SITE_TITLE ?></title>
    <?php if (PAGE_DESCRIPTION != "") : ?>
      <meta name="description" content="<?= PAGE_DESCRIPTION ?>" />
    <?php endif ?>
     <?php if (PAGE_KEYWORDS != "") :?>
         <meta name="keywords" content="<?= PAGE_KEYWORDS ?>" />
     <?php endif?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <!--  <meta name="author" content="CashbackEngine.net" />-->
    <!-- N.R. remove robots index temporarily -->
    <!--<meta name="robots" content="index, follow" /> -->

    <!-- N.R. CSS reset -->
    <!-- <link rel="stylesheet" href="<?php echo e($consts['SITE_URL']); ?>css/reset.css"> -->
    <!--N.R. Modernizr -->
   <!--  <script src="<?php echo e($consts['SITE_URL']); ?>js/modernizr.js"></script> -->


  <!--  <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet" type="text/css" /> -->
  <!--  <link rel="stylesheet" type="text/css" href="<?php echo e($consts['SITE_URL']); ?>css/style.css" /> -->
   <!-- <script type="text/javascript" src="<?php echo e($consts['SITE_URL']); ?>js/jquery.min.js"></script> -->
    <?php if (FACEBOOK_CONNECT == 1 && FACEBOOK_APPID != "" && FACEBOOK_SECRET != ""): ?> 
   <!-- <script type="text/javascript" src="https://connect.facebook.net/en_US/all.js#appId=<?php echo e($mc['FACEBOOK_APPID']); ?>&amp;xfbml=1"></script> -->
    <?php endif ?>
    <?php if (ADDTHIS_SHARE == 1): ?>
   <!--  <script type="text/javascript" src="https://s7.addthis.com/js/250/addthis_widget.js#username=<?php echo e($mc['ADDTHIS_ID']); ?>"></script> -->
    <?php endif ?>
   <!--  <script type="text/javascript" async src="//platform.twitter.com/widgets.js"></script> -->
    <!-- <script type="text/javascript" src="<?php echo e($consts['SITE_URL']); ?>js/autocomplete.js"></script> -->
   <!--  <script type="text/javascript" src="<?php echo e($consts['SITE_URL']); ?>js/jsCarousel.js"></script> -->
   <!--  <script type="text/javascript" src="<?php echo e($consts['SITE_URL']); ?>js/clipboard.js"></script> -->
  <!--  <script type="text/javascript" src="<?php echo e($consts['SITE_URL']); ?>js/cashbackengine.js"></script> -->
 <!--   <script type="text/javascript" src="<?php echo e($consts['SITE_URL']); ?>js/easySlider1.7.js"></script> -->
    <!-- N.R. resource not found
	<script type="text/javascript" src="<?php echo e($consts['SITE_URL']); ?>js/jquery.tools.tabs.min.js"></script>
	end -->
    <link type="image/ico" rel="shortcut icon" href="<?=SITE_URL?>images/apopou/<?php echo e($GLOBALS['top_level_domain']); ?>/theme/favicon.ico" />
    <!--<link rel="icon" type="image/ico" href="<?//=SITE_URL?>favicon.ico" />-->
    <meta property="og:title" content="<?=PAGE_TITLE?>" />
    <meta property="og:url" content="<?=SITE_URL?>" />
    <meta property="og:description" content="<?=PAGE_DESCRIPTION?>" />
    <meta property="og:image" content="<?=SITE_URL?>images/logo.png" />
    <?=GOOGLE_ANALYTICS?>

    <!--Start of Tawk.to Script-->
<!--    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5a072258198bd56b8c03a909/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script> -->
    <!--End of Tawk.to Script-->

    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<!--   Cookies message 
https://cookieconsent.insites.com/download/#
-->        
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#eaf7f7",
      "text": "#5c7291"
    },
    "button": {
      "background": "#56cbdb",
      "text": "#ffffff"
    }
  },
  "content": {
    "message": "Η σελίδα μας χρησιμοποιεί cookies για να έχετε την καλύτερη δυνατή εμπειρία.",
    "dismiss": "Εντάξει",
    "link": "Μάθε Περισσότερα",
    "href": "https://apopou.gr/cookiepolicy"
  }
})});
</script>

    <link rel="stylesheet" href="<?=SITE_URL?>grabdid-front-felix/build/js/owl.theme.default.min.css" />
    <link rel="stylesheet" href="<?=SITE_URL?>grabdid-front-felix/build/js/owl.carousel.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?=SITE_URL?>grabdid-front-felix/build/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_URL?>grabdid-front-felix/build/js/search.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_URL?>grabdid-front-felix/build/js/transition.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_URL?>grabdid-front-felix/build/js/dropdown.min.css" />
    <script src="<?=SITE_URL?>grabdid-front-felix/build/js/jquery-3.3.1.min.js"></script>
    <script src="<?=SITE_URL?>grabdid-front-felix/build/js/ftellipsis.js"></script>
    <script src="<?=SITE_URL?>grabdid-front-felix/build/js/easing.min.js"></script>
    <script src="<?=SITE_URL?>grabdid-front-felix/build/js/transition.min.js" ></script>
    <script src="<?=SITE_URL?>grabdid-front-felix/build/js/dropdown.min.js"></script>
    <script src="<?=SITE_URL?>grabdid-front-felix/build/js/search.min.js" ></script>
    <script src="<?=SITE_URL?>grabdid-front-felix/build/js/validate.js"></script>
        