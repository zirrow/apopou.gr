<?php $__env->startSection('head'); ?>

        <?php echo $__env->make('inc.head', ['meta' => $meta, 'mc' => $mc], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('head-styles'); ?>
        
    <link rel="stylesheet" type="text/css" href="<?php echo e($consts['SITE_URL']); ?>css/grabdid_com/style.css" />

    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head-scripts'); ?>
    <script src="<?php echo e($consts['SITE_URL']); ?>js/grabdid_com/jquery-3.3.1.min.js"></script>
    <script src="<?php echo e($consts['SITE_URL']); ?>js/grabdid_com/ftellipsis.js"></script>
    <script src="<?php echo e($consts['SITE_URL']); ?>js/grabdid_com/easing.min.js"></script>
    <script src="<?php echo e($consts['SITE_URL']); ?>js/grabdid_com/transition.min.js" ></script>
    <script src="<?php echo e($consts['SITE_URL']); ?>js/grabdid_com/dropdown.min.js"></script>
    <script src="<?php echo e($consts['SITE_URL']); ?>js/grabdid_com/search.min.js" ></script>
    <script src="<?php echo e($consts['SITE_URL']); ?>js/grabdid_com/validate.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo e($consts['SITE_URL']); ?>js/grabdid_com/search.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e($consts['SITE_URL']); ?>js/grabdid_com/transition.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e($consts['SITE_URL']); ?>js/grabdid_com/dropdown.min.css" />
<?php $__env->stopSection(); ?>



<?php $__env->startSection('header'); ?>

  <?php echo $__env->make('inc.header', ['meta' => $meta, 'mc' => $mc], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  
<?php $__env->stopSection(); ?> 



<?php $__env->startSection('content'); ?>

	<h1><?php echo CBE1_COUPONS_TITLE; ?></h1>

		<div id="tabs_container">
			<ul id="tabs">
				<li class="active"><a href="#all"><span><?php echo CBE1_COUPONS_ALL; ?></span></a></li>
				<li><a href="#top-coupons"><span><?php echo CBE1_COUPONS_POPULAR; ?></span></a></li>
				<?php if($exclusive_coupons_total > 0): ?> 
				<li><a href="#exclusive"><span><?php echo CBE1_COUPONS_EXCLUSIVE; ?></span></a></li>
				<?php endif; ?>
				<li><a href="#latest"><span><?php echo CBE1_COUPONS_LATEST; ?></span></a></li>
				<?php if($expiring_coupons_total > 0): ?>
				<li><a href="#expiring"><span><?php echo CBE1_COUPONS_EXPIRING; ?></span></a></li>
				<?php endif; ?>
			</ul>
		</div>

		<div id="all" class="tab_content">
		

		<?php if($total > 0): ?> 

	
		<div class="browse_top">
			<div class="sortby">
				<form action="" id="form1" name="form1" method="get">
					<span><?php echo CBE1_SORT_BY; ?>:</span>
					<select name="column" id="column" onChange="document.form1.submit()">
						<option value="added" <?php if ($_GET['column'] == "added") echo "selected"; ?>><?php echo CBE1_COUPONS_SDATE; ?></option>
						<option value="visits" <?php if ($_GET['column'] == "visits") echo "selected"; ?>><?php echo CBE1_COUPONS_SPOPULAR; ?></option>
						<option value="retailer_id" <?php if ($_GET['column'] == "retailer_id") echo "selected"; ?>><?php echo CBE1_COUPONS_SSTORE; ?></option>
						<option value="end_date" <?php if ($_GET['column'] == "end_date") echo "selected"; ?>><?php echo CBE1_COUPONS_SEND; ?></option>
					</select>
					<select name="order" id="order" onChange="document.form1.submit()">
						<option value="desc" <?php if ($_GET['order'] == "desc") echo "selected"; ?>><?php echo CBE1_SORT_DESC; ?></option>
						<option value="asc" <?php if ($_GET['order'] == "asc") echo "selected"; ?>><?php echo CBE1_SORT_ASC; ?></option>
					</select>
					<input type="hidden" name="page" value="<?php echo $page; ?>" />
				</form>
			</div>
			<div class="results">
				<?php echo CBE1_RESULTS_SHOWING; ?> <?php echo ($from + 1); ?> - <?php echo min($from + $total_on_page, $total); ?> <?php echo CBE1_RESULTS_OF; ?> <?php echo $total; ?>
			</div>
		</div>

			

				<?php echo ShowPagination("coupons",$results_per_page,"coupons.php?column=$rrorder&order=$rorder&","WHERE ".$where); ?>

			<?php else: ?>
				<p align="center"><?php echo CBE1_COUPONS_NO; ?></p>
				<div class="sline"></div>
		   <?php endif; ?> 
		</div>


		<div id="top-coupons" class="tab_content">
		


				<?php if($top_total > 0): ?>
				
				<table align="center" width="100%" border="0" cellspacing="0" cellpadding="5">
				<?php while ($top_row = mysqli_fetch_array($top_result)) { $cc++; ?>
				<tr>
					<td class="td_coupon" width="<?php echo IMAGE_WIDTH; ?>" align="center" valign="top">
						<?php if ($top_row['exclusive'] == 1) { ?><span class="exclusive" alt="<?php echo CBE1_COUPONS_EXCLUSIVE; ?>" title="<?php echo CBE1_COUPONS_EXCLUSIVE; ?>"><?php echo CBE1_COUPONS_EXCLUSIVE; ?></span><?php } ?>
						<div class="imagebox"><a href="<?php echo GetRetailerLink($top_row['retailer_id'], $tops_row['title']); ?>"><img src="<?php if (!stristr($top_row['image'], 'http')) echo SITE_URL."img/"; echo $top_row['image']; ?>" width="<?php echo IMAGE_WIDTH; ?>" height="<?php echo IMAGE_HEIGHT; ?>" alt="<?php echo $top_row['title']; ?>" title="<?php echo $top_row['title']; ?>" border="0" /></a></div>
						<br/><a class="more" href="<?php echo GetRetailerLink($top_row['retailer_id'], $tops_row['title']); ?>#coupons"><?php echo CBE1_COUPONS_SEEALL; ?></a>
					</td>
					<td width="80%" class="td_coupon" align="left" valign="top">
						<span class="coupon_name"><?php echo $top_row['title']; ?> <a href="<?php echo SITE_URL; ?>go2store.php?id=<?php echo $top_row['retailer_id']; ?>&c=<?php echo $top_row['coupon_id']; ?>" target="_blank"><?php echo $top_row['coupon_title']; ?></a></span>
						<?php echo ($top_row['visits'] > 0) ? "<span class='coupon_times_used'><sup>".$top_row['visits']." ".CBE1_COUPONS_TUSED."</sup></span>" : ""; ?>
						<br/>
						<?php if ($top_row['description'] != "") { ?><div class="coupon_description"><?php echo TruncateText($top_row['description'], COUPONS_DESCRIPTION_LIMIT, $more_link = 1); ?>&nbsp;</div><?php } ?>
						<?php if ($top_row['end_date'] != "0000-00-00 00:00:00") { ?>
							<span class="expires"><?php echo CBE1_COUPONS_EXPIRES; ?>: <?php echo $top_row['coupon_end_date']; ?></span> &nbsp; 
							<span class="time_left"><?php echo CBE1_COUPONS_TIMELEFT; ?>: <?php echo GetTimeLeft($top_row['time_left']); ?></span>
						<?php } ?>
					</td>
					<td class="td_coupon" align="left" valign="bottom">
						<?php if ($top_row['code'] != "") { ?><span class="coupon_code"><?php echo (HIDE_COUPONS == 0 || isLoggedIn()) ? $top_row['code'] : CBE1_COUPONS_CODE_HIDDEN; ?></span><?php } ?>
						<a class="go2store" href="<?php echo SITE_URL; ?>go2store.php?id=<?php echo $top_row['retailer_id']; ?>&c=<?php echo $top_row['coupon_id']; ?>" target="_blank"><?php echo ($top_row['code'] != "") ? CBE1_COUPONS_LINK : CBE1_COUPONS_LINK2; ?></a>
					</td>
				</tr>
				<?php } ?>
				</table>

				
				<?php else: ?>
					<p align="center"><?php echo CBE1_COUPONS_NO; ?></p>
					<div class="sline"></div>
				<?php endif; ?> 
		</div>


		<div id="latest" class="tab_content">
		<?php


				if ($last_total > 0)
				{
			?>
				<table align="center" width="100%" border="0" cellspacing="0" cellpadding="5">
				<?php while ($last_row = mysqli_fetch_array($last_result)) { $cc++; ?>
				<tr>
					<td class="td_coupon" width="<?php echo IMAGE_WIDTH; ?>" align="center" valign="top">
						<?php if ($last_row['exclusive'] == 1) { ?><span class="exclusive" alt="<?php echo CBE1_COUPONS_EXCLUSIVE; ?>" title="<?php echo CBE1_COUPONS_EXCLUSIVE; ?>"><?php echo CBE1_COUPONS_EXCLUSIVE; ?></span><?php } ?>
						<div class="imagebox"><a href="<?php echo GetRetailerLink($last_row['retailer_id'], $last_row['title']); ?>"><img src="<?php if (!stristr($last_row['image'], 'http')) echo SITE_URL."img/"; echo $last_row['image']; ?>" width="<?php echo IMAGE_WIDTH; ?>" height="<?php echo IMAGE_HEIGHT; ?>" alt="<?php echo $last_row['title']; ?>" title="<?php echo $last_row['title']; ?>" border="0" /></a></div>
						<br/><a class="more" href="<?php echo GetRetailerLink($last_row['retailer_id'], $last_row['title']); ?>#coupons"><?php echo CBE1_COUPONS_SEEALL; ?></a>
					</td>
					<td width="80%" class="td_coupon" align="left" valign="top">
						<span class="coupon_name"><?php echo $last_row['title']; ?> <a href="<?php echo SITE_URL; ?>go2store.php?id=<?php echo $last_row['retailer_id']; ?>&c=<?php echo $last_row['coupon_id']; ?>" target="_blank"><?php echo $last_row['coupon_title']; ?></a></span>
						<?php echo ($last_row['visits'] > 0) ? "<span class='coupon_times_used'><sup>".$last_row['visits']." ".CBE1_COUPONS_TUSED."</sup></span>" : ""; ?>
						<br/>
						<?php if ($last_row['description'] != "") { ?><div class="coupon_description"><?php echo TruncateText($last_row['description'], COUPONS_DESCRIPTION_LIMIT, $more_link = 1); ?>&nbsp;</div><?php } ?>
						<?php if ($last_row['end_date'] != "0000-00-00 00:00:00") { ?>
							<span class="expires"><?php echo CBE1_COUPONS_EXPIRES; ?>: <?php echo $last_row['coupon_end_date']; ?></span> &nbsp; 
							<span class="time_left"><?php echo CBE1_COUPONS_TIMELEFT; ?>: <?php echo GetTimeLeft($last_row['time_left']); ?></span>
						<?php } ?>
					</td>
					<td class="td_coupon" align="left" valign="bottom">
						<?php if ($last_row['code'] != "") { ?><span class="coupon_code"><?php echo (HIDE_COUPONS == 0 || isLoggedIn()) ? $last_row['code'] : CBE1_COUPONS_CODE_HIDDEN; ?></span><?php } ?>
						<a class="go2store" href="<?php echo SITE_URL; ?>go2store.php?id=<?php echo $last_row['retailer_id']; ?>&c=<?php echo $last_row['coupon_id']; ?>" target="_blank"><?php echo ($last_row['code'] != "") ? CBE1_COUPONS_LINK : CBE1_COUPONS_LINK2; ?></a>
					</td>
				</tr>
				<?php } ?>
				</table>

				<?php }else{ ?>
					<p align="center"><?php echo CBE1_COUPONS_NO; ?></p>
					<div class="sline"></div>
				<?php } ?>
		</div>


		<div id="exclusive" class="tab_content">
		<?php


				if ($ex_total > 0)
				{
			?>
				<table align="center" width="100%" border="0" cellspacing="0" cellpadding="5">
				<?php while ($ex_row = mysqli_fetch_array($ex_result)) { $cc++; ?>
				<tr>
					<td class="td_coupon" width="<?php echo IMAGE_WIDTH; ?>" align="center" valign="top">
						<?php if ($ex_row['exclusive'] == 1) { ?><span class="exclusive" alt="<?php echo CBE1_COUPONS_EXCLUSIVE; ?>" title="<?php echo CBE1_COUPONS_EXCLUSIVE; ?>"><?php echo CBE1_COUPONS_EXCLUSIVE; ?></span><?php } ?>
						<div class="imagebox"><a href="<?php echo GetRetailerLink($ex_row['retailer_id'], $ex_row['title']); ?>"><img src="<?php if (!stristr($ex_row['image'], 'http')) echo SITE_URL."img/"; echo $ex_row['image']; ?>" width="<?php echo IMAGE_WIDTH; ?>" height="<?php echo IMAGE_HEIGHT; ?>" alt="<?php echo $ex_row['title']; ?>" title="<?php echo $ex_row['title']; ?>" border="0" /></a></div>
						<br/><a class="more" href="<?php echo GetRetailerLink($ex_row['retailer_id'], $ex_row['title']); ?>#coupons"><?php echo CBE1_COUPONS_SEEALL; ?></a>
					</td>
					<td width="80%" class="td_coupon" align="left" valign="top">
						<span class="coupon_name"><?php echo $ex_row['title']; ?> <a href="<?php echo SITE_URL; ?>go2store.php?id=<?php echo $ex_row['retailer_id']; ?>&c=<?php echo $ex_row['coupon_id']; ?>" target="_blank"><?php echo $ex_row['coupon_title']; ?></a></span>
						<?php echo ($ex_row['visits'] > 0) ? "<span class='coupon_times_used'><sup>".$ex_row['visits']." ".CBE1_COUPONS_TUSED."</sup></span>" : ""; ?>
						<br/>
						<?php if ($ex_row['description'] != "") { ?><div class="coupon_description"><?php echo TruncateText($ex_row['description'], COUPONS_DESCRIPTION_LIMIT, $more_link = 1); ?>&nbsp;</div><?php } ?>
						<?php if ($ex_row['end_date'] != "0000-00-00 00:00:00") { ?>
							<span class="expires"><?php echo CBE1_COUPONS_EXPIRES; ?>: <?php echo $ex_row['coupon_end_date']; ?></span> &nbsp; 
							<span class="time_left"><?php echo CBE1_COUPONS_TIMELEFT; ?>: <?php echo GetTimeLeft($ex_row['time_left']); ?></span>
						<?php } ?>
					</td>
					<td class="td_coupon" align="left" valign="bottom">
						<?php if ($ex_row['code'] != "") { ?><span class="coupon_code"><?php echo (HIDE_COUPONS == 0 || isLoggedIn()) ? $ex_row['code'] : CBE1_COUPONS_CODE_HIDDEN; ?></span><?php } ?>
						<a class="go2store" href="<?php echo SITE_URL; ?>go2store.php?id=<?php echo $ex_row['retailer_id']; ?>&c=<?php echo $ex_row['coupon_id']; ?>" target="_blank"><?php echo ($ex_row['code'] != "") ? CBE1_COUPONS_LINK : CBE1_COUPONS_LINK2; ?></a>
					</td>
				</tr>
				<?php } ?>
				</table>

				<?php }else{ ?>
					<p align="center"><?php echo CBE1_COUPONS_NO; ?></p>
					<div class="sline"></div>
				<?php } ?>
		</div>


		<div id="expiring" class="tab_content">
		<?php
				$cc = 0;
				// show expires in 3 days coupons //
				$exp_query = "SELECT c.*, DATE_FORMAT(c.end_date, '".DATE_FORMAT."') AS coupon_end_date, UNIX_TIMESTAMP(c.end_date) - UNIX_TIMESTAMP() AS time_left, c.title AS coupon_title, r.image, r.title FROM cashbackengine_coupons c LEFT JOIN cashbackengine_retailers r ON c.retailer_id=r.retailer_id WHERE c.end_date!='0000-00-00 00:00:00' AND (c.end_date BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 DAY)) AND c.status='active' AND (r.end_date='0000-00-00 00:00:00' OR r.end_date > NOW()) AND r.status='active' ORDER BY c.added DESC LIMIT $results_per_page";
				$exp_result = smart_mysql_query($exp_query);
				$exp_total = mysqli_num_rows($exp_result);

				if ($exp_total > 0)
				{
			?>
				<table align="center" width="100%" border="0" cellspacing="0" cellpadding="5">
				<?php while ($exp_row = mysqli_fetch_array($exp_result)) { $cc++; ?>
				<tr>
					<td class="td_coupon" width="<?php echo IMAGE_WIDTH; ?>" align="center" valign="top">
						<?php if ($exp_row['exclusive'] == 1) { ?><span class="exclusive" alt="<?php echo CBE1_COUPONS_EXCLUSIVE; ?>" title="<?php echo CBE1_COUPONS_EXCLUSIVE; ?>"><?php echo CBE1_COUPONS_EXCLUSIVE; ?></span><?php } ?>
						<div class="imagebox"><a href="<?php echo GetRetailerLink($exp_row['retailer_id'], $exp_row['title']); ?>"><img src="<?php if (!stristr($exp_row['image'], 'http')) echo SITE_URL."img/"; echo $exp_row['image']; ?>" width="<?php echo IMAGE_WIDTH; ?>" height="<?php echo IMAGE_HEIGHT; ?>" alt="<?php echo $exp_row['title']; ?>" title="<?php echo $exp_row['title']; ?>" border="0" /></a></div>
						<br/><a class="more" href="<?php echo GetRetailerLink($exp_row['retailer_id'], $exp_row['title']); ?>#coupons"><?php echo CBE1_COUPONS_SEEALL; ?></a>
					</td>
					<td width="80%" class="td_coupon" align="left" valign="top">
						<span class="coupon_name"><?php echo $exp_row['title']; ?> <a href="<?php echo SITE_URL; ?>go2store.php?id=<?php echo $exp_row['retailer_id']; ?>&c=<?php echo $exp_row['coupon_id']; ?>" target="_blank"><?php echo $exp_row['coupon_title']; ?></a></span>
						<?php echo ($exp_row['visits'] > 0) ? "<span class='coupon_times_used'><sup>".$exp_row['visits']." ".CBE1_COUPONS_TUSED."</sup></span>" : ""; ?>
						<br/>
						<?php if ($exp_row['description'] != "") { ?><div class="coupon_description"><?php echo TruncateText($exp_row['description'], COUPONS_DESCRIPTION_LIMIT, $more_link = 1); ?>&nbsp;</div><?php } ?>
						<?php if ($exp_row['end_date'] != "0000-00-00 00:00:00") { ?>
							<span class="expires"><?php echo CBE1_COUPONS_EXPIRES; ?>: <?php echo $exp_row['coupon_end_date']; ?></span> &nbsp; 
							<span class="time_left"><?php echo CBE1_COUPONS_TIMELEFT; ?>: <span class="exp_soon_label"><?php echo GetTimeLeft($exp_row['time_left']); ?></span></span>
						<?php } ?>
					</td>
					<td class="td_coupon" align="left" valign="bottom">
						<?php if ($exp_row['code'] != "") { ?><span class="coupon_code"><?php echo (HIDE_COUPONS == 0 || isLoggedIn()) ? $exp_row['code'] : CBE1_COUPONS_CODE_HIDDEN; ?></span><?php } ?>
						<a class="go2store" href="<?php echo SITE_URL; ?>go2store.php?id=<?php echo $exp_row['retailer_id']; ?>&c=<?php echo $exp_row['coupon_id']; ?>" target="_blank"><?php echo ($exp_row['code'] != "") ? CBE1_COUPONS_LINK : CBE1_COUPONS_LINK2; ?></a>
					</td>
				</tr>
				<?php } ?>
				</table>

				<?php }else{ ?>
					<p align="center"><?php echo CBE1_COUPONS_NO; ?></p>
					<div class="sline"></div>
				<?php } ?>
		</div>


	<?php



		if ($total > 0 && $astores_total > 0)
		{

	?>

		<h1><?php echo CBE1_COUPONS_BYSTORE; ?></h1>

		<div id="alphabet">
		<ul>
			<?php
					$numLetters = count($alphabet);
					$i = 0;

					foreach ($alphabet as $letter)
					{
						$i++;
						if ($i == $numLetters) $lilast = ' class="last"'; else $lilast = '';
						echo "<li".$lilast."><a href=\"#$letter\">".$letter."</a></li>";
					}
			?>
		</ul>
		</div>

		<ul class="stores_list">
		<?php while ($astores_row = mysqli_fetch_array($astores_result)) { ?>
			<?php

				$first_letter = ucfirst(substr($astores_row['title'], 0, 1));
				if ($old_letter != $first_letter)
				{
					if ($b != 0 && $vv != 1) echo "</ul>";
					if (!in_array($first_letter, $alphabet))
					{
						if ($vv != 1)
						{
							echo "<li class='store2'><div class='letter'>0-9<a name='0-9'></a></div><ul>";
							$vv = 1;
						}
					}
					else
					{
						if ($vv == 1) echo "</ul>";
						echo "<li class='store2'><div class='letter'>$first_letter<a name='$first_letter'></a></div><ul>";
					}
							
					$old_letter = $first_letter;
					$b++;
					$bb = 0;
				}
			?>
				<?php if ($astores_row['featured'] == 1) { $ftag1 = "<b>"; $ftag2 = "</b>"; }else{  $ftag1 = $ftag2 = ""; } ?>

				<li><a href="<?php echo GetRetailerLink($astores_row['retailer_id'], $astores_row['title']); ?>"><?php echo $ftag1; ?><?php echo (strlen($astores_row['title']) > 75) ? substr($astores_row["title"], 0, 70)."..." : $astores_row["title"]; ?><?php echo $ftag2; ?></a> <span class="coupons"><?php echo GetStoreCouponsTotal($astores_row['retailer_id']); ?></span></li>

				<?php $bb++; if ($bb%$stores_per_column == 0) echo "</ul><ul>"; ?>
			<?php } ?>
		</ul>
	<?php } ?>


  <?php $__env->stopSection(); ?> 


<?php $__env->startSection('footer'); ?>

  <?php echo $__env->make('inc.footer', ['fc' => $fc], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
           
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>