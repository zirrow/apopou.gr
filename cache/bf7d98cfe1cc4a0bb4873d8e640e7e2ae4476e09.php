<?php $__env->startSection('head'); ?>

    <?php echo $__env->make('inc.head', ['head' => $head], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $__env->stopSection(); ?>


<?php $__env->startSection('head-styles'); ?>

 

<?php $__env->stopSection(); ?>

<?php $__env->startSection('head-scripts'); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('header'); ?>

    <?php echo $__env->make('inc.header', ['header'=>$header], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $__env->stopSection(); ?> 

<?php $__env->startSection('content'); ?>

<div class="ui-layout-center__content">
         <h1 class="ui-font-size--24 ui-text-align--center ui-margin-30-0 login__d">Μπείτε στο μεγαλύτερο Cash Back Shopping Site Κύπρου και Ελλάδας</h1>
         <div class="login">
            <!-- *Mobile only* switching tabs for signup/login forms -->
            <div class="login__tabs login__m">
                <div class="login__tab login__tab--active" id="signup">Εγγραφή</div>
                <div class="login__tab" id="login">Είσοδος μέλους</div>
            </div>
        </div>
         <div class="login__forms">
             <!-- Signup form -->
             <div class="login__form-wrapper login__form-wrapper--signup ui-float--left">
                <h2 class="login__form-heading login__form-heading--signup login__d">Εγγραφείτε Δωρεάν Τώρα!</h2>
                <form action="#" id="signUpForm" class="login__form">
                    <div class="login__form-fields">
                        <p class="ui-font-size--15 ui-margin-bottom--15 ui-padding-top--15 login__d">Έχετε λογαριασμό facebook;</p>
                        <a href="#" class="login__fb">
                            <p class="login__m ui-text-align--center ui-color--white login__fb-m ui-font-size--13 ui-padding--10">
                                <span class="fa fa-facebook ui-float--left ui-font-size--18"></span>
                                Συνδεθείτε με Λογαριασμό Facebook
                            </p>
                        </a>
                        <p class="ui-font-size--11 ui-margin-top--10 ui-margin-bottom--10 ui-color--light-grey login__d">Ποτέ δεν θα δημοσιεύσουμε χωρίς την προσωπική σας άδεια.</p>
                        <div class="login__or-box">
                            <div class="login__or-text">OR</div>
                        </div>
                        <input value="<?php echo e(getPostParameter('email')); ?>" type="text" 
                            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" 
                            required 
                            name="username" 
                            id="username" 
                            placeholder="* Διέυθυνση Email"
                            class="login__input-t textbox">
                        <br>
                        <input type="password" 
                            name="password" 
                            id="password" 
                            pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" 
                            placeholder="* Νέος Κωδικός Password"
                            required
                            class="login__input-t ui-margin-bottom--15 textbox" value="">
                        <br>
                        * <select name="country" aria-required="true" id="country" required class="ui-margin-bottom--15 selection search" class="textbox2">
                           
				<option value=""><?php echo CBE1_LABEL_COUNTRY_SELECT; ?></option>
				<?php

					$sql_country = "SELECT * FROM cashbackengine_countries WHERE signup='1' AND status='active' ORDER BY sort_order, name";
					$rs_country = smart_mysql_query($sql_country);
					$total_country = mysqli_num_rows($rs_country);

					if ($total_country > 0)
					{
						while ($row_country = mysqli_fetch_array($rs_country))
						{
							if ($country == $row_country['country_id'])
								echo "<option value='".$row_country['country_id']."' selected>".$row_country['name']."</option>\n";
							else
								echo "<option value='".$row_country['country_id']."'>".$row_country['name']."</option>\n";
						}
					}

				?>
				</select>
				<br >
                        <div class="g-recaptcha ui-width--300 ui-margin--center" data-sitekey="6Lf7rEUUAAAAADFrTtVT_fZvsX7HEJfxJN8b-UMW"></div>
                        <div class="login__check-agree">
                            <input type="checkbox" name="news" id="news" class="login__news-checkbox">
                            <label for="news" class="login__news-label ui-font-size--13">Παρακαλώ όπως μου αποστέλεται με email newsletters, κουπόνια, και προσφορές από το ApoPou. </label>
                        </div>
                        <p class="ui-font-size--11 ui-color--light-grey"><span class="ui-color--red">*</span> Υποχρεωτικά Πεδία</p>
                        <button type="submit" class="login__submit-btn"><span class="traftext traftext--px6 traftext--white">Εγγραφείτε σήμερα</span> 
                        </button>
                        <p class="ui-font-size--11 ui-color--light-grey">Μπορείτε να ζητήσετε διαγραφή στοιχείων. </p>
                    </div>
                </form>
                <p class="login__agreement">Σαν μέλος, συμφωνείς στα <a href="#" class="ui-color--green">Terms</a> &amp; <a href="#" class="ui-color--green">Conditions και Privacy Policy.</a></p> 
            </div>
            <!-- Login form -->
            
       
            
            <div class="login__form-wrapper login__form-wrapper--login login__form-wrapper--active ui-float--right">
                <h2 class="login__form-heading login__form-heading--login login__d">Είσοδος Μέλους Apopou</h2>
                <form action="#" id="signUpForm" class="login__form">
                    <div class="login__form-fields">
                        <p class="ui-font-size--15 ui-margin-bottom--15 ui-padding-top--15 login__d">Έχετε λογαριασμό facebook;</p>
                        <a href="#" class="login__fb">
                            <p class="login__m ui-text-align--center ui-color--white login__fb-m ui-font-size--13 ui-padding--10">
                                <span class="fa fa-facebook ui-float--left ui-font-size--18"></span>
                                Εγγραφείτε με Λογαριασμό Facebook
                            </p>
                        </a>
                        <p class="ui-font-size--11 ui-margin-top--10 ui-margin-bottom--10 ui-color--light-grey login__d">Ποτέ δεν θα δημοσιεύσουμε χωρίς την προσωπική σας άδεια.</p>
                        <div class="login__or-box">
                            <div class="login__or-text">OR</div>
                        </div>
                        <input type="text" value="<?php echo e(getPostParameter('username')); ?>"
                            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" 
                            required 
                            name="username" 
                            id="username" 
                            placeholder="* Διεύθυνση Email"
                            class="login__input-t">
                            
                        <br>
                        <input type="password" 
                            name="password" 
                            id="password" 
                            pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$"
                            placeholder="* Κωδικός Password"
                            required
                            class="login__input-t ui-margin-bottom--15">
                        <div class="login__check-agree">
                            <input type="checkbox" name="news" id="news" class="login__news-checkbox">
                            <label for="news" class="login__news-label ui-font-size--13">Αποθήκευση; </label>
                        </div>
                        <br >
                        
                        <div class="g-recaptcha ui-width--300 ui-margin--center" data-sitekey="6Lf7rEUUAAAAADFrTtVT_fZvsX7HEJfxJN8b-UMW"></div>
                        <p class="ui-font-size--11 ui-color--light-grey ui-text-align--left ui-display--block clr ui-margin-top--15">
                            <span class="ui-color--red">*</span> 
                            Υποχρεωτικά Πεδία
                            <a href="<?php echo e(SITE_URL); ?>forgot.php" class="ui-font-size--13 ui-float--right ui-color--green"><?php echo CBE1_LOGIN_FORGOT; ?></a>
                            	<a href=""></a>
					
                            
                        </p>
                        	<input type="hidden" name="action" value="login" />
					
                        <button type="submit" name="login" id="login" class="login__submit-btn submit">
                            <span class="traftext traftext--px6 traftext--white"><?php echo CBE1_LOGIN_BUTTON; ?>
                        </span>
                        </button>
                    </div>
                </form>
                <p class="login__agreement">Σαν μέλος, συμφωνείς στα <a href="#" class="ui-color--green">Terms</a> &amp; <a href="#" class="ui-color--green">Conditions και Privacy Policy.</a></p> 
            </div> 
         </div>
     </div>
     
     <script>
         $(function () {
    $("input[name=password]").on("invalid", function () {
        this.setCustomValidity("Τουλάχιστον 8 χαρακτήρες με ένα κεφαλαίο και ένα μικρό γράμμα καθώς επίσης και ένα αριθμό");
    });

    $("input[name=username]").on("invalid", function () {
        this.setCustomValidity("Πληκτρολογίστε σωστά την διεύθυνση email σας");
    });
});
     </script>
     <!-- ----------------------------------------------------------- -->

	<table width="100%" align="center" cellpadding="2" cellspacing="0" border="0">
	<tr>
	<td width="50%" valign="top" align="left">

			<h1><?php echo CBE1_LOGIN_TITLE; ?></h1>

			<?php if (isset($errormsg) || isset($_GET['msg'])) { ?>
				<div style="width: 83%;" class="error_msg">
					<?php if (isset($errormsg) && $errormsg != "") { ?>
						<?php echo $errormsg; ?>
					<?php }else{ ?>
						<?php if ($_GET['msg'] == 1) { echo CBE1_LOGIN_ERR1; } ?>
						<?php if ($_GET['msg'] == 2) { echo CBE1_LOGIN_ERR2; } ?>
						<?php if ($_GET['msg'] == 3) { echo CBE1_LOGIN_ERR3; } ?>
						<?php if ($_GET['msg'] == 4) { echo CBE1_LOGIN_ERR4; } ?>
						<?php if ($_GET['msg'] == 5) { echo CBE1_LOGIN_ERR1." ".(int)$_SESSION['attems_left']." ".CBE1_LOGIN_ATTEMPTS; } ?>
						<?php if ($_GET['msg'] == 6) { echo CBE1_LOGIN_ERR6; } ?>
					<?php } ?>
				</div>
			<?php } ?>

			<div class="login_box">
			<form action="" method="post">
			<table width="100%" align="center" cellpadding="3" cellspacing="0" border="0">
			  <tr>
				<td align="right" valign="middle"><?php echo CBE1_LOGIN_EMAIL2; ?>:</td>
				<td valign="top">
				    <input type="text" class="textbox" name="username" value="<?php echo getPostParameter('username'); ?>" size="25" required="required" />
				    </td>
			  </tr>
			  <tr>
				<td align="right" valign="middle"><?php echo CBE1_LOGIN_PASSWORD; ?>:</td>
				<td valign="top">
				    <input type="password" class="textbox" name="password" value="" size="25" required="required" /></td>
			  </tr>
			  <tr>
				<td align="right" valign="middle">&nbsp;</td>
				<td valign="top"><input type="checkbox" class="checkboxx" name="rememberme" id="rememberme" value="1" checked="checked" /> <?php echo CBE1_LOGIN_REMEMBER; ?></td>
			  </tr>
			  <tr>
				<td valign="top" align="middle">&nbsp;</td>
				<td align="left" valign="bottom">
					<input type="hidden" name="action" value="login" />
					<input type="submit" class="submit" name="login" id="login" value="<?php echo CBE1_LOGIN_BUTTON; ?>" />
				</td>
			  </tr>
			  <tr>
			   <td valign="top" align="middle">&nbsp;</td>
				<td align="left" valign="bottom">
					<a href="<?php echo SITE_URL; ?>forgot.php"><?php echo CBE1_LOGIN_FORGOT; ?></a>
					<?php if (ACCOUNT_ACTIVATION == 1) { ?>
						<p><a href="<?php echo SITE_URL; ?>activation_email.php"><?php echo CBE1_LOGIN_AEMAIL; ?></a></p>
					<?php } ?>
				</td>
			  </tr>
			</table>
		  </form>
		  </div>

		<?php if (FACEBOOK_CONNECT == 1 && FACEBOOK_APPID != "" && FACEBOOK_SECRET != "") { ?>
			<div style="border-bottom: 1px solid #ECF0F1; margin-bottom: 15px;">
				<div style="font-weight: bold; background: #FFF; color: #CECECE; margin: 0 auto; top: 5px; text-align: center; width: 50px; position: relative;">or</div>
			</div>
			<p align="center"><a href="javascript: void(0);" onclick="facebook_login();" class="connect-f"><img src="<?php echo SITE_URL; ?>images/facebook_connect.png" /></a></p>
		<?php } ?>

	</td>
	<td width="2%" valign="top" align="left">&nbsp;</td>
	<td width="48%" valign="top" align="left">
		
		<h1><?php echo CBE1_LOGIN_NMEMBER; ?></h1>
		<p><?php echo CBE1_LOGIN_TEXT2; ?></p>

		<p><b><?php echo str_replace("%site_title%",SITE_TITLE,CBE1_LOGIN_TXT1); ?></b></p>
		<ul id="benefits">
			<li><?php echo CBE1_LOGIN_LI1; ?></li>
			<?php if (SIGNUP_BONUS > 0) { ?><li><?php echo str_replace("%amount%",DisplayMoney(SIGNUP_BONUS),CBE1_LOGIN_LI2); ?></li><?php } ?>
			<?php if (REFER_FRIEND_BONUS > 0) { ?><li><?php echo str_replace("%amount%",DisplayMoney(REFER_FRIEND_BONUS),CBE1_LOGIN_LI3); ?></li><?php } ?>
			<li><?php echo CBE1_LOGIN_LI4; ?></li>
			<li><?php echo CBE1_LOGIN_LI5; ?></li>
			<li><?php echo CBE1_LOGIN_LI6; ?></li>
		</ul>

		<p align="center"><a class="button" href="<?php echo SITE_URL; ?>login.php"><b><?php echo CBE_SIGNUP; ?></b></a></p>

	</td>
	</tr>
	</table>
	
			<?php if (isset($_SESSION['goto']) && $_SESSION['goto'] != "" && isset($_GET['msg']) && $_GET['msg'] == 4) { ?>
			<?php if (isset($_GET['msg']) && $_GET['msg'] == 4) { ?><div class="login_msg"><?php echo CBE1_LOGIN_ERR4; ?></div><?php } ?>
			<table bgcolor="#F9F9F9" style="border-top: 1px dotted #F5F5F5; border-bottom: 1px dotted #F5F5F5" align="center" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="<?php echo IMAGE_WIDTH; ?>" align="center" valign="top">
					<br/>
					<div class="imagebox"><img src="<?php if (!stristr($row['image'], 'http')) echo SITE_URL."img/"; echo $row['image']; ?>" width="<?php echo IMAGE_WIDTH; ?>" height="<?php echo IMAGE_HEIGHT; ?>" alt="<?php echo $row['title']; ?>" title="<?php echo $row['title']; ?>" border="0" /></div>
				</td>
				<td align="left" valign="top">
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td width="80%" align="left" valign="middle"><h2 class="stitle"><?php echo $row['title']; ?></h1></td>
							<td nowrap="nowrap" width="20%" align="center" valign="middle">
							<?php if ($row['cashback'] != "") { ?>
								<?php if ($row['old_cashback'] != "") { ?><span class="old_cashback"><?php echo DisplayCashback($row['old_cashback']); ?></span><?php } ?>
								<span class="cashback"><span class="value"><?php echo DisplayCashback($row['cashback']); ?></span> <?php echo CBE1_CASHBACK; ?></span>
							<?php } ?>
							</td>
						</tr>
						<tr>
							<td colspan="2" valign="top" align="left">
								<div class="retailer_description"><?php echo TruncateText(stripslashes($row['description']), STORES_DESCRIPTION_LIMIT, $more_link = 1); ?>&nbsp;</div>
								<?php echo GetStoreCountries($row['retailer_id']); ?>
								<?php if ($row['conditions'] != "") { ?>
									<br/><b><?php echo CBE1_CONDITIONS; ?></b><br/>
									<?php echo $row['conditions']; ?>
								<?php } ?>
							</td>
						</tr>							
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" valign="top" align="center">
					<div class="sline"></div>
					<h3><?php echo CBE1_LOGIN_THX; ?></h3>
					<p><a class="go2store_large" href="<?php echo $_SESSION['goto']; ?>" target="_blank"><?php echo CBE1_LOGIN_CONTINUE; ?></a></p>
				</td>
			</tr>
			</table>
		<?php } ?>




<script src='https://www.google.com/recaptcha/api.js'></script>


        <table width="100%" style="border-bottom: 2px solid #F7F7F7;" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
			<td align="left" valign="middle"><h1 style="margin-bottom:0;border:none;"><?php echo CBE1_SIGNUP_TITLE; ?></h1></td>
			<td align="right" valign="bottom"><?php echo CBE1_SIGNUP_MEMBER; ?> <a href="<?php echo SITE_URL; ?>login.php"><?php echo CBE1_LOGIN_TITLE; ?></a></td>
        </tr>
        </table>

		<?php if (FACEBOOK_CONNECT == 1 && FACEBOOK_APPID != "" && FACEBOOK_SECRET != "") { ?>
			<p align="center"><a href="javascript: void(0);" onclick="facebook_login();" class="connect-f"><img src="<?php echo SITE_URL; ?>images/facebook_connect.png" /></a></p>
			<div style="border-bottom: 1px solid #ECF0F1; width: 400px; margin: 0 auto;">
				<div style="font-weight: bold; background: #FFF; color: #CECECE; margin: 0 auto; top: 5px; text-align: center; width: 50px; position: relative;">or</div>
			</div><br/>
		<?php } ?>

		<?php if (isset($allerrors) || isset($_GET['msg'])) { ?>
			<div class="error_msg">
				<?php if (isset($_GET['msg']) && $_GET['msg'] == "exists") { ?>
					<?php echo CBE1_SIGNUP_ERR10; ?> <a href="<?php echo SITE_URL; ?>forgot.php"><?php echo CBE1_LOGIN_FORGOT; ?></a></font><br/>
				<?php }elseif (isset($allerrors)) { ?>
					<?php echo $allerrors; ?>
				<?php }	?>
			</div>
		<?php } ?>

        <form action="" method="post">
        <table width="100%" align="center" cellpadding="3" cellspacing="0" border="0">
          <tr>
            <td colspan="2" align="right" valign="top"><span class="req">* <?php echo CBE1_LABEL_REQUIRED; ?></span></td>
          </tr>
       <!--   <tr>
            <td width="210" align="right" valign="middle"><span class="req">* </span><?php echo CBE1_LABEL_FNAME; ?>:</td>
            <td align="left" valign="middle"><input type="text" id="fname" class="textbox" name="fname" value="<?php echo getPostParameter('fname'); ?>" size="27" /></td>
          </tr>
        
          <tr>
            <td align="right" valign="middle"><span class="req">* </span><?php echo CBE1_LABEL_LNAME; ?>:</td>
            <td align="left" valign="middle"><input type="text" id="lname" class="textbox" name="lname" value="<?php echo getPostParameter('lname'); ?>" size="27" /></td>
          </tr>
        -->
          <tr>
            <td align="right" valign="middle"><span class="req">* </span><?php echo CBE1_LABEL_EMAIL2; ?>:</td>
            <td align="left" valign="middle">
                <input type="text" id="email" class="textbox" name="email" value="<?php echo getPostParameter('email'); ?>" size="27" /></td>
          </tr>
          <tr>
            <td align="right" valign="middle"><span class="req">* </span><?php echo CBE1_LABEL_PWD; ?>:</td>
            <td nowrap="nowrap" align="left" valign="middle"><input type="password" id="password" class="textbox" name="password" value="" size="27" /> <span class="note"><?php echo CBE1_SIGNUP_PTEXT; ?></span></td>
          </tr>
          <!--
          <tr>
            <td align="right" valign="middle"><span class="req">* </span><?php echo CBE1_LABEL_CPWD; ?>:</td>
            <td nowrap="nowrap" align="left" valign="middle"><input type="password" id="password2" class="textbox" name="password2" value="" size="27" /></td>
          </tr>
          -->
          <tr>
            <td align="right" valign="middle"><span class="req">* </span><?php echo CBE1_LABEL_COUNTRY; ?>:</td>
            <td align="left" valign="middle">
				<select name="country" class="textbox2" id="country" style="width: 180px;">
				<option value=""><?php echo CBE1_LABEL_COUNTRY_SELECT; ?></option>
				<?php

					$sql_country = "SELECT * FROM cashbackengine_countries WHERE signup='1' AND status='active' ORDER BY sort_order, name";
					$rs_country = smart_mysql_query($sql_country);
					$total_country = mysqli_num_rows($rs_country);

					if ($total_country > 0)
					{
						while ($row_country = mysqli_fetch_array($rs_country))
						{
							if ($country == $row_country['country_id'])
								echo "<option value='".$row_country['country_id']."' selected>".$row_country['name']."</option>\n";
							else
								echo "<option value='".$row_country['country_id']."'>".$row_country['name']."</option>\n";
						}
					}

				?>
				</select>
			</td>
          </tr>
      <!--
          <tr>
            <td align="right" valign="middle"><?php echo CBE1_LABEL_PHONE; ?>:</td>
            <td align="left" valign="middle"><input type="text" id="phone" class="textbox" name="phone" value="<?php echo getPostParameter('phone'); ?>" size="27" /></td>
          </tr>
          -->
		  <?php if (SIGNUP_CAPTCHA == 1) { ?>
          <tr>
            <td align="right" valign="middle"><span class="req">* </span><?php echo CBE1_SIGNUP_SCODE; ?>:</td>
            <td align="left" valign="middle">
				<input type="text" id="captcha" class="textbox" name="captcha" value="" size="8" />
				<img src="<?php echo SITE_URL; ?>captcha.php?rand=<?php echo rand(); ?>" id="captchaimg" align="absmiddle" /> <small><a href="javascript: refreshCaptcha();" title="<?php echo CBE1_SIGNUP_RIMG; ?>"><img src="<?php echo SITE_URL; ?>images/icon_refresh.png" align="absmiddle" alt="<?php echo CBE1_SIGNUP_RIMG; ?>" /></a></small>
			</td>
          </tr>
			<script language="javascript" type="text/javascript">
				function refreshCaptcha()
				{
					var img = document.images['captchaimg'];
					img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
				}
			</script>
		  <?php } ?>
		  <?php if (is_array($reg_sources) && count($reg_sources) > 0) { ?>
       <!--
          <tr>
            <td align="right" valign="middle">&nbsp;</td>
            <td align="left" valign="middle">
				<select name="reg_source" class="textbox2" id="reg_source">
					<option value=""><?php echo CBE1_SIGNUP_REG_SOURCE; ?></option>
					<?php foreach ($reg_sources as $v) { ?>
						<option value="<?php echo trim($v); ?>" <?php if ($reg_source == $v) echo "selected"; ?>><?php echo trim($v); ?></option>
					<?php } ?>
				</select>
			</td>
          </tr>
        -->
		  <?php } ?>
      <!--    <tr>
            <td align="right" valign="middle">&nbsp;</td>
            <td align="left" valign="middle"><input type="checkbox" name="newsletter" class="checkboxx" value="1" <?php // echo (!$_POST['action'] || @$newsletter == 1) ? "checked" : "" ?>/> <?php // echo CBE1_SIGNUP_NEWSLETTER; ?></td>
          </tr>
          -->
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="middle"><input type="checkbox" name="tos" class="checkboxx" value="1" <?php echo (!$_POST['action'] || @$tos == 1) ? "checked" : "" ?>/> <?php echo CBE1_SIGNUP_AGREE; ?> <a href="<?php echo SITE_URL; ?>terms.php" target="_blank"><?php echo CBE1_SIGNUP_TERMS; ?></a></td>
          </tr>
      <!--  </tr>  -->
        <tr>
        <div class="g-recaptcha" data-sitekey="6LdAjTkUAAAAANUYtZ4o43KfrioV2F5RnbRyhBxQ"></div>
        </tr>
          <tr>
            <td align="right" valign="middle">&nbsp;</td>
			<td align="left" valign="middle">
				<?php if (isset($_COOKIE['referer_id']) && is_numeric($_COOKIE['referer_id'])) { ?>
					<input type="hidden" name="referer_id" id="referer_id" value="<?php echo (int)$_COOKIE['referer_id']; ?>" />
				<?php } ?>
				<input type="hidden" name="action" id="action" value="signup" />
				<input type="submit" class="submit signup" name="Signup" id="Signup" value="<?php echo CBE1_SIGNUP_BUTTON; ?>" />
		  </td>
          </tr>
        </table>
        </form>

<?php $__env->stopSection(); ?> 


<?php $__env->startSection('footer'); ?>

    <?php echo $__env->make('inc.footer', ['fc' => $fc], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>