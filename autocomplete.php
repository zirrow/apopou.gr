<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	require_once("inc/config.inc.php");
    require_once("inc/var_config.inc.php");

	$query = $_POST["query"];
	if (!$query) {
	    return;
	}

    $query = strtolower(mysqli_real_escape_string($conn, $query));
    $query = substr(trim($query), 0, 100);

    $sql = "SELECT retailer_id, title, image, cashback, website FROM cashbackengine_retailers WHERE (title LIKE '%". $query ."%' OR website LIKE '%". $query ."%') AND status='active' LIMIT 20";

	$search_result = smart_mysql_query($sql);

    $search_result_array = [];
//

	if (mysqli_num_rows($search_result) > 0)
	{
		while ($row = mysqli_fetch_array($search_result))
		{
			$search_result_array[] = [
			        'retailer_id' => $row['retailer_id'],
			        'title' => $row['title'],
			        'image' => $row['image'],
			        'cashback' => $row['cashback'],
			        'website' => $row['website']
            ];
		}
    }

    echo json_encode($search_result_array);

?>