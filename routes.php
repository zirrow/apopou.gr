<?php


header("Content-Type: text/html");
require_once("inc/config.inc.php");
require_once("inc/var_config.inc.php");

/* Match the current request */
$match = $router->match();
 //var_dump($router->match()); exit();

if($match) {

//	call_user_func_array( $match['target'], $match['params'] );

	$_REQUEST['params'] = $match['params'];

	require $match['target'];
} else {
	header("HTTP/1.0 404 Not Found");
	require '404.php';
}
