<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	session_start();
	require_once("inc/config.inc.php");
	require_once("inc/var_config.inc.php");
	require_once("inc/blade_config.inc.php");

	$ADDTHIS_SHARE = 1;
	// N.R. change
	//$retailer_id = $match['params']['id'];
	//var_dump($retailer_id); exit();

	if (isset($_REQUEST['params']['id']) && is_numeric($_REQUEST['params']['id']))
	{
		$retailer_id = (int)$_REQUEST['params']['id'];
		
	} else {
		header ("Location: /");
		exit();
	}

	// end of change

	// ADD REVIEW ///////////////////////////////////////////////////////////////////////
	if (isset($_POST['action']) && $_POST['action'] == "add_review" && isLoggedIn()) {

		$userid			= (int)$_SESSION['userid'];
		$retailer_id	= (int)getPostParameter('retailer_id');
		$rating			= (int)getPostParameter('rating');
		$review_title	= mysqli_real_escape_string($conn, getPostParameter('review_title'));
		$review			= mysqli_real_escape_string($conn, nl2br(trim(getPostParameter('review'))));
		$review			= ucfirst(strtolower($review));

		unset($errs);
		$errs = array();

		if (!($userid && $retailer_id && $rating && $review_title && $review)) {
			echo json_encode(['error' => CBE1_REVIEW_ERR]);
			$errs[] = CBE1_REVIEW_ERR;
			exit();

		} else {
			$number_lines = count(explode("<br />", $review));

			if (strlen($review) > MAX_REVIEW_LENGTH){

				$err_str = str_replace("%length%",MAX_REVIEW_LENGTH,CBE1_REVIEW_ERR2);
				echo json_encode(['error' => $err_str]);
				$errs[] = $err_str;
				exit();

			} else if ($number_lines > 5) {

				echo json_encode(['error' => CBE1_REVIEW_ERR3]);
				$errs[] = CBE1_REVIEW_ERR3;
				exit();
			} else if (stristr($review, 'http')) {

				echo json_encode(['error' => CBE1_REVIEW_ERR4]);
				$errs[] = CBE1_REVIEW_ERR4;
				exit();
			}
		}

		if (count($errs) == 0) {

			$review = substr($review, 0, MAX_REVIEW_LENGTH);

			if (ONE_REVIEW == 1){
				$check_review = mysqli_num_rows(smart_mysql_query("SELECT * FROM cashbackengine_reviews WHERE retailer_id='". $retailer_id. "' AND user_id='". $userid ."'"));
			} else {
				$check_review = 0;
			}

			if ($check_review == 0) {

				(REVIEWS_APPROVE == 1) ? $status = "pending" : $status = "active";

				$review_query = "INSERT INTO cashbackengine_reviews 
									SET retailer_id='$retailer_id', 
									rating='$rating', 
									user_id='$userid', 
									review_title='$review_title', 
									review='$review', 
									status='$status', 
									added=NOW()";

				$review_result = smart_mysql_query($review_query);
				$review_added = 1;

				// send email notification //
				if (NEW_REVIEW_ALERT == 1) {
					SendEmail(SITE_ALERTS_MAIL, CBE1_EMAIL_ALERT2, CBE1_EMAIL_ALERT2_MSG);
				}

				echo json_encode(['success' => 'Review added successfully']);
				exit();
				/////////////////////////////
			} else {

				echo json_encode(['error' => CBE1_REVIEW_ERR5]);
				$errs[] = CBE1_REVIEW_ERR5;
				exit();
			}

			unset($_POST['review']);
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////


	$retailer = GetRetailerById($retailer_id);

	if ($retailer) {

		$retailer_id	= $retailer['retailer_id'];
		$cashback		= DisplayCashback($retailer['cashback']);
		$retailer_url	= GetRetailerLink($retailer['retailer_id'], $retailer['title']);

		if (isLoggedIn()){
			$retailer_url .= "&ref=".(int)$_SESSION['userid'];
		}

		// save referral //
		if (!isLoggedIn() && isset($_GET['ref']) && is_numeric($_GET['ref'])) {
			$ref_id = (int)$_GET['ref'];
			setReferral($ref_id);
		}

		if ($retailer['seo_title'] != "") {
			$ptitle	= $retailer['seo_title'];
		} else {

			if ($cashback != "") {
				$ptitle	= $retailer['title'].". ".CBE1_STORE_EARN." ".$cashback." ".CBE1_CASHBACK2;
			} else {
				$ptitle	= $retailer['title'];
			}
		}		

	} else {

		header("HTTP/1.0 404 Not Found");
		require '404.php';
		exit();

//		$ptitle = CBE1_STORE_NOT_FOUND;
	}

	$content = GetContent('view_retailer');


	///////////////  Page config  ///////////////
	$PAGE_TITLE			= $ptitle;
	$PAGE_DESCRIPTION	= $retailer['meta_description'];
	$PAGE_KEYWORDS		= $retailer['meta_keywords'];

	$data = [
		'head'=>$head,
		'header'=>$header,
		'footer'=>$footer,
		'router'=>$router,
		'PAGE_TITLE'=>$PAGE_TITLE,
		'PAGE_DESCRIPTION'=>$PAGE_DESCRIPTION,
		'PAGE_KEYWORDS'=>$PAGE_KEYWORDS,
		'countries'=>GetCountries(),
		'languages'=>GetLanguagesArray(),
		'current_lang'=>isset($_COOKIE['site_lang']) ? $_COOKIE['site_lang'] : SITE_LANGUAGE,
		'multilanguage'=>MULTILINGUAL,
		'search_array'=>GetRetailersForSearch(),
		'content' => $content,
		'user_info'=>GetUserInfo(),

		'retailer' => $retailer,
		'latest' => GetLatestRetailers(4),
	];

//	print "<pre>";
//	print_r($data);
//	print "</pre>";

	echo $blade->make('view_retailer', $data);

?>	

