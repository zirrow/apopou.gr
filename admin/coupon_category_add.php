<?php
// N.R. add coupon category.

/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	session_start();
	require_once("../inc/adm_auth.inc.php");
	require_once("../inc/config.inc.php");
	require_once("./inc/admin_funcs.inc.php");


	if (isset($_POST['action']) && $_POST['action'] == "add")
	{
		$category_name			= mysqli_real_escape_string($conn, getPostParameter('catname'));
		$category_description	= mysqli_real_escape_string($conn, nl2br(getPostParameter('description')));
		$parent_category		= (int)getPostParameter('parent_id');
		$meta_description		= mysqli_real_escape_string($conn, nl2br(getPostParameter('meta_description')));
		$meta_keywords			= mysqli_real_escape_string($conn, getPostParameter('meta_keywords'));
		$sort_order				= (int)getPostParameter('sort_order');
 //var_dump($category_name);exit();
		if (isset($category_name) && $category_name != "")
		{
			
;			//$category_name = html_entity_decode($category_name, ENT_COMPAT | ENT_HTML401, "UTF-8");
			$check_query = smart_mysql_query("SELECT * FROM cashbackengine_coupon_categories WHERE parent_id='$parent_category' AND name='$category_name' AND category_url='$category_url'");
			if (mysqli_num_rows($check_query) == 0)
			{
			// N.R. addition
			$category_name = html_entity_decode($category_name);
			$category_description = str_replace("[category_name]", $category_name, $category_description);
			$meta_description = str_replace("[category_name]", $category_name, $meta_description);
			// end
				$sql = "INSERT INTO cashbackengine_coupon_categories SET parent_id='$parent_category', name='$category_name', description='$category_description', category_url='', meta_description='$meta_description', meta_keywords='$meta_keywords', sort_order='$sort_order'";

				if (smart_mysql_query($sql))
				{
					header("Location: coupon_categories.php?msg=added");
					exit();
				}
			}
			else
			{
				header("Location: coupon_categories.php?msg=exists");
				exit();
			}
		}
	}

	$title = "Add Category";
	require_once ("inc/header.inc.php");

?>

		  <h2>Add Coupon Category</h2>

		  <form action="" method="post">
		  <table bgcolor="#F9F9F9" align="center" width="100%" border="0" cellpadding="3" cellspacing="0">
          <tr>
            <td colspan="2" align="right" valign="top"><font color="red">* denotes required field</font></td>
          </tr>
          <tr>
            <td width="30%" nowrap="nowrap" valign="middle" align="right" class="tb1"><span class="req">* </span>Category Name:</td>
			<td align="left">
				<input type="text" name="catname" id="catname" value="<?php echo getPostParameter('catname'); ?>" size="40" class="textbox" />
			</td>
          </tr>
          <tr>
            <td nowrap="nowrap" valign="middle" align="right" class="tb1">Parent Category:</td>
			<td align="left">
				<select name="parent_id">
					<option value=""> ---------- None ---------- </option>
					<?php CouponCategoriesDropDown (0); ?>
				</select>
			</td>
          </tr>
          <tr>
            <td nowrap="nowrap" valign="middle" align="right" class="tb1">Description:</td>
			<td align="left" valign="top"><textarea name="description" cols="75" rows="35" class="textbox2">Βρείτε όλα τα κουπόνια και τις προσφορές [category_name] όλων τον συνεργαζόμενων καταστημάτων με το grabdid. Οι προσφορές που θα βρείτε χωρίζονται σε τρείς κατηγορίες. 1) προσφορές 2) online εκπτωτικά κουπόνια ( κωδικούς έκπτωσης ) 3) εκτυπώσιμα κουπόνια για χρήση στα φυσικά κατάστημα. Όλες οι προσφορές και τα κουπόνια που βρίσκονται στο grabdid συνδυάζονται με επιστροφή χρημάτων εάν είναι διαθέσιμη από το κατάστημα.

<h2>Online προσφορά [category_name] </h2>

Για να ενεργοποιήσετε την προσφορά απλά κάνετε κλικ στο ενεργοποίησης προσφοράς και θα μεταφερθείτε στο κατάστημα. Δεν χρειάζεται άλλη ενέργεια από εσάς. Για να ενεργοποιηθεί όμως η επιστροφή χρημάτων θα πρέπει να είστε συνδεδεμένος με τον λογαριασμό σας στο grabdid.

<h2>Online κουπόνι [category_name] </h2>

Για να ενεργοποιήσετε το online κουπόνι κάνετε κλικ στο δες το κουπόνι. Θα ανοίξει ένα παράθυρο και θα εμφανιστεί ο κωδικός του κουπονιού παράλληλα θα μεταφερθείτε στο κατάστημα. Στο ταμείο την ώρα της πληρωμής πληκτρολογήστε τον κωδικό του κουπονιού για να ενεργοποιηθεί η έκπτωση. Για να ενεργοποιηθεί όμως η επιστροφή χρημάτων θα πρέπει να είστε συνδεδεμένος με τον λογαριασμό σας στο grabdid.

<h2>Εκτυπώσιμα κουπόνι [category_name] </h2>

Για να ενεργοποιήσετε το εκτυπώσιμο κουπόνι κάνετε κλικ στο δες το κουπόνι. Θα ανοίξει ένα παράθυρο και θα εμφανιστεί το κουπόνι μαζί με τις λεπτομέρειες της προσφοράς . Στο ταμείο την ώρα της πληρωμής δείξετε το κουπόνι στον ταμεία για να σας κάνει την έκπτωση. Στο παρών στάδιο τα εκτυπώσιμα κουπόνια δεν συνδυάζονται με επιστροφή χρημάτων. </textarea></select>
			</td>
          </tr>
			<tr>
				<td valign="middle" align="right" class="tb1">Meta Description:</td>
				<td valign="top"><textarea name="meta_description" cols="75" rows="2" class="textbox2">Κουπόνια και προσφορές [category_name] &#9989; Βρείτε όλες τις προσφορές σε Ελλάδα και Κύπρο και κάνετε τις αγορές σας σε χιλιάδες προϊόντα φθηνότερα.</textarea></td>
            </tr>
			<tr>
				<td valign="middle" align="right" class="tb1">Seo title (ex. Meta Keywords):</td>
				<td valign="top"><input type="text" name="meta_keywords" id="meta_keywords" value="<?php echo getPostParameter('meta_keywords'); ?>" size="78" style="width: 390px" class="textbox" /></td>
            </tr>
            <tr>
				<td valign="middle" align="right" class="tb1">Sort Order:</td>
				<td valign="middle"><input type="text" class="textbox" name="sort_order" value="<?php echo getPostParameter('sort_order'); ?>" size="5" /></td>
            </tr>
          <tr>
			<td>&nbsp;</td>
			<td valign="middle" align="left">
				<input type="hidden" name="action" id="action" value="add" />
				<input type="submit" name="add" id="add" class="submit" value="Add Category" />
		    </td>
          </tr>
		  </table>
		  </form>


<?php require_once ("inc/footer.inc.php"); ?>