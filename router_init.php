<?php

	function routerInit(){
		global $router;

		$router->map('GET','/howitworks', 'howitworks.php', 'howitworks'); //ok
		$router->map('GET','/featured', 'featured.php', 'featured'); //ok
		$router->map('GET','/aboutus', 'aboutus.php', 'aboutus'); //ok
		$router->map('GET','/privacy', 'privacy.php', 'privacy'); //ok
		//$router->map('GET','/signup', 'signup.php', 'signup'); //ok
		$router->map('GET','/terms', 'terms.php', 'terms'); //ok
		$router->map('GET','/submit_coupon', 'submit_coupon.php', 'submit_coupon'); //ok
		$router->map('GET','/news', 'news.php', 'news'); //ok
		$router->map('GET|POST','/help', 'help.php', 'help'); //ok
		//$router->map('GET','/forgot', 'forgot.php', 'forgot'); //ok
		$router->map('GET','/rss', 'rss.php', 'rss'); //ok
		$router->map('GET','/online-katasthmata', 'retailers.php', 'retailers'); //ok

		$router->map('GET|POST','/login', 'signup.php', 'login'); //ok
		$router->map('GET|POST','/forgot', 'forgot.php', 'forgot'); //ok
		$router->map('GET','/fblogin', 'fblogin.php', 'fblogin'); //ok


		$router->map('GET','/contact', 'contact.php', 'contact'); //ok

		$router->map('GET','/kouponia', 'coupons.php', 'coupons'); //ok
		$router->map('GET','/prosfores/[*:slug]/[*:cat_id]', 'coupons.php', 'coupons_by_category');


		$router->map('GET','/retailers/[*:letter]', 'retailers.php', 'retailers_by_letter'); //ok

		$router->map('GET','/online-katasthmata/[*:slug]/[*:cat_id]', 'retailers.php', 'retailers_by_category');
		$router->map('GET|POST','/kouponia/[*:slug]/[*:id]/[*:ref]?','view_retailer.php','view_retailer');

		//account routes
		$router->map('GET|POST','/invite', 'invite.php', 'invite');
		$router->map('GET','/my-account', 'myaccount.php', 'my-account');
		$router->map('GET|POST','/my-profile', 'myprofile.php', 'my-profile');
		$router->map('GET|POST','/my-favorites', 'myfavorites.php', 'my-favorites');
		$router->map('GET','/my-clicks', 'myclicks.php', 'my-clicks');
		$router->map('GET','/my-balance', 'mybalance.php', 'my-balance');
		$router->map('GET','/withdraw', 'withdraw.php', 'withdraw');
	}