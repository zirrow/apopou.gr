<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	session_start();
	require_once("inc/config.inc.php");
    require_once("inc/blade_config.inc.php");
    require_once("inc/var_config.inc.php");


	///////////////  Page config  ///////////////
	$PAGE_TITLE = CBE1_404_TITLE;


    $data = [
    'head'=>$head,
    'header'=>$header,
    'footer'=>$footer,
    'router'=>$router,
    'PAGE_TITLE'=>$PAGE_TITLE,
    'PAGE_DESCRIPTION'=>$PAGE_DESCRIPTION,
    'PAGE_KEYWORDS'=>$PAGE_KEYWORDS,
    'countries'=>GetCountries(),
    'languages'=>GetLanguagesArray(),
    'current_lang'=>isset($_COOKIE['site_lang']) ? $_COOKIE['site_lang'] : SITE_LANGUAGE,
    'multilanguage'=>MULTILINGUAL,
    'search_array'=>GetRetailersForSearch(),
    'user_info'=>GetUserInfo(),
    ];

    echo $blade->make('404', $data);




