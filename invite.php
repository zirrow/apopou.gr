<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	session_start();
	require_once("inc/auth.inc.php");
	require_once("inc/config.inc.php");
	require_once("inc/pagination.inc.php");
    require_once("inc/blade_config.inc.php");
    require_once("inc/var_config.inc.php");

	define('FRIENDS_INVITATIONS_LIMIT', 5);
	
	$referral_link	    = SITE_URL."?ref=".$userid;
	$user_message		= CBE1_INVITE_EMAIL_MESSAGE;
	$user_message		= str_replace("<br/>", "&#13;&#10;", $user_message);
	$user_message		= str_replace("%site_title%", SITE_TITLE, $user_message);
	$user_message		= str_replace("%referral_link%", $referral_link, $user_message);


	if (isset($_POST['action']) && $_POST['action'] == "friend")
	{

		$user_name		    = $_SESSION['username'];
		$friend_email		= getPostParameter('friend_email');
		$user_message	    = nl2br(getPostParameter('umessage'));

		if(empty($friend_email) || !preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $friend_email)) {
			echo json_encode(['error' => CBE1_INVITE_ERR2]);
		} else {
			$etemplate = GetEmailTemplate('invite_friend');
				
			$recipients = "";
			$friend_email	= substr(htmlentities(trim($friend_email)), 0, 70);

			$esubject = $etemplate['email_subject'];

			$emessage = $etemplate['email_message'];
			$emessage = str_replace("{first_name}", $user_name, $emessage);
			$emessage = str_replace("{referral_link}", $referral_link, $emessage);


			$recipients .= "<". $friend_email .">";

			$to_email ='<'. $friend_email .'>';

			SendEmail($to_email, $esubject, $emessage, $noreply_mail = 1);

			// save invitations info //
			smart_mysql_query("INSERT INTO cashbackengine_invitations SET user_id='".(int)$userid."', recipients='".mysqli_real_escape_string($conn, $recipients)."', message='".mysqli_real_escape_string($conn, $user_message)."', sent_date=NOW()");

			echo json_encode(['success' => 'success']);
			exit();
		}
	}

	if(!isset($_POST['action'])) {
		///////////////  Page config  ///////////////
		$PAGE_TITLE = CBE1_INVITE_TITLE;
		$content = GetContent('invite');

		///////////////  Page config  ///////////////
		$PAGE_TITLE = !empty($content['title']) ? $content['title'] : '';
		$PAGE_DESCRIPTION = !empty($content['meta_description']) ? $content['meta_description'] : '';
		$PAGE_KEYWORDS = !empty($content['meta_keywords']) ? $content['meta_description'] : '';

		$user_id = (int)$_SESSION['userid'];

		$data = [
		    'head' => $head,
	        'header' => $header,
	        'footer' => $footer,
			'router'=>$router,
	        'PAGE_TITLE' => $PAGE_TITLE,
	        'PAGE_DESCRIPTION' => $PAGE_DESCRIPTION,
	        'PAGE_KEYWORDS' => $PAGE_KEYWORDS,
	        'content' => $content,
	        //ADDED by Denis
			'countries' => GetCountries(),
	        'languages' => GetLanguagesArray(),
	        'current_lang' => $_COOKIE['site_lang'],
	        'multilanguage' => MULTILINGUAL,
	        'search_array' => GetRetailersForSearch(),
	        'user_info' => GetUserInfo(),
			'referral_link' => urlencode($referral_link),
			'referral_link_main' => $referral_link,
			'referral_link_fb' => str_replace("/?","/index.php?", $referral_link),
			'referral_clicks' => GetRefClicksTotal($user_id),
			'referrals_total' => GetReferralsTotal($user_id),
			'referrals_pending_bonuses' => GetReferralsPendingBonuses($user_id),
			'referrals_paid_bonuses' => GetReferralsPaidBonuses($user_id),
			'referrals_invited' => GetAllReferrals($user_id),
			'referrals_invites' => GetAllInvites($user_id),
			'referrals_qualified' => GetAllInvitesWithTransactions($user_id),
	        ];

		echo $blade->make('invite', $data);
	}