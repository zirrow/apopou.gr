<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	session_start();
	require_once("inc/config.inc.php");
	require_once("inc/pagination.inc.php");
    require_once("inc/blade_config.inc.php");
    require_once("inc/var_config.inc.php");

	////////////////// filter  //////////////////////
    if (isset($_GET['column']) && $_GET['column'] != "") {

        switch ($_GET['column']) {
            case "added": $column = "added"; break;
            case "visits": $column = "visits"; break;
            case "retailer_id": $column = "retailer_id"; break;
            case "end_date": $column = "end_date"; break;
            default: $column = "added"; break;
        }
    } else {
        $column = "added";
    }

    $cat_id = 0;
    if(isset($_REQUEST['params']['cat_id']) && !empty($_REQUEST['params']['cat_id'])){
	    $cat_id = $_REQUEST['params']['cat_id'];
    }

	if ($cat_id > 0) {

		$cat_query = "SELECT * FROM cashbackengine_coupon_categories WHERE category_id='". $cat_id. "' LIMIT 1";
		$cat_result = smart_mysql_query($cat_query);

		if (mysqli_num_rows($cat_result) > 0) {
			$content = mysqli_fetch_array($cat_result);
		} else {

			header ("Location: /kouponia");
			exit();
		}

	} else {

		$content = GetContent('coupons');
	}

    ///////////////  Page config  ///////////////
    $PAGE_TITLE			= $content['name']." ".CBE1_COUPONS_TITLE;
    $PAGE_DESCRIPTION	= $content['meta_description'];
    $PAGE_KEYWORDS		= $content['meta_keywords'];

	$breadcrumbs[] = [
		'name' => 'Home',
		'link' => '/'
	];

	if($cat_id != 0){
		$breadcrumbs[] = [
			'name' => CBE1_STORE_COUPONS,
			'link' => '/kouponia'
		];

		$breadcrumbs[] = [
			'name' => $content['name'],
			'link' => ''
		];

	} else {

		$breadcrumbs[] = [
			'name' => CBE1_STORE_COUPONS,
			'link' => ''
		];
	}


    $data = [
        'head'=>$head,
        'header'=>$header,
        'footer'=>$footer,
	    'router'=>$router,
        'PAGE_TITLE'=>$PAGE_TITLE,
        'PAGE_DESCRIPTION'=>$PAGE_DESCRIPTION,
        'PAGE_KEYWORDS'=>$PAGE_KEYWORDS,
        'content' => $content,
        'countries'=>GetCountries(),
        'languages'=>GetLanguagesArray(),
        'current_lang'=>isset($_COOKIE['site_lang']) ? $_COOKIE['site_lang'] : SITE_LANGUAGE,
        'multilanguage'=>MULTILINGUAL,
        'search_array'=>GetRetailersForSearch(),
        'user_info'=>GetUserInfo(),
	    'breadcrumbs' => $breadcrumbs,

        'recommended_coupons'=>GetLatestCoupons(),
	    'coupons_menu'=>ShowCategoriesVerticaMenu(0, 'coupons'),
	    'double_cash_module'=>GetLatestRetailers(9),
	    'coupons'=>GetLatestCoupons(0, $column, $cat_id),
	    'coupons_per_page'=>COUPONS_PER_PAGE,
	    'current_sort'=>$column,
    ];

    echo $blade->make('coupons', $data);