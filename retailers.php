<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	session_start();
	require_once("inc/config.inc.php");
	require_once("inc/pagination.inc.php");
    require_once("inc/blade_config.inc.php");
    require_once("inc/var_config.inc.php");

    ////////////////// filter  //////////////////////
    if (isset($_GET['column']) && $_GET['column'] != "") {

        switch ($_GET['column']) {
            case "added": $column = "added"; break;
            case "visits": $column = "visits"; break;
            case "cashback": $column = "cashback"; break;
            default: $column = "added"; break;
        }
    } else {
        $column = "added";
    }

    $cat_id = 0;
    if(isset($_REQUEST['params']['cat_id']) && !empty($_REQUEST['params']['cat_id'])){
        $cat_id = (int) $_REQUEST['params']['cat_id'];
    }

	if ($cat_id > 0) {

		$cat_query = "SELECT * FROM cashbackengine_categories WHERE category_id='". $cat_id. "' LIMIT 1";
		$cat_result = smart_mysql_query($cat_query);

		if (mysqli_num_rows($cat_result) > 0) {
			$content = mysqli_fetch_array($cat_result);
		} else {

			header ("Location: /online-katasthmata");
			exit();
		}

	} else {

		$content = GetContent('retailers');
    }

	///////////////  Page config  ///////////////
	$PAGE_TITLE	        = $content['name']." ".CBE1_STORES_STORES;
    $PAGE_DESCRIPTION	= $content['meta_description'];
    $PAGE_KEYWORDS		= $content['meta_keywords'];

	$breadcrumbs[] = [
        'name' => 'Home',
        'link' => '/'
    ];

	if($cat_id != 0){
		$breadcrumbs[] = [
			'name' => CBE1_STORES_STORES,
			'link' => '/online-katasthmata'
		];

		$breadcrumbs[] = [
			'name' => $content['name'],
			'link' => ''
		];

    } else {

		$breadcrumbs[] = [
			'name' => CBE1_STORES_STORES,
			'link' => ''
		];
    }


    $data = [
        'head'=>$head,
        'header'=>$header,
        'footer'=>$footer,
	    'router'=>$router,
        'PAGE_TITLE'=>$PAGE_TITLE,
        'PAGE_DESCRIPTION'=>$PAGE_DESCRIPTION,
        'PAGE_KEYWORDS'=>$PAGE_KEYWORDS,
        'countries'=>GetCountries(),
        'languages'=>GetLanguagesArray(),
        'current_lang'=>isset($_COOKIE['site_lang']) ? $_COOKIE['site_lang'] : SITE_LANGUAGE,
        'multilanguage'=>MULTILINGUAL,
        'search_array'=>GetRetailersForSearch(),
        'content' => $content,
        'user_info'=>GetUserInfo(),
        'breadcrumbs' => $breadcrumbs,

	    'recommended_coupons'=>GetLatestCoupons(6),
	    'coupons_menu'=>ShowCategoriesVerticaMenu(),
	    'double_cash_module'=>GetLatestRetailers(9),
	    'retailers'=>GetLatestRetailers(0, $column, $cat_id),
	    'retailers_per_page'=>RESULTS_PER_PAGE,
	    'current_sort'=>$column,
    ];

	echo $data['coupons_menu']['active'];
	echo "<br>";
	echo $data['coupons_menu']['active_parent'];

//    	print "<pre>";
//    	print_r($data['coupons_menu']);
//    	print "</pre>";

    echo $blade->make('retailers', $data);

