<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	session_start();
	require_once("inc/iflogged.inc.php");
	require_once("inc/config.inc.php");
	require_once("inc/blade_config.inc.php");
	require_once("inc/var_config.inc.php");


	if (isset($_POST['action']) && $_POST['action'] == "login")
	{
		$username	= mysqli_real_escape_string($conn, getPostParameter('username'));
		$pwd		= mysqli_real_escape_string($conn, getPostParameter('password'));
//		$remember	= (int)getPostParameter('rememberme');
		$ip			= mysqli_real_escape_string($conn, getenv("REMOTE_ADDR"));

		if (empty($username)  || empty($pwd)) {

		    echo json_encode(['errs'=>CBE1_LOGIN_ERR]);
		} else {
			$sql = "SELECT * FROM cashbackengine_users WHERE username='$username' AND password='".PasswordEncryption($pwd)."' LIMIT 1";
			$result = smart_mysql_query($sql);

			if (mysqli_num_rows($result) != 0) {
					$row = mysqli_fetch_array($result);

					if ($row['status'] == 'inactive') {
						echo json_encode(['errs'=>'inactive']);
						exit();
					}

					if (LOGIN_ATTEMPTS_LIMIT == 1) {
						unset($_SESSION['attems_'.$username."_".$ip], $_SESSION['attems_left']);
					}

//					if ($remember == 1) {
//						$cookie_hash = md5(sha1($username.$ip));
//						setcookie("usname", $cookie_hash, time()+3600*24*365, '/');
//						$login_sql = "login_session = '$cookie_hash', ";
//					}

					smart_mysql_query("UPDATE cashbackengine_users SET ".$login_sql." last_ip='$ip', login_count=login_count+1, last_login=NOW() WHERE user_id='".(int)$row['user_id']."' LIMIT 1");

					if (!session_id()) session_start();
					$_SESSION['userid']		= $row['user_id'];
					$_SESSION['FirstName']	= !empty($row['fname']) ? $row['fname'] : $row['username'];

					if ($_SESSION['goto']) {
						$redirect_url = $_SESSION['goto'];
						unset($_SESSION['goto'], $_SESSION['goto_created']);
					} else {
						$redirect_url = "my-account";
					}

					echo json_encode(['success'=>$redirect_url]);
			} else {

				if (LOGIN_ATTEMPTS_LIMIT == 1) {
					$check_sql = "SELECT * FROM cashbackengine_users WHERE username='$username' AND status!='inactive' AND block_reason!='login attempts limit' LIMIT 1";
					$check_result = smart_mysql_query($check_sql);

					if (mysqli_num_rows($check_result) != 0) {
						if (!session_id()){
							session_start();
                        }
						$_SESSION['attems_'.$username."_".$ip] += 1;
						$_SESSION['attems_left'] = LOGIN_ATTEMPTS - $_SESSION['attems_'.$username.'_'.$ip];

						if ($_SESSION['attems_left'] == 0) {
							// block user //
							smart_mysql_query("UPDATE cashbackengine_users SET status='inactive', block_reason='login attempts limit' WHERE username='$username' LIMIT 1"); 
							unset($_SESSION['attems_'.$username."_".$ip], $_SESSION['attems_left']);

							echo json_encode(['errs'=>'account is blocked']);
						} else {
							echo json_encode(['errs'=>'Login or password is wrong. '. $_SESSION['attems_left'] .' more attempts left']);
						}
					}
				}
				echo json_encode(['errs'=>'Login or password is wrong']);
			}
		}
	}


	if (isset($_SESSION['goRetailerID']) && $_SESSION['goRetailerID'] != "" && isset($_GET['msg']) && $_GET['msg'] == 4)
	{
		$retailer_id = (int)$_SESSION['goRetailerID'];
		$result = smart_mysql_query("SELECT * FROM cashbackengine_retailers WHERE retailer_id='$retailer_id' LIMIT 1");
		if (mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_array($result);
		}
	}

	//REGISTRATION STARTED

	if (isset($_POST['action']) && $_POST['action'] == "signup")
	{
		unset($errs);
		$errs = array();

		$email		= mysqli_real_escape_string($conn, strtolower(getPostParameter('email')));
		$username   = $email;
		$pwd		= mysqli_real_escape_string($conn, getPostParameter('password'));
		$country	= (int)getPostParameter('country');
//		$captcha	= mysqli_real_escape_string($conn, getPostParameter('captcha'));
		$reg_source	= mysqli_real_escape_string($conn, getPostParameter('reg_source'));
		$sub_news	= (int)getPostParameter('sub_news');
		$tos		= (int)getPostParameter('tos');
		$ref_id		= (int)getPostParameter('referer_id');
		$ip			= mysqli_real_escape_string($conn, getenv("REMOTE_ADDR"));
//		$g_recaptcha_response = getPostParameter('g-recaptcha-response');


//        if(!$g_recaptcha_response){
//          $errs[] = 'Please click on the recaptcha checkbox!';
//        }
//
//        $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LdAjTkUAAAAAPbS2MEvuKfG6IabFNdHMWJ6xxyS&response=".$g_recaptcha_response."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
//        if($response['success'] == false)
//        {
//          $errs[] = 'spam!';
//        }

		//if (!($fname && $lname && $email && $pwd && $pwd2 && $country))


		if (empty($email) || empty($pwd) || empty($country)) {
			$errs[] = CBE1_SIGNUP_ERR;
		}

		if (isset($email) && $email != "" && !preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email))
		{
			$errs[] = CBE1_SIGNUP_ERR4;
		}

		//if (isset($pwd) && $pwd != "" && isset($pwd2) && $pwd2 != "")
		if (isset($pwd) && $pwd != "" )
		{
			//elseif ((strlen($pwd)) < 6 || (strlen($pwd2) < 6) || (strlen($pwd)) > 20 || (strlen($pwd2) > 20))
			if ((strlen($pwd)) < 6 || (strlen($pwd)) > 20) {
				json_encode(['errs'=>CBE1_SIGNUP_ERR7]);
			} elseif (stristr($pwd, ' ')) {
				json_encode(['errs'=>CBE1_SIGNUP_ERR8]);
			}
		}

//		if (SIGNUP_CAPTCHA == 1) {
//			if (!$captcha) {
//				$errs[] = CBE1_SIGNUP_ERR2;
//			} else {
//				if (empty($_SESSION['captcha']) || strcasecmp($_SESSION['captcha'], $captcha) != 0) {
//					$errs[] = CBE1_SIGNUP_ERR3;
//				}
//			}
//		}

		if (!(isset($tos) && $tos == 1))
		{
			json_encode(['errs'=>CBE1_SIGNUP_ERR9]);
		}

		if (count($errs) == 0) {
            $query = "SELECT username FROM cashbackengine_users WHERE username='$email' OR email='$email' LIMIT 1";
            $result = smart_mysql_query($query);

            if (mysqli_num_rows($result) != 0) {
	            echo json_encode(['errs'=>'This email address is already registered in the system.']);
	            exit();
            }

            // check referral
            if ($ref_id > 0) {
                $check_referral_query = "SELECT email FROM cashbackengine_users WHERE user_id='$ref_id' LIMIT 1";
                $check_referral_result = smart_mysql_query($check_referral_query);

                if (mysqli_num_rows($check_referral_result) != 0){
                    $ref_id = $ref_id;
                } else {
                    $ref_id = 0;
                }
            }

            $unsubscribe_key = GenerateKey($username);

			if(!isset($sub_news) || empty($sub_news)){
				$sub_news = 0;
			}

            if (ACCOUNT_ACTIVATION == 1) {
                $activation_key = GenerateKey($username);
                $insert_query = "INSERT INTO cashbackengine_users SET username='$username', password='".PasswordEncryption($pwd)."', email='$email',  country='$country',  reg_source='$reg_source', ref_id='$ref_id', newsletter='$sub_news', ip='$ip', status='inactive', activation_key='$activation_key', unsubscribe_key='$unsubscribe_key', created=NOW()";
            } else {
                $insert_query = "INSERT INTO cashbackengine_users SET username='$username', password='".PasswordEncryption($pwd)."', email='$email',  country='$country', reg_source='$reg_source', ref_id='$ref_id', newsletter='$sub_news', ip='$ip', status='active', activation_key='', unsubscribe_key='$unsubscribe_key', last_login=NOW(), login_count='1', last_ip='$ip', created=NOW()";
            }
            smart_mysql_query($insert_query);
            $new_user_id = mysqli_insert_id($conn);

            // save SIGN UP BONUS transaction //
            if (SIGNUP_BONUS > 0)
            {
                $reference_id = GenerateReferenceID();
                smart_mysql_query("INSERT INTO cashbackengine_transactions SET reference_id='$reference_id', user_id='$new_user_id', payment_type='signup_bonus', amount='".SIGNUP_BONUS."', status='confirmed', created=NOW(), process_date=NOW()");
            }

            // add bonus to referral, save transaction //
            if (REFER_FRIEND_BONUS > 0 && isset($ref_id) && $ref_id > 0)
            {
                $reference_id = GenerateReferenceID();
                $ref_res = smart_mysql_query("INSERT INTO cashbackengine_transactions SET reference_id='$reference_id', user_id='$ref_id', ref_id='$new_user_id', payment_type='friend_bonus', amount='".REFER_FRIEND_BONUS."', status='pending', created=NOW()");
            }

            if (ACCOUNT_ACTIVATION == 1) {
                ////////////////////////////////  Send Message  //////////////////////////////
                $etemplate = GetEmailTemplate('activate');
                $esubject = $etemplate['email_subject'];
                $emessage = $etemplate['email_message'];

                $activate_link = SITE_URL."activate.php?key=".$activation_key;

                //$emessage = str_replace("{first_name}", $fname, $emessage);
                $emessage = str_replace("{first_name}", 'and welcome', $emessage);
                $emessage = str_replace("{username}", $email, $emessage);
                $emessage = str_replace("{password}", $pwd, $emessage);
                $emessage = str_replace("{activate_link}", $activate_link, $emessage);
                $to_email = $fname.' '.$lname.' <'.$email.'>';

                SendEmail($to_email, $esubject, $emessage, $noreply_mail = 1);
                ////////////////////////////////////////////////////////////////////////////////

                // show activation message

	            echo json_encode(['success'=>'activate.php?msg=1']);

            } else {
                ////////////////////////////////  Send welcome message  ////////////////
                $etemplate = GetEmailTemplate('signup');
                $esubject = $etemplate['email_subject'];
                $emessage = $etemplate['email_message'];

                //$emessage = str_replace("{first_name}", $fname, $emessage);
                $emessage = str_replace("{first_name}", $email, $emessage);
                $emessage = str_replace("{username}", $email, $emessage);
                $emessage = str_replace("{password}", $pwd, $emessage);
                $emessage = str_replace("{login_url}", SITE_URL."login.php", $emessage);
                $to_email = $fname.' '.$lname.' <'.$email.'>';

                SendEmail($to_email, $esubject, $emessage, $noreply_mail = 1);
                /////////////////////////////////////////////////////////////////////////

                if (!session_id()) session_start();
                $_SESSION['userid']		= $new_user_id;
            //	$_SESSION['FirstName']	= $fname;
                $_SESSION['FirstName']	= $email;
                if ($_SESSION['goto'])
                {
                    $redirect_url = $_SESSION['goto'];
                    unset($_SESSION['goto'], $_SESSION['goto_created']);
                } else {
                    // forward new user to account dashboard
                    $redirect_url = $router->generate('my-account');
                }

	            echo json_encode(['success'=>$redirect_url]);
                exit();
            }
		} else {
            echo json_encode($errs);
			exit();
		}
	}

	if(!isset($_POST['action'])){
		$content = GetContent('login');

		///////////////  Page config  ///////////////
		$PAGE_TITLE			= !empty($content['title']) ? $content['title'] : '';
		$PAGE_DESCRIPTION	= !empty($content['meta_description']) ? $content['meta_description'] : '';
		$PAGE_KEYWORDS		= !empty($content['meta_keywords']) ? $content['meta_description'] : '';

		$data = [
			'head'=>$head,
			'header'=>$header,
			'footer'=>$footer,
			'router'=>$router,
			'PAGE_TITLE'=>$PAGE_TITLE,
			'PAGE_DESCRIPTION'=>$PAGE_DESCRIPTION,
			'PAGE_KEYWORDS'=>$PAGE_KEYWORDS,
			'content' => $content,
			//ADDED by Denis
			'countries'=>GetCountries(),
			'languages'=>GetLanguagesArray(),
			'current_lang'=>$_COOKIE['site_lang'],
			'multilanguage'=>MULTILINGUAL,
			'search_array'=>GetRetailersForSearch(),
			'user_info'=>GetUserInfo(),
		];

		echo $blade->make('login', $data);
	}

?>

