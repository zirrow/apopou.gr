<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	session_start();
	require_once("inc/config.inc.php");
    require_once("inc/blade_config.inc.php");
    require_once("inc/var_config.inc.php");


	if (isset($_POST['action']) && $_POST['action'] == "forgot") {

		$email		= strtolower(mysqli_real_escape_string($conn, getPostParameter('email')));
		$captcha	= mysqli_real_escape_string($conn, getPostParameter('captcha'));

		if (!($email) || $email == "") {

			echo json_encode(['error' => CBE1_FORGOT_MSG1, 'step' => 1]);
			$errs[] = CBE1_FORGOT_MSG1;
		} else {
			if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email)) {

				echo json_encode(['error' => CBE1_FORGOT_MSG2, 'step' => 2]);
				$errs[] = CBE1_FORGOT_MSG2;
			}

			if (!$captcha) {
				echo json_encode(['error' => CBE1_SIGNUP_ERR2]);
				$errs[] = CBE1_SIGNUP_ERR2;
			} else {
				if (empty($_SESSION['captcha']) || strcasecmp($_SESSION['captcha'], $captcha) != 0) {

					echo json_encode(['error' => CBE1_SIGNUP_ERR3, 'step' => 3]);
					$errs[] = CBE1_SIGNUP_ERR3;
				}
			}
		}

		if (!isset($errs)) {
			$query = "SELECT * FROM cashbackengine_users WHERE email='". $email ."' AND status='active' LIMIT 1";
			$result = smart_mysql_query($query);

			if (mysqli_num_rows($result) > 0) {
				$row = mysqli_fetch_array($result);
				
				$newPassword = generatePassword(11);
				$update_query = "UPDATE cashbackengine_users SET password='".PasswordEncryption($newPassword)."' WHERE user_id='".(int)$row['user_id']."' LIMIT 1";
				
				if (smart_mysql_query($update_query)) {
					////////////////////////////////  Send Message  //////////////////////////////
					$etemplate = GetEmailTemplate('forgot_password');
					$esubject = $etemplate['email_subject'];
					$emessage = $etemplate['email_message'];

					$emessage = str_replace("{first_name}", $row['fname'], $emessage);
					$emessage = str_replace("{username}", $row['username'], $emessage);
					$emessage = str_replace("{password}", $newPassword, $emessage);
					$emessage = str_replace("{login_url}", SITE_URL."login.php", $emessage);

					$headers= "MIME-Version: 1.0\r\n";
					$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

					mail($email, $esubject, $emessage, $headers);

//					SendEmail($to_email, $esubject, $emessage, $noreply_mail = 1);

					echo json_encode(['success' => 'message sent']);
					exit();
					///////////////////////////////////////////////////////////////////////////////
				}
			} else {
				echo json_encode(['error' => 'User does not exist']);
				exit();
			}
		}
	}


    if(!isset($_POST['action'])) {
        ///////////////  Page config  ///////////////

        $content = GetContent('forgot');

        ///////////////  Page config  ///////////////
        $PAGE_TITLE = CBE1_FORGOT_TITLE;
        $PAGE_DESCRIPTION = !empty($content['meta_description']) ? $content['meta_description'] : '';
        $PAGE_KEYWORDS = !empty($content['meta_keywords']) ? $content['meta_description'] : '';

        $data = [
            'head' => $head,
            'header' => $header,
            'footer' => $footer,
	        'router'=>$router,
            'PAGE_TITLE' => $PAGE_TITLE,
            'PAGE_DESCRIPTION' => $PAGE_DESCRIPTION,
            'PAGE_KEYWORDS' => $PAGE_KEYWORDS,
            'content' => $content,
            //ADDED by Denis
        ];


        echo $blade->make('forgot', $data);
    }