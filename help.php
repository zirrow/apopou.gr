<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	session_start();
    require_once("inc/config.inc.php");
    require_once("inc/blade_config.inc.php");
    require_once("inc/var_config.inc.php");

    if(isset($_POST['action']) && $_POST['action'] == 'send_request'){

	    $recipient = NOREPLY_MAIL;
	    $subject = 'Question from the feedback form';
	    $message = 'User '.htmlspecialchars($_POST['name']).' asks - '.htmlspecialchars($_POST['question']);
	    $from = $_POST['email'];

	    SendEmail($recipient, $subject, $message, $noreply_mail = 0, $from);

	    echo json_encode(['success' => 'success']);
	    exit();
    }

	$content = GetContent('help');

	///////////////  Page config  ///////////////
	$PAGE_TITLE			= $content['title'];
	$PAGE_DESCRIPTION	= $content['meta_description'];
	$PAGE_KEYWORDS		= $content['meta_keywords'];

    $data = [
        'head'=>$head,
        'header'=>$header,
        'footer'=>$footer,
	    'router'=>$router,
        'PAGE_TITLE'=>$PAGE_TITLE,
        'PAGE_DESCRIPTION'=>$PAGE_DESCRIPTION,
        'PAGE_KEYWORDS'=>$PAGE_KEYWORDS,
        'countries'=>GetCountries(),
        'languages'=>GetLanguagesArray(),
        'current_lang'=>isset($_COOKIE['site_lang']) ? $_COOKIE['site_lang'] : SITE_LANGUAGE,
        'multilanguage'=>MULTILINGUAL,
        'search_array'=>GetRetailersForSearch(),
        'content' => $content,
        'user_info'=>GetUserInfo(),

    ];

    echo $blade->make('help', $data);



