<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	session_start();
	require_once("inc/auth.inc.php");
	require_once("inc/config.inc.php");
    require_once("inc/blade_config.inc.php");
    require_once("inc/var_config.inc.php");

    $content = GetContent('myaccount');

    ///////////////  Page config  ///////////////
    $PAGE_TITLE			= !empty($content['title']) ? $content['title'] : '';
    $PAGE_DESCRIPTION	= !empty($content['meta_description']) ? $content['meta_description'] : '';
    $PAGE_KEYWORDS		= !empty($content['meta_keywords']) ? $content['meta_description'] : '';

    $data = [
        'head'=>$head,
        'header'=>$header,
        'footer'=>$footer,
	    'router'=>$router,
        'PAGE_TITLE'=>CBE1_ACCOUNT_TITLE,
        'PAGE_DESCRIPTION'=>$PAGE_DESCRIPTION,
        'PAGE_KEYWORDS'=>$PAGE_KEYWORDS,
        'content' => $content,
        //ADDED by Denis
        'countries'=>GetCountries(),
        'languages'=>GetLanguagesArray(),
        'current_lang'=>$_COOKIE['site_lang'],
        'multilanguage'=>MULTILINGUAL,
	    'search_array'=>GetRetailersForSearch(),
        'user_info'=>GetUserInfo(),
	    'account_menu' => GetAccountMenu(),
	    'lifetime_cash_back'=>GetLifetimeCashback(),
	    'favorite_stores_double'=>GetUserFavoritesByUserId(2),
	    'favorite_stores'=>GetUserFavoritesByUserId(4),
	    'recommended'=>GetLatestRetailers(6),
	    'recommended_deals'=>GetLatestCoupons(2),
	    'transactions'=>GetUserTransactions(5),
    ];
//
//    print "<pre>";
//    print_r($data);
//    print "</pre>";

    echo $blade->make('myaccount', $data);