<?php
/*******************************************************************\
 * CashbackEngine v3.0
 * http://www.CashbackEngine.net
 *
  * Copyright (c) 2010-2017 CashbackEngine Software. All rights reserved.
 * ------------ CashbackEngine IS NOT FREE SOFTWARE --------------
\*******************************************************************/

	session_start();
	require_once("inc/auth.inc.php");
	require_once("inc/config.inc.php");
	require_once("inc/pagination.inc.php");
    require_once("inc/blade_config.inc.php");
    require_once("inc/var_config.inc.php");

	// cancel pending withdrawal request
	if (isset($_GET['act']) && $_GET['act'] == "cancel" && CANCEL_WITHDRAWAL == 1) {
		$transaction_id = (int)$_GET['id'];
		smart_mysql_query("DELETE FROM cashbackengine_transactions WHERE user_id='". $userid ."' AND transaction_id='" .$transaction_id ."' AND payment_type='Withdrawal' AND status='request'");

		echo json_encode(['success' => 'Transaction successfully deleted']);
		exit();
	}

    $content = GetContent('mybalance');

    ///////////////  Page config  ///////////////
    $PAGE_TITLE			= !empty($content['title']) ? $content['title'] : '';
    $PAGE_DESCRIPTION	= !empty($content['meta_description']) ? $content['meta_description'] : '';
    $PAGE_KEYWORDS		= !empty($content['meta_keywords']) ? $content['meta_description'] : '';

    $data = [
        'head'=>$head,
        'header'=>$header,
        'footer'=>$footer,
	    'router'=>$router,
        'PAGE_TITLE'=>CBE1_BALANCE_TITLE,
        'PAGE_DESCRIPTION'=>$PAGE_DESCRIPTION,
        'PAGE_KEYWORDS'=>$PAGE_KEYWORDS,
        'content' => $content,
        //ADDED by Denis
        'countries'=>GetCountries(),
        'languages'=>GetLanguagesArray(),
        'current_lang'=>$_COOKIE['site_lang'],
        'multilanguage'=>MULTILINGUAL,
        'search_array'=>GetRetailersForSearch(),
        'user_info'=>GetUserInfo(),
        'account_menu' => GetAccountMenu(),

        'user_balance' => GetUserBalance($userid),
        'pending_balance' => GetPendingBalance(),
        'declined_balance' => GetDeclinedBalance(),
        'cash_out_reqest' => GetCashOutRequested(),
        'cash_out_processed' => GetCashOutProcessed(),
        'lifetime_cashback' => GetLifetimeCashback(),
        'user_transactions' => GetUserTransactions(),

    ];

    echo $blade->make('mybalance', $data);

